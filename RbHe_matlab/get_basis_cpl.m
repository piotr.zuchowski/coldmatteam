function [primdress_cpl]=get_basis_cpl(sa,ia,sb,mtot,Lmax)


Nprim=0;
Smin=abs(sa-sb);
Smax=sa+sb;

for sab=Smin:Smax
    for i0=-sab:sab
        for j0=-ia:ia
            Nprim=Nprim+1;
        end
    end
end

basprim_cpl=repmat(struct('SAB',-100,'MSAB',-100,'IA',ia,'MIA',-100,...
                    'MT',-100,'num',-100),Nprim,1);
j=0;
for sab=Smin:Smax
    for i0=-sab:sab
        for j0=-ia:ia
            j=j+1;
            MSAB=i0;  MIA=j0;  MT=MIA+MSAB;
            basprim_cpl(j)=struct('SAB',sab,'MSAB',MSAB,'IA',ia,...
            'MIA',MIA,'MT',MT,'num',j);
        end
    end
end
 



Nbas=0;
for L=0:2:Lmax
    for ML=-L:L    
        for ibas=1:Nprim
            MT=basprim_cpl(ibas).MT;
            MTOT0=MT+ML;
            if (MTOT0==mtot)
                Nbas=Nbas+1; 
            end
        end
    end
end

numprim=0;
primdress_cpl=repmat(struct('L',-100,'ML',-100,...
    'SAB',-100,'MSAB',-100,'IA',ia,'MIA',-100,'MTOT',-100,'numprim',numprim,'num',1),Nbas,1);   



j=0;
for L=0:2:Lmax
    for ML=-L:L    
        for ibas=1:Nprim
            SAB=basprim_cpl(ibas).SAB;
            MSAB=basprim_cpl(ibas).MSAB;
            MIA=basprim_cpl(ibas).MIA;
            numprim=basprim_cpl(ibas).num;
            MT=basprim_cpl(ibas).MT;
            MTOT0=MT+ML;
            if (MTOT0==mtot)
                j=j+1;
    primdress_cpl(j)=struct('L',L,'ML',ML,'SAB',SAB,'MSAB',MSAB,'IA',ia,'MIA',MIA,...
    'MTOT',MTOT0,'numprim',numprim,'num',j);


            end
        end
    end
end


return








