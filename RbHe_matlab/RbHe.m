    clear all
%data_recommended=load('allel.recommended.txt');
%data_ecp=load('ecp.recommended.txt');
 
 const
 atomic_mass
 mu1=1/(1/rb87+1/he4);
 mu2=1/(1/rb87+1/he3);
 mu3=1/(1/rb85+1/he4);
 mu4=1/(1/rb85+1/he3);
 
 
 nstates=2;
 
 [e2,p2,R2]=get_bnd_states(grid,mu2,0,@(x)(RbHe_pot_quart_new(x)),nstates);
 
 %
 
 return
 
%[a1,b1,c1]= get_re_de_points(data_recommended(:,1),data_recommended(:,2),0.001,mu);
%[a2,b2,c2]= get_re_de_points(data_ecp(:,1),data_ecp(:,3),0.001,mu);
 

beta=0.386557626132798;

%r=rang.*aubohr;
De=451.6565;
re=9.411;xe=re;

%morse=De.*(1-exp())-De;

nstates=2

grid.rmin=6.5/aubohr;
grid.dr=0.01;
grid.rmax=500;
lambdas=1.015;
nl=length(lambdas);
EE1=zeros(nl,nstates);
EE2=zeros(nl,nstates);
EE3=zeros(nl,nstates);
EE4=zeros(nl,nstates);

EE1L2=zeros(nl,nstates);
EE2L2=zeros(nl,nstates);

EE3L2=zeros(nl,nstates);
EE4L2=zeros(nl,nstates);

%if(1==1)
%for il=1:nl
%    lam=lambdas(il);% 1.015;
lam=1;
[e1,p1,R1]=get_bnd_states_e_psi(grid,mu1,0,@(x)(RbHe_pot_quart(x,lam)),nstates);e1=e1./cmmhz;

%    [e2,p2,R2]=get_bnd_states_e_psi(grid,mu2,0,@(x)(RbHe_pot_quart(x,lam)),nstates);e2=e2./cmmhz;
%    [e3,p1,R1]=get_bnd_states_e_psi(grid,mu3,0,@(x)(RbHe_pot_quart(x,lam)),nstates);e3=e3./cmmhz;
%    [e4,p2,R2]=get_bnd_states_e_psi(grid,mu4,0,@(x)(RbHe_pot_quart(x,lam)),nstates);e4=e4/cmmhz;
% 
%     
%    EE1(il,:)=e1;
%    EE2(il,:)=e2;
%    EE3(il,:)=e3;
%    EE4(il,:)=e4;
    
 %   [e1l2,p1l2,R1]=get_bnd_states_e_psi(grid,mu1,2,@(x)(RbHe_pot(x,lam)),nstates);e1l2=e1l2./cmmhz;
 %   [e2l2,p2l2,R2]=get_bnd_states_e_psi(grid,mu2,2,@(x)(RbHe_pot_quart(x,lam)),nstates);e2l2=e2l2/cmmhz;
 %   [e3l2,p3l2,R1]=get_bnd_states_e_psi(grid,mu3,2,@(x)(RbHe_pot_quart(x,lam)),nstates);e3l2=e3l2./cmmhz;
 %   [e4l2,p4l2,R2]=get_bnd_states_e_psi(grid,mu4,2,@(x)(RbHe_pot_quart(x,lam)),nstates);e4l2=e4l2/cmmhz;

    
    
 %  EE1L2(il,:)=e1l2;
 %  EE2L2(il,:)=e2l2;
 %  EE3L2(il,:)=e3l2;
 %  EE4L2(il,:)=e4l2;
    
%     
%    a0_1(il)=numerov_1channel(1e-8,@(x)(RbHe_pot_quart(x,lam)),0,mu1,grid.rmin,grid.rmax,grid.dr);
%    a0_2(il)=numerov_1channel(1e-8,@(x)(RbHe_pot_quart(x,lam)),0,mu2,grid.rmin,grid.rmax,grid.dr);
%    a0_3(il)=numerov_1channel(1e-8,@(x)(RbHe_pot_quart(x,lam)),0,mu3,grid.rmin,grid.rmax,grid.dr);
%    a0_4(il)=numerov_1channel(1e-8,@(x)(RbHe_pot_quart(x,lam)),0,mu4,grid.rmin,grid.rmax,grid.dr);
% 

%end
 
%end

% fprintf('%10.3f\n',e1l2);
% fprintf('%10.3f\n',e2l2)
% fprintf('%10.3f\n',e3l2);
% fprintf('%10.3f\n',e4l2);


% return
sa=1/2;
sb=1;
ia=3/2;
ib=0; 



%rb 85
ARb=1011.911;
ga=1.6013071; 
gb=0; 

%Rb87
ARb=3417.0;
gb=0;
ga=3.552582;
MT1=-1
MT2=-1



fzsa=ge*bohrmag;
fzia=-nucmag*ga;
 
% 
% first mechanism
%
 inb1=[];
ibas1.num=0;
for i0=-sa:1:sa
    for k0=-ia:1:ia
                     
            ibas1.msa=i0;
            ibas1.mia=k0;
            
            mt=i0+k0;
         %   if mt==MT1
                ibas1.mtot=mt;
                ibas1.num=ibas1.num+1;
                inb1=[ibas1;inb1];
         %   end
        
    end
end
disp(' basis set size ')
bas_size1=length(inb1)

Hz=zeros(bas_size1,bas_size1);
His=zeros(bas_size1,bas_size1);
for i=1:bas_size1
    for j=1:bas_size1
        a=0;
        mia1=inb1(i).mia;
        mia2=inb1(j).mia;
        

        msa1=inb1(i).msa;
        msa2=inb1(j).msa;
        
 
        num1=inb1(i).num;
        num2=inb1(j).num;
        if((msa1==msa2) && (mia1==mia2) )
            a=fzsa*msa1+fzia*mia1;
        end
        
        Hz(num1,num2)=a*cmmhz;  %in MHz
        His(num1,num2)=sdoti(ARb,sa,msa1,msa2,ia,mia1,mia2);


    end
end 



% =====================================================================
 inb=[];
 ibas.num=0;
 for j0=-sb:1:sb
             ibas.msb=j0;
             mt=j0;
             ibas.mtot=mt;
             
         %    if (mt==MT2)
             ibas.num=ibas.num+1;
             inb=[ibas;inb];
        %     end
end
% =====================================================================


bas_size=length(inb);

Hz2=zeros(bas_size,bas_size);

for i=1:bas_size
    for j=1:bas_size
        msb1=inb(i).msb;
        msb2=inb(j).msb;

        num1=inb(i).num;
        num2=inb(j).num;
        if(msb1==msb2)
            a=fzsa*msb1;
              Hz2(num1,num2)=a*cmmhz; 
        end
        

    end
end 




% fid=fopen('danetestowe_spex.txt','w');
% fid1=fopen('danetestowe1_spex.txt','w');
%-----------------------------


B=[0:10:2500];
vb1=zeros(length(B),bas_size1,bas_size1);
evb1=zeros(length(B),bas_size1);
vb2=zeros(length(B),bas_size,bas_size);
evb2=zeros(length(B),bas_size);


for i=1:length(B)
    Htot=Hz.*B(i)+His;
    Htot2=Hz2.*B(i);
    [v1,dev1]=eig(Htot);
    [v2,dev2]=eig(Htot2);

    vb1(i,:,:)=v1;
    evb1(i,:)=diag(dev1);

    vb2(i,:,:)=v2;
    evb2(i,:)=diag(dev2);
end


ethr=zeros(length(B),bas_size*bas_size1);
load('thresh.mat');
ethr0=ethr;
clear ethr;

for i=1:length(B)
 iijj=1;
    for ii=1:bas_size1
        for jj=1:bas_size
            ethr(i,iijj)=(evb2(i,jj)+evb1(i,ii))/1000;
            iijj=iijj+1;
        end
    end
    
end
hold on
  plot(B,ethr ,'k:','LineWidth',1);
% 
% [X0,Y0]=intersections(B,ethr0,B,ethr(:,1)+e1l2*cmmhz/1000,1)

return
for i=1:bas_size
    for j=1:bas_size
        a=0;
        mia1=inb(i).mia;
        mia2=inb(j).mia;
        

        msa1=inb(i).msa;
        msa2=inb(j).msa;
        
        msb1=inb(i).msb;
        msb2=inb(j).msb;

        num1=inb(i).num;
        num2=inb(j).num;
        if((msa1==msa2) && (mia1==mia2)&& (msb1==msb2))
            a=fzsa*msa1+fzia*mia1+ fzsa*msb1;
        end
        
        Hz(num1,num2)=a*cmmhz;  %in MHz
        if(msb1==msb2)
            His(num1,num2)=sdoti(ARb,sa,msa1,msa2,ia,mia1,mia2);
        end
        testQ(num1,num2)=Wigner3j(sa,sb,1.5,msa1,msb1,-msa1-msb1)*...
    Wigner3j(sa,sb,1.5,msa2,msb2,-msa2-msb2);            

        testD(num1,num2)=Wigner3j(sa,sb,0.5,msa1,msb1,-msa1-msb1)*...
    Wigner3j(sa,sb,0.5,msa2,msb2,-msa2-msb2);            
        if(testQ(num1,num2)~=0 && testD(num1,num2)~=0)
           fprintf(fid,' there is spn exchange between %f %f and %f %f \n',...
               msa1,msa2,msa2,msb2);
        end
        if(testQ(num1,num2)~=0 && testD(num1,num2)==0)
           fprintf(fid,'  there is NO spn exchange between %f %f  and %f %f \n',...
               msa1,msb1,msa2,msb2);
        end
           fprintf(fid1,' < %f %f | VD| %f %f > =  %f     ------    < %f %f | VQ| %f %f > =  %f \n',...
               msa1,msb1,msa2,msb2, testD(num1,num2), msa1,msb1,msa2,msb2, testQ(num1,num2));
        
        

    end
end 
fclose(fid);

B=[0:10:2000];

evb=[];
for i=1:length(B)
    Htot=Hz.*B(i)+His;
    ev=eig(Htot);
    evb=[evb,ev];
end




% 
% for j=1:nstates
%     evbj{j}=evb+ee12(nstates+1-j)   
% end
% 
% plot(B,evb ,'k','LineWidth',1.5);
% plot(B,evbj{1} ,'r',B,evbj{2} ,'g',B,evbj{3} ,'b',B,evbj{4} ,'m');
% 
% %ylim([-7000 -2000]) ;
% grid('minor');
% 
% 
%     
