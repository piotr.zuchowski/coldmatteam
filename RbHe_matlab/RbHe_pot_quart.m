function [ output ] = RbHe_pot_quart(r,lscal)
% Fitted potential for quartet state of RbHe system
% based on ab-initio all-electron DK CCSD(T) calculations
%  MLJ potential
if(nargin<2)
    lscal=1;
end
    

p=3;
const;

De=451.6565;
re=9.411;xe=re;
C6=841026782.16;
C8=79757080542.0;
C10=9424240612200.0;

psi0=-2.97745;
psi1=0.39203;
psi2=-1.88629;
psi4= 1.37339;

y=@(x)((x.^p-re.^p)./(x.^p+re.^p));
ulr=@(x)(C6.*x.^(-6)+C8.*x.^(-8)+C10.*x.^(-10));
pinf=log(2*De/ulr(xe));

%phf=[   -7.1588   21.4647 ];
%pcc=[   -2.2108   11.4149 ];


psi=@(x)((1-y(x)).*(psi0+psi1.*y(x)+psi2.*y(x).^2+psi4.*y(x).^4)+y(x).*pinf);
glr=@(x)(De.*(1-(ulr(x)./ulr(xe)).*exp(-psi(x).*y(x))).^2-De);

oo=glr(r).*lscal;

%ii=exp(polyval(phf,log(r)))-exp(polyval(pcc,log(r)));
%ii=ii.*lscal;

%fsw=zeros(1,length(r));
%for j=1:length(r)
%fsw(j)=swfn(r(j),7.6,8.3);
%end

output=oo;% ii.*(1-fsw)+oo.*fsw;
output=output/219474.63;
return    
    
    

