
% that was a file with alkali atom polarizabilities
% 5th colum in the file was occupied by Rb atom, alpha(0) was not given, 
% so I added it...

alk=load('alkali');
ARb=[ 318.60 ; alk(:,5)];

% there is a separate file with quadratures from Porsev & Babb et all paper
quad=load('quadratures'); 
quad2=[ 0 ; quad(:,2)];

% a1 file contains: quadratures, weigths values in column 2 3 4 
a1=load('a1.dat');   q2=a1(:,2);  w2=a1(:,3);  
format longg

% here I calculate polarizabilities of Rb in quadratures given in a1.dat
% file, unfortunately they do not agree, but splined values of Rb dyn.pols
% should be fine from spline

ARb_40=spline(quad2,ARb,q2)

% test-calc C6 for Rb
c6rb2=integ_c6(w2,ARb_40,ARb_40)

% same for He*2
c6he2=integ_c6(w2,a1(:,4),a1(:,4))

%calc for mixture
c6herb=integ_c6(w2,a1(:,4),ARb_40)

exp1=(3276.67-c6he2)/c6he2;

exp2=23/c6rb2;

del=0.5*(exp1^2+exp2^2)^0.5


del*c6herb

%
% you might skip other part of file,, this is for C8 for which we did not
% have dyn. pols. of Rb 
%

% C8 calculation

a1Rb=318.6;
da1Rb=0.6;
a2Rb=6525;
da2Rb=37;

C6RbRb=4690;
dC6RbRb=23;


C8RbRb=5.77e5;
dC8RbRb=0.08e5;


phi2Rb=(15/2)*a1Rb*a2Rb/C8RbRb -(3/4)*a1Rb.^2/C6RbRb;
phi1Rb=(3/4)*a1Rb.^2/C6RbRb;

a2Rbw=@(w)(a2Rb./(1+(w.*phi2Rb).^2));
a1Rbw=@(w)(a1Rb./(1+(w.*phi1Rb).^2));




%
%
%

a2=load('a2.dat');   t2=a2(:,2);  v2=a2(:,3); a2He=a2(:,4)  ; a1He=a1(:,4);

%test C6 Rb2 

C6test=integ_c6(v2,a1Rbw(t2),a1Rbw(t2))
% C8test=(15/(2*3))*(integ_c6(v2,a2He(t2),a1He(t2))*2)

%a2He=a2Rbw(t2);
%a1He=a1Rbw(t2);


a1A=a1He;
a1B=a1Rbw(t2);
a2A=a2He;
a2B=a2Rbw(t2);
%C8mixed=(15/(2*3))*(integ_c6(v2,a2Rbw(t2),a1He) + integ_c6(v2,a1Rbw(t2),a2He) )
test=0;
for i=1:length(v2)
    test=test+(3/pi)*(a1A(i)^2)*v2(i);
end
test

% below is the final formula for C8 
C8mixed=(15/(2*3))*(integ_c6(v2,a2A,a1B) + integ_c6(v2,a1A,a2B) )


