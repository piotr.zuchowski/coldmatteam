clear all




rmax=300
rmin=3
dr=0.005
mu=5


lamt=[0.1:0.005:2]
vpot=@(r)(RbHe_pot_quart_new(r,lam))
%al=numerov_1channel(1e-12,vpot,0,mu,rmin,rmax,dr)

i=0;
lambt=0.01:0.005:100;

lambt=0.85:0.0001:1.2;
N=10
for lam=lambt
i=i+1;
Phi=sqrt(lam)*pi*N;
abar=1;
aa(i)=(1-tan(pi/4)*tan(Phi-pi/8));
end

plot(lambt,aa,'k.')
ylim([-2 4])
xlim([0.9 1.1])