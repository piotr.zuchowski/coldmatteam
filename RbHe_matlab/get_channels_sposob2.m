function channels=get_channels(basprim,primdress,Bfield)

cmmhz=29979.2458; 
ge=2.0023193043622;
bohrmag=4.66864515e-05;
nucmag=2.542623616e-08;

ARb=3417.0/cmmhz; 
ga=3.552582;

fzsa=ge*bohrmag;
fzia=-nucmag*ga;

Nprim=length(basprim);
Nbas=length(primdress);


MTs=unique([primdress.MT])

MFAs=unique([primdress.MFA]);
MSBs=unique([primdress.MSB]);

%[primdress.L; primdress.ML; primdress.MFA; primdress.MSB; primdress.MTOT]
[basprim.MT;basprim.MFA;basprim.MSB]
for imts=1:length(MTs)
        MG=MTs(imts);
        i0=0;j0=0;
        
        mtsize=length(find([basprim.MT]==MG))
        Ham=zeros(mtsize,mtsize);
        fprintf('Mtot block size:  %d for  MT = %d\n',mtsize,MG);
        for ib1=1:Nprim
            SA=basprim(ib1).SA;
            IA=basprim(ib1).IA;
            SB=basprim(ib1).SB;
            MT1=basprim(ib1).MT;
            MIA1=basprim(ib1).MIA;
            MSA1=basprim(ib1).MSA;
            MSB1=basprim(ib1).MSB;
            n1=basprim(ib1).num;
            if(MT1==MG)
                i0=i0+1;
                for ib2=1:Nprim
                    
                    MT2=basprim(ib2).MT;
                    MIA2=basprim(ib2).MIA;
                    MSA2=basprim(ib2).MSA;
                    MSB2=basprim(ib2).MSB;
                    n2=basprim(ib2).num;
                    if(MT2==MG)
                                            [MT1 MT2 MG]

                        j0=j0+1; 
%                        [i0 j0 ]
%                        [MG MIA2 MSA2 MSB2 ]
                        
                        % hyperfine term of Rb
                        if(MSB1==MSB2) 
                            Ham(i0,j0)=Ham(i0,j0)+sdoti(ARb,SA,MSA1,MSA2,IA,MIA1,MIA2);
                        end
                        % Zeeman terms
                        if(MIA1==MIA2 && MSA1==MSA2 && MSB1==MSB2)
                           Ham(i0,j0)=Ham(i0,j0)+...
                               Bfield*(MIA1*fzia+MSA1*fzsa+MSB1*fzsa); 
                        end
                        
                    end                    
                end
            end
        end
        Ham
        clear Ham;
    end


return
%     
% channels=repmat(struct('vec',zeros(Nprim,1),'energy',-1000,'L',-100,'ML',-100,...
%     'MFA',-100,'MSB',-100,'num',1),Nbas,1);   
% 
% j=0;
% for L=0:2:Lmax
%     for ML=-L:1:L
%         
%         for i=1:Nprim
% 
%                MS=basprim(i).MS;
% %%%            num=basprim(i).num;
%             if(MS+ML==mtot)
%                 j=j+1;
%                 
%            %    [ ML    ML+MS    mtot]
%                 
%                 primdress(j).L=L;
%                 primdress(j).ML=ML;
%                 primdress(j).MS=MS;
%                 primdress(j).num=j;
%                 
%                 channels(j).vec=ev(:,i);
%                 channels(j).energy=ee(i);
%                 channels(j).num=j;
%                 channels(j).L=L;
%                 channels(j).ML=ML;       
%                 channels(j).MS=MS;  
%             end
%         end
%     end
% end
