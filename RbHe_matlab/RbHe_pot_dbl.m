function output  = RbHe_pot_dbl(r)
% Fitted potential for quartet state of RbHe system
% based on ab-initio all-electron DK CCSD(T) calculations
%  MLJ potential
% global Gamma
% if(nargin==1)
%     lscal=1; 
% end
global lscald lscal  Gamma
p=3;
const;

De=451.6565;
re=9.411;xe=re;
C6=841026782.16;
C8=79757080542.0;
C10=9424240612200.0;

psi0=-2.97745;
psi1=0.39203;
psi2=-1.88629;
psi4= 1.37339;

y=@(x)((x.^p-re.^p)./(x.^p+re.^p));
ulr=@(x)(C6.*x.^(-6)+C8.*x.^(-8)+C10.*x.^(-10));
pinf=log(2*De/ulr(xe));

phf=[   -7.1588   21.4647 ];
pcc=[   -2.2108   11.4149 ];


psi=@(x)((1-y(x)).*(psi0+psi1.*y(x)+psi2.*y(x).^2+psi4.*y(x).^4)+y(x).*pinf);
glr=@(x)(De.*(1-(ulr(x)./ulr(xe)).*exp(-psi(x).*y(x))).^2-De);

oo=glr(r).*lscald;

ii=exp(polyval(phf,log(r)))-exp(polyval(pcc,log(r)));
ii=ii.*lscal;


   
fsw=zeros(1,length(r));
for j=1:length(r)
fsw(j)=swfn(r(j),7.9,8.2);
end
    
Aex=0.00420636*219474.63*0.86;
ipex=1.1836;
gammaex=4.94173;

p=3/2;
Eexch=@(x)(p.*Aex.*x.^gammaex.*exp(-ipex.*x));
ee=Eexch(r);
Gamma0=Gamma*ee./(p*Aex);
output=(ii.*(1-fsw)+oo.*fsw)-ee + 1i*Gamma0;
output=output/219474.63;

end

