function [basprim,primdress]=get_basis(sa,ia,sb,mtot,Lmax)


Nprim=0;
for i0=-sa:sa
    for j0=-ia:ia
        for k0=-sb:sb
              Nprim=Nprim+1;
        end
    end
end
 
basprim=repmat(struct('SA',sa,'IA',ia,'SB',sb,...
    'MSA',-100,'MIA',-100,'MSB',-100,...
    'MFA',-100,'MT',-100,'MSTOT',-100,'num',-100),Nprim,1);
j=0;
for i0=-sa:sa
    for j0=-ia:ia
        for k0=-sb:sb
            j=j+1;
            MSA=i0;  MSB=k0;  MIA=j0;  MFA=MSA+MIA; MT=MFA+MSB;
        basprim(j)=struct('SA',sa,'IA',ia,'SB',sb,'MSA',MSA,...
            'MIA',MIA,'MSB',MSB,'MFA',MFA,'MT',MT,'MSTOT',MSA+MSB,'num',j);
        end
    end
end
 



Nbas=0;
for L=0:2:Lmax
    for ML=-L:L    
        for ibas=1:Nprim
            MSA=basprim(ibas).MSA;
            MSB=basprim(ibas).MSB;
            MIA=basprim(ibas).MIA;
            MFA=basprim(ibas).MFA;
            MTOT=MFA+MSB+ML;
            if (MT==mtot)
                Nbas=Nbas+1; 
            end
        end
    end
end
numprim=0;
primdress=repmat(struct('L',-100,'ML',-100,'SA',sa,'IA',ia,'SB',sb,'MFA',-100,'MSB',-100,...
    'MSA',-100,'MIA',-100,'MTOT',-100,'MT',-100,'numprim',numprim,'num',1),Nbas,1);   



j=0;
for L=0:2:Lmax
    for ML=-L:L    
        for ibas=1:Nprim
            MSA=basprim(ibas).MSA;
            MSB=basprim(ibas).MSB;
            MIA=basprim(ibas).MIA;
            MFA=basprim(ibas).MFA;
            numprim=basprim(ibas).num;
            MT=MFA+MSB;
            MTOT=MFA+MSB+ML;
            if (MTOT==mtot)
                j=j+1;
    primdress(j)=struct('L',L,'ML',ML,'SA',sa,'IA',ia,'SB',sb,'MFA',MFA,'MSB',MSB,...
    'MSA',MSA,'MIA',MIA,'MTOT',MTOT,'MT',MT,'numprim',numprim,'num',j);


            end
        end
    end
end


return








