function [vs,de,channels]=get_channels(primdress,Bfld)

cmmhz=29979.2458;
ge=2.0023193043622;
bohrmag=4.66864515e-05;
nucmag=2.542623616e-08;
convert=219474.63;
ARb=3417.0/cmmhz;
ga=2.75124;
gb=-1.09283;

fzsa=ge*bohrmag;
fzla=bohrmag;
fzia=-nucmag*ga;
fzib=-nucmag*gb;

Nbas=length(primdress);

Hz=zeros(Nbas,Nbas);
Hhf=zeros(Nbas,Nbas);
Hso=zeros(Nbas,Nbas);

for i0=1:Nbas
    SA1=primdress(i0).SA;
    IA1=primdress(i0).IA;
    LA1=primdress(i0).LA;
    MSA1=primdress(i0).MSA;
    MIA1=primdress(i0).MIA;
    MLA1=primdress(i0).MLA;
    MFA1=primdress(i0).MFA;
    SB1=primdress(i0).SB;
    IB1=primdress(i0).IB;
    MSB1=primdress(i0).MSB;
    MIB1=primdress(i0).MIB;
    MFB1=primdress(i0).MFB;

    
    L1=primdress(i0).L;    
    ML1=primdress(i0).ML;
    
    
    n1=primdress(i0).num;
    
    for j0=1:Nbas
        
        
    SA2=primdress(j0).SA;
    IA2=primdress(j0).IA;
    LA2=primdress(j0).LA;
    MSA2=primdress(j0).MSA;
    MIA2=primdress(j0).MIA;
    MLA2=primdress(j0).MLA;
    MFA2=primdress(j0).MFA;

    
    SB2=primdress(j0).SB;
    IB2=primdress(j0).IB;
    MSB2=primdress(j0).MSB;
    MIB2=primdress(j0).MIB;
    MFB2=primdress(j0).MFA;
    L2=primdress(j0).L;    
    ML2=primdress(j0).ML;
    
    
    n2=primdress(i0).num;
    
        
        
        if(L2==L1 && ML1==ML2)
             if( MSB1==MSB2 && MIA1==MIA2 && MIA1==MIA2 && MSA1==MSA2 && MLA1==MLA2 )  % Zeeman
                Hz(i0,j0)=Hz(i0,j0)+ Bfld*(MSA1*fzsa+MSB1*fzsa+MIA1*fzia+MIB1*fzib+MLA1*fzla);  
             end
            
        end
        
        
    end
end

channels=repmat(struct('vec',zeros(Nbas,1),'energy',-100,'L',-100,'ML',-100,'MFA',-100,'MSB',-100,...
    'MSAB',-100,'MTOT',-100,'MT',-100,'num',1),Nbas,1);   

[vs,e]=eig(Has);

MFAs=diag(vs'*diag([primdress.MFA])*vs);
MSBs=diag(vs'*diag([primdress.MSB])*vs);
MSABs=diag(vs'*(diag([primdress.MSA])+diag([primdress.MSB]))*vs);
Ls=diag(vs'*diag([primdress.L])*vs);
MLs=diag(vs'*diag([primdress.ML])*vs);
de=zeros(Nbas,1);

for i0=1:Nbas
    de(i0)=e(i0,i0)*cmmhz/1000;
    channels(i0).num=i0;     
    channels(i0).MFA=MFAs(i0);
    channels(i0).MSB=MSBs(i0);
    channels(i0).MSAB=MSABs(i0);
    channels(i0).ML=MLs(i0);
    channels(i0).L=Ls(i0);
    channels(i0).energy=e(i0,i0)/convert;
    channels(i0).vec=vs(:,i0);
%    [MFA MSB  ML  L];
end

return


