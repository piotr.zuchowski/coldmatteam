clear all
const; atomic_mass;

mu=1/(1/he4+1/rb87);
muau=mu*1822.889;
npots=3; convert=219474.63068;

sa=1/2; sb=1; ia=3/2;      mtot=2; Lmax=0;

global lscald Gamma lscal
Gamma=0;
lscald=1; lscal=1;


%-------------------------------------------------------------------------- 
% potencjaly & energia kinet.
 
Ecoll=1d-8;        Ec=Ecoll/convert;
rmin=6.5; rmax=400;  rmid=40;  steps=25;


[bas,basL]=get_basis(sa,ia,sb,mtot,Lmax);
%[hs,vs,es,channels]=get_channels(basL,1500);

% --------------------------    GRID 
disp(' Make grid ');
tic
rmid=40;
gridget=make_grid_dbl_ver2(rmin,rmid,rmax,steps,Ec,muau,@RbHe_pot_quart); 
toc
fprintf(' Grid step at short range: %f\n Grid size: %f\n',gridget.dR(1),gridget.NR);


%-------------------------------------------------------------------------- 
Btab=1:1:1200;
NB=length(Btab);
NCNT=[];
disp(' Basis set size:' )
Nbas=length(basL)
Eb=zeros(Nbas,NB);
cplu=zeros(Nbas,Nbas,npots);
for i1=1:Nbas
    for j1=1:Nbas
        [c_d,c_q,css]=cpl_RbHe_pot(basL(i1),basL(j1));
        cplu(i1,j1,1)= c_d;
        cplu(i1,j1,2)= c_q; 
        cplu(i1,j1,3)= css;

    end
end
fprintf(' Basic channel information (decoupled basis) \n');
fprintf('   L: ');fprintf(' %3.1f ',basL.L); fprintf('\n');
fprintf('  ML: ');fprintf(' %3.1f ',basL.ML); fprintf('\n');
fprintf(' MSA: ');fprintf(' %3.1f ',basL.MSA); fprintf('\n');
fprintf(' MSB: ');fprintf(' %3.1f ',basL.MSB); fprintf('\n');
fprintf(' MIA: ');fprintf(' %3.1f ',basL.MIA); fprintf('\n');


basL_cpl=get_basis_cpl(sa,ia,sb,mtot,Lmax);
Nbas_cpl=length(basL_cpl);

fprintf(' Basic channel information (coupled) \n');
fprintf('    L: ');fprintf(' %3.1f ',basL_cpl.L); fprintf('\n');
fprintf('   ML: ');fprintf(' %3.1f ',basL_cpl.ML); fprintf('\n');
fprintf('  SAB: ');fprintf(' %3.1f ',basL_cpl.SAB); fprintf('\n');
fprintf(' MSAB: ');fprintf(' %3.1f ',basL_cpl.MSAB); fprintf('\n');
fprintf('  MIA: ');fprintf(' %3.1f ',basL_cpl.MIA); fprintf('\n');

% test: 
% I'm checking the transformation 
%  cpl<->dcpl - the cpling matrix for central potential should be 
% diagonal in (SA SB) SAB MSAB coupling scheme - it really works!
% 
% T=zeros(Nbas,Nbas_cpl);
% 
% for i=1:Nbas
%     SA=basL(i).SA;
%     SB=basL(i).SB;
%     MSA=basL(i).MSA;
%     MSB=basL(i).MSB;
%     L1=basL(i).L; ML1=basL(i).ML;
%     MI1=basL(i).MIA;
%     
%     for j=1:Nbas_cpl
%         
%     L2=basL_cpl(j).L; ML2=basL_cpl(j).ML; MI2=basL_cpl(j).MIA;
%     SAB=basL_cpl(j).SAB;    MSAB=basL_cpl(j).MSAB;
%         
%         if(L1==L2 && ML1==ML2 && MI1==MI2)   
%             T(i,j)=ClebshGordan(SA,SB,SAB,MSA,MSB,MSAB);
%         end           
%     end
% end
% testcpl1=T'*cpl(:,:,1)*T;
% testcpl2=T'*cpl(:,:,2)*T;
% TT=transformation_matrix(basL,'SA','SB','MSA','MSB',basL_cpl,'SAB','MSAB');



cpl=zeros(Nbas,Nbas,npots);
%sl=zeros(NB,Nbas);
sl=[];
msab=zeros(Nbas,NB);
energie=zeros(Nbas,NB);

for ib=1:length(Btab)

B=Btab(ib);
%fprintf(' Basic channel information \n');
%[channels.num;channels.ML; channels.MS; channels.L]

[vs,es,channels]=get_channels(basL,B);
Eb(:,ib)=es;
msab(:,ib)=[channels.MSAB];
energie(:,ib)=[channels.energy];

  
% % -------------------------------------------------------------------------
%      Ereference=unique([channels(find([channels.L]==0 & [channels.MSB]==-1)).energy])
%      Nreference=unique([channels(find([channels.L]==0 & [channels.MSB]==-1)).num])
%      if(length(Ereference)>1)
%          error('Initial channel has to be uniqely defined!')
%      end
     % transform coupling (interaction) matrix into asymptotic eigenstates 
%      cpl(:,:,1)=vs'*cplu(:,:,1)*vs;
%      cpl(:,:,2)=vs'*cplu(:,:,2)*vs;
%      cpl(:,:,3)=vs'*cplu(:,:,3)*vs;
     
     
%      propagate
%      parsW.E=Ec+Ereference;
%      parsW.mu=muau;     parsW.npots=npots;   parsW.Nbasis=Nbas;
%      parsW.pot_handle={@RbHe_pot_quart,@RbHe_pot_quart,@RbHe_dipolar}  ; %@CrYb_potmatrix;
%      parsW.cpl=cpl; 
%      fprintf(' Propagation in progress for the field: %f \n',B);
%      tic
%      [Q1,RN,RP,ncnt1,QQ]=numerov_prop_dbl_ver2(gridget,parsW,channels);    
%      toc 
%      NCNT=[ncnt1 NCNT];
%      [S1,K,iopen,slen1]=match_asymp_ver2(Q1,RN,RP,parsW,channels);
%      sl = [sl;slen1];
end
% hold on
% plot(Btab,Eb,'m')

