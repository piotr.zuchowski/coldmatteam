function res=RbHe_dipolar(r)

alphai=137.035999679;
res=alphai^(-2)*r.^(-3);

end

