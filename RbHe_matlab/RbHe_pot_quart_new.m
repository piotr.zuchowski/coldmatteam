function [ output ] = RbHe_pot_quart_new(r,lscal)
% Fitted potential for quartet state of RbHe system
% based on ab-initio all-electron DK CCSD(T) calculations
%  MLJ potential
if(nargin<2)
    lscal=1;
end
    

p=5;q=4;
const;

De=452.70537*lscal;
re=9.4079;


C6=846733122.54;
C8=80108239950.0;
C10=9424240612200.0;


psi0=-1.828444439385;
psi1=0.486779802373283;
psi2=-0.065080919024332;
psi3=-0.300874370172909;
psi4=-1.5194948193886;


y=@(x)((x.^p-re.^p)./(x.^p+re.^p));
yq=@(x)((x.^q-re.^q)./(x.^q+re.^q));

ulr=@(x)(C6.*x.^(-6)+C8.*x.^(-8)+C10.*x.^(-10));
pinf=log(2*De/ulr(re))
ulr(re)



psi=@(x)((1-y(x)).*(psi0+psi1.*yq(x)+psi2.*yq(x).^2+psi3.*yq(x).^3+psi4.*yq(x).^4)+y(x).*pinf);
glr=@(x)(De.*(1-(ulr(x)./ulr(re)).*exp(-psi(x).*y(x))).^2-De);

%`fprintf(' %f %f %f %f \n',r,y(r),yq(r),psi(r),ulr(r));

oo=glr(r);
output=oo/219474.63069;
return    
    
    

