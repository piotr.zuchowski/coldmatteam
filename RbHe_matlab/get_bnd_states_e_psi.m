function [e,psi,R]=get_bnd_states_e_psi(grid,mu,L,vpot,nstates)
%
% function returns binding energies of 
%
%


const;



Rmin=grid.rmin; Rmax=grid.rmax; dR=grid.dr;

y=[log(Rmin):dR:log(Rmax)];  R=exp(y);

T=hbar2/(2*mu).*(-diag(exp(-y))*D2(y)*diag(exp(-y))+diag(exp(-2*y)).*0.25);

V=vpot(R);

%disp(' inner turn point ')
%min(R(find(V<0)))
%disp(' minimum ');
%min(V)

H=T+diag(V) +  diag((L*(L+1)*hbar2/(2*mu)).*R.^(-2));

N=length(H);

fprintf('  potential  depth: %f  \n',min(V) );

[vv,ee]=eig(H);
eed=diag(ee);
% [eu,iu]=sort(ee);
ieed=find(eed<0);

eu1=eed(ieed);
vv1=vv(:,ieed);


Nbound=length(ieed);
%length(eu1)
e=zeros(nstates,1);
psi1=zeros(N,nstates);
psi=zeros(N,nstates);

if(nstates>0)

for i=1:nstates
  e(i)=eu1(end-i+1)*cmmhz;  
  psi1(:,i)=vv1(:,end-i+1)./sqrt(exp(y'));
end


for i=1:nstates
%  e(i)=eu1(end-i+1)*cmmhz;  
 % psi1(:,i)=vv1(:,end-i+1)./sqrt(exp(y'));
  psi(:,i)=norm_v(psi1(:,i),R);
end
else
   
    
for i=1:length(ieed)
  e(i)=eu1(end-i+1)*cmmhz;  
  psi1(:,i)=vv1(:,end-i+1)./sqrt(exp(y'));
end


for i=1:length(ieed)
%  e(i)=eu1(end-i+1)*cmmhz;  
 % psi1(:,i)=vv1(:,end-i+1)./sqrt(exp(y'));
  psi(:,i)=norm_v(psi1(:,i),R);
end
end


return

% [vv,ee]=eig(H);
% nstates=9;q
% eed=diag(ee);
% [ees,ies]=sort(eed);
% Fun=zeros(nbasis,nstates);
% 
% 
% for i=1:nstates
% 
%     psi_scat=vv(:,ies(i));
%     psi_scat=psi_scat./sqrt(exp(y'));
%     Fun(:,i)=norm_v(psi_scat,RR);
% 
%  % dide(nn)=dvibde(ee12cm(nn),abs(ee12cm(nn)/100),@RbYb_pot,L,mu,Rmin,Rmax*0.75,ddr);
% end
