function [rec_d,rec_q,redip]=cpl_pot(bas1,bas2)


L=bas1.L; ML=bas1.ML; SA=bas1.SA; SB=bas1.SB; IA=bas1.IA; MSA=bas1.MSA; MIA=bas1.MIA; MSB=bas1.MSB;

Lp=bas2.L; MLp=bas2.ML; SAp=bas2.SA; SBp=bas1.SB; IAp=bas2.IA; MSAp=bas2.MSA; MIAp=bas2.MIA; MSBp=bas2.MSB;

% central potential
if(SA~=SAp)
    stop (' error! SA is wrong, asshole!' );
end
if(IA~=IAp)
    stop (' error! IA is wrong, asshole!' );
end
if(SB~=SBp)
    stop (' error! SB is wrong, asshole!' );
end
    
Stot=1/2;
rec_d=0;
if(L==Lp && ML==MLp && MIA==MIAp)
    term1=xparit(2*SA-2*SB+MSA+MSB+MSAp+MSBp)*(2*Stot+1);
    term2=w3j(SA,SB,Stot,MSA,MSB,-MSA-MSB)*w3j(SA,SB,Stot,MSAp,MSBp,-MSAp-MSBp);
    rec_d=term1*term2;
end

Stot=3/2;
rec_q=0;
if(L==Lp && ML==MLp && MIA==MIAp)
    term1=xparit(2*SA-2*SB+MSA+MSB+MSAp+MSBp)*(2*Stot+1);
    term2=w3j(SA,SB,Stot,MSA,MSB,-MSA-MSB)*w3j(SA,SB,Stot,MSAp,MSBp,-MSAp-MSBp);
    rec_q=term1*term2;
end

redip=0;

if( MIA==MIAp)
    redip=spspin(SA,SB,MSA,MSB,L,ML,MSAp,MSBp,Lp,MLp);    
end

return



