clear all
const; atomic_mass;

mu=1/(1/he4+1/rb87);

% 
% e_hf=load('data_hf.txt');
% 
% e_int=load('data.txt');
% 
% e_corr=load('dat_corr.txt');
% 
% pd=polyfit(e_corr(:,1),e_corr(:,4),1);
% 
% 
% lx=[2.0794415417	8
% 2.0541237337	7.8
% 2.0281482473	7.6
% 2.0149030205	7.5
% 1.9459101491	7
% 1.8718021769	6.5
% 1.7917594692	6
% 1.7047480922	5.5];


% lecor=polyval(pd,lx(:,1))
% ecorr=-exp(lecor)
rmaxtab=[50:25:500];
wynik=zeros(1,length(rmaxtab));
tanwynik=zeros(1,length(rmaxtab));

scal=1.000000
%for j=1:length(rmaxtab)
rmax=500;
dr=0.005;
rtest=[7.2:dr:rmax];

gridg.rmin=rtest(1)/aubohr;
gridg.dr=0.002;
gridg.rmax=250;
Vtest=RbHe_pot_quart_new(rtest,scal)*219474.63069;
r=rtest(find(Vtest<0));
V=Vtest(find(Vtest<0));
sqV=sqrt(-2*V*mu/hbar2);

inte=0;
for i=1:length(V)-1
    inte=dr*(sqV(i+1)+sqV(i))/2 +inte;  
end
disp('int sqrt(-V)');
[evdw,rvdw,abar]=vdw_params(mu,3858.6)
wynik=inte/aubohr

N=floor(wynik/pi+3/8)

tanwynik=tan(wynik-pi/8)
%a=abar*(1-tanwynik)*aubohr

a0=numerov_1channel(1e-8,@(x)(RbHe_pot_quart_new(x*aubohr,scal)*autocm) , 0,mu, 7.3/aubohr, rmax/aubohr, dr/aubohr)
disp(' ------------------ scattering length --------------- ')
a0*aubohr
%a
min(V)

%e=get_bnd_states_e_psi(gridg,mu,0,@(x)(RbHe_pot_quart_new(x*aubohr,scal)*autocm),0)





