
! Extracts coordinates from .xyz files.
! First map atoms in .xyz file to atoms in main input file, then use 
!     module find_geo to determine dimer orientation
! Optionally a maximum closest-contact atom distance cutoff can be given.

program geo
  use const
  use dimer
  use utility
  use find_geo
  use dgrid
  implicit none
  
  real(8), parameter :: thresh = 2.0d-2 ! Error threshold in bohrs
  
  integer :: i, j, k, l
  real(8) :: s1(MAX_NSITE,3), s1_mapped(2,MAX_NSITE,3)
  logical :: s1Mapped(0:MAX_NSITE)
  integer :: s1Anum(MAX_NSITE)
  integer :: natomtot, mapStart
  integer :: atomMap(2,MAX_NSITE)
  real(8) :: distLim, dist, err, sumDist(2)
  real(8) :: geom(MAX_NCOOR)
  logical :: mapped, valid
  character(100) :: name, input_file, xyz_file, geo_file
  real(8), allocatable :: sitRAB(:,:), s1RAB(:,:)
  real(8) :: xyzcoor(3)
  character(100) :: str
  real(8) :: val
  
  call getarg(1,name)
  call getarg(2,xyz_file)
  call getarg(3,str)
  read(str,*) distLim
  if (distLim <= 0.0d0) distLim = 1.0d100
  
  geo_file = trim(xyz_file)//'.geo'
  
  input_file = trim(name)//'.input_long'
  call read_dimer(input_file)
  call initGridCoors(0)
  
  open(101,file=trim(xyz_file))
  read(101,*) natomtot
  if (natomtot /= natom(1) + natom(2)) then
     write(*,*) 'Error: Incorrect number of atoms'
     stop
  endif
  read(101,'(A)') str
  do i = 1,natomtot
     read(101,*) str, xyzcoor(1:3)
     s1(i,1:3) = xyzcoor(1:3)*A2bohr
     s1Anum(i) = getAnum(str)
  enddo
  close(101)
  
  allocate( sitRAB(nsitemax,nsitemax) )
  allocate( s1RAB(natomtot,natomtot) )
  do i = 1,natomtot
     do j = 1,natomtot
        s1RAB(i,j) = r12(s1(i,:), s1(j,:))
     enddo
  enddo
  
  atomMap(:,:) = 0
  mapped = .true.
  s1Mapped(:) = .false.
  
  ! Assign atom types to atoms
  i = 1
  do while (i <= 2)
     
     do j = 1,natom(i)
        do k = 1,natom(i)
           sitRAB(j,k) = r12(sit0(i,j,:), sit0(i,k,:))
        enddo
     enddo
     
     j = 1
     do while (j <= natom(i))
        mapStart = atomMap(i,j) + 1
        s1Mapped(atomMap(i,j)) = .false.
        atomMap(i,j) = 0
        
        do k = mapStart,natomtot
           ! Check if site j of monomer i can be mapped to atom k
           if (.not. s1Mapped(k) .and. s1Anum(k) == sanum(i,j)) then
              valid = .true.
              do l = 1,natom(i)
                 if (j /= l .and. atomMap(i,l) > 0) then
                    if (abs(s1RAB(k,atomMap(i,l)) - sitRAB(j,l)) > thresh) then
                       valid = .false.
                       exit
                    endif
                 endif
              enddo
              
              if (valid) then
                 atomMap(i,j) = k
                 s1Mapped(k) = .true.
                 exit
              endif
           endif
        enddo
        
        if (atomMap(i,j) > 0) then
           !write(*,'(A,2I4,A,I4)') 'Map ',i,j,' to ',atomMap(i,j)
           j = j + 1
        else
           if (j > 1) then
              j = j - 1
           else
              mapped = .false.
              exit
           endif
        endif
        
     enddo
     
     if (mapped) then
        i = i + 1
     else
        if (i > 1) then
           i = i - 1
        else
           exit
        endif
     endif
  enddo
  
  if (.not. mapped) then
     write(*,*) 'Unable to map atoms'
     stop
  endif
  
  ! Determine dimer configuration given mapped atoms
  do i = 1,2
     do j = 1,natom(i)
        s1_mapped(i,j,:) = s1(atomMap(i,j),:)
     enddo
  enddo
  
  err = findGeo(s1_mapped, geom)
  
  if (err < thresh .and. closestContact() < distLim*A2bohr) then
     open(102,file=geo_file,status='new')
     call writeCoors(geom, 0, 102)
     close(102)
     write(*,'(A,E12.5,A)') ' Geometry found, RMSE=',err,' bohrs'
     !call printCoords()
     stop
  else
     write(*,*) 'Cannot find dimer orientation'
  endif
  
  
  stop
  
end program

