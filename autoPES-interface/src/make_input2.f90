
! Parses the file NAME.input and creates NAME.input_long
! Takes info from other data files if they are present, otherwise uses default values
program make_input2
  use const
  use utility
  implicit none
  
  integer, parameter :: INUNIT = 11, OUTUNIT = 12
  integer, parameter :: LABLEN = 100
  integer, parameter :: S_MON(2) = [1, 2], S_COMP = 3, S_UNIT = 4, NSEC = 4
  integer, parameter :: SS_IDOF = 1, SS_CHI = 2, SS_OPTIM = 3, NSSEC = 3
  
  character(200) :: input_file, output_file, in_name, IP_file, DM_file, PZ_file
  character(1000) :: line
  character(LABLEN) :: slabel(NSEC), sslabel(NSSEC), lab
  integer :: isec(NSEC,0:NSSEC)
  integer :: sec, ssec, imon, issec
  integer :: nline
  logical :: lval
  real(8) :: unitFactor, IP(2), DM(2), PZ(2)
  integer :: nsite, charge, nIDOF, nIDOF_OA, nIDOF_phys, nmcset
  character(10) :: label, label1, label2, mctlabel, labels(MAX_NSITE), mclabels(MAX_NSITE)
  real(8) :: pos(3), COM(3)
  real(8) :: massSum, mass
  integer :: atomNum, sittype, ffType, mctype, mcgroup
  real(8) :: mcfact
  integer :: ntype, typeMap(1000)
  logical :: same, polarizable, oaseg
  logical :: lbuf(10)
  logical :: incComp(MAX_NSITE,4)
  integer :: i, j, k
  
  slabel(S_MON(1)) = 'MONOA'
  slabel(S_MON(2)) = 'MONOB'
  slabel(S_COMP) = 'COMPONENTS'
  slabel(S_UNIT) = 'UNIT'
  sslabel(SS_IDOF) = 'IDOF'
  sslabel(SS_CHI) = 'CHIRAL'
  sslabel(SS_OPTIM) = 'OAOPTIM'
  
  call getarg(1,in_name)
  call getarg(2,IP_file)
  call getarg(3,DM_file)
  call getarg(4,PZ_file)
  
  input_file = trim(in_name)//'.input'
  output_file = trim(in_name)//'.input_long'
  
  ! Read ionization potential if file exists
  nline = nlines(IP_file)
  if (nline >= 2) then
     open(10,file=IP_file)
     do i = 1,2
        read(10,*) IP(i)
     enddo
     close(10)
  else
     IP(:) = 0.0d0
  endif
  
  ! Read dipole moment if file exists
  nline = nlines(DM_file)
  if (nline >= 2) then
     open(10,file=DM_file)
     do i = 1,2
        read(10,*) DM(i)
     enddo
     close(10)
  else
     DM(:) = 0.0d0
  endif
  
  ! Read isotropic polarizability if file exists
  nline = nlines(PZ_file)
  if (nline >= 2) then
     open(10,file=PZ_file)
     do i = 1,2
        read(10,*) PZ(i)
     enddo
     close(10)
  else
     PZ(:) = 0.0d0
  endif
  
  
  nline = nlines(input_file)
  open(unit=INUNIT,file=input_file)
  
  ! Find locations of input sections
  isec(:,:) = 0
  sec = 0
  do i = 1,nline
     read(INUNIT,'(A)') line
     if (line(1:1) == '*') then
        
        ssec = -1
        if (line(2:2) /= '*') then
           lab = to_upper(line(2:LABLEN))
           do j = 1,NSEC
              if (lab == slabel(j)) then
                 sec = j
                 ssec = 0
              endif
           enddo
        else
           lab = line(3:LABLEN)
           do j = 1,NSSEC
              if (lab == to_upper(sslabel(j))) then
                 ssec = j
              endif
           enddo
        endif
        
        if (ssec == -1) then
           write(*,*) 'ERROR: Invalid section label: ',trim(line)
           stop
        else
           isec(sec,ssec) = i
        endif
        
     endif
  enddo
  
  if (isec(S_MON(1),0) == 0 .or. isec(S_MON(2),0) == 0) then
     write(*,*) 'ERROR: sections ',trim(slabel(S_MON(1))),' and ',trim(slabel(S_MON(2))),' are required'
     stop
  endif
  
  lval = gotosec(S_MON(2),0)
  read(INUNIT,'(A)') line
  same = to_upper(line) == 'SAME'
  
  if (gotosec(S_UNIT,0)) then
     read(INUNIT,'(A)') line
     line = to_upper(line)
     if (line == 'BOHR') then
        unitFactor = 1.0d0
     elseif (line == 'ANGSTROM') then
        unitfactor = A2bohr
     else
        write(*,*) 'ERROR: Invalid unit speficied: ',trim(line)
        stop
     endif
  else
     unitFactor = A2bohr
  endif
  
  open(OUTUNIT,file=output_file,status='replace')
  
  ! Print monomer info
  do i = 1,2
     if (same) then
        imon = 1
     else
        imon = i
     endif
     
     if (gotosec(S_MON(imon), SS_IDOF)) then
        read(INUNIT,*) nIDOF_phys
     else
        nIDOF_phys = 0
     endif
     if ((i == 1 .or. .not. same) .and. gotosec(S_MON(imon),SS_CHI)) then
        nIDOF_phys = nIDOF_phys + 1
     endif
     if (gotosec(S_MON(imon), SS_OPTIM)) then
        read(INUNIT,*) nIDOF_OA
     else
        nIDOF_OA = 0
     endif
     nIDOF = nIDOF_phys + nIDOF_OA
     
     lval = gotosec(S_MON(imon), 0)
     read(INUNIT,*) nsite, charge
     
     write(OUTUNIT,'(4I5,3E16.6)') &
          nsite, nIDOF_phys, nIDOF_OA, charge, IP(imon), DM(imon), PZ(imon)
  enddo
  
  ntype = 0
  typeMap(:) = 0
  
  do i = 1,2
     if (same) then
        imon = 1
     else
        imon = i
     endif
     
     lval = gotosec(S_MON(imon), 0)
     read(INUNIT,*) nsite
     
     ! Compute COM
     massSum = 0.0d0
     COM(:) = 0.0d0
     oaseg = .false.
     do j = 1,nsite
        read(INUNIT,*) label, pos(1:3), atomNum
        if (atomNum > 0) then
           if (oaseg) then
              write(*,*) 'ERROR: off-atomic sites must me listed after atoms'
              stop
           endif
           mass = atomMass(atomNum)
           massSum = massSum + mass
           COM(:) = COM(:) + pos(1:3)*mass
        elseif (atomNum == 0) then
           oaseg = .true.
        else
           write(*,*) 'ERROR: Invalid atomic number:',atomnum
           stop
        endif
     enddo
     COM(:) = COM(:)/massSum
     
     ! Write monomer sites
     lval = gotosec(S_MON(imon), 0)
     read(INUNIT,*) nsite
     do j = 1,nsite
        read(INUNIT,*) labels(j), pos(1:3), atomNum, sittype, ffType
        pos(1:3) = unitFactor*(pos(1:3) - COM(1:3))
        write(OUTUNIT,'(A5,3F16.10,I5,I5,I5)') &
             labels(j), pos(1:3), atomNum, sittype, ffType!, ' 0 0 0 0 0 0 0 0'
        ntype = max(ntype, sittype)
        typeMap(sittype) = atomNum
     enddo
     
     ! Write monomer IDOF
     if ((i == 1 .or. .not. same) .and. gotosec(S_MON(imon),SS_CHI)) then
        write(OUTUNIT,'(5I5,F10.5)') MC_CHI, 0, 0, 0, 0, 1.0d0
     endif
     
     do issec = 1,2
        if (issec == 1) then
           ssec = SS_IDOF
        else
           ssec = SS_OPTIM
        endif
        
        if (gotosec(S_MON(imon),ssec)) then
           read(INUNIT,*) nIDOF
           do j = 1,nIDOF
              if (ssec == SS_IDOF) then
                 read(INUNIT,*) mctlabel, label1, label2, nmcset
                 mcgroup = 0
                 mcfact = 1.0d0
              else
                 read(INUNIT,*) mctlabel, label1, label2, nmcset, mcgroup, mcfact
              endif
              if (mctlabel == 'ANG') then
                 mctype = MC_ANG
              elseif (mctlabel == 'LIN') then
                 mctype = MC_LIN
              else
                 write(*,*) 'ERROR: Invalid IDOF type: ',trim(mctlabel)
                 stop
              endif
              read(INUNIT,*) mclabels(1:nmcset)
              write(OUTUNIT,'(5I5,F10.5)') &
                   mctype, strIndex(label1, labels, nsite), strIndex(label2, labels, nsite), nmcset, mcgroup, mcfact
              do k = 1,nmcset
                 write(OUTUNIT,'(I5,A)',advance='no') strIndex(mclabels(k), labels, nsite),' '
              enddo
              write(OUTUNIT,'(A)') ''
           enddo
        endif
     enddo
  enddo
  
  ! Write fit components
  polarizable = PZ(1) > 1.0d-10 .and. PZ(2) > 1.0d-10
  do i = 1,ntype
     incComp(i,F_els) = .true.
     incComp(i,F_pol) = polarizable .and. typeMap(i) > 1
     incComp(i,F_vdw) = typeMap(i) > 0
     incComp(i,F_exp) = .true.
  enddo
  
  if (gotosec(S_COMP,0)) then
     do i = 1,ntype
        read(INUNIT,*,end=101,err=101) sittype, lbuf(1:n_FF_comp)
        if (sittype > ntype) then
           write(*,*) 'ERROR: Invalid site type index :',sittype
           stop
        endif
        incComp(sittype,1:n_FF_comp) = lbuf(1:n_FF_comp)
     end do
101  continue
  endif
  
  do i = 1, ntype
     write(OUTUNIT,'(4L2)') incComp(i,1:n_FF_comp)
  end do
  
  close(INUNIT)
  close(OUTUNIT)
  
contains
  
  integer function strIndex(str, strArr, arrlen)
    integer, intent(in) :: arrlen
    character(10), intent(in) :: strArr(arrlen), str
    integer :: i
    
    strIndex = 0
    do i = 1,arrlen
       if (str == strArr(i)) then
          strIndex = i
          exit
       endif
    enddo
    
    if (strIndex == 0) then
       write(*,*) 'ERROR: Cannot identify atom label ',str
       stop
    endif
  end function strIndex
  
  
  logical function gotosec(sec, ssec)
    integer, intent(in) :: sec, ssec
    character(1) :: line
    integer :: i
    
    rewind(INUNIT)
    if (isec(sec,ssec) > 0) then
       do i = 1,isec(sec,ssec)
          read(INUNIT,'(A)') line
       enddo
       gotosec = .true.
    else
       gotosec = .false.
    endif
  end function gotosec
  
end program make_input2

