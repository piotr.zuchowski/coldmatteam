module force_field_guid
 use double
 use dimer
 real(kind=dbl), parameter :: twosix=1.122462048309372981d0
 integer :: NTYPEMAX
 character(len=15) :: epsrule, radrule, radtype, radsize
 real(kind=dbl), allocatable :: vdw_parms(:,:), ch_parms(:), epsilon_vdw(:,:), radmin_vdw(:,:), ch_vals(:,:)
 
contains

subroutine ff_init_guid(parms_file)
 implicit none
 character(len=200) :: parms_file, file_vdwrules
 logical :: ex
 
 file_vdwrules=''
 
 call read_parms(parms_file)
 
 inquire(file=trim(file_vdwrules),exist=ex)

 epsrule = ' '
 radrule = ' '
 radtype = ' '
 radsize = ' '
 if (ex) then
  open(unit=28,file=trim(file_vdwrules))
  read(28,*,err=10,end=10) epsrule, radrule
 end if

10 continue

 call calc_2B_parms

 allocate( ch_vals(2,nsitemax) )
 call regularizeCharge
 
 return
end subroutine ff_init_guid

function nFreeParm_guid()
  integer :: nFreeParm_guid
  
  nFreeParm_guid = 0
end function nFreeParm_guid

function getFreeParm_guid(iparm)
  real(8) :: getFreeParm_guid
  integer :: iparm
  
  getFreeParm_guid = 0.0d0
end function getFreeParm_guid

subroutine setFreeParm_guid(iparm, val)
  integer :: iparm
  real(8) :: val
  
end subroutine setFreeParm_guid

subroutine read_parms(parms_file)
 implicit none
 character(len=200) :: parms_file
 integer :: i, k, it
 character(len=15) :: label, label2
 real(kind=dbl) :: x1, x2, x3
 character(len=120) :: line
 
 open(unit=27,file=trim(parms_file),status='old')
 
 NTYPEMAX = 0
 do while (.true.)
   read(27,'(A120)',err=11,end=21) line
   read(line,*,err=31,end=31) label, label2
   call upcase(label)
   if ( trim(label).eq.'RADIUSRULE' ) then
     call upcase(label2)
     radrule = trim(label2)
   else if ( trim(label).eq.'RADIUSTYPE' ) then
     call upcase(label2)
     radtype = trim(label2)
   else if ( trim(label).eq.'RADIUSSIZE' ) then
     call upcase(label2)
     radsize = trim(label2)
   else if ( trim(label).eq.'EPSILONRULE' ) then
     call upcase(label2)
     epsrule = trim(label2)
   end if
31 continue
   read(line,*,err=11,end=11) label, it
   call upcase(label)
   if ( trim(label).eq.'ATOM'.and.it.gt.NTYPEMAX ) NTYPEMAX = it
11 continue
 end do
21 continue
   

 allocate (vdw_parms(2,NTYPEMAX),ch_parms(NTYPEMAX))
 vdw_parms = 0.0d0
 ch_parms  = 0.0d0

 rewind(27)

 do while(.true.)
  read(27,'(A120)',err=12,end=22) line
  read(line,*,err=12,end=12) label, k, x1, x2
  call upcase(label)
  if ( trim(label).eq.'VDW' ) then
   if ( trim(radtype).eq.'SIGMA' ) x1 = x1 * twosix
   if ( trim(radsize).eq.'DIAMETER' ) x1 = 0.5d0 * x1
   vdw_parms(1,k) = x1
   vdw_parms(2,k) = x2
  end if
12 continue
 end do
22 continue

 rewind(27)
 
 do while(.true.)
  read(27,'(A120)',err=13,end=23) line
  read(line,*,err=13,end=13) label, k, x1
  call upcase(label)
  if ( trim(label).eq.'CHARGE' ) ch_parms(k) = x1
13 continue
 end do
23 continue

 close (27)
 return
end subroutine read_parms

subroutine calc_2B_parms
 implicit none
 integer :: i, k
 real(kind=dbl) :: radi, radk, sradi, sradk, rd
 real(kind=dbl) :: epsi, epsk, sepsi, sepsk, ep
!
 allocate (epsilon_vdw(NTYPEMAX,NTYPEMAX))
 allocate (radmin_vdw(NTYPEMAX,NTYPEMAX))
 do i = 1, NTYPEMAX
  radi = vdw_parms(1,i)
  sradi = dsqrt(radi)
  epsi = vdw_parms(2,i)
  sepsi = dsqrt(epsi)
  do k = i, NTYPEMAX
   radk = vdw_parms(1,k)
   sradk = sqrt(radk)
   epsk = vdw_parms(2,k)
   sepsk = dsqrt(epsk)
! epsilon
   if (epsi.eq.0.0d0 .and. epsk.eq.0.0d0) then
    ep = 0.0d0
   else if (trim(epsrule) .eq. 'ARITHMETIC') then
    ep = 0.5d0 * (epsi + epsk)
   else if (trim(epsrule) .eq. 'GEOMETRIC') then
    ep = sepsi * sepsk
   else if (trim(epsrule) .eq. 'HARMONIC') then
    ep = 2.0d0 * (epsi*epsk) / (epsi+epsk)
   else if (trim(epsrule) .eq. 'HHG') then
    ep = 4.0d0 * (epsi*epsk) / (sepsi+sepsk)**2
   else
    ep = sepsi * sepsk
   end if
   epsilon_vdw(i,k) = ep
   epsilon_vdw(k,i) = ep
! radmin
   if (radi.eq.0.0d0 .and. radk.eq.0.0d0) then
      rd = 0.0d0
   else if (trim(radrule) .eq. 'ARITHMETIC') then
      rd = radi + radk
   else if (trim(radrule) .eq. 'GEOMETRIC') then
      rd = 2.0d0 * (sradi * sradk)
   else if (trim(radrule) .eq. 'CUBIC-MEAN') then
      rd = 2.0d0 * (radi**3+radk**3)/(radi**2+radk**2)
   else
      rd = radi + radk
   end if
   radmin_vdw(i,k) = rd
   radmin_vdw(k,i) = rd
  end do
 end do
 return
end subroutine calc_2B_parms


subroutine regularizeCharge
  implicit none
  real(kind=dbl) :: totDiff, totCharge, ch_parm, tgtchar
  integer :: i, j
  
  do i = 1,2
     tgtchar = dble(molchar(i))
     
     totCharge = 0.0d0
     totDiff = 0.0d0
     do j = 1,natom(i)
        if (fftype(i,j) > 0) then
           ch_parm = ch_parms(fftype(i,j))
           totCharge = totCharge + ch_parm
           totDiff = totDiff + abs(tgtchar - ch_parm)
        endif
     enddo
     
     do j = 1,natom(i)
        if (fftype(i,j) > 0) then
           ch_parm = ch_parms(fftype(i,j))
           !ch_vals(i,j) = ch_parm + (tgtChar - totCharge)*abs(ch_parm - tgtchar)/totDiff
           if (abs(totDiff) > 1.0d-10) then
              ch_vals(i,j) = ch_parm + (tgtChar - totCharge)*abs(ch_parm - tgtchar)/totDiff
           else
              ch_vals(i,j) = ch_parm
           endif
        else
           ch_vals(i,j) = 0.0d0
        endif
     enddo
     
  enddo
  
end subroutine regularizeCharge


real(kind=dbl) function vdw_pair_ener(i,k)
 implicit none
 integer, intent(in) :: i, k
 integer :: it, kt, fftype1, fftype2
 real(kind=dbl) :: xi, yi, zi, xk, yk, zk, dx, dy, dz
 real(kind=dbl) :: r2, r6, r12, rmin, eps
 
 it = fftype(1,i)
 kt = fftype(2,k)
 
 if (it > 0 .and. kt > 0) then
    xi = sit(1,i,1)*a0
    yi = sit(1,i,2)*a0
    zi = sit(1,i,3)*a0
    xk = sit(2,k,1)*a0
    yk = sit(2,k,2)*a0
    zk = sit(2,k,3)*a0
    dx = xi - xk
    dy = yi - yk
    dz = zi - zk
    r2 = dx*dx + dy*dy + dz*dz
    
    rmin = radmin_vdw(kt,it)
    eps = epsilon_vdw(kt,it)
    r6 = rmin**6 / r2**3
    r12 = r6 * r6

    vdw_pair_ener = eps * (r12 - 2.0d0*r6)
 else
    vdw_pair_ener = 0.0d0
 endif
! write(*,*) rmin, eps, vdw_pair_ener

 return
end function vdw_pair_ener

real(kind=dbl) function energy_ff_vdw()
 implicit none
 integer :: i, j
 real(kind=dbl) :: e
 energy_ff_vdw = 0.0d0
 do i = 1, natom(1)
  do j = 1, natom(2)
   e = vdw_pair_ener(i,j)
   energy_ff_vdw = energy_ff_vdw + e
  end do
 end do
 return
end function energy_ff_vdw

real(kind=dbl) function qq_pair_ener(i,k)
 use dimer
 implicit none
 integer, intent(in) :: i, k
 !integer :: it, kt
 real(kind=dbl) :: xi, yi, zi, xk, yk, zk, dx, dy, dz
 real(kind=dbl) :: r2, r, qi, qk

 !it = fftype(1,i) !atomtypea(i)
 !kt = fftype(2,k) !atomtypeb(k)
 if (fftype(1,i) > 0 .and. fftype(2,k) > 0) then
    xi = sit(1,i,1)
    yi = sit(1,i,2)
    zi = sit(1,i,3)
    xk = sit(2,k,1)
    yk = sit(2,k,2)
    zk = sit(2,k,3)
    dx = xi - xk
    dy = yi - yk
    dz = zi - zk
    r2 = dx*dx + dy*dy + dz*dz
    r = dsqrt(r2)
    
    qi = ch_vals(1,i) !ch_parms(it)
    qk = ch_vals(2,k) !ch_parms(kt)
    
    qq_pair_ener = h2kcal * qi * qk / r
 else
    qq_pair_ener = 0.0d0
 endif
 return
end function qq_pair_ener

real(kind=dbl) function energy_ff_qq()
 implicit none
 integer :: i, j
 real(kind=dbl) :: e
 energy_ff_qq = 0.0d0
 do i = 1, natom(1)
  do j = 1, natom(2)
   e = qq_pair_ener(i,j)
   energy_ff_qq = energy_ff_qq + e
  end do
 end do
 return
end function energy_ff_qq

subroutine upcase(string)
 implicit none
 character(len=*), intent(inout) :: string
 integer :: length, code, i
 character :: letter
 
 length = len(string)
 do i = 1, length
  letter = string(i:i)
  code = ichar(letter)
  if (letter.ge.'a'.and.letter.le.'z') string(i:i) = char(code-32)
 end do

 return
end subroutine upcase

end module force_field_guid

