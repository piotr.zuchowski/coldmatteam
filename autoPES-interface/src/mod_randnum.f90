!---------------------------------
!    MODULE : RANDOM
!---------------------------------
module randnum
! use double
! use ifport
contains
  
 subroutine init_random_seed()
   !use iso_fortran_env, only: int64
   implicit none
   integer, allocatable :: seed(:)
   integer :: i, n, un, istat, dt(8), pid
   integer(8) :: t

   n = 10
   un = 105

   call random_seed(size = n)
   allocate(seed(n))
   ! First try if the OS provides a random number generator
   open(unit=un, file="/dev/urandom", access="stream", &
        form="unformatted", action="read", status="old", iostat=istat)
   if (istat == 0) then
      read(un) seed
      close(un)
   else
      ! Fallback to XOR:ing the current time and pid. The PID is
      ! useful in case one launches multiple instances of the same
      ! program in parallel.
      call system_clock(t)
      if (t == 0) then
         call date_and_time(values=dt)
         t = (dt(1) - 1970) * 365_8 * 24 * 60 * 60 * 1000 &
              + dt(2) * 31_8 * 24 * 60 * 60 * 1000 &
              + dt(3) * 24_8 * 60 * 60 * 1000 &
              + dt(5) * 60 * 60 * 1000 &
              + dt(6) * 60 * 1000 + dt(7) * 1000 &
              + dt(8)
      end if
      pid = 0 !getpid()
      t = ieor(t, int(pid, kind(t)))
      do i = 1, n
         seed(i) = lcg(t)
      end do
   end if
   call random_seed(put=seed)
 contains
   ! This simple PRNG might not be good enough for real work, but is
   ! sufficient for seeding a better PRNG.
   function lcg(s)
     integer :: lcg
     integer(8) :: s
     if (s == 0) then
        s = 104729
     else
        s = mod(s, 4294967296_8)
     end if
     s = mod(s * 279470273_8, 4294967291_8)
     lcg = int(mod(s, int(huge(0),8)), kind(0))
   end function lcg
 end subroutine init_random_seed
 
 subroutine init_fixed_seed
   integer, allocatable :: seed(:)
   integer :: nseed
   integer :: i

   CALL RANDOM_SEED(size = nseed)
   ALLOCATE(seed(nseed))

   seed(1) = 1000000
   do i = 2,nseed
      seed(i) = seed(i-1) + 1000000
   enddo

   call RANDOM_SEED(put = seed)
 end subroutine init_fixed_seed
 
      FUNCTION ran00(idum)
      INTEGER idum,IA,IM,IQ,IR,NTAB,NDIV
      REAL ran00,AM,EPS,RNMX
      PARAMETER (IA=16807,IM=2147483647,AM=1./IM,IQ=127773,IR=2836,NTAB=32,NDIV=1+(IM-1)/NTAB,EPS=1.2e-7,RNMX=1.-EPS)
      INTEGER j,k,iv(NTAB),iy
      SAVE iv,iy
      DATA iv /NTAB*0/, iy /0/
      if (idum.le.0.or.iy.eq.0) then
        idum=max(-idum,1)
        do 11 j=NTAB+8,1,-1
          k=idum/IQ
          idum=IA*(idum-k*IQ)-IR*k
          if (idum.lt.0) idum=idum+IM
          if (j.le.NTAB) iv(j)=idum
11      continue
        iy=iv(1)
      endif
      k=idum/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum=idum+IM
      j=1+iy/NDIV
      iy=iv(j)
      iv(j)=idum
      ran00=min(AM*iy,RNMX)
      return
      END FUNCTION ran00

real(8) FUNCTION ran1(idum)
 implicit none
 INTEGER :: idum
 integer, parameter :: IA=16807,IM=2147483647,IQ=127773,IR=2836,NTAB=32,NDIV=1+(IM-1)/NTAB
 real(8), parameter :: AM=1.0d0/IM,EPS=1.2d-7,RNMX=1.0d0-EPS
 INTEGER :: j,k
 integer, save :: iv(NTAB),iy
 DATA iv /NTAB*0/, iy /0/
 if (idum.le.0.or.iy.eq.0) then
    idum=max(-idum,1)
  do j=NTAB+8,1,-1
   k=idum/IQ
   idum=IA*(idum-k*IQ)-IR*k
   if (idum.lt.0) idum=idum+IM
   if (j.le.NTAB) iv(j)=idum
  enddo
  iy=iv(1)
 endif
 k=idum/IQ
 idum=IA*(idum-k*IQ)-IR*k
 if (idum.lt.0) idum=idum+IM
 j=1+iy/NDIV
 iy=iv(j)
 iv(j)=idum
 ran1=min(AM*iy,RNMX)
 return
END FUNCTION ran1

subroutine normal_random(rand)
  use const
  implicit none
  
  real(8) :: rand
  real(8) :: r1, r2
  
  call random_number(r1)
  call random_number(r2)
  rand = sqrt(-2.0d0*log(r1))*cos(2.0d0*pi*r2)
end subroutine normal_random


! Maps a uniformly distributed random variable in the range (0,1)
!    to a normally distributed variable. Very slow.
subroutine uni2norm(u)
  real(8), intent(inout) :: u
  real(8) :: inv, step
  logical :: neg
  integer :: i
  
  u = 2.0d0*u - 1.0d0
  neg = u < 0.0d0
  if (neg) u = -1*u
  
  ! Compute inverse of error function in range (-1,1) via bisector method
  inv = 1.0d0
  i = 1
  do while (erf(inv) < u)
     inv = 2*inv
     i = i + 1
     if (i == 30) then
        goto 101
     endif
  enddo
  
  inv = inv/2
  step = inv/2
  
  do i = 1,30
     if (erf(inv) < u) then
        inv = inv + step
     elseif (erf(inv) > u) then
        inv = inv - step
     else
        exit
     endif
     step = step/2
  enddo
  
101 continue
  
  if (neg) inv = -inv
  u = sqrt(2.0d0)*inv
end subroutine uni2norm


end module randnum
