!
!************************ COMMAND LINE PARAMETERS **************************
!
! NAME (string): Name of the system, used to open NAME.ener and NAME.input_long
!
!**************************** STANDARD INPUT *******************************
!
!
!************************* REQUIRED INPUT FILES ****************************
!
! NAME.input_long : Contains info about the system in the standard autoPES format.
! NAME.ener : Contains set of dimer configurations and energies.
!
!***************************** OUTPUT FILES ********************************
!
! fit_intra.dat : Contains fit parameters of intramonomer potential
!
!***************************************************************************
!


!***************************************************************************
!***************************** DEFINITIONS *********************************
!***************************************************************************

module fitIntraDef
use utility
use const
use dimer
use dgrid
use powell
use force_field_intra_gen
implicit none

integer, parameter :: T_rigid = 1, T_fit = 2, T_neg = 3
integer, parameter :: fit_intra_dat = 11 ! File handles
real(8), parameter :: ener_cutoff = 0.1d0


!***************************************************************************
!*************************** GLOBAL VARIABLES ******************************
!***************************************************************************

integer :: praxisPrint
character(200) :: sysname
character(200) :: enerfile, existingFitFile

integer :: ndat, nparm
integer :: nparmper, nparmper_final
real(8) :: constraintScale, weightScale
logical :: existingFit
real(8) :: minEner, ener0, thresh
real(8), allocatable :: grid(:,:)
real(8), allocatable :: ptweight(:)
integer, allocatable :: sptType(:)
real(8), allocatable :: minIDOF(:), maxIDOF(:)

real(8) :: rBuf(20)
character(100) :: args(10)

real(8), allocatable :: rAB(:,:,:)
real(8) :: praxisScale
real(8) :: sum
integer :: imon, count, index

real(8), allocatable :: optimVect(:)

contains

!***************************************************************************
!****************************** ROUTINES ***********************************
!***************************************************************************


! Initialize fitting weights
function readPoints(fileName, imon)
  logical :: readPoints
  character(*), intent(in) :: fileName
  integer, intent(in) :: imon
  
  logical :: isZero
  integer :: count
  integer :: i, j, k, l
  
  call initGrid(enerfile, grid, ndat, n_ener)
  
  if (ndat == 0) then
     readPoints = .false.
     return
  endif
  
  if (allocated(rAB) ) deallocate( rAB )
  allocate( rAB(ndat,nsite(imon),nsite(imon)) )
  do i = 1,ndat
     do j = 1,nsite(imon)
        do k = 1,nsite(imon)
           rAB(i,j,k) = norm(r12(i, imon, j, k))
        enddo
     enddo
  enddo
  
  minEner = 1.0d100
  do i = 1,ndat
     minEner = min(minEner, grid(i,C_Etot))
  enddo
  
  ! Compute mean energy at monomer zero geometry
  ener0 = 0.0d0
  count = 0
  do i = 1,ndat
     isZero = .true.
     do j = 1,nmcPhys(imon)
        isZero = isZero .and. abs(grid(i,C_mon(imon)+j)) < 1.0d-5
     enddo
     
     if (isZero) then
        ener0 = ener0 + grid(i,C_ener+E_mon(imon))
        count = count + 1
     endif
  enddo
  ener0 = ener0/count
  
  ! Initialize fitting weights
  if (allocated(ptweight) ) deallocate( ptWeight )
  allocate( ptweight(ndat) )
  do i = 1,ndat
     ptweight(i) = gweight(grid(i,C_ener+E_mon(imon)) - ener0, minEner, weightScale)
  enddo
  
  readPoints = .true.
end function


! Categorize the given site pair type based on dependence on IDOFs
subroutine categorizeSPTs(imon)
  integer, parameter :: ninc = 5, nSptPerIdof = 4
  integer, intent(in) :: imon
  real(8) :: gpoint(MAX_NGIND)
  real(8) :: mci, mcj
  real(8) :: r, dr, maxdr(maxnspt)
  integer :: sptype, maxspt
  integer :: i, j, k, l, m, n
  
  sptType(:) = T_RIGID
  do i = 1,nmcPhys(imon)
     mci = minIDOF(i)
     do j = i,nmcPhys(imon)
        mcj = minIDOF(j)
        
        ! Find maximum change in site-site distance for each SPT over given pair of IDOFs
        maxdr(:) = 0.0d0
        do k = 1,ninc+1
           do l = 1,ninc+1
              
              gpoint(:) = 0.0d0
              gpoint(C_mon(imon)+i) = mci
              gpoint(C_mon(imon)+j) = mcj
              call moveDimerGP(gpoint)
              
              do m = 1,nsite(imon)
                 do n = 1,nsite(imon)
                    if (m /= n) then
                       sptype = typeMapMon(stype(imon,m),stype(imon,n))
                       if (sptype > 0) then
                          r = cdist(imon,m,imon,n)
                          dr = abs(r - rAB0(imon,m,n))/rAB0(imon,m,n)
                          maxdr(sptype) = max(maxdr(sptype), dr)
                       endif
                    endif
                 enddo
              enddo
              
              mcj = mcj + (maxIDOF(j) - minIDOF(j))/ninc
           enddo
           mci = mci + (maxIDOF(i) - minIDOF(i))/ninc
        enddo
        
        ! Identify non-rigid SPTs
        do k = 1,nsptypeMon(imon)
           if (maxdr(k) > 1.0d-2 .and. sptType(k) == T_RIGID) then
              sptType(k) = T_NEG
           endif
        enddo
        
        ! Find nSptPerIdof SPTs with greatest dr to use in fit
        do k = 1,nSptPerIdof
           maxspt = 1
           do l = 1,nsptypeMon(imon)
              if (maxdr(l) > maxdr(maxspt)) maxspt = l
           enddo
           
           if (maxdr(maxspt) > 5.0d-2) then
              sptType(maxspt) = T_FIT
           endif
           maxdr(maxspt) = 0.0d0
        enddo
        
     enddo
  enddo
  
end subroutine categorizeSPTs


! Calculate vector between given sites for given configuration
function r12(idat, imon, site1, site2)
  real(8) :: r12(3)
  integer :: idat, imon, site1, site2
  
  call moveDimerGP(grid(idat,:))
  r12 = sit(imon,site1,:) - sit(imon,site2,:)
end function


subroutine loadParmVect(vect)
  real(8) :: vect(:)
  integer :: index
  integer :: i
  
  index = 0
  do i = 1,nsptypeMon(imon)
     if (sptType(i) == T_FIT) then
        if (nparmper >= 1) then
           index = index + 1
           vect(index) = encodeLog(bn(imon,i,2))
        endif
        if (nparmper >= 2) then
           index = index + 1
           vect(index) = bn(imon,i,3)
        endif
        if (nparmper >= 3) then
           index = index + 1
           vect(index) = encodeLog(bn(imon,i,4) - b40(bn(imon,i,2), bn(imon,i,3)))
        endif
        if (nparmper >= 4) then
           index = index + 1
           vect(index) = bn(imon,i,1)
        endif
     endif
  enddo
end subroutine loadParmVect


subroutine unloadParmVect(vect)
  real(8) :: vect(:)
  integer :: index
  integer :: i
  
  index = 0
  do i = 1,nsptypeMon(imon)
     if (sptType(i) == T_FIT) then
        if (nparmper >= 1) then
           index = index + 1
           bn(imon,i,2) = decodeLog(vect(index))
        endif
        if (nparmper >= 2) then
           index = index + 1
           bn(imon,i,3) = vect(index)
           bn(imon,i,4) = b40(bn(imon,i,2), bn(imon,i,3))
        endif
        if (nparmper >= 3) then
           index = index + 1
           bn(imon,i,4) = bn(imon,i,4) + decodeLog(vect(index))
        endif
        if (nparmper >= 4) then
           index = index + 1
           bn(imon,i,1) = vect(index)
        endif
     endif
  enddo
  
  if (homog .and. imon == 1) then
     bn(2,:,:) = bn(1,:,:)
  endif
end subroutine unloadParmVect


! Minimum dr^4 term for given dr^2 and dr^3 terms
real(8) function b40(b2, b3)
  real(8), intent(in) :: b2, b3
  
  ! A value of 0.0 guarantees the only local minimum is at the zero geometry.
  ! A value of 1.0 is a stronger constraint which guarantees the second derivative is positive everywhere.
  real(8), parameter :: b40_const = 0.1d0
  real(8), parameter :: b40_coeff = b40_const*3.0d0/8.0d0 + &
       (1.0d0 - b40_const)*9.0d0/32.0d0
  
  b40 = b40_coeff*b3*b3/b2
end function


! Funciton called by optimizer
real(8) function errFunc(x, n)
  integer, intent(in) :: n
  real(8), intent(in) :: x(n)
  real(8) :: sum
  integer :: i, j
  
  call unloadParmVect(x)
  
  sum = 0.0d0
  do i = 1,ndat
     do j = 1,2
        if (j == imon .or. homog) then
           if (gridEner(j,i) > ener_cutoff) then
              sum = sum + ptweight(i)*(fitEner(j,i) - gridEner(j,i))**2
           endif
        endif
     enddo
  enddo
  
  errFunc = sum
end function errFunc


! RMS error for all points given current energies
real(8) function rmsErr(imon)
  integer :: imon
  
  integer :: i
  real(8) :: sum
  
  sum = 0.0d0
  do i = 1,ndat
     if (gridEner(imon,i) > ener_cutoff) then
        sum = sum + (gridEner(imon,i) - fitEner(imon,i))**2
     endif
  enddo
  rmsErr = sqrt(sum/ndat)
end function

! Percent error for all points given current energies
real(8) function pctErr(imon)
  integer :: imon
  
  integer :: i
  real(8) :: sum
  
  sum = 0.0d0
  do i = 1,ndat
     if (gridEner(imon,i) > ener_cutoff) then
        sum = sum + 2.0d0*abs(gridEner(imon,i) - fitEner(imon,i))/ &
             (abs(gridEner(imon,i)) + abs(fitEner(imon,i)))
     endif
  end do
  pctErr = 100.0d0*sum/ndat
end function


real(8) function gridEner(imon, igrid)
  integer, intent(in) :: imon, igrid
  
  gridEner = grid(igrid,C_ener+E_mon(imon)) - ener0
end function


real(8) function fitEner(imon, igrid)
  integer, intent(in) :: imon, igrid
  
  call moveDimerGP(grid(igrid,:))
  fitEner = energy_ff_intra_gen(imon)
end function


subroutine writePES(iunit)
  integer, intent(in) :: iunit
  integer :: i, j, k
  
  write(iunit,'(I5)') nbterm
  do i = 1,2
     write(iunit,'(I5)') nsptypeMon(i)
     do j = 1,nsptypeMon(i)
        write(iunit,'(I4,A,I4)',advance='no') &
             typeMapInvMon(1,j,1),' ',typeMapInvMon(1,j,2)
        do k = 1,nbterm
           write(iunit,'(A,E15.8)',advance='no') &
                ' ',bn(i,j,k)
        enddo
        write(iunit,'()')
     enddo
  enddo
end subroutine writePES


!*************************** PURE  FUNCTIONS *******************************


! Norm of vector
function norm(rVect)
  real(8) :: norm
  real(8) :: rVect(3)
  
  norm = sqrt(rVect(1)**2 + rVect(2)**2 + rVect(3)**2)
end function


end module


!***************************************************************************
!**************************** MAIN PROGRAM *********************************
!***************************************************************************

program fitIntra
use fitIntraDef
use const
use utility
use dimer
use dgrid
implicit none

integer :: i, j, k, l

!***************************************************************************
!*************************** INITIALIZATION ********************************
!***************************************************************************

! Read command line parameters
do i = 1,10
   call getarg(i, args(i))
enddo
read(args(1),*) sysname
read(args(2),*) nparmper_final
read(args(3),*) constraintScale
read(args(4),*) existingFitFile
read(args(5),*) weightScale

enerfile = trim(sysname)//'.ener'
praxisPrint = 1

inquire(file=existingFitFile,exist=existingFit)

call read_dimer(trim(sysname)//'.input_long')

if (nparmper_final == 1) then
   nbterm = 2
elseif (nparmper_final == 2 .or. nparmper_final == 3) then
   nbterm = 4
elseif (nparmper_final == 4) then
   nbterm = 4
else
   write(*,*) 'ERROR: Invalid number of free parameters per site pair type:', nparmper_final
   stop
endif

if (existingFit) then
   call read_fit_intra_gen(existingFitFile)
else
   call initIntraFF(nbterm)
endif

allocate( sptType(maxnspt) )
allocate( minIDOF(nmcmax), maxIDOF(nmcmax) )


!***************************************************************************
!******************************* OPTIMIZE **********************************
!***************************************************************************


do i = 1,2
   if (nmcPhys(i) > 0) then
      imon = i
      
      ! Initialize grid
      if (.not. readPoints(enerfile, i)) then
         write(*,*) 'Error reading '//trim(enerfile)
         stop
      endif
      
      count = 0
      do j = 1,ndat
         if (gridEner(i,j) > ener_cutoff) count = count + 1
      enddo
      write(*,'(A,I6,A,F8.3,A,I6)') 'ndat = ',ndat,'      ndat > ',ener_cutoff,' kcal/mol = ',count
      
      do j = 1,nmcPhys(i)
         if (existingFit) then
            thresh = 0.8d0*abs(minEner)
         else
            thresh = 0.12d0
         endif
         call IDOF_limits(i, j, minIDOF(j), maxIDOF(j), existingFit, thresh)
         write(*,'(A,I3,I4,2F12.5)') 'IDOF Limits ',i,j,minIDOF(j),maxIDOF(j)
      enddo
      
      call categorizeSPTs(i)
      
      ! Set starting values
      bn(i,:,:) = 0.0d0
      do j = 1,nsptypeMon(i)
         select case (sptType(j))
         case(T_FIT)
            bn(i,j,2) = 1.0d0
         case(T_RIGID)
            sum = 0.0d0
            count = 0
            do k = 1,nsite(i)
               do l = 1,nsite(i)
                  if (k /= l .and. typeMapMon(stype(i,k),stype(i,l)) == j) then
                     sum = sum + rAB0(i,k,l)
                     count = count + 1
                  endif
               enddo
            enddo
            bn(i,j,2) = constraintScale/(sum/count)**2
         case(T_NEG)
            
         endselect
      enddo
      
      ! Optimization stages
      do nparmper = 1,nparmper_final
         write(*,*)
         call writePES(6)
         
         nparm = 0
         do j = 1,nsptypeMon(i)
            if (sptType(j) == T_fit) nparm = nparm + nparmper
         enddo
         allocate( optimVect(nparm) )
         
         if (nparmper == 3) then
            do j = 1,nsptypeMon(i)
               if (sptType(j) == T_fit) bn(i,j,3) = bn(i,j,3) + 1.0d-4
            enddo
         endif
         
         if (nparm > 0) then
            write(*,*)
            write(*,*) 'Beginning optimization ', nparm
            praxisScale = sqrt(real(nparm))
            call loadParmVect(optimVect)
            call praxis(30, 1.0d-3, 2.0d0*praxisScale, nparm, praxisPrint, optimVect, errFunc, 0, .false.)
            call unloadParmVect(optimVect)
         endif
         
         write(*,*) ''
         write(*,'(A,E12.5,A,F12.6,A)') &
              'Error: ',rmsErr(i),' kcal/mol   ',pctErr(i),'%'
         
         deallocate( optimVect )         
      enddo
      
   endif
   
   if (homog) exit
enddo


!***************************************************************************
!******************************** OUTPUT ***********************************
!***************************************************************************

write(*,*) ''

open(fit_intra_dat, file = 'fit_intra.dat', status = 'replace')
call writePES(fit_intra_dat)
close(fit_intra_dat)

end program

