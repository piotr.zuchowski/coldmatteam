!
!************************ COMMAND LINE PARAMETERS **************************
!
! NAME (string): Name of the system, used to open NAME.ener and NAME.input_long
! comRuleMode (integer): 0 to fit each site pair separately in vdW expansion,
!     1 to use geometric mean combination rule
! maxCn (integer): maximum n in distributed vdW expansion (6, 8, 10, or 12)
!
!**************************** STANDARD INPUT *******************************
!
!
!************************* REQUIRED INPUT FILES ****************************
!
! NAME.input_long : Contains info about the system in the standard autoPES format.
! NAME.ener : Contains set of dimer configurations and energies.
!
!***************************** OUTPUT FILES ********************************
!
! fit_asymp.dat : Input file for final fitting
!
!***************************************************************************
!


!***************************************************************************
!***************************** DEFINITIONS *********************************
!***************************************************************************

module fitAsympDef
use utility
use const
use dimer
use dgrid
use powell
use polarization
implicit none

integer, parameter :: asymp_dat = 1 ! File IO units

! Used for calculating C10, C12, C14.
! J. Chem. Phys. 89, 2092 (1988)
real(8), parameter :: xi4 = 1.225, omega4 = 1.0281, omega5 = 0.9749

!***************************************************************************
!*************************** GLOBAL VARIABLES ******************************
!***************************************************************************

logical :: verbose
integer :: praxisPrint
integer :: comRuleMode
logical :: useCparmPen, usePolPen
character(100) :: sysname
character(100) :: enerfile

integer :: ndat, nsptypeUsed, npolsite, ncterm
integer :: nchargeparm, npolparm, ncparm
real(8), allocatable :: grid(:,:)
real(8), allocatable :: weightEls(:), weightVdw(:), weightPol(:)
real(8), allocatable :: epol(:)
real(8), allocatable :: charge(:), chargeEN(:), pz(:), pz0(:)
real(8), allocatable :: C(:,:), C0(:,:), spC(:,:), spC0(:,:)
logical, allocatable :: optimPol(:)
integer, allocatable :: spmap(:,:), spmapInv(:,:)
integer, allocatable :: polSiteCount(:)
integer :: chargeBalStype(2)
real(8) :: elsDecay, vdwDecay
integer :: cstart
logical :: isNeutral(2)

real(8) :: rBuf(20)
character(100) :: cBuf(10)

real(8), allocatable :: rABinv(:,:,:)
logical :: optimizeCOM
real(8) :: CparmFitScale, polParmFitScale
real(8) :: praxisScale, avgEN, indDispRatio
real(8) :: val, val2
integer :: iVal, index
real(8) :: cparm0(14)

real(8), allocatable :: optimVectPol(:), optimVectCharge(:), optimVectCparm(:)

contains

!***************************************************************************
!****************************** ROUTINES ***********************************
!***************************************************************************

! Returns dldf coefficient index for atom
function dldfIndx(imon, iatom)
  integer, parameter :: atIndx(MAX_ANUM) = [ &
       00,                                                16, &
       00,00,                              23,24,25,26,27,28, &
       00,00,                              35,36,37,38,39,40, &
       -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
  integer, parameter :: hydIndx(MAX_ANUM) = [ &
       01,                                                -1, &
       02,03,                              04,05,06,07,08,-1, &
       09,10,                              11,12,13,14,15,-1, &
       -1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
  integer, parameter :: liIndx(5) = &
       [17,18,19,-1,-1]
  integer, parameter :: beIndx(5) = &
       [20,21,22,-1,-1]
  integer, parameter :: naIndx(5) = &
       [29,30,31,-1,-1]
  integer, parameter :: mgIndx(5) = &
       [32,33,34,-1,-1]
  
  integer :: imon, iatom
  integer :: dldfIndx
  integer :: i
  real(8) :: dist, nearestDist
  integer :: nneighbor, nearest, neighborType, anum
  
  nneighbor = 0
  nearest = 0
  nearestDist = 1.0d100
  do i = 1,nsite(imon)
     anum = sanum(imon,i)
     if (i /= iatom .and. anum > 0) then
        dist = sqrt((sit0(imon,iatom,1) - sit0(imon,i,1))**2 + &
             (sit0(imon,iatom,2) - sit0(imon,i,2))**2 + &
             (sit0(imon,iatom,3) - sit0(imon,i,3))**2)
        if (dist < 3.9d0) nneighbor = nneighbor + 1
        if (dist < nearestDist) then
           nearestDist = dist
           nearest = anum
        endif
     endif
  enddo
  
  if (nneighbor == 0) then
     neighborType = 1
  elseif (nneighbor == 1 .and. nearest == 1) then
     neighborType = 2
  else
     neighborType = 3
  endif
  
  anum = sanum(imon,iatom)
  if (anum == 1) then
     dldfIndx = hydIndx(nearest)
  elseif (anum == 3) then
     dldfIndx = liIndx(neighborType)
  elseif (anum == 4) then
     dldfIndx = beIndx(nneighbor + 1)
  elseif (anum == 11) then
     dldfIndx = naIndx(neighborType)
  elseif (anum == 12) then
     dldfIndx = mgIndx(nneighbor + 1)
  elseif (anum > 0) then
     dldfIndx = atIndx(anum)
  else
     dldfIndx = -1
  endif
end function


! Return mean dldf version 2 Cn coefficient for atoms of given type
function dldfCn(st,n)
  real(8), parameter :: nnc6(40) = 1.0884d4*[ &
       0.2044,6.1438,0.4117,0.3242,0.0794,0.1792,0.1863,0.0939,5.3056,0.7772, &
       0.6309,0.5608,0.1477,0.1272,0.1965,0.0692,39.7169,0.0727,0.5970,12.2461, &
       5.1374,1.2402,0.4142,1.5763,1.0182,0.6889,0.5181,0.2459,45.7198,0.0000, &
       4.5841,27.5251,25.8102,2.7405,1.7066,1.1809,3.2334,7.9959,5.1041,3.0519]
  
  real(8), parameter :: nnc8(40) = 3.8869d6*[ &
       0.0097,0.0000,0.1598,0.0729,0.0132,0.0040,0.0017,0.0000,0.0013,0.6239, &
       0.1697,0.1011,0.5304,0.0674,0.0009,0.0045,37.6958,0.0000,0.0000,4.1540, &
       3.8813,0.0408,0.0020,0.1613,0.1481,0.0445,0.1202,0.0312,64.1860,21.2101, &
       0.0000,18.3248,34.2080,0.0458,0.1561,0.0000,0.8047,0.3412,1.0469,0.6312]
  
  integer :: st, n
  real(8) :: dldfCn
  integer :: indx, nsit
  integer :: i, j
  
  dldfCn = 0.0d0
  nsit = 0
  do i = 1,2
     do j = 1,nsite(i)
        if (stype(i,j) == st) then
           indx = dldfIndx(i, j)
           if (indx > 0) then
              if (n == 6) then
                 dldfCn = dldfCn + nnc6(indx)
              elseif (n == 8) then
                 dldfCn = dldfCn + nnc8(indx)
              endif
           endif
           nsit = nsit + 1
        endif
     enddo
  enddo
  dldfCn = dldfCn/nsit
  
end function


! Input grid and initialize related variables
function readPoints(fileName)
  logical :: readPoints
  character(*) :: fileName
  
  integer :: i, j, k
  
  call initGrid(fileName, grid, ndat, n_ener)
  
  if (ndat == 0) then
     readPoints = .false.
     return
  endif
  
  do i = 1,ndat
     if (grid(i,C_ener+E_ind) == 0.0d0 .or. grid(i,C_ener+E_dsp) == 0.0d0) then
        readPoints = .false.
        return
     endif
  enddo
  
  allocate( rABinv(ndat,nsite(1),nsite(2)) )
  do i = 1,ndat
     call moveDimerGP(grid(i,:))
     do j = 1,nsite(1)
        do k = 1,nsite(2)
           rABinv(i,j,k) = 1.0d0/cartnorm(sit(1,j,:) - sit(2,k,:))
        enddo
     enddo
  enddo
  
  allocate( epol(ndat) )
  epol(:) = 0.0d0
  
  readPoints = .true.
end function


! Calculate electrostatic energy for given configuration
function elsEnergy(idat)
  integer :: idat
  real(8) :: elsEnergy
  
  integer :: i, j
  
  elsEnergy = 0.0d0
  do i = 1,nsite(1)
     do j = 1,nsite(2)
        if (incInter(stype(1,i),F_els) .and. incInter(stype(2,j),F_els)) then
           elsEnergy = elsEnergy + charge(stype(1,i))*charge(stype(2,j))*rABinv(idat,i,j)*h2kcal
        endif
     enddo
  enddo
end function


! Calculate VDW energy for given configuration
function CEnergy(idat)
  integer :: idat
  real(8) :: CEnergy
  
  integer :: i, j, k
  real(8) :: rInv2, rInvN, Cval
  
  CEnergy = 0.0d0
  do i = 1,nsite(1)
     do j = 1,nsite(2)
        if (incInter(stype(1,i),F_vdw) .and. incInter(stype(2,j),F_vdw)) then
           
           rInv2 = rABinv(idat,i,j)**2
           rInvN = rInv2**((cStart - 2)/2)
           do k = 1,ncterm
              rInvN = rInvN*rInv2
              Cval = spC(spmap(stype(1,i),stype(2,j)),k)
              CEnergy = CEnergy - Cval*rInvN
           enddo
           
        endif
     enddo
  enddo
  
end function


! Calculate polarization energy for given configuration
real(8) function polEnerPoint(idat)
  integer :: idat
  
  call moveDimerGP(grid(idat,:))
  polEnerPoint = polEnerDim(5, charge, pz)
end function


! Scale polarizabilities of each monomer so they sum to COM polarizability
subroutine normalizePz
  integer :: i, j
  real(8) :: sum
  
  do i = 1,2
     sum = 0.0d0
     do j = 1,nstype
        sum = sum + stcount(i,j)*pz(j)
     enddo
     
     if (sum > 0.0d0) then
        sum = molPZ(i)/sum
        do j = 1,nstype
           if (stcount(i,j) > 0 .and. incInter(j,F_pol)) then
              pz(j) = pz(j)*sum
           endif
        enddo
     endif
  enddo
    
end subroutine


! Prepare given parameter vector with program values for charge optimization
subroutine loadParmVectCharge(parmVect)
  real(8) :: parmVect(nchargeparm)
  
  integer :: i
  integer :: index
  
  index = 0
  do i = 1,nstype
     if (incInter(i,F_els) .and. chargeBalStype(1) /= i .and. chargeBalStype(2) /= i) then
        index = index + 1
        parmVect(index) = charge(i)
     endif
  enddo
end subroutine


! Set program values to ones from parameter vector for charges
! chargeBalStype for each monomer set so that charge sums to zero
subroutine unloadParmVectCharge(parmVect)
  real(8) :: parmVect(nchargeparm)
  
  integer :: i, j
  integer :: index
  real(8) :: chargeSum(2)
  logical :: isBalanceStype
  
  index = 0
  chargeSum(:) = 0.0d0
  do i = 1,nstype
     if (incInter(i,F_els)) then
        
        isBalanceStype = .false.
        do j = 1,2
           if (chargeBalStype(j) == i) then
              charge(i) = (molChar(j) - chargeSum(j))/stcount(j,i)
              isBalanceStype = .true.
           endif
        enddo
        
        if (.not. isBalanceStype) then
           index = index + 1
           charge(i) = parmVect(index)
           
           do j = 1,2
              chargeSum(j) = chargeSum(j) + stcount(j,i)*charge(i)
           enddo
        endif
        
     endif
  enddo
  
end subroutine


! Prepare given parameter vector with program values for polarizability optimization
subroutine loadParmVectPol(parmVect)
  real(8) :: parmVect(npolparm)
  
  integer :: i
  integer :: index
  
  index = 0
  do i = 1,nstype
     if (optimPol(i)) then
        index = index + 1
        parmVect(index) = encode(pz(i))
     endif
  enddo
end subroutine


! Set program values to ones from parameter vector for polarizability
subroutine unloadParmVectPol(parmVect)
  real(8) :: parmVect(npolparm)
  
  integer :: i, j
  integer :: index
  
  index = 0
  do i = 1,nstype
     if (optimPol(i)) then
        index = index + 1
        pz(i) = decode(parmVect(index))
     endif
  enddo
  
  if (optimizeCOM) then
     do i = 1,2
        molPZ(i) = 0.0d0
        do j = 1,nstype
           molPZ(i) = molPZ(i) + pz(j)*stcount(i,j)
        enddo
     enddo
  endif
  call normalizePz
end subroutine


! Prepare given parameter vector with program values for C-parm optimization
subroutine loadParmVectCparm(parmVect)
  real(8) :: parmVect(ncparm)
  
  integer :: i, j
  integer :: index
  
  index = 0
  if (comRuleMode == 0) then
     do i = 1,nsptypeUsed
        if (incInter(spmapInv(i,1),F_vdw) .and. incInter(spmapInv(i,2),F_vdw)) then
           do j = 1,ncterm
              index = index + 1
              parmVect(index) = encode(spC(i,j))
           enddo
        endif
     enddo
  else
     do i = 1,nstype
        if (incInter(i,F_vdw)) then
           do j = 1,ncterm
              index = index + 1
              parmVect(index) = encode(C(i,j))
           enddo
        endif
     enddo
  endif
end subroutine


! Set program values to ones from parameter vector for C-parms
subroutine unloadParmVectCparm(parmVect)
  real(8) :: parmVect(ncparm)
  
  integer :: i, j
  integer :: index
  real(8) :: factor
  
  index = 0
  if (comRuleMode == 0) then
     do i = 1,nsptypeUsed
        if (incInter(spmapInv(i,1),F_vdw) .and. incInter(spmapInv(i,2),F_vdw)) then
           do j = 1,ncterm
              index = index + 1
              spC(i,j) = decode(parmVect(index))
           enddo
        endif
     enddo
  else
     do i = 1,nstype
        if (incInter(i,F_vdw)) then
           do j = 1,ncterm
              index = index + 1
              C(i,j) = decode(parmVect(index))
           enddo
        endif
     enddo
     do i = 1,nsptypeUsed
        if (incInter(spmapInv(i,1),F_vdw) .and. incInter(spmapInv(i,2),F_vdw)) then
           do j = 1,ncterm
              spC(i,j) = sqrt(C(spmapInv(i,1),j) * C(spmapInv(i,2),j))
           enddo
        endif
     enddo
  endif
end subroutine


! Function called by the optimizer for charge optimization
function errFuncCharge(x, n)
  real(8) :: errFuncCharge
  real(8) :: x(n)
  integer :: n
  
  integer :: i
  integer :: index
  
  call unloadParmVectCharge(x)
  
  errFuncCharge = 0.0
  do i = 1,ndat
     errFuncCharge = errFuncCharge + weightEls(i)*(grid(i,C_ener+E_els) - elsEnergy(i))**2
  enddo
end function


! Function called by the optimizer for polarizability optimization
function errFuncPol(x, n)
  real(8), parameter :: penConst = 1.0d0
  
  real(8) :: errFuncPol
  real(8) :: x(n)
  integer :: n
  
  integer :: i
  integer :: index, npen
  real(8) :: penalty, pol1, pol0
  
  call unloadParmVectPol(x)
  
  errFuncPol = 0.0
  do i = 1,ndat
     errFuncPol = errFuncPol + weightPol(i)*(grid(i,C_ener+E_ind) - polEnerPoint(i))**2
  enddo
  
  ! Penalty function
  !if (.not. optimizeCOM) then
  !   do i = 1,nstype
  !      if (incInter(i,F_pol)) then
  !         errFuncPol = errFuncPol - polParmFitScale*log(pz(i))
  !      endif
  !   enddo
  !endif
  
  ! Penalty function
  if (usePolPen) then
     penalty = 0.0d0
     npen = 0
     do i = 1,nstype
        if (incInter(i,F_pol) .and. stanum(i) > 0) then
           pol1 = pz(i)
           pol0 = pz0(i)
           if (pol0 > 1.0d-10) then
              penalty = penalty + (max(pol1,pol0)/min(pol1,pol0))**penConst
              npen = npen + 1
           endif
        endif
     enddo
     if (npen > 0) then
        penalty = penalty/npen
     else
        penalty = 1.0d0
     endif
     
     errFuncPol = errFuncPol*penalty
  endif
  
end function


! Function called by the optimizer for C-parm optimization
function errFuncCparm(x, n)
  real(8), parameter :: penConst = 0.04d0
  
  real(8) :: errFuncCparm
  real(8) :: x(n)
  integer :: n
  
  integer :: i, j
  integer :: index, npen
  !real(8) :: sizeMult
  real(8) :: penalty, C, C0
  
  call unloadParmVectCparm(x)
  
  errFuncCparm = 0.0
  do i = 1,ndat
     errFuncCparm = errFuncCparm + weightVdw(i)* &
          ((grid(i,C_ener+E_ind) + grid(i,C_ener+E_dsp) - epol(i)) - CEnergy(i))**2
  enddo
  errFuncCparm = sqrt(errFuncCparm)
  
  ! Penalty function
  if (useCparmPen) then
     penalty = 0.0d0
     npen = 0
     do i = 1,ncterm
        do j = 1,nsptypeUsed
           if (incInter(spmapInv(j,1),F_vdw) .and. incInter(spmapInv(j,2),F_vdw)) then
              C = spC(j,i)
              C0 = spC0(j,i)
              if (C0 > 1.0d-10) then
                 penalty = penalty + (max(C,C0)/min(C,C0))**penConst
                 npen = npen + 1
              endif
           endif
        enddo
     enddo
     if (npen > 0) then
        penalty = penalty/npen
     else
        penalty = 1.0d0
     endif
     
     errFuncCparm = errFuncCparm*penalty
  endif
  
end function


! Gives sum of site-site distances to power -pow
function sumInvDist(ipt, pow)
  real(8) :: sumInvDist
  integer :: ipt
  real(8) :: pow
  
  integer :: i, j
  real(8) :: sum
  
  sum = 0.0d0
  do i = 1,nsite(1)
     do j = 1,nsite(2)
        sum = sum + rABinv(ipt,i,j)**pow
     enddo
  enddo
  sumInvDist = sum
end function


! RMS error for all points given current els energies
function rmsErrEls()
  real(8) :: rmsErrEls
  
  integer :: i
  real(8) :: sum
  
  sum = 0.0d0
  do i = 1,ndat
     sum = sum + (grid(i,C_ener+E_els) - elsEnergy(i))**2
  enddo
  rmsErrEls = sqrt(sum/ndat)
end function


! RMS error for all points given current vdw energies
function rmsErrVDW()
  real(8) :: rmsErrVDW
  
  integer :: i
  real(8) :: sum
  
  sum = 0.0d0
  do i = 1,ndat
     sum = sum + (grid(i,C_ener+E_ind) + grid(i,C_ener+E_dsp) - (CEnergy(i) + polEnerPoint(i)))**2
  enddo
  rmsErrVDW = sqrt(sum/ndat)
end function


! Percent error for all points given current els energies
function pctErrEls()
  real(8) :: pctErrEls
  
  integer :: i
  real(8) :: sum
  
  sum = 0.0d0
  do i = 1,ndat   
     sum = sum + 2.0d0*abs(grid(i,C_ener+E_els) - elsEnergy(i))/ &
          (abs(grid(i,C_ener+E_els)) + abs(elsEnergy(i)))
  end do
  pctErrEls = 100.0d0*sum/ndat
end function


! Percent error for all points given current els energies
function pctErrVDW()
  real(8) :: pctErrVDW
  
  integer :: i
  real(8) :: sum, ener
  
  sum = 0.0d0
  do i = 1,ndat
     ener = grid(i,C_ener+E_ind) + grid(i,C_ener+E_dsp)
     sum = sum + abs((ener - (polEnerPoint(i) + CEnergy(i)))/ener)
  end do
  pctErrVDW = 100.0d0*sum/ndat
end function


subroutine writeVDWErr(polOnly)
  logical, intent(in) :: polOnly
  integer :: i, j
  real(8) :: sumPct, sumSq, ener, fitEner, minR, maxR
  real(8) :: sumPctTot, sumSqTot
  integer :: nTot, nRange
  
  nTot = 0
  sumPctTot = 0.0d0
  sumSqTot = 0.0d0
  do i = 1,1000
     minR = i/bohr2A
     maxR = (i + 1)/bohr2A
     nRange = 0
     sumPct = 0.0d0
     sumSq = 0.0d0
     do j = 1,ndat
        if (grid(j,C_rcom) > minR .and. grid(j,C_rcom) <= maxR) then
           ener = grid(j,C_ener+E_ind)
           fitEner = polEnerPoint(j)
           if (.not. polOnly) then
              ener = ener + grid(j,C_ener+E_dsp)
              fitEner = fitEner + CEnergy(j)
           endif
           sumPct = sumPct + abs((ener - fitEner)/ener)
           sumSq = sumSq + (ener - fitEner)**2
           nRange = nRange + 1
        endif
     enddo
     nTot = nTot + nRange
     sumPctTot = sumPctTot + sumPct
     sumSqTot = sumSqTot + sumSq
     if (nRange > 0) then
        write(*,'(F6.1,A,F6.1,A,I5,A,E12.5,A,F12.6,A)') &
             minR*bohr2A,' - ',maxR*bohr2A,': ',nRange,' pts   ', &
             sqrt(sumSq/nRange)*h2kcal,' kcal/mol   ',100.0d0*sumPct/nRange,'%'
     endif
  enddo
  write(*,'(I5,A,E12.5,A,F12.6,A)') &
       nTot,' pts   ',sqrt(sumSqTot/nTot)*h2kcal,' kcal/mol   ',100.0d0*sumPctTot/nTot,'%'
end subroutine


!*************************** PURE  FUNCTIONS *******************************


! Encode parameter for fitting procedure
pure function encode(x)
  real(8), intent(in) :: x
  real(8) :: encode
  real(8) :: xPos
  
  xPos = max(x, 1.0d-5)
  
  if (xPos > 1.0d0) then
     encode = xPos - 1.0d0
  else
     encode = 1.0d0 - 1.0d0/xPos
  endif
end function


! Decode fitting result to parameter value
pure function decode(x)
  real(8), intent(in) :: x
  real(8) :: decode
  
  if (x < 0.0d0) then
     decode = 1.0d0/(-1.0d0*x + 1.0d0)
  else
     decode = x + 1.0d0
  endif
end function


end module


!***************************************************************************
!**************************** MAIN PROGRAM *********************************
!***************************************************************************

program fitAsymp
use fitAsympDef
use const
use utility
use omp_lib
implicit none

integer :: i,j,k,l

!***************************************************************************
!*************************** INITIALIZATION ********************************
!***************************************************************************

optimizeCOM = .false.

! Read command line parameters
do i = 1,4
   call getarg(i, cbuf(i))
enddo

read(cbuf(1),*) sysname
read(cbuf(2),*) comRuleMode
select case (trim(cbuf(3)))
case ('6')
   ncterm = 1
case ('8')
   ncterm = 2
case ('10')
   ncterm = 3
case ('12')
   ncterm = 4
case ('14')
   ncterm = 5
end select
read(cbuf(4),*) useCparmPen

usePolPen = .true.
verbose = .true.
enerfile = trim(sysname)//'.ener'

if (verbose) then
   praxisPrint = 1
else
   praxisPrint = 0
endif

! Initialize sites
call read_dimer(trim(sysname)//'.input_long')
allocate( charge(nstype), pz(nstype), pz0(nstype), optimPol(nstype) )

! Initialize grid
if (.not. readPoints(enerfile)) then
   write(*,*) 'Error reading '//trim(enerfile)
   stop
endif

! Determine energy decay powers
if (abs(molchar(1)) < 1.0d-10 .and. abs(molchar(2)) < 1.0d-10) then
   elsDecay = 3.0d0
   if (abs(DM(1)) < 1.0d-2) elsDecay = elsDecay + 1.0d0
   if (abs(DM(2)) < 1.0d-2) elsDecay = elsDecay + 1.0d0
   vdwDecay = 6.0d0
   cStart = 6
elseif (abs(molchar(1)) > 1.0d-10 .and. abs(molchar(2)) > 1.0d-10) then
   elsDecay = 1.0d0
   vdwDecay = 4.0d0
   cStart = 4
   ncterm = ncterm + 1
else
   elsDecay = 2.0d0
   if (abs(molchar(1)) > 1.0d-10 .and. abs(DM(2)) < 1.0d-2)  elsDecay = elsDecay + 1.0d0
   if (abs(molchar(2)) > 1.0d-10 .and. abs(DM(1)) < 1.0d-2)  elsDecay = elsDecay + 1.0d0
   vdwDecay = 4.0d0
   cStart = 4
   ncterm = ncterm + 1
endif

write(*,*) 'Els decay=',elsDecay
write(*,*) 'ind+disp decay=',vdwDecay
write(*,*) 'Cn start=',cStart

! Calculate additional information about the sites and set initial pz
allocate( polSiteCount(2) )
npolsite = 0
pz(:) = 0.0d0
do i = 1,2
   polSiteCount(i) = 0
   do j = 1,nstype
      
      do k = 1,nsite(i)
         if (stype(i,k) == j) then
            if (incInter(stype(i,k),F_pol)) then
               polSiteCount(i) = polSiteCount(i) + 1
               npolsite = npolsite + 1
               
               if (sanum(i,k) > 0) then
                  pz(j) = atomPol(sanum(i,k))
               else
                  pz(j) = atomPol(1)
               endif
            endif
         endif
      enddo
      
   enddo
enddo

call normalizePz
pz0(:) = pz(:)

! Set special site types for balancing charge of each monomer
chargeBalStype(:) = -1
do i = 1,2
   isNeutral(i) = abs(molchar(i)) < 1.0d-10
   do j = 1,nstype
      if (incInter(j,F_els) .and. stcount(i,j) > 0) then
         if (chargeBalStype(i) > 0) isNeutral(i) = .false.
         chargeBalStype(i) = j
      endif
   enddo
enddo

if (isNeutral(1) .or. isNeutral(2)) then
   do i = 1,nstype
      incInter(i,F_els) = .false.
   enddo
endif

! Set EN charges
allocate( chargeEN(nstype) )
chargeEN(:) = 0.0d0
do i = 1,2
   avgEN = 0.0d0
   do j = 1,nsite(i)
      if (sanum(i,j) > 0) then
         avgEN = avgEN + electroNeg(sanum(i,j))
      endif
   enddo
   avgEN = avgEN/nsite(i)
   
   do j = 1,nstype
      if (stanum(j) > 0 .and. incInter(j,F_els)) then
         if (stcount(i,j) > 0) then
            chargeEN(j) = 1.0d0*(avgEN - electroNeg(stanum(j))) + molchar(i)/nsite(i)
         endif
      else
         chargeEN(j) = 0.0d0
      endif
   enddo
enddo

! Set starting charges
charge(:) = 0.0d0
do i = 1,2
   if (i == 1) then
      open(101,file='loewdinA.dat',status='old',err=1001)
   else
      open(101,file='loewdinB.dat',status='old',err=1001)
   endif
   do j = 1,nsite(i)
      if (sanum(i,j) > 0) then
         read(101,*) val
         charge(stype(i,j)) = charge(stype(i,j)) + val
      endif
   enddo
   close(101)
enddo
do i = 1,nstype
   charge(i) = charge(i)/(stcount(1,i) + stcount(2,i))
enddo
1001 continue

! Initialize site pair types
allocate( spmap(nstype,nstype), spmapInv(nstype**2,2) )
index = 1
do i = 1,nstype
   do j = 1,nstype
      if (stcount(1,i) > 0 .and. stcount(2,j) > 0 .and. &
           (j >= i .or. stcount(1,j) == 0)) then
         do k = 1,nsite(1)
            do l = 1,nsite(2)
               if ((stype(1,k) == i .and. stype(2,l) == j) .or. &
                    (stype(1,k) == j .and. stype(2,l) == i)) then
                  spmap(stype(1,k),stype(2,l)) = index
                  spmap(stype(2,l),stype(1,k)) = index
                  spmapInv(index,1) = stype(1,k)
                  spmapInv(index,2) = stype(2,l)
               endif
            enddo
         enddo
         index = index + 1
      endif
   enddo
enddo
nsptypeUsed = index - 1

! Initialize parameter optimization info
npolparm = 0
optimPol(:) = .false.
do i = 1,nstype
   if (incInter(i,F_pol)) then
      optimPol(i) = .true.
      do j = 1,2
         if (stcount(j,i) == polSiteCount(j) .and. .not. optimizeCOM) optimPol(i) = .false.
      enddo
   endif
   if (optimPol(i)) npolparm = npolparm + 1
enddo
allocate( optimVectPol(npolparm) )

nchargeparm = 0
do i = 1,nstype
   if (incInter(i,F_els) .and. chargeBalStype(1) /= i .and. chargeBalStype(2) /= i) nchargeparm = nchargeparm + 1 
enddo
allocate( optimVectCharge(nchargeparm) )

ncparm = 0
do i = 1,nstype
   if (incInter(i,F_vdw)) ncparm = ncparm + ncterm
enddo
allocate( optimVectCparm(ncparm) )

indDispRatio = 0.0d0
do i = 1,ndat
   if (abs(grid(i,C_ener+E_dsp)) > 0.0d0) then
      indDispRatio = indDispRatio + grid(i,C_ener+E_ind)/grid(i,C_ener+E_dsp)
   endif
enddo
indDispRatio = indDispRatio/ndat

! Set starting C-parameters
allocate( C(nstype,ncparm), C0(nstype,ncparm) )
do i = 1,nstype
   if (.not. incInter(i,F_vdw)) then
      C0(i,:) = 0.0d0
      C(i,:) = 0.0d0
   else
      cparm0(:) = 0.0d0
      cparm0(4) = 0.1d0
      cparm0(6) = dldfCn(i, 6)*(1.0d0 + indDispRatio) !C6_0(stype(i)%atomNum)
      cparm0(8) = dldfCn(i, 8)*(1.0d0 + indDispRatio)
      cparm0(10) = xi4*cparm0(8)**2/cparm0(6)
      cparm0(12) = omega4*cparm0(6)*(cparm0(10)/cparm0(8))**3
      cparm0(14) = omega5*cparm0(8)*(cparm0(12)/cparm0(10))**3
      
      ival = cstart
      do j = 1,ncterm
         C0(i,j) = cparm0(ival)
         if (C0(i,j) > 1.0d-10) then
            C(i,j) = C0(i,j)
         else
            C(i,j) = 1.0d0
         endif
         ival = ival + 2
      enddo
   endif   
end do

allocate( spC(nsptypeUsed,ncparm), spC0(nsptypeUsed,ncparm) )
do i = 1,nsptypeUsed
   do j = 1,ncparm
      spC0(i,j) = sqrt(C0(spmapInv(i,1),j) * C0(spmapInv(i,2),j))
      spC(i,j) = spC0(i,j)
   enddo
enddo

! Initialize fitting weights
allocate( weightEls(ndat), weightVdw(ndat), weightPol(ndat) )
do i = 1,ndat
   weightEls(i) = sumInvDist(i, elsDecay)**(-2.0d0)
   weightPol(i) = abs(grid(i,C_ener+E_ind))**(-2.0d0)! * sumInvDist(i, 1.0d0)
   weightVdw(i) = abs(grid(i,C_ener+E_ind) + grid(i,C_ener+E_dsp))**(-2.0d0)
enddo

! Initialize penalty functions
polParmFitScale = 0.0d0
do i = 1,ndat
   polParmFitScale = polParmFitScale + weightPol(i)*grid(i,C_ener+E_ind)**2
enddo
polParmFitScale = 1.0d-4*polParmFitScale/npolparm


!***************************************************************************
!******************************* OPTIMIZE **********************************
!***************************************************************************

val = rmsErrEls()
val = pctErrEls()
val = rmsErrVDW()
val = pctErrVDW()

write(*,*) ''
write(*,*) ' Initial els error:'
write(*,'(E12.5,A,F12.6,A)') rmsErrEls(),' kcal/mol   ',pctErrEls(),'%'

if (nchargeparm > 0) then
   write(*,*)
   write(*,*) 'BEGIN OPTIMIZING CHARGES ', nchargeparm
   praxisScale = sqrt(real(nchargeparm))
   call loadParmVectCharge(optimVectCharge)
   call praxis(100, 1.0d-4, 2.0d0*praxisScale, nchargeparm, praxisPrint, optimVectCharge, errFuncCharge, 0, .false.)
   call unloadParmVectCharge(optimVectCharge)
   
   write(*,*) ''
   write(*,*) 'Charges (AU):'
   do i = 1,nstype
      write(*,'(I4,F15.10)') i, charge(i)
   enddo
endif

write(*,*) ''
write(*,*) ' Final els error:'
write(*,'(E12.5,A,F12.6,A)') rmsErrEls(),' kcal/mol   ',pctErrEls(),'%'

! Determine if charge sign convention needs to be switched to match electronegativity
val = 0.0d0
val2 = 0.0d0
do i = 1,nstype
   val = val + (chargeEN(i) - charge(i))**2
   val2 = val2 + (chargeEN(i) + charge(i))**2
enddo
if (val2 < val) then
   do i = 1,nstype
      charge(i) = -1.0d0*charge(i)
   enddo
endif

write(*,*) ''
write(*,*) ' Initial ind+disp error:'
call writeVDWErr(.false.)

if (npolparm > 0) then
   write(*,*)
   do i = 1,nstype
      write(*,'(A,I4,A,F12.5)') 'Initial PZ',i,'=',pz(i)
   enddo
   
   write(*,*)
   write(*,*) 'BEGIN OPTIMIZING POLARIZABILITIES ', npolparm
   praxisScale = sqrt(real(npolparm))
   call loadParmVectPol(optimVectPol)
   call praxis(50, 1.0d-3, 1.0d0-3*praxisScale, npolparm, praxisPrint, optimVectPol, errFuncPol, 0, .false.)
   call unloadParmVectPol(optimVectPol)
   
   do i = 1,ndat
      epol(i) = polEnerPoint(i)
   enddo
   
   write(*,*) ''
   write(*,*) 'Polarizabilities (AU):'
   do i = 1,nstype
      write(*,'(I4,F15.10)') i, pz(i)
   enddo
   if (optimizeCOM) then
      write(*,*) ''
      write(*,*) 'Net polarizability (AU):'
      do i = 1,2
         write(*,'(I4,F15.10)') i, molPZ(i)
      enddo
   endif
endif

if (molPZ(1) > 1.0d-5 .or. molPZ(2) > 1.0d-5) then
   write(*,*) ''
   write(*,*) ' Final polarization error:'
   call writeVDWErr(.true.)
   write(*,*) ' ind+disp error:'
   call writeVDWErr(.false.)
endif

if (ncparm > 0) then
   write(*,*)
   write(*,*) 'BEGIN CONSTRAINED OPTIMIZATION VDW PARAMETERS', ncparm
   
   praxisScale = 1.0d-1*sqrt(real(ncparm))
   call loadParmVectCparm(optimVectCparm)
   call praxis(600, 1.0d-4, praxisScale, ncparm, praxisPrint, optimVectCparm, errFuncCparm, 0, .false.)
   call unloadParmVectCparm(optimVectCparm)
   
   if (comRuleMode == 0) then
      comRuleMode = 0
      ncparm = 0
      do i = 1,nsptypeUsed
         if (incInter(spmapInv(i,1),F_vdw) .and. incInter(spmapInv(i,2),F_vdw)) then
            ncparm = ncparm + ncterm
         endif
      enddo
      deallocate( optimVectCparm )
      allocate( optimVectCparm(ncparm) )
      
      write(*,*)
      write(*,*) 'BEGIN UNCONSTRAINED OPTIMIZATION VDW PARAMETERS', ncparm
      praxisScale = 1.0d-2*sqrt(real(ncparm))
      call loadParmVectCparm(optimVectCparm)
      call praxis(300, 1.0d-4, praxisScale, ncparm, praxisPrint, optimVectCparm, errFuncCparm, 0, .false.)
      call unloadParmVectCparm(optimVectCparm)
   endif
endif

write(*,*) ''
write(*,*) ' Final ind+disp error:'
call writeVDWErr(.false.)


!***************************************************************************
!******************************** OUTPUT ***********************************
!***************************************************************************

write(*,*) ''
write(*,'(A,F20.6)') 'Mean ratio of E_ind/E_dsp = ',indDispRatio

open(asymp_dat, file = 'fit_asymp.dat', status = 'new')

do i = 1,nstype
   write(asymp_dat,'(I4,A,E20.12,A,E20.12)') &
        stanum(i),' ',charge(i),' ',pz(i)
enddo

write(asymp_dat,'(I5,A,I5,A,I5)') ncterm,' ',comRuleMode,' ',cStart

if (comRuleMode == 0) then
   do i = 1,nsptypeUsed
      iVal = cStart
      do j = 1,ncterm
         write(asymp_dat,'(E15.8,A)',advance='no') &
              spC(i,j)*bohr2A**iVal,' '
         iVal = iVal + 2
      enddo
      write(asymp_dat,'()')
   enddo
else
   do i = 1,nstype
      iVal = cStart
      do j = 1,ncterm
         write(asymp_dat,'(E15.8,A)',advance='no') &
              C(i,j)*bohr2A**iVal,' '
         iVal = iVal + 2
      enddo
      write(asymp_dat,'()')
   enddo
endif

close(asymp_dat)

end program

