C
C General PES walk procedure for clusters of rigid-bodies.
c
c                See description in PESwalk.doc
c
c
! KPISZ
      use dimer
      use force_field
! END KPISZ
      IMPLICIT REAL*8 (A-H,O-Z)
      include 'params.inc'
      LOGICAL RSTART, c_freq,c_alc,r_trudge,r_saddle,e_three
      logical c_vshape
      dimension WORK(8*maxb+3*maxb**2)
      dimension oa(3), ob(3),forc(3),tora(3),torb(3)
      dimension indopt0(6,maxmol)      ! to save indopt before freq
      COMMON/EOPT/TOLF,TOLR,ANOISE,TIMLIM,LINP,LOUT,ALFST,IPR,NPAR,
     &            SAFETL
      common/first/ itime
      common/inou/ icon(80)
      common/over_rot/ rot_ang_overall,rot_incr
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
      common/walk_pars/ scale,neig,nprt,dirs,dirm,dmax,hh,eps,
     1                  r_trudge,r_saddle,e_three,c_vshape,vs_eps
      common/freqs/ site(3,natommax,ncmax),am(natommax,ncmax),
     1     nsit(ncmax),ishp(ncmax),c_freq,c_alc,ph_shft,
     1     iitypa(natommax,ncmax)
      common/dat_g/ comm(3,maxmol),aol(3,maxmol)
      EXTERNAL flsqgen
!KPISZ
      integer :: ff_type
       character(len=200) :: in_name, input_file, ch_ff_type, parms_file
       
       call getarg(1,in_name)
       call getarg(2,parms_file)
       call getarg(3,ch_ff_type)
       input_file = trim(in_name)//'.input_long'
       read(ch_ff_type,*) ff_type
       call read_dimer(input_file)
       call ff_init(parms_file,ff_type)
!END KPISZ
c
c Banner outprint
c
      write(6,*)
      write(6,*)'      *****************************************'
      write(6,*)'        Potential Energy Surface walk program'
      write(6,*)'          by Robert Bukowski (March, 2001)'
      write(6,*)'            last updated: April 20, 2001  '
      write(6,*)
      write(6,*)'            proceed at your own risk'
      write(6,*)'      *****************************************'
      write(6,*)
c
 10   call data_min
      ipr0 = ipr
c
      RSTART=.FALSE.
      rot_ang_overall = 0.d0
      n_alc = 3*natommax*maxmol+1    ! entry point for work space in alcplot..
 13   itime=0
c
c Some stuff to be considered later
c
c      call simann(flsqgen,work)
      if(r_trudge) then
       write(6,*)'--------------------------------------'
       write(6,*)'      TRUDGE minimization started'
       write(6,*)'--------------------------------------'
       CALL TRUDGE(RSTART,flsqgen,WORK,maxb)
       write(6,*)'--------------------------------------'
       write(6,*)'      TRUDGE minimization completed'
       write(6,*)'--------------------------------------'
       ipr = 0
       call gradhess(flsqgen,work)
       ipr = ipr0
       ires = 0                ! check beta angles and decide on a rotation
       call angles_check(ires)
       if(ires.ne.0) go to 13
       if(c_alc) call alcplot(work,work(n_alc)) 
      endif
c
c Find a saddle point or minimum using Wales's eigenvector following
      if(r_saddle) then
       ipr = 1
       neig0 = neig
       call saddle(flsqgen,work)
       ires = 0                ! check beta angles and decide on a rotation
       call angles_check(ires)
       if(ires.ne.0) go to 13
       ires = 1
c       call angles_check(ires)   ! perform the back-rotation
c
c Dump the ALCHEMY plot
c
       if(c_alc) call alcplot(work,work(n_alc))
       ipr = ipr0
       neig = neig0
      endif      ! if(r_saddle)
c
c Dump the ALCHEMY plot if this is the only operation requested
c
      if(.not.r_trudge.and..not.r_saddle.and.c_alc)
     1call alcplot(work,work(n_alc))
c
c Frequency calculations
c
      if(c_freq) then
c Save the original indopt
       do k=1,nmol
       do j=1,6
        indopt0(j,k) = indopt(j,k)
       end do
       end do
c Construct the new indopt
       do k=1,nmol
       do j=1,3
        indopt(j,k) = 1      ! All COM Cartesians move
        indopt(3+j,k) = 0    
       end do
       do j=1,ishp(mtype(k))
        indopt(3+j,k) = 1    ! Only selected angles move
       end do
       end do
c Test write:
       write(6,*)
       write(6,*)'           *******************************'
       write(6,*)'                 Normal mode analysis'
       write(6,*)'           *******************************'
       write(6,*)
       write(6,*)'Active coordinates for frequency calculation:'
       write(6,*)'    x     y     z  alpha  beta  gamma'
       do k=1,nmol
        write(6,'(6i6)') (indopt(j,k),j=1,6)
       end do
c Load the new (extended) coordinate set into r
       call npar2r(work,npar)
c Calculate frequencies
       ier = 1
       ieh = ier + maxb
       iea = ieh + maxb*maxb
       ief = iea + maxb*maxb
       call rigfreq(flsqgen,nmol,work(ieh),work(iea),work(ier),
     1              site,am,nsit,ishp,work(ief),hh,mtype,n_types,
     1              label_type)
       call r2npar(work,npar)
c If requested, call the vibshape procedure...
c work(iea) will now be used as work space in calcgeom
       if(c_vshape)
     1 call vibshape(work(ieh),work(ier),work(iea),npar,vs_eps)
c Restore the original indopt parameters for subsequent optimization
       do k=1,nmol
       do j=1,6
        indopt(j,k) = indopt0(j,k)
       end do
       end do
c End of the frequency path
      endif
c
      ires = 1
      call angles_check(ires)   ! perform the back-rotation

      call energy(0,rmse)
      write(6,*) 'Energy after minimization:',rmse
      call energy(2,rmse)
      go to 10
c
      stop
      end
c
      subroutine flsqgen(inout,r,rmse)
c r2npar/npar2r probably redundant, but let's leave it this way for now...
      implicit real*8 (a-h,o-z)
      dimension r(1)
      COMMON/EOPT/TOLF,TOLR,ANOISE,TIMLIM,LINP,LOUT,ALFST,IPR,NPAR,
     &            SAFETL
      common/first/ itime
      common/inou/ icon(80)
      save
      icon(9)=0
c
      if(inout.eq.-1.and.itime.eq.0) then
c
c       Put nonlinear parameters in matrix R, return the number of nonl. pars.
c
        call npar2r(r,nonl)
c--- set NPAR in common/eopt/
        npar=nonl
c
c       end of inout=-1 part  
c
      endif
c
c     put new trial values into the nonlinar parameters
c     (all inouts including -1 go through this part)
c
      call r2npar(r,npar)
c
c Compute the functional (rmse is the value of the minimized function)
c inout is usually 0, and 2 o special occasions (e.g. convergence), when
c it is worth to print something more. Inout may thus be used to instruct
c evpoints to print or not to print. inout=-1 is used to tell the
c routine ENERGY to read in the potential data.
c
      call energy(inout,rmse)
c
      return
      end
c
      subroutine npar2r(r,nonl)
      implicit real*8 (a-h,o-z)
      include 'params.inc'
      dimension r(1)
      common/dat_g/ comm(3,maxmol),aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
      nonl = 0
      do k=1,nmol
       do i=1,3
        if(indopt(i,k).ne.0) then
         nonl = nonl + 1
         r(nonl) = comm(i,k)
        endif
       end do
       do i=1,3
        if(indopt(i+3,k).ne.0) then
         nonl = nonl + 1
         r(nonl) = aol(i,k)
        endif
       end do
      end do
      return
      end
c
      subroutine r2npar(r,nonl)
      implicit real*8 (a-h,o-z)
      include 'params.inc'
      dimension r(1)
      common/dat_g/ comm(3,maxmol), aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
      common/over_rot/ rot_ang_overall
      non = 0
      do k=1,nmol
       do i=1,3
        if(indopt(i,k).ne.0) then
         non = non + 1
         comm(i,k) = r(non)
        endif
       end do
c Keep the straight line between mols 1 and 2
       if(indopt(1,k).eq.0.and.indopt(2,k).eq.0.and.
     1    indopt(3,k).eq.1) then
        comm(1,k) = comm(3,k)*dtan(rot_ang_overall)
       endif
       do i=1,3
        if(indopt(i+3,k).ne.0) then
         non = non + 1
         aol(i,k) = r(non)
        endif
       end do
      end do
      return
      end
c
c Calculate the energy of the cluster using the calls to
c 2-body potentials. In the future, modify to include
c 3B interactions.
c
      subroutine energy(inout,rmse)
      implicit real*8 (a-h,o-z)
      logical first, r_trudge,r_saddle,e_three,c_vshape
      include 'params.inc'
      dimension valder(8),oa(3),ob(3),oan(3),obn(3)
      dimension rr(3),forc(3),tora(3),torb(3)
      dimension ba_ij(maxmol,maxmol),ga_ij(maxmol,maxmol),
     1          ab_ij(maxmol,maxmol),bb_ij(maxmol,maxmol),
     1          gb_ij(maxmol,maxmol),pot_ij(maxmol,maxmol),
     1          r_ij(maxmol,maxmol),oi(3),oj(3),ok(3)
      common/dat_g/ comm(3,maxmol), aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
      common/walk_pars/ scale,neig,nprt,dirs,dirm,dmax,hh,eps,
     1                  r_trudge,r_saddle,e_three,c_vshape,vs_eps
      data d /1.162047d0/, zero /0.d0/
      data oa,ob /6*0.d0/
      data first /.true./
      save
c
      pi = dacos(-1.d0)
      d2rad = pi/180.d0
      rad2d = 1.d0/d2rad
c
c If inout=-1, perform the initial operations (read potential data, e.g.)
c
      if(inout.eq.-1.and.first) then
       imode = -1
       call en2B(comm(1,i),aol(1,i),comm(1,j),aol(1,j),mtype(i),
     1          mtype(j),r,oan,obn,pot12,imode)
       if(e_three)
     1 call en3B(comm(1,i),aol(1,i),comm(1,j),aol(1,j),comm(1,k),
     1          aol(1,k),mtype(i),mtype(j),mtype(k),pot123,imode)
       first = .false.
      endif
c
c Potential parameters read in, calculate energy now...
c
      imode = 1      ! any positive value would do...
c
      rmse = 0.d0
      rmse3 = 0.d0
c
c Calculate the pairwise-additive part of the energy:
c
      do i=1,nmol-1
      do j=i+1,nmol
c Make the relative coordinates calculation optional...
c Calculate the relative coordinates of molecules i-j
c Assume that if (mtype(i).le.mtype(j)) then i=A and j=B,
c and j=A and i=B otherwise.
      if(mtype(i).le.mtype(j)) then
       call angreg(aol(1,i),oa)
       call angreg(aol(1,j),ob)
       do k=1,3
        rr(k) = comm(k,j) - comm(k,i)
        oi(k) = oa(k) ! regularize angles for en2B just in case
        oj(k) = ob(k)
       end do
      else
       call angreg(aol(1,i),ob)
       call angreg(aol(1,j),oa)
       do k=1,3
        rr(k) = comm(k,i) - comm(k,j)
        oi(k) = ob(k) ! regularize angles for en2B just in case
        oj(k) = oa(k)
       end do
      endif
      call eulerloc(oa,ob,rr,oan,obn,r)
c Save the relative angles for molecules i and j
      obn(1) = obn(1) - oan(1)
      oan(1) = 0.d0
      ba_ij(i,j) = rad2d*oan(2)
      ga_ij(i,j) = rad2d*oan(3)
      ab_ij(i,j) = rad2d*obn(1)
      bb_ij(i,j) = rad2d*obn(2) 
      gb_ij(i,j) = rad2d*obn(3)
      r_ij(i,j) = r
c User-supplied general moleculse pair interaction routine
      call en2B(comm(1,i),oi,comm(1,j),oj,mtype(i),mtype(j),
     1          r,oan,obn,pot12,imode)
      pot_ij(i,j) = pot12
      rmse = rmse + pot_ij(i,j)
c End the loop over molecules
      end do
      end do
c
c End of pairwise-additive part. Optionally, calculate the three-body part
c
      if(e_three) then
      do i=1,nmol-2
      do j=i+1,nmol-1
      do k=j+1,nmol
c User-supplied general moleculse triplet interaction routine
c Note: angles not regularized!!! Does not matter if angular fncts not used.
      call en3B(comm(1,i),aol(1,i),comm(1,j),aol(1,j),comm(1,k),
     1          aol(1,k),mtype(i),mtype(j),mtype(k),pot123,imode)
      rmse3 = rmse3 + pot123
      end do
      end do
      end do
      endif
c
c End of cluster energy calculation
c
c RB test
c      if(inout.eq.2) then
      if(inout.eq.2.or.inout.eq.-1) then
c end RB test
       write(6,*)
       write(6,*)'Intermediate/Final geometry:'
      write(6,*)'        x            y           z        alpha       b
     1eta       gamma'
       do k=1,nmol
        call angreg(aol(1,k),oa)
        write(6,'(6f12.6)') (comm(i,k),i=1,3),(oa(i)*rad2d,i=1,3)
       end do
       write(6,*)
       write(6,*)'Pair energies:'
       do i=1,nmol-1
       do j=i+1,nmol
        write(6,'(2i4,e20.12)')i,j,pot_ij(i,j)
       end do
       end do
       write(6,*)
       write(6,*)'Relative coordinates:'
      write(6,*)'MolA MolB     R        beta_A       gamma_A     alpha_B
     1     beta_B     gamma_B'
       do i=1,nmol-1
       do j=i+1,nmol
c Decide which molecule is A and which is B
        i1=i
        j1=j
        if(mtype(i).gt.mtype(j)) then
         i1=j
         j1=i
        endif 
        write(6,'(2i4,6f12.6)')i1,j1,r_ij(i,j),ba_ij(i,j),ga_ij(i,j),
     1                    ab_ij(i,j),bb_ij(i,j),gb_ij(i,j)
       end do
       end do
       write(6,*)
c RB test
       write(6,*)'2B energy:'
       write(6,*) rmse
       write(6,*)'3B energy:'
       write(6,*) rmse3 
c end RB test
      endif
      rmse = rmse + rmse3
      return
      end
c
      subroutine data_min
      implicit real*8 (a-h,o-z)
      logical first,c_freq,flag,c_alc,r_trudge,r_saddle,o_full,
     1        e_three,c_vshape
      include 'params.inc'
      common/dat_g/ comm(3,maxmol), aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
      common/walk_pars/ scale,neig,nprt,dirs,dirm,dmax,hh,eps,
     1                  r_trudge,r_saddle,e_three,c_vshape,vs_eps
      common/freqs/ site(3,natommax,ncmax),am(natommax,ncmax),
     1     nsit(ncmax),ishp(ncmax),c_freq,c_alc,ph_shft,
     1     iitypa(natommax,ncmax)
      COMMON/EOPT/TOLF,TOLR,ANOISE,TIMLIM,LINP,LOUT,ALFST,IPR,NPAR,
     &            SAFETL
      common/inou/ icon(80)
      common/over_rot/ rot_ang_overall,rot_incr
      namelist/cntr/ neig,nprt,dirs,dirm,dmax,hh,eps,c_freq,c_alc,
     1               r_trudge,r_saddle,o_full,
     1               TOLF, TOLR, ANOISE, ALFST, IPR, maxcyc,rot_incr,
     1               e_three,c_vshape,vs_eps,ph_shft
c
      data first /.true./
      save

      pi = dacos(-1.d0)
      d2rad = pi/180.d0
c
c Read in the overall PES walk parameters....
c
      if(first) then
c Zero out the mtype vector
       do i=1,maxmol
        mtype(i) = 0
       end do
c Open the input files
       open(unit=2,file="cluster.dat",form="formatted")
       open(unit=3,file="coords.dat",form="formatted")
       open(unit=4,file="control.dat",form="formatted")
c Defaults for control parameters
       neig = 0 
       nprt = 100 
       dirs = 0.1d0 
       dirm = 0.d0 
       dmax = 0.2d0  
       hh = 0.001d0 
       eps = 1.d-6
       c_freq = .false.
       c_alc = .false.
       r_trudge = .true.
       r_saddle = .false.
       o_full = .true.
       rot_incr = 45.d0
       e_three = .false.
       scale = 1.d0      ! generalizations yet to be implemented
       c_vshape = .false.
       vs_eps = 1.d0
       ph_shft = 5.d0
c Defaults for the TRUDGE routine...
      TOLF = 0.10d-6
      TOLR = 0.20d-2
      ANOISE = 0.10d-8
      TIMLIM = 880000.
      LINP = 5
      LOUT = 7
      ALFST = 0.01d+00
      SAFETL = 0.2d+03
      IPR = 1
      maxcyc = 100
c Read the updates of control parameters 
      read(4,cntr)
      icon(2) = maxcyc
c Read in the number of molecules
       read(2,*)
       read(2,*) nmol
c Read in the molecule types
       read(2,*)
       read(2,*) (mtype(i),i=1,nmol)
c Read the atoms information for frequencies and plots
c       if(c_freq.or.c_alc) then
        n_types = 0
        read(2,*)
 21     read(2,*,end=20,err=20) n_type
        read(2,*) nsit(n_type),ishp(n_type)
        k=n_type
        do j=1,nsit(k)
         read(2,*)xx,yy,zz,amass,iitypa(j,k)
         site(1,j,k) = xx
         site(2,j,k) = yy
         site(3,j,k) = zz
         am(j,k) = amass
        end do
        n_types = n_types + 1
        label_type(n_types) = n_type
        go to 21
 20     continue
c        write(6,*)'Atom geometry read in for',n_types,' types'
c Verify that all molecules have their type geometries read in
        do k=1,nmol
         kt = mtype(k)
         flag = .false.
         do i=1,n_types
          if(kt.eq.label_type(i)) flag = .true.
         end do
         if(.not.flag) then
          write(6,*)'Atoms geometry for type',kt,' missing. Exiting'
          stop
         endif
        end do
       if(o_full) then
c Construct the indopt matrix for full optimization
       do k=1,nmol
       do j=1,3
        indopt(j,k) = 1      ! All COM Cartesians move
       end do
       do j=1,ishp(mtype(k))
        indopt(3+j,k) = 1    ! Only selected angles move
       end do
       end do
c Set up the first, second, and third molecule
       do j=1,3
        indopt(j,1) = 0
       end do
        indopt(1,2) = 0
        indopt(2,2) = 0
        indopt(2,3) = 0
        if(nmol.eq.2) then
c One alpha must be turned off....
         if(indopt(4,1).eq.1) then
          indopt(4,1)=0
         else
          indopt(4,2)=0
         endif
        endif
       else
c Explicitly read what to optimize   
c       read(2,*)
       do k=1,nmol
        read(2,*) (indopt(i,k),i=1,6)
       end do
       endif
c
c       endif
c Echo the input
       write(6,*)'*****************************************'
       write(6,*)
       write(6,*)'           Input parameters:'
       write(6,*)'-----------------------------------------' 
c       write(6,*)'All steps scaled by',scale,' -- not used'
       if(r_trudge) then
        write(6,*)'TRUDGE minimization will be run as first'
       endif
       if(r_saddle) then
        write(6,*)'SADDLE walk will be performed'
       endif
c
       if(r_saddle) then
c
       write(6,'(a,i3)')'Eigenvector followed:',neig
       write(6,'(a,f6.3,a,i3)')'Start from',dirs,' along eigenvector',
     1                          neig
       if(dirm.ne.0.d0)
     1 write(6,'(a,f6.3,a,i3)')'Minimization from',dirm,' along eig.',
     1                          neig
       write(6,'(a,f6.3)')'Maximum step:',dmax
       write(6,'(a,e11.3)')'Gradient length tolerance:',eps
c
       endif
c
       write(6,'(a,f6.3)')'Numerical derivatives with step:',hh
       write(6,'(a,i3)')'Number of molecules in the cluster:',nmol
       write(6,'(a,i3)')'Number of different molecular types:',n_types
       write(6,*)'Molecule types:'
       write(6,*)' Mol Type'
       do i=1,nmol
        write(6,'(2i4)') i, mtype(i)
        if(mtype(i).eq.0) then
         write(6,*)'Molecule',i,' has an undefined type. Exiting'
         stop
        endif
       end do
       write(6,*)'Parameters to be optimized'
       if(o_full) 
     1 write(6,*)'(automatically generated for full optimization)'
       write(6,*)'    x     y     z  alpha  beta  gamma'
       do k=1,nmol
        write(6,'(6i6)') (indopt(i,k),i=1,6)
       end do
       if(c_freq) then
        write(6,*)'Harmonic frequencies will be calculated'
        write(6,*)
       endif
       if(c_alc) then
        write(6,*)'ALCHEMY geometry files for Rasmol will be dumped'
        write(6,*)'   and rotational constants will be calculated'
       endif
       write(6,'(a,f7.2,a)')'Beta-singular geometries will be rotated',
     1                  rot_incr,' degrees around y and reoptimized'
c       if(c_freq.or.c_alc) then
        write(6,*)'Molecular types specifications:'
        write(6,*)'-------------------------------'
        do it=1,n_types
         mit = label_type(it)
         write(6,*)'Type',mit
         write(6,*)'       x            y          z       at. mass   at
     1om'
         do j=1,nsit(mit)
          write(6,'(4f12.6,a6)') (site(i,j,mit),i=1,3),am(j,mit),
     1                           iitypa(j,mit)
         end do
        end do
c       endif
       write(6,*)
       write(6,*)'***************************************'
       write(6,*)
       first = .false.
       i_walk_cnt = 0
c
       endif
c Read in the initial geometry
      read(3,*,end=10)                         ! blank line
      do k=1,nmol
       read(3,*,end=10) (comm(i,k),i=1,3),(aol(i,k),i=1,3) ! Com and angles
      end do
      i_walk_cnt = i_walk_cnt + 1
      write(6,*)'            ********************************'
      write(6,*)'                   PES walk',i_walk_cnt
      write(6,*)'            ********************************'
      write(6,*)
      write(6,*)'Starting geometry:' 
      write(6,*)'        x            y           z        alpha       b
     1eta       gamma'
      do k=1,nmol
       write(6,'(6f12.6)') (comm(i,k),i=1,3),(aol(i,k),i=1,3)
      end do
      write(6,*)
c Convert angles into radians
      do i=1,3
       do j=1,nmol
        aol(i,j) = d2rad*aol(i,j)
       end do
      end do
c Shift and rotate the cluster so that molecule 1 is at the origin,
c molecule 2 is on the z axis, and molecule 3 is in the x-z plane
      if(o_full) call shft_rot
      return
 10   continue      ! jump here if nothing else to read
      write(6,*)'No more input geometries. Exiting'
      stop
      end
c
c Calculate gradient and Hessian for a given set of parameters...
c
      subroutine gradhess(flsqgen,r)
      implicit real*8 (a-h,o-z)
      logical r_trudge, r_saddle,e_three,c_vshape
      include 'params.inc'
      dimension r(1), grdpom(maxb)
      common/hess/ grad(maxb), hess(maxb,maxb),sss(maxb,maxb),
     1 e(maxb),v(maxb)
      COMMON/EOPT/TOLF,TOLR,ANOISE,TIMLIM,LINP,LOUT,ALFST,IPR,NPAR,
     &            SAFETL
      common/walk_pars/ scale, neig, nprt, dirs, dirm, dmax,hh,eps,
     1                  r_trudge, r_saddle, e_three,c_vshape,vs_eps
      external flsqgen
c
      h = hh
c      call npar2r(r,npar)   ! does not seem right in here because of angreg
c       write(6,*)'Calculations for h=',h
c
c Calculate gradient...
c
!      write (55,*) 'grad'
      do i=1,npar
       r(i) = r(i) + h
c       call r2npar(r,npar)
       inout = 0
       call flsqgen(inout,r,valp)
       r(i) = r(i) - 2*h
c       call r2npar(r,npar)
       inout = 0
       call flsqgen(inout,r,valm)
!       write (55,'(i3,2g20.10)') i,valp,valm
       grad(i) = 0.5d0*(valp - valm)/h
       r(i) = r(i) + h
       call r2npar(r,npar)
      end do
!      write (55,*) 'grad end'
c
c Print out gradient
c
c      write(6,*)'Gradient:'
c      do i=1,npar
c       write(6,*)i,grad(i) 
c      end do
c
c Calculate Hessian matrix
c Diagonal first...
c
      do i = 1,npar
       ri0 = r(i)
c       call r2npar(r,npar)
       call flsqgen(inout,r,val0)
       r(i) = ri0 + 2*h
c       call r2npar(r,npar)
       call flsqgen(inout,r,valp)
       r(i) = ri0 - 2*h
c       call r2npar(r,npar)
       call flsqgen(inout,r,valm)
       r(i) = ri0
       call r2npar(r,npar)
       hess(i,i) = 0.25d0*(valp + valm - 2.d0*val0)/(h*h)
      end do
c
c Off diagonal now...
c
      do i=1,npar
      do j=1,i-1
       ri0 = r(i)
       rj0 = r(j)
       r(i) = ri0 + h
       r(j) = rj0 + h
c       call r2npar(r,npar)
       call flsqgen(inout,r,val1)
       r(i) = ri0 + h
       r(j) = rj0 - h
c       call r2npar(r,npar)
       call flsqgen(inout,r,val2)
       r(i) = ri0 - h
       r(j) = rj0 + h
c       call r2npar(r,npar)
       call flsqgen(inout,r,val3)
       r(i) = ri0 - h
       r(j) = rj0 - h
c       call r2npar(r,npar)
       call flsqgen(inout,r,val4)
       r(i) = ri0 
       r(j) = rj0 
       call r2npar(r,npar)
       hess(i,j) = 0.25d0*(val1 + val4 - val2 - val3)/(h*h)
       hess(j,i) = hess(i,j)
      end do
      end do
c
c Print out the Hessian matrix...
c
c      write(6,*)'Hessian:'
c      do i=1,npar
c       write(6,12)(hess(i,j),j=1,npar)
c      end do 
c
c Diagonalize...
c
      do i=1,npar
      do j=1,i-1
       sss(i,j) = 0.d0
       sss(j,i) = 0.d0
      end do
      end do
      do i=1,npar
       sss(i,i) = 1.d0
      end do
      call DIAGON(hess,sss,maxb,npar,E,V,DIA)
      if(ipr.eq.0) then
      write(6,*)'Hessian eigenvalues:'
      write(6,12)(v(i),i=1,npar)
c      write(6,*)'Eigenvectors:'
c      do i=1,npar
c       write(6,12)(hess(i,j),j=1,npar)
c      end do
      endif
c
c Transform gradient
c
      do i=1,npar
       pom = 0.d0
       do j=1,npar
        pom = pom + hess(j,i)*grad(j) 
       end do
       grdpom(i) = pom
      end do
      grdl = 0.d0
      do i=1,npar
       grad(i) = grdpom(i)
       grdl = grdl + grad(i)*grad(i)
      end do
      grdl = dsqrt(grdl)
      
      !do i = 1,npar
      !   write(*,*) i,grad(i)
      !enddo
c
c Print out transformed gradient
c
      if(ipr.eq.0) then
       write(6,*)'Gradient length:',grdl
c      write(6,*)'Transformed gradient:'
c      do i=1,npar
c       write(6,*)i,grad(i)
c      end do
      endif
c
 12   format(6e11.3)
      return
      end
c
c Check if any of the beta angles is close to singular
c zero or 180 degrees. If so, rotate the whole cluster around
c the LAB-fixed y axis.
c
      subroutine angles_check(ires)
      implicit real*8 (a-h,o-z)
      include 'params.inc'
      logical flag
      common/over_rot/ rot_ang_overall,rot_incr
      common/dat_g/ comm(3,maxmol),aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
c      data eps /0.001d0/
      data eps /0.01d0/

      data zero /0.d0/
c
      pi = dacos(-1.d0)
c
c      write(6,*)'Angles_check entered with ires',ires
      flag = .false.
      if(ires.eq.0) then
       do i=1,nmol
        flag = flag.or.(dabs(aol(2,i)-pi).lt.eps)
        flag = flag.or.(dabs(aol(2,i)).lt.eps)
       end do
       rot_ang = 0.d0
       if(flag) rot_ang = rot_incr*pi/180.d0   ! rotate by an increment
       if(flag) ires = 1
       if(flag) then
        write(6,*)
        write(6,*)'!!!!!!! Singular beta detected, rotating cluster'
c Switch the x and y optimization parameters (good for 90 deg rotation only)
c        do i=1,nmol
c         i1 = indopt(1,i)
c         i3 = indopt(3,i)
c         indopt(1,i) = i3
c         indopt(3,i) = i1
c        end do
c        write(6,*)'New active coordinates:'
c        write(6,*)'    x     y     z  alpha  beta  gamma'
c        do k=1,nmol
c         write(6,'(6i6)') (indopt(j,k),j=1,6)
c        end do
       endif
      else
       rot_ang = -rot_ang_overall   ! rotate back to the original
       flag = .true.
       write(6,*)'Final back-rotated geometry:'
      endif
c
      if(flag) call rot_cluster(zero,rot_ang,zero)
c
      return
      end
c
c rotate the cluster by (alpha,beta,gamma) Euler angles to prevent
c beta = 0 or 180 degrees...
c
      subroutine rot_cluster(alpha,beta,gamma)
      implicit real*8 (a-h,o-z)
      include 'params.inc'
      dimension rot(3,3), pomvec(3),obl(3),oal(3)
      common/over_rot/ rot_ang_overall,rot_incr
      common/dat_g/ comm(3,maxmol),aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
      common/first/ itime
c
      pi = dacos(-1.d0)
      d2rad = pi/180.d0
      rad2d = 1.d0/d2rad
c
c Update the overall rotation angle
c
      if(itime.eq.0)
     1rot_ang_overall = rot_ang_overall + beta
      write(6,*)'Cluster rotated by'
      write(6,'(3f15.5)')  rad2d*alpha,  rad2d*beta,  rad2d*gamma
c
c First, find the new Euler angles of all molecules
c
c      obl(1) = 0.d0
c      obl(2) = -rot_ang
c      obl(3) = 0.d0
      call rotmat(alpha,beta,gamma,rot)
      do j=1,nmol
       call euler_rot(aol(1,j),rot,oal)
       do i=1,3
        aol(i,j) = oal(i)
       end do
      end do
c
c Second, calculate the new COM coordinates.
c Overall rotation matrix:
c
      call rotmat(alpha,beta,gamma,rot)
c
      do j=1,nmol
       call matvec(rot,comm(1,j))
      end do
c
c Print out the new coordinates 
c
      write(6,*)
      write(6,*)'        Geometry after rotation:'
      write(6,*)'        x            y           z        alpha       b
     1eta       gamma'
      do j=1,nmol
       write(6,'(6f12.6)')(comm(i,j),i=1,3),(rad2d*aol(i,j),i=1,3)
      end do
      write(6,*)
c
      return
      end
c
C A simple program to find a saddle point
c using the method of Cerjan and Miller 
c (JCP 75, 2800, 1981) with individual steps by Wales
c (JCP 101, 3750, 1994)
c
      subroutine saddle(flsqgen,r)
      implicit real*8 (a-h,o-z)
      logical r_trudge, r_saddle, e_three,c_vshape
      include 'params.inc'
        dimension r(1), dx(maxb), dxpom(maxb), dlam0(maxb)
        common/hess/ grad(maxb), hess(maxb,maxb),sss(maxb,maxb),
     1               e(maxb),v(maxb)
        COMMON/EOPT/TOLF,TOLR,ANOISE,TIMLIM,LINP,LOUT,ALFST,IPR,NPAR,
     &            SAFETL
        common/walk_pars/ scale, neig, nprt, dirs, dirm, dmax,hh,eps,
     1                   r_trudge, r_saddle, e_three,c_vshape,vs_eps
        external flsqgen
        save
c
        call npar2r(r,npar)      ! does not seem necessary (maybe after inert)
        call flsqgen(-1,r,rmse)  ! does npar2r only if itime=0, i.e., for new geom
        write(6,*)
        write(6,*)'Initial energy:',rmse
        write(6,*)
c
c Initial move in the direction of eigenvector neig.
c
        if(neig.ne.0) then
         call gradhess(flsqgen,r)
         d_length = dsqrt(scalp(hess(1,neig),hess(1,neig),npar))
         do i=1,npar
          r(i) = r(i) + dirs*hess(i,neig)/d_length
         end do
         call r2npar(r,npar)
        endif
c
c The following should be included in a loop...
c
 49     continue
        if(neig.ne.0) then 
         write(6,*)'           ---------------------------'
         write(6,*)'           Saddle point search started'
         write(6,*)'           ---------------------------'
         write(6,*)
        else
         write(6,*)'           ---------------------------'
         write(6,*)'              Minimization started'
         write(6,*)'           ---------------------------'
         write(6,*)
        endif
        icount = 0
 50     continue
        icount = icount + 1
c
c Compute the gradient and diagonalized Hessian 
c
        call gradhess(flsqgen,r)
c
c Decide which eigendirection to follow next
c
        if(neig.gt.1.and.icount.ne.1) then
        neig0 = 1
        overmx = 0.d0
        do j=1,npar
         over = 0.d0
         do k=1,npar
          over = over + dx(k)*hess(k,j)
         end do
         over = dabs(over)/dlsq
         if(over.gt.overmx) then
          overmx = over
          neig0 = j
         endif
        end do
        write(6,*)'Following direction',neig0,overmx
        neig = neig0
        endif
c
c Compute dlam0 for each direction from Eq. 10 of Wales
c
        call lamfnd(dlam0,neig)
c
c Compute the energy increment (not correct for scaled steps,
c but not needed anyway...)
c
        sum = 0.d0
        do i=1,npar
         pom = grad(i)*grad(i)*(dlam0(i)-0.5d0*v(i))
         sum = sum + pom/(dlam0(i)-v(i))**2
c         write(6,*)'suminc',sum
        end do
c        write(6,*)'Increment of energy:',sum
c
c Compute the step vector in the eigenvector representation,
c gradient length, and overall step length
c
        grl = 0.d0
        adxm = -1.d0
        do i=1,npar
           if (abs(dlam0(i) - v(i)) > 1.0d-20) then 
              dxpom(i) = grad(i)/(dlam0(i) - v(i))
           else
              dxpom(i) = 0.0d0
           endif
           
         adx = dabs(dxpom(i))
         if(adx.gt.adxm) adxm = adx
         grl = grl + grad(i)*grad(i)
         !write(*,*) i,dxpom(i),v(i)
        end do
        grl = dsqrt(grl)
        if(adxm.gt.dmax) then
c scale the step vector
        !write(6,*)'Scaling by:',dmax/adxm
         do i=1,npar
          dxpom(i) = dmax*dxpom(i)/adxm
         end do
        endif
        dlsq = 0.d0
        do i=1,npar
         dlsq = dlsq + dxpom(i)*dxpom(i)
        end do
        dlsq = dsqrt(dlsq)
c Scale the steps according to Eq. (15) of Wales
        go to 113      ! skip this part for now...
        avhes = 0.d0
        do i=1,npar
         avhes = avhes + v(i)
        end do
        avhes = avhes/npar
        phi = avhes/grl
        do i=1,npar
         pom = dsqrt(1.d0 + phi*dxpom(i)*dxpom(i))
         dxpom(i) = 2.d0*dxpom(i)/(1.d0 + pom)
        end do
 113    continue
c
c Transform dx back into the original representation
c
        do i=1,npar
         dx(i) = 0.d0
         do j=1,npar
          dx(i) = dx(i) + hess(i,j)*dxpom(j)
         end do
        end do
c
        !write(6,*)'dx vector in req and eig reps:'
        !do i=1,npar
        ! write(6,*) dx(i), dxpom(i)
        !end do
c
c Update the r vector
c
        do i=1,npar
         r(i) = r(i) + scale*dx(i)
        end do
        call r2npar(r,npar)
        inout = 0
        if(mod(icount,nprt).eq.0) inout = 2
        call flsqgen(inout,r,rmse)
        !write(6,'(a4,i5,3e16.7)') 'ITER',icount,rmse,dlsq,grl
c
c Exit condition
c
cwc        if((grl.gt.eps.or.dlsq.gt.eps).and.icount.lt.100) go to 50
        if((grl.gt.eps.or.dlsq.gt.eps).and.icount.lt.icount_max) goto 50
        if(neig.ne.0) then
         write(6,*)
         write(6,*)'           ------------------------------'
         write(6,*)'            Saddle point search completed'
         write(6,*)'           ------------------------------'
cwc         if(icount.ge.100) write(6,*)'without convergence'
         if(icount.ge.icount_max) write(6,*)'without convergence'
         write(6,*)
        else
         write(6,*)
         write(6,*)'           ------------------------------'
         write(6,*)'               Minimization completed'
         write(6,*)'           ------------------------------'
         write(6,*)
        endif
        write(6,*)'Final energy:',rmse
        call flsqgen(0,r,rmse)
c
c Compute the final Hessian with outprint...
c
        ipr = 0
        call gradhess(flsqgen,r)
        ipr = 1
c
        if(neig.ne.0) then
c
c Move the r vector forward in the neig direction
c to start minimization...
c
        d_length = dsqrt(scalp(hess(1,neig),hess(1,neig),npar))
        do i=1,npar
         r(i) = r(i) + dirm*hess(i,neig)/d_length
        end do
        call r2npar(r,npar)
        if(dirm.ne.0.d0) then
         neig = 0
         go to 49
        endif
        endif
        return
        end
c
c Find dlam0 vector of Eq. 10 of Wales 
c
        subroutine lamfnd(dlam0,neig)
        implicit real*8 (a-h,o-z)
        include 'params.inc'
        dimension dlam0(maxb)
        common/hess/ grad(maxb), hess(maxb,maxb),sss(maxb,maxb),
     1               e(maxb),v(maxb)
        COMMON/EOPT/TOLF,TOLR,ANOISE,TIMLIM,LINP,LOUT,ALFST,IPR,NPAR,
     &            SAFETL
        data eps /1.d-10/
c Enter zeroes for minimization.....
c        if(neig.eq.0) then
c        do i=1,npar
c         dlam0(i) = 0.d0
c        end do
c        return
c        endif
c
        do i=1,npar
         pom = v(i)*v(i) + 4.d0*grad(i)*grad(i)
         pom = 0.5d0*(dabs(v(i)) + dsqrt(pom))
         if(i.eq.neig) then
          dlam0(i) = v(i) + pom
         else
          dlam0(i) = v(i) - pom
         endif
        end do 
c
        return
        end
c
c Function dlds -- call after diagonalizing Hessian
c and transforming the gradient vector.
c
        function dlds(x0,n)
        implicit real*8 (a-h,o-z)
        include 'params.inc'
        common/hess/ grad(maxb), hess(maxb,maxb),sss(maxb,maxb),
     1               e(maxb),v(maxb)
        COMMON/EOPT/TOLF,TOLR,ANOISE,TIMLIM,LINP,LOUT,ALFST,IPR,NPAR,
     &            SAFETL         
c
        sum = 0.d0
        do i=1,npar
         sum = sum +grad(i)*grad(i)/(x0-v(i))**n
        end do
        dlds = sum
        return
        end
c
      subroutine shft_rot
      implicit real*8 (a-h,o-z)
      include 'params.inc'
      dimension vec1(3),vec2(3),ao(3)
      common/dat_g/ comm(3,maxmol),aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
c
c Shift the origin to molecule 1
c
      do i=2,nmol
      do j=1,3
       comm(j,i) = comm(j,i) - comm(j,1)
      end do
      end do
      do j=1,3
       comm(j,1) = 0.d0
      end do
      write(6,*)'Origin shifted to molecule 1'
c
c Calculate vecor vec1 (will be along z)
c
      v1l = dsqrt(scalp(comm(1,2),comm(1,2),3))
      do j=1,3
       vec1(j) = comm(j,2)/v1l
      end do
c
c Calculate vector vec2 by projecting out R_12 from R_13 (will be along x)
c Set vec2 to zeto if there are only two molecules involved...
      do j=1,3
       vec2(j) = 0.d0
      end do
      if(nmol.ge.3) then
      pom = scalp(comm(1,3),vec1,3)
      do j=1,3
       vec2(j) = comm(j,3) - pom*vec1(j)
      end do
      v2l = dsqrt(scalp(vec2,vec2,3))
      do j=1,3
       if(v2l.gt.0.d0) vec2(j) = vec2(j)/v2l
      end do
      endif
c
c Calculate the angles
c
      call angles(vec1,vec2,ao(1),ao(2),ao(3))
c
c Rotate the whole cluster in the oposite direction:
c
      write(6,*)'Cluster will be rotated now'
      call rot_cluster(-ao(3),-ao(2),-ao(1))
c
      return
      end
c
c A subroutine to generate the Cartesian coordinates
c of the cluster from the COM locations and Euler angles.
c The Cartesians are written in the ALCHEMY form, readable
c by RasMol plotting program.
c
c sitt(3,natommax,maxmol) - matrix used to place the instantenuous
c Cartesians of the atoms
c
c work   - some work space needed by the called subroutine inert
c
c The ALCHEMY files are printed in the coordinate system of
c principal axes obtained from the inert routine. The subsequent
c files are named str001.alc, atr002.alc, e.t.c.
c
        subroutine alcplot(sitt,work)
        implicit real*8 (a-h,o-z)
        include 'params.inc'
        character*6 atoms, bonds
        character*8 charges
        character*6 single
        character*10 fff      !  strXXX.alc
        character*3 suff
        data atoms /6HATOMS,/, bonds /6HBONDS,/
        data charges /8HCHARGES,/, single /6HSINGLE/
        dimension amtot(ncmax),sitt(3,natommax,maxmol),tran(3,3)
        dimension comt(3)
        dimension work(1)    ! space for inert routine...
c
      common/dat_g/ comm(3,maxmol),aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
      common/freqs/ site(3,natommax,ncmax),am(natommax,ncmax),
     1     nsit(ncmax),ishp(ncmax),c_freq,ph_shft,iitypa(natommax,ncmax)
c
      data zero /0.d0/
      data n_plots  / 0 /    ! number of plot dumps; will be updated
c
        pi = dacos(-1.d0)
        rad2d = 180.d0/pi
c
c Place the origins of the monomer coord systems in the
c centers of mass of the monomers...
c
c total masses of monomers
      ncent = n_types
      do k=1,ncent
      kt = label_type(k)
      amtot(k) = 0.d0
      do i=1,nsit(kt)
       amtot(k) = amtot(k) + am(i,kt)
      end do
      end do
c---- Compute the COMs of monomers 
      do k=1,ncent
      kt = label_type(k)
      do i=1,3
       comt(i) = 0.d0
       do j=1,nsit(kt)
        comt(i) = comt(i) + am(j,kt)*site(i,j,kt)
       end do
       comt(i) = comt(i)/amtot(k)
      end do
c---- place the monomer origins in the COMs 
      do i=1,3
       do j=1,nsit(kt)
        site(i,j,kt) = site(i,j,kt) - comt(i)
       end do
      end do
      end do
c
      nc = nmol      ! set equal to the number of molecules
c
c Rotate the monomers...
c
      do k=1,nc      ! loop over molecules
      mk = mtype(k)  ! type of this molecule
      call rotmat(aol(1,k),aol(2,k),aol(3,k),tran)
      do i=1,nsit(mk)
       do j=1,3
        sitt(j,i,k) = site(j,i,mk)
       end do
       call matvec(tran,sitt(1,i,k))
      end do
      end do
c
c Translate monomers by COMs vectors
c
      do k=1,nc
      do i=1,nsit(mtype(k))
      do j=1,3
       sitt(j,i,k) = sitt(j,i,k) + comm(j,k)
      end do
      end do
      end do
c
c Prepare input for the calculation of moments of inertia:
c Entry points is the work vector....
      nec = 1
      ndm = nec 
      do k=1,nmol
       ndm = ndm + 3*nsit(mtype(k))   ! count all Cartesians
      end do
      ncm = ndm + (ndm-1)/3
      ndi = ncm + 3
      ndv = ndi + 9
      n_sites = (ndm-1)/3
c Masses:
      i_cnt = 0
      do k=1,nmol
      do i=1,nsit(mtype(k))
       work(ndm + i_cnt) = am(i,mtype(k))
       i_cnt = i_cnt + 1 
      end do
      end do 
c Coordinates:
      i_cnt = 0
      do k=1,nmol
      do i=1,nsit(mtype(k))
      do j=1,3
       work(i_cnt+j) =  sitt(j,i,k) 
      end do
      i_cnt = i_cnt + 3
      end do
      end do
c Call inert
c                 c   dmass     n    com       din        v
      call inert(work,work(ndm),n_sites,work(ncm),work(ndi),work(ndv))
c Retrieve the coordinates rotated into the principal axis system
      i_cnt = 0
      do k=1,nmol
      do i=1,nsit(mtype(k))
      do j=1,3
       sitt(j,i,k) = work(i_cnt+j)
      end do
      i_cnt = i_cnt + 3
      end do
      end do
c
c Outprint
c
c Prepare the file
c
        n_plots = n_plots + 1
        call get_suff(n_plots,suff)
        fff = 'str'//suff//'.alc'
        open(unit=8,file=fff,form='formatted')
c Do the printing
        natoms = 0
        do k=1,nc
         natoms = natoms + nsit(mtype(k))
        end do
        nbonds = 0 
        ncharges = 0
        djunk = 1.9d0
        write(8,10) natoms,atoms,nbonds,bonds,ncharges,charges
 10     format(i5,a7,i6,a7,i6,a9)
        ii = 0
        do k=1,nc
        mk = mtype(k)
        do i=1,nsit(mk)
         ii = ii + 1
         write(8,20) ii,iitypa(i,mk),(sitt(j,i,k),j=1,3),djunk
        end do
        end do
 20     format(i5,a3,f12.4,2f9.4,f12.4)
c Close the file
        close(8)
c
        write(6,*)
        write(6,*)'ALCHEMY structure dumped into the file ',fff
        write(6,*)
c
      return
      end
