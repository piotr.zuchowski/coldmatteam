c
      SUBROUTINE DIAGON(H,S,NR,N,E,V,DIA)
      IMPLICIT REAL*8 (A-H,O-Z)
C**********
C
C     VERSION 11/4/81-1
C     CHANGED NOT TO USE ANY COMMON. DIA MAY BE NOT ENTERED
C     THIS PARAMETER IS NOT USED IN THE PRESENT VERSION.
C     THE VALUE OF DIA IN DATA IS TAKEN FROM TQL2. IT IS
C     EQUAL (SUPPOSEDLY) TO 16.0D0**(-13)
C     THIS VERSION USES EISPACK ROUTINES TRED2 & TQL2 INSTEAD
C     OF OLD WARSAW QL, WHICH IS NOT WORSE BUT DOES NOT HAVE
C     SCALLING OF THE NUMBERS, WHAT IS QUITE IMPORTANT ON IBM
C
C**********
C
C     SOLVES (H-V*S)*C=0, NR - COLUMN DIM OF H AND S
C     H & S HAVE TO BE SYMMETRIC MATRICES, S POSITIVELY DEFINITE
C     N - DIMENSION OF PROBLEM, E - ONE-DIM AUXILARY VEC OF LENGTH
C     N, V - EIGENVALUES (SIC#)
C     ON OUTPUT ORDERED EIGENVECTORS ARE ON H, SO ALL H MAT IS DESTROYED.
C     BUT ONLY THE LOWER TRIANGLE OF H IS NEEDED ON INPUT.
C     ON OUTPUT ORDERED EIGENVECTORS ARE ON H, SO ALL H MAT IS DESTROYED,
C     ONLY LOWER TRIANGLE OF THE S MATRIX IS USED. THE UPPER ONE
C     (EXCLUDING DIAGONAL) MAY BE ARBITRARY AND IS UNCHANGED.
C
      DIMENSION H(NR,NR),S(NR,NR)
      DIMENSION E(1),V(1)
CCDC     1
C     DIAC=1.0D-13
CVAX     1
      DIAC=2.0D0**(-54)
CIBM     1
C     DIAC=16.0D0**(-13)
      DIA=DIAC
  602 FORMAT(' EIGENVALUES',3D25.15/(12X,3D25.15))
C                                       Q  Q*(TRANSPQ)=S
      CALL SPOL(S,NR,N)
C                                       Q(-1)  (TRANSPQ(-1))*Q(-1)=S(-1)
      CALL REV(S,NR,N)
C                                       HPRIM=Q(-1)*H*(TRANSPQ(-1))
      CALL QFQ(H,S,NR,N,E)
C                                       CPRIM:  (TRANSPCPRIM)*HPRIM*CPRIM
C                                       =EPS,WHICH IS DIAGONAL MATRIX
C                                       NONZERO ELEMENTS OF EPS ARE ON V
      CALL TRED2(NR,N,H,V,E,H)
      CALL TQL2(NR,N,V,E,H,IERR)
C                                       C=(TRANSPQ(-1))*CPRIM
C                                       (TRANSPC)*H*C=EPS
      CALL TRANSp(H,S,NR,N)
      RETURN
      END

      SUBROUTINE SPOL(S,NR,N)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION S(NR,NR)
C
C     SPOL CONSTRUCTES A TRIANGULAR MATRIX Q. Q*(TRANSQ)=S.
C     Q IS ON THE DIAGONAL AND BELOW.
C     S MUST BE POSITIVE DEFINITE
C
  100 FORMAT(' MATRIX DECOMPOSED IS NOT POSITIVE DEFINITE')
  101 FORMAT(D23.15)
      DET=1D0
      DO 1 K=1,N
      W=S(K,K)
      KK=K-1
      IF(KK.LT.1) GO TO 200
      DO 2 J=1,KK
      W=W-S(K,J)**2
    2 CONTINUE
  200 CONTINUE
      IF(W) 11,11,10
   11 PRINT 100
      RETURN
   10 S(K,K)= SQRT( ABS(W))
      DET=DET*W
      KL=K+1
      IF(N.LT.KL) GO TO 1
      DO 20 L=KL,N
      W=S(L,K)
      KU=K-1
      IF(KU.LT.1) GO TO 300
      DO 30 J=1,KU
      W=W-S(K,J)*S(L,J)
   30 CONTINUE
  300 CONTINUE
      S(L,K)=W/S(K,K)
   20 CONTINUE
    1 CONTINUE
  103 FORMAT(' DETERMINANT=',D23.15)
      RETURN
      END

      SUBROUTINE REV(Q,NR,N)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION Q(NR,NR)
C
C     Q IS A TRIANGULAR MATRIX,NON ZERO ELEMENTS ARE BELOW.
C     REV COMPUTES Q(-1) ALSO OF THE SAME CONSTRUCTION.
C     THE ZERO PART OF Q MAY NOT BE ZERO AND IS NOT DESTROYED.
C
      DO 1 J=1,N
      DO 1 I=J,N
      IF(I-J)10,11,10
   11 II=1
      GO TO 12
   10 II=0
   12 S=0.0D0
      KK=I-1
      IF(KK.LT.J) GO TO 200
      DO 2 K=J,KK
      S=S-Q(I,K)*Q(K,J)
    2 CONTINUE
  200 CONTINUE
      Q(I,J)=(II+S)/Q(I,I)
    1 CONTINUE
      RETURN
      END

      SUBROUTINE QFQ(F,S,NR,IOR,HULP)
      IMPLICIT REAL*8 (A-H,O-Z)
      DIMENSION HULP(1)
      DIMENSION F(NR,NR),S(NR,NR)
C
C     QFQ COMPUTES S*F*(TRANSPS), WHERE S IS LOWER TRIANGULAR MATRIX
C     AND F MUST BE SYMMETRIC
C     THE RESULT IS ON F
C
      DO 801 I=1,IOR
      II=IOR-I+1
      DO 202 K=1,II
      SUM=0D0
      DO 204 L=1,II
      FKL=F(K,L)
      IF(K.LT.L) FKL=F(L,K)
  204 SUM=SUM+FKL*S(II,L)
  202 HULP(K)=SUM
      DO 802 J=1,II
      SUM=0D0
      DO 206 K=1,J
  206 SUM=SUM+S(J,K)*HULP(K)
      F(II,J)=SUM
  802 F(J,II)=SUM
  801 CONTINUE
      RETURN
      END

      SUBROUTINE TRANSp(F,P,NR,IOR)
      IMPLICIT REAL*8 (A-H,O-Z)
C
C     TRANS COPMUTES (TRANSPP)*F, WHERE P IS LOWER TRIANGULAR MATRIX
C
      DIMENSION F(NR,NR),P(NR,NR)
      DO 550 K=1,IOR
      DO 550 I=1,IOR
      SS=0.0D0
      DO 551 J=I,IOR
      SS=SS+P(J,I)*F(J,K)
  551 CONTINUE
      F(I,K)=SS
  550 CONTINUE
      RETURN
      END
*DECK EISPQL
      SUBROUTINE TQL2(NM,N,D,E,Z,IERR)
C
C     THERE ARE TWO VERSIONS OF TQL2/TRED2 ROUTINES. NEWER (?) VERSION
C     HAS SLIGHTLY MORE EXTENSIVE COMMENTS (WITH SPELLING MISTAKES
C     REMOVED) AND "FANCY" STYLE OF WRITING FORTRAN STATEMENS.
C     BESIDES THE TWO VERSIONS ARE EQUIVALENT. THE PRESENT VERSION
C     IS THIS NEWER ONE WITH QP OPTION FROM THE OTHER OPTION INSERTED.
C     USE THIS VERSION FROM NOW ON.
C     ON CDC IN CDC SINGLE PRECISION CONSTANS ARE UNNECESSARY OF DOUBLE
C     PRECISION TYPE. IT WAS WORKING OK THIS WAY BUT MIGHT BE SLIGHTLY
C     SLOWER THAN NECESSARY.
C     K.SZ./JULY 21, 1986
C
      INTEGER I,J,K,L,M,N,II,L1,NM,MML,IERR
CCDC     3
C     REAL   D(N),E(N),Z(NM,N)
C     REAL   B,C,F,G,H,P,R,S,MACHEP
C     REAL    SQRT, ABS, SIGN
CNOCDC   3
      REAL *8  D(N),E(N),Z(NM,N)
      REAL *8  B,C,F,G,H,P,R,S,MACHEP
      REAL *8  SQRT, ABS, SIGN
CQP      4
C     REAL*16 D(N),E(N),Z(NM,N)
C     REAL*16 B,C,F,G,H,P,R,S
C     REAL*16 MACHEP
C     REAL*16 DABS,DSQRT,DSIGN
C
C     THIS SUBROUTINE IS A TRANSLATION OF THE ALGOL PROCEDURE TQL2,
C     NUM. MATH. 11, 293-306(1968) BY BOWDLER, MARTIN, REINSCH, AND
C     WILKINSON.
C     HANDBOOK FOR AUTO. COMP., VOL.II-LINEAR ALGEBRA, 227-240(1971).
C
C     THIS SUBROUTINE FINDS THE EIGENVALUES AND EIGENVECTORS
C     OF A SYMMETRIC TRIDIAGONAL MATRIX BY THE QL METHOD.
C     THE EIGENVECTORS OF A FULL SYMMETRIC MATRIX CAN ALSO
C     BE FOUND IF  TRED2  HAS BEEN USED TO REDUCE THIS
C     FULL MATRIX TO TRIDIAGONAL FORM.
C
C     ON INPUT
C
C        NM MUST BE SET TO THE ROW DIMENSION OF TWO-DIMENSIONAL
C          ARRAY PARAMETERS AS DECLARED IN THE CALLING PROGRAM
C          DIMENSION STATEMENT;
C
C        N IS THE ORDER OF THE MATRIX;
C
C        D CONTAINS THE DIAGONAL ELEMENTS OF THE INPUT MATRIX;
C
C        E CONTAINS THE SUBDIAGONAL ELEMENTS OF THE INPUT MATRIX
C          IN ITS LAST N-1 POSITIONS.  E(1) IS ARBITRARY;
C
C        Z CONTAINS THE TRANSFORMATION MATRIX PRODUCED IN THE
C          REDUCTION BY  TRED2, IF PERFORMED.  IF THE EIGENVECTORS
C          OF THE TRIDIAGONAL MATRIX ARE DESIRED, Z MUST CONTAIN
C          THE IDENTITY MATRIX.
C
C      ON OUTPUT
C
C        D CONTAINS THE EIGENVALUES IN ASCENDING ORDER.  IF AN
C          ERROR EXIT IS MADE, THE EIGENVALUES ARE CORRECT BUT
C          UNORDERED FOR INDICES 1,2,...,IERR-1;
C
C        E HAS BEEN DESTROYED;
C
C        Z CONTAINS ORTHONORMAL EIGENVECTORS OF THE SYMMETRIC
C          TRIDIAGONAL (OR FULL) MATRIX.  IF AN ERROR EXIT IS MADE,
C          Z CONTAINS THE EIGENVECTORS ASSOCIATED WITH THE STORED
C          EIGENVALUES;
C
C        IERR IS SET TO
C          ZERO       FOR NORMAL RETURN,
C          J          IF THE J-TH EIGENVALUE HAS NOT BEEN
C                     DETERMINED AFTER 30 ITERATIONS.
C
C     QUESTIONS AND COMMENTS SHOULD BE DIRECTED TO B. S. GARBOW,
C     APPLIED MATHEMATICS DIVISION, ARGONNE NATIONAL LABORATORY
C
C     ------------------------------------------------------------------
C
C                MACHEP IS A MACHINE DEPENDENT PARAMETER SPECIFYING
C                THE RELATIVE PRECISION OF dfloatING POINT ARITHMETIC.
C                MACHEP = 16.0D0**(-13) FOR LONG FORM ARITHMETIC
C                ON S360
CCDC     1
C     DATA MACHEP/1.0E-13/
CIBM     1
C     MACHEP=16.0D0**(-13)
CVAX     1
      MACHEP=2.0D0**(-54)
CQP      1
C     MACHEP=16D0**(-26)
C
      IERR = 0
      IF (N .EQ. 1) GO TO 1001
C
      DO 100 I = 2, N
  100 E(I-1) = E(I)
C
      F = 0.0D0
      B = 0.0D0
      E(N) = 0.0D0
C
      DO 240 L = 1, N
         J = 0
         H = MACHEP * ( ABS(D(L)) +  ABS(E(L)))
         IF (B .LT. H) B = H
C                LOOK FOR SMALL SUB-DIAGONAL ELEMENT
         DO 110 M = L, N
            IF ( ABS(E(M)) .LE. B) GO TO 120
C                E(N) IS ALWAYS ZERO, SO THERE IS NO EXIT
C                THROUGH THE BOTTOM OF THE LOOP
  110    CONTINUE
C
  120    IF (M .EQ. L) GO TO 220
  130    IF (J .EQ. 30) GO TO 1000
         J = J + 1
C                FORM SHIFT
         L1 = L + 1
         G = D(L)
         P = (D(L1) - G) / (2.0D0 * E(L))
         R =  SQRT(P*P+1.0D0)
         D(L) = E(L) / (P +  SIGN(R,P))
         H = G - D(L)
C
         DO 140 I = L1, N
  140    D(I) = D(I) - H
C
         F = F + H
C                QL TRANSFORMATION
         P = D(M)
         C = 1.0D0
         S = 0.0D0
         MML = M - L
C                FOR I=M-1 STEP -1 UNTIL L DO --
         DO 200 II = 1, MML
            I = M - II
            G = C * E(I)
            H = C * P
            IF ( ABS(P) .LT.  ABS(E(I))) GO TO 150
            C = E(I) / P
            R =  SQRT(C*C+1.0D0)
            E(I+1) = S * P * R
            S = C / R
            C = 1.0D0 / R
            GO TO 160
  150       C = P / E(I)
            R =  SQRT(C*C+1.0D0)
            E(I+1) = S * E(I) * R
            S = 1.0D0 / R
            C = C * S
  160       P = C * D(I) - S * G
            D(I+1) = H + S * (C * G + S * D(I))
C                FORM VECTOR
            DO 180 K = 1, N
               H = Z(K,I+1)
               Z(K,I+1) = S * Z(K,I) + C * H
               Z(K,I) = C * Z(K,I) - S * H
  180       CONTINUE
C
  200    CONTINUE
C
         E(L) = S * P
         D(L) = C * P
         IF ( ABS(E(L)) .GT. B) GO TO 130
  220    D(L) = D(L) + F
  240 CONTINUE
C                ORDER EIGENVALUES AND EIGENVECTORS
      DO 300 II = 2, N
         I = II - 1
         K = I
         P = D(I)
C
         DO 260 J = II, N
            IF (D(J) .GE. P) GO TO 260
            K = J
            P = D(J)
  260    CONTINUE
C
         IF (K .EQ. I) GO TO 300
         D(K) = D(I)
         D(I) = P
C
         DO 280 J = 1, N
            P = Z(J,I)
            Z(J,I) = Z(J,K)
            Z(J,K) = P
  280    CONTINUE
C
  300 CONTINUE
C
      GO TO 1001
C                SET ERROR -- NO CONVERGENCE TO AN
C                EIGENVALUE AFTER 30 ITERATIONS
 1000 IERR = L
 1001 RETURN
C                LAST CARD OF TQL2
      END
C
C     ------------------------------------------------------------------
C
      SUBROUTINE TRED2(NM,N,A,D,E,Z)
C
      INTEGER I,J,K,L,N,II,NM,JP1
CCDC     3
C     REAL   A(NM,N),D(N),E(N),Z(NM,N)
C     REAL   F,G,H,HH,SCALE
C     REAL    SQRT, ABS, SIGN
CNOCDC   3
      REAL *8  A(NM,N),D(N),E(N),Z(NM,N)
      REAL *8  F,G,H,HH,SCALE
      REAL *8  SQRT, ABS, SIGN
CQP      3
C     REAL*16 Z(NM,N),D(N),E(N),A(NM,N)
C     REAL*16 F,G,H,HH,SCALE
C     REAL*16 X,Y,DABS,DSQRT,DSIGN
C
C     THIS SUBROUTINE IS A TRANSLATION OF THE ALGOL PROCEDURE TRED2,
C     NUM. MATH. 11, 181-195(1968) BY MARTIN, REINSCH, AND WILKINSON.
C     HANDBOOK FOR AUTO. COMP., VOL.II-LINEAR ALGEBRA, 212-226(1971).
C
C     THIS SUBROUTINE REDUCES A REAL SYMMETRIC MATRIX TO A
C     SYMMETRIC TRIDIAGONAL MATRIX USING AND ACCUMULATING
C     ORTHOGONAL SIMILARITY TRANSFORMATIONS.
C
C     ON INPUT
C
C        NM MUST BE SET TO THE ROW DIMENSION OF TWO-DIMENSIONAL
C          ARRAY PARAMETERS AS DECLARED IN THE CALLING PROGRAM
C          DIMENSION STATEMENT;
C
C        N IS THE ORDER OF THE MATRIX;
C
C        A CONTAINS THE REAL SYMMETRIC INPUT MATRIX.  ONLY THE
C          LOWER TRIANGLE OF THE MATRIX NEED BE SUPPLIED.
C
C     ON OUTPUT
C
C        D CONTAINS THE DIAGONAL ELEMENTS OF THE TRIDIAGONAL MATRIX;
C
C        E CONTAINS THE SUBDIAGONAL ELEMENTS OF THE TRIDIAGONAL
C          MATRIX IN ITS LAST N-1 POSITIONS.  E(1) IS SET TO ZERO;
C
C        Z CONTAINS THE ORTHOGONAL TRANSFORMATION MATRIX
C          PRODUCED IN THE REDUCTION;
C
C        A AND Z MAY COINCIDE.  IF DISTINCT, A IS UNALTERED.
C
C     QUESTIONS AND COMMENTS SHOULD BE DIRECTED TO B. S. GARBOW,
C     APPLIED MATHEMATICS DIVISION, ARGONNE NATIONAL LABORATORY
C
C     ------------------------------------------------------------------
C
      DO 100 I = 1, N
C
         DO 100 J = 1, I
            Z(I,J) = A(I,J)
  100 CONTINUE
C
      IF (N .EQ. 1) GO TO 320
C                FOR I=N STEP -1 UNTIL 2 DO --
      DO 300 II = 2, N
         I = N + 2 - II
         L = I - 1
         H = 0.0D0
         SCALE = 0.0D0
         IF (L .LT. 2) GO TO 130
C                SCALE ROW (ALGOL TOL THEN NOT NEEDED)
         DO 120 K = 1, L
  120    SCALE = SCALE +  ABS(Z(I,K))
C
         IF (SCALE .NE. 0.0D0) GO TO 140
  130    E(I) = Z(I,L)
         GO TO 290
C
  140    DO 150 K = 1, L
            Z(I,K) = Z(I,K) / SCALE
            H = H + Z(I,K) * Z(I,K)
  150    CONTINUE
C
         F = Z(I,L)
         G = - SIGN( SQRT(H),F)
         E(I) = SCALE * G
         H = H - F * G
         Z(I,L) = F - G
         F = 0.0D0
C
         DO 240 J = 1, L
            Z(J,I) = Z(I,J) / H
            G = 0.0D0
C                FORM ELEMENT OF A*U
            DO 180 K = 1, J
  180       G = G + Z(J,K) * Z(I,K)
C
            JP1 = J + 1
            IF (L .LT. JP1) GO TO 220
C
            DO 200 K = JP1, L
  200       G = G + Z(K,J) * Z(I,K)
C                FORM ELEMENT OF P
  220       E(J) = G / H
            F = F + E(J) * Z(I,J)
  240    CONTINUE
C
         HH = F / (H + H)
C                FORM REDUCED A
         DO 260 J = 1, L
            F = Z(I,J)
            G = E(J) - HH * F
            E(J) = G
C
            DO 260 K = 1, J
               Z(J,K) = Z(J,K) - F * E(K) - G * Z(I,K)
  260    CONTINUE
C
  290    D(I) = H
  300 CONTINUE
C
  320 D(1) = 0.0D0
      E(1) = 0.0D0
C                ACCUMULATION OF TRANSFORMATION MATRICES
      DO 500 I = 1, N
         L = I - 1
         IF (D(I) .EQ. 0.0D0) GO TO 380
C
         DO 360 J = 1, L
            G = 0.0D0
C
            DO 340 K = 1, L
  340       G = G + Z(I,K) * Z(K,J)
C
            DO 360 K = 1, L
               Z(K,J) = Z(K,J) - G * Z(K,I)
  360    CONTINUE
C
  380    D(I) = Z(I,I)
         Z(I,I) = 1.0D0
         IF (L .LT. 1) GO TO 500
C
         DO 400 J = 1, L
            Z(I,J) = 0.0D0
            Z(J,I) = 0.0D0
  400    CONTINUE
C
  500 CONTINUE
C
      RETURN
C                LAST CARD OF TRED2
      END
C
C     ------------------------------------------------------------------
c Performs the Lowdin symmetric orthonormalization.
c
c INPUT:
c   s -  overlap matrix with physical dimension nr*nr
c   unit - work area nr*nr (used for unity matrix)
c   diag,v - work areas of size at least n
c   n - dimension of the problem
c OUTPUT:
c   s - S^(-1/2) transformation matrix
c
       subroutine low_orth(s,unit,nr,n,diag,v)
       implicit real*8 (a-h,o-z)
       dimension s(nr,1),unit(nr,1),diag(1),v(1)
c
       nd= nr*nr
       call r8zero(unit,nd)
       do i=1,n
        unit(i,i) = 1.d0
       end do
c
       call diagon(s,unit,nr,n,v,diag,dia)
c
       do i=1,n
        diag(i) = 1.d0/dsqrt(diag(i))
       end do
c Premultiply column j of s by diag(j):
       do i=1,n
       do j=1,n
        unit(i,j) = s(i,j)*diag(j)
       end do
       end do
c Multiply unit by S^t
       do j=1,n
c save j-th column of S^t in v
        do i=1,n
         v(i) = s(j,i)
        end do
c Generate j-th column (= the j-th row) of the product, load into s
        do i=1,n
         s(j,i) = 0.d0
         do k=1,n
          s(j,i)= s(j,i) + unit(i,k)*v(k)
         end do
        end do
c Move to the next column
       end do
c
c S contains now S^(-1/2), exit the routine...
c
      return
      end

