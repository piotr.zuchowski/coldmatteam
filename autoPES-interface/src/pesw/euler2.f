        subroutine rotmat(ad,bd,gd,u)
c
c Construct the rotation matrix corresponding to
c the Euler angles ad,bd,gd (in radians).
c
        implicit real*8 (a-h,o-z)
        dimension u(3,3)
        sad = dsin(ad)
        sbd = dsin(bd)
        sgd = dsin(gd)
        cad = dcos(ad)
        cbd = dcos(bd)
        cgd = dcos(gd)
c----- construct the transformation matrix
        u(1,1) = cad*cbd*cgd - sad*sgd
        u(1,2) = -cad*cbd*sgd - sad*cgd
        u(1,3) = cad*sbd
        u(2,1) = sad*cbd*cgd + cad*sgd
        u(2,2) = -sad*cbd*sgd + cad*cgd
        u(2,3) = sad*sbd
        u(3,1) = -sbd*cgd
        u(3,2) = sbd*sgd
        u(3,3) = cbd
        return
        end
c--------------------------------------------------------
c
c Put the angles in the proper ranges...
c
      subroutine angreg(oa,ob)
      implicit real*8 (a-h,o-z)
      dimension oa(3),ob(3)
      data pi / 3.14159265358979312d0 /
      data twopi / 6.28318530717958623d0 /
      data rtwopi / 0.159154943091895346d0 /
c
c old version
c      pi = dacos(-1.d0)
c      do i=1,3
c       ob(i) = oa(i) - 2.d0*pi*int(oa(i)/(2.d0*pi))
c      end do
c end of the old version
c
      ob(1) = oa(1)
      ob(2) = oa(2) - twopi*int(rtwopi*oa(2))
      ob(3) = oa(3)
      if(ob(2).lt.0.d0) then
       ob(2) = -ob(2)
       ob(1) = ob(1) + pi
       ob(3) = ob(3) + pi
      endif
      if(ob(2).gt.pi) then
       ob(2) = twopi - ob(2)
       ob(1) = ob(1) + pi
       ob(3) = ob(3) + pi
      endif
      ob(1) = ob(1) - twopi*int(rtwopi*ob(1))
      ob(3) = ob(3) - twopi*int(rtwopi*ob(3))
      return
      end
c--------------------------------------------------------
