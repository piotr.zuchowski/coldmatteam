      subroutine EN2B(Ri,Ei,Rj,Ej,mi,mj,dist,Eli,Elj,pot12,imode)
c Ri,Rj: COM's in Angstroms; Ei,Ej: LAB Euler angles in radians
      use double
      use const
      use dimer
      use force_field
      implicit real*8 (a-h,o-z)
      dimension Ei(3),Ej(3),Ri(3),Rj(3)
      dimension R(3,2),E(3,2),Eli(3),Elj(3),Rij(3)
      real(kind=dbl) :: rmat(3,3)
      real(kind=dbl) :: Urep, Udisp, Uelec, Rmin
      real(kind=dbl), parameter :: Rminimal = 1.0d0, Elarge=1.0d5
      
      if(imode.eq.-1) return
      
      do 10 i=1,3
         R(i,1)=Ri(i)
         R(i,2)=Rj(i)
         E(i,1)=Ei(i)
         E(i,2)=Ej(i)
 10   continue
!
      U0=zero
!     do 20 i=1,N-1
!     do 20 j=i+1,N
      Rij(1)=R(1,2)-R(1,1)
      Rij(2)=R(2,2)-R(2,1)
      Rij(3)=R(3,2)-R(3,1)
      call eulerloc (E(1,1),E(1,2),Rij, Eli,Elj,dist)
      
      call moveDimer(dist,Eli(2),Eli(3),Elj(1)-Eli(1),Elj(2),Elj(3))
!     
!     calculate the energy
!     
!      sita_new = sita_new*a0
!      sitb_new = sitb_new*a0
!      pot12 = energy_ff_vdw() + energy_ff_qq()
      pot12 = energy_ff()
      
      return
      end 
      
      subroutine en3B(Ri,Ei,Rj,Ej,Rk,Ek,mi,mj,mk,pot123,imode)
      implicit real*8 (a-h,o-z)
      dimension Ei(3),Ej(3),Ri(3),Rj(3),Rk(3),Ek(3)
      pot123=0.0d0
      return
      end

