c Calculate harmonic frequencies of a cluster of n
c general (point/linear/nonlinear) rigid rotors. 
c
c Input parameters:
c -----------------
c
c clusten(I,R,VAL) - subroutine calculating potential energy (val) of a
c                    cluster of rotors with coordinates in R. This same 
c                    routine is passed to TRUDGE or SADDLE PES walk programs.
c                    I is the inout parameter supplied by these programs.
c                    Here clusten will always be called with I=0 (i.e.,
c                    regular calculation without outprints will be requested).
c n                - number of molecules in the cluster
c n_types          - number of moleculer types in the cluster
c hess(maxb,maxb)  - storage space to keep Hessian
c a(maxb,maxb)     - storage space to keep the kinetic energy matrix
c r(maxb)          - coordinates of the molecules arranged in the sequence:
c                    x1,y1,z1,alpha1,beta1,gamma1,
c                    x2,y2,z2,alpha2,beta2,gamma2, e.t.c.
c                    For linear molecules the gamma entry will be absent
c                    for an atom only the Cartesians will be present.
c am(natommax,ncmax) - am(i,j) is the mass (in amu) of site i of molecule type j
c site(3,natommax,ncmax) - site(i,j,k) initial (all Euler angles = 0) Cartesian i of 
c                    site j in molecule type k
c                    To avoid unit problems, the Cartesians should be given 
c                    in Angstroms
c ns(ncmax)        - ns(i) number of centers in monomer of type i
c h                - step for calculating the derivatives. The same step
c                    is currently used for all variables and derivatives.
c                    Somewhat more sophisticated differentiation procedure
c                    may be introduced in the future...
c ishp(ncmax)      - ishp(i)=0 if molecule_type i is an atom, ishp(i)=2
c                    if molecule_type i is a linear molecule, and
c                    ishp(i)=0 if molecule_type i is a nonlinear molecule
c mtype(maxmol)    - mtype(i) is the type (denoted by an integer) of
c                    molecule i
c label_type(ncmax)- label_type(i) is the integer label of the type whose
c                    sequence number is i (it does not have to be equal to i
c                    but labels can run only from 1 to ncmax)
c
c Output parameters:
c -----------------
c
c freq(maxb)       - frequencies in cm^-1. For a minimum, freq(1)-freq(6) 
c                    correspond to overall translation and rotation. They are
c                    sometimes imaginary (denoted by "-" sign) and
c                    should be  (about two?) orders of magnitude less than
c                    the normal mode frequencies freq(7), freq(8), ...
c hess(*,i)        - contains the normal mode coordinates for mode i
c                    corresponding to coordinates arrangement in r().
c                    For a minimum, i=1,...,6 are overall translation and 
c                    rotation, or some combinations of these...
c
        subroutine rigfreq(clusten,n,hess,a,r,sit,am,ns,ishp,
     1                     freq,h,mtype,n_types,label_type)
        implicit real*8 (a-h,o-z)
        include 'params.inc'
        character*8 omegas(6*maxmol)
        character*5 crds(6*maxmol)
        character*3 suff
        character*13 dash
        dimension r(1),freq(1),ishp(1),mtype(1)
        dimension hess(maxb,1),a(maxb,1),e(maxb),grad(maxb)
        dimension site(3,natommax,ncmax),amtot(ncmax)
        dimension am(natommax,ncmax),com(3,ncmax),label_type(ncmax)
        dimension ua(3,3,3),ns(1),temp1(3),temp2(3)
c for inverse of A (i.e., G)
        data thr /1.d-4/
        external clusten
        sc2cm = 108.606d0
c
c total masses of monomers
      do k=1,n_types
      kt = label_type(k)
      amtot(kt) = 0.d0
      do i=1,ns(kt)
       amtot(kt) = amtot(kt) + am(i,kt)
      end do
      end do
c---- Compute the COMs of monomers
      do k=1,n_types
      kt = label_type(k)
      do i=1,3
       com(i,kt) = 0.d0
       do j=1,ns(kt)
        com(i,kt) = com(i,kt) + am(j,kt)*site(i,j,kt)
       end do
       com(i,kt) = com(i,kt)/amtot(kt)
      end do
      end do
c---- place the monomer origins in the COMs
c      write(6,*)'Monomer coords in COM frame'
      do k=1,n_types
      kt = label_type(k)
      do i=1,3
       do j=1,ns(kt)
        site(i,j,kt) = site(i,j,kt) - com(i,kt)
       end do
      end do
      end do
c
c Calculate the energy at input geometry
c
      call clusten(0,r,valp)
      write(6,*)'Cluster energy:',valp
c
c Dimension of the problem = n6
        n6 = 3*n
        do k=1,n
         n6 = n6 + ishp(mtype(k))
        end do
c
c Calculate gradient
c
      npar = n6
      do i=1,npar
       r0 = r(i)
       r(i) = r0 + h
       call clusten(0,r,valp)
       r(i) = r0 - h
       call clusten(0,r,valm)
       grad(i) = 0.5d0*(valp - valm)/h
       r(i) = r0 
      end do
      grl = dsqrt(scalp(grad,grad,npar))
      write(6,*)'Length of gradient in rigfreq:',grl
      write(6,*)
c RB test
c      write(6,*)'Gradient itself:'
c      do i=1,npar
c       write(6,*) grad(i)
c      end do
c
c Form the A matrix
        write(6,'(a,i3,a,i3,a)')'Building the',n6,' by',n6,
     1                          ' G^(-1) matrix...' 
c Diagonal elements related to COM positions
        do i=1,n6
        do j=1,n6
         a(i,j) = 0.d0
        end do
        end do
        i6 = 0
        do k=1,n
         mk = mtype(k)
         do j=1,3
          ii = i6 + j
          a(ii,ii) = amtot(mk)
         end do
         i6 = i6 + 3 + ishp(mk)
        end do
c Rotational blocks
        i6 = 0
        do k=1,n
         mk = mtype(k)
         ad = 0.d0
         bd = 0.d0
         gd = 0.d0
        if(ishp(mk).ne.0) then
         ad = r(i6+4)
         bd = r(i6+5)
         if(ishp(mk).eq.3) gd = r(i6+6)
        endif
        call rotmatd(ad,bd,gd,ua(1,1,1),ua(1,1,2),ua(1,1,3))
        do ii=1,ishp(mk)    ! 0, 2, or 3
        do jj=1,ii
         pom = 0.d0
         do i=1,ns(mk)
          do j=1,3
           temp1(j) = site(j,i,mk) 
           temp2(j) = temp1(j)
          end do
          call matvec(ua(1,1,ii),temp1)
          call matvec(ua(1,1,jj),temp2)
          scal = 0.d0
          do l=1,3
           scal = scal + temp1(l)*temp2(l) 
          end do
          pom = pom + am(i,mk)*scal
         end do
         i1 = i6+3+ii
         j1 = i6+3+jj
         a(i1,j1) = pom
         a(j1,i1) = pom
        end do
        end do
        i6 = i6 + 3 + ishp(mk)
        end do
c
c Calculate the Hessian matrix...
c Diagonal first...
c
      write(6,*)'Calculating Hessian....'
      do i = 1,npar
       ri0 = r(i)
       call clusten(0,r,val0)
       r(i) = ri0 + 2.d0*h
       call clusten(0,r,valp)
       r(i) = ri0 - 2.d0*h
       call clusten(0,r,valm)
       r(i) = ri0
       pom = 0.25d0*(valp + valm - 2.d0*val0)/(h*h)
c       if(pom.lt.thr) pom = 0.d0
       hess(i,i) = pom
      end do        
c
c Off diagonal now...
c
      do i=1,npar
      do j=1,i-1
       ri0 = r(i)
       rj0 = r(j)
       r(i) = ri0 + h
       r(j) = rj0 + h
       call clusten(0,r,val1)
       r(i) = ri0 + h
       r(j) = rj0 - h
       call clusten(0,r,val2)
       r(i) = ri0 - h
       r(j) = rj0 + h
       call clusten(0,r,val3)
       r(i) = ri0 - h
       r(j) = rj0 - h
       call clusten(0,r,val4)
       r(i) = ri0
       r(j) = rj0
       pom = 0.25d0*(val1 + val4 - val2 - val3)/(h*h)
c       if(pom.lt.thr) pom = 0.d0
       hess(i,j) = pom
       hess(j,i) = hess(i,j)
      end do
      end do        
c
c Diagonalize...
c
c      do i=1,npar
c       a(i,i) = 1.d0
c      end do
      write(6,*)'Diagonalizing....'
      nc6 = maxb
      call DIAGON(hess,a,nc6,npar,E,freq,DIA)
c
      do i=1,npar
       if(freq(i).ge.0.d0) then
        freq(i) = sc2cm*dsqrt(freq(i))
       else
        freq(i) = -sc2cm*dsqrt(-freq(i))
       endif
      end do
c        
      write(6,*)
      write(6,*)'***********************************************'
      write(6,*)' Frequencies [cm^(-1)] and normal modes:       '
      write(6,*)'***********************************************'
      write(6,*)
      do i=1,npar
       call get_suff(i,suff)
       omegas(i) = 'v '//suff
      end do
      dash = '-------------'
      i6 = 0
      do i=1,n     ! loop over molecules
       mi = mtype(i)
       call get_suff(i,suff)
       crds(i6+1) = 'x'//suff
       crds(i6+2) = 'y'//suff
       crds(i6+3) = 'z'//suff
       if(ishp(mi).ne.0) then
        crds(i6+4) = 'a'//suff
        crds(i6+5) = 'b'//suff
       endif
       if(ishp(mi).eq.3) crds(i6+6) = 'g'//suff
       i6 = i6 + 3 + ishp(mi)
      end do
      ncol = 6
      nrest = mod(npar,ncol)
      nout = (npar - nrest)/ncol 
      do jc=1,nout
      nb = ncol*(jc-1)+1
      ne = ncol*jc
      write(6,13)(dash,i=nb,ne)
      write(6,13)(omegas(i),i=nb,ne)
      write(6,12)'     ',(freq(i),i=nb,ne)
      write(6,13)(dash,i=nb,ne)
      do i=1,npar
       write(6,12)crds(i),(hess(i,j),j=nb,ne)
      end do
      end do     ! loop over j
      if(ne.ne.npar) then
      nb = ncol*nout+1
      ne = npar
      write(6,13)(dash,i=nb,ne)
      write(6,13)(omegas(i),i=nb,ne)
      write(6,12)'     ',(freq(i),i=nb,ne)
      write(6,13)(dash,i=nb,ne)
      do i=1,npar
       write(6,12)crds(i),(hess(i,j),j=nb,ne)
      end do
      endif
      write(6,13)(dash,i=nb,ne)
c
 12   format(a5,6e13.5)
 13   format(5x,6a13)
      return
      end
c
c This subrotuine is still to be implemented in the PESwalk program.
c It will calculate Cartesians of the cluster displaced in the plus/minus
c directions along the normal modes.
c
      subroutine vibshape(hess,r,sitt,npar,sc)
      implicit real*8 (a-h,o-z)
      include 'params.inc'
      character*14 fff      !  vibXXX_YYY.alc
      character*3 suff,suff1
      dimension hess(maxb,maxb),r(maxb),sitt(1)
      data n_plots  / 0 /    ! number of plot dumps; will be updated
c
        n_plots = n_plots + 1
        call get_suff(n_plots,suff)
c
        do i=1,npar
c Open the file....
         call get_suff(i,suff1)
         fff = 'vib'//suff//'_'//suff1//'.alc'
         open(unit=8,file=fff,form='formatted')
c minus configuration
         do j=1,npar
          r(j) = r(j) - sc*hess(j,i)
         end do
         call r2npar(r,npar)
         icount = -1
         call calcgeom(r,sitt,icount)
c equilibrium configuration
         do j=1,npar
          r(j) = r(j) + sc*hess(j,i)
         end do
         call r2npar(r,npar)
         icount = 0
         call calcgeom(r,sitt,icount)
c plus configuration
         do j=1,npar
          r(j) = r(j) + sc*hess(j,i)
         end do
         call r2npar(r,npar)
         icount = 1
         call calcgeom(r,sitt,icount)
c return to equilibrium...
         do j=1,npar
          r(j) = r(j) - sc*hess(j,i)
         end do
         call r2npar(r,npar)
         close(8)
        write(6,*)'ALCHEMY structure dumped into the file ',fff
        end do
        return
        end
c
      subroutine calcgeom(r,sitt,icount)
      implicit real*8 (a-h,o-z)
      include 'params.inc'
      character*6 atoms, bonds
      character*8 charges
      character*6 single
      dimension r(1),sitt(3,natommax,maxmol),tran(3,3)
      common/dat_g/ com(3,maxmol),aol(3,maxmol)
      common/mol_types/ mtype(maxmol),indopt(6,maxmol),
     1                  label_type(ncmax),nmol,n_types
      common/freqs/ site(3,natommax,ncmax),am(natommax,ncmax),ns(ncmax),
     1           ishp(ncmax),c_freq,ph_shft,iitypa(natommax,ncmax)
      data atoms /6HATOMS,/, bonds /6HBONDS,/
      data charges /8HCHARGES,/, single /6HSINGLE/
c
c How to shift different phases of the vibration
c
        rshiftz = (1-abs(icount))*ph_shft
        rshifty = icount*ph_shft
c
c Rotate the monomers. Assume that sit already shifted to COMs by rigfreq
c
      do k=1,nmol      ! loop over molecules
      mk = mtype(k)  ! type of this molecule
      call rotmat(aol(1,k),aol(2,k),aol(3,k),tran)
      do i=1,ns(mk)
       do j=1,3
        sitt(j,i,k) = site(j,i,mk)
       end do
       call matvec(tran,sitt(1,i,k))
      end do
      end do
c
c Translate monomers by COMs vectors and overall shifts
c
      do k=1,nmol
      do i=1,ns(mtype(k))
      do j=1,3
       sitt(j,i,k) = sitt(j,i,k) + com(j,k)
      end do
       sitt(2,i,k) = sitt(2,i,k) + rshifty
       sitt(3,i,k) = sitt(3,i,k) + rshiftz 
      end do
      end do
c
c Outprint (output file 8 opened in vibshape)
c
      if(icount.eq.-1) then    ! set up and print the header
        natoms = 0
        do k=1,nmol
         natoms = natoms + ns(mtype(k))
        end do
        natoms = 3*natoms
        nbonds = 0
        ncharges = 0
        djunk = 1.9d0
        write(8,10) natoms,atoms,nbonds,bonds,ncharges,charges
        ii = 0
      endif
        do k=1,nmol
        mk = mtype(k)
        do i=1,ns(mk)
         ii = ii + 1
         write(8,20) ii,iitypa(i,mk),(sitt(j,i,k),j=1,3),djunk
        end do
        end do
 10     format(i5,a7,i6,a7,i6,a9)
 20     format(i5,a3,f12.4,2f9.4,f12.4)
c
        return
        end
