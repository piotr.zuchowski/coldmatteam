c
      subroutine second(time)
      implicit real*8 (a-h,o-z)
      time=0.0d0
      return
      end
      subroutine exitx
      write(6,*) 'exitx called'
      stop
      end
c
      subroutine mulmat(a,b,c)
      implicit real*8 (a-h,o-z)
      dimension a(3,3), b(3,3), c(3,3)
      do i=1,3
      do j=1,3
       c(i,j) = 0.d0
       do k=1,3
        c(i,j) = c(i,j) + a(i,k)*b(k,j)
       end do
      end do
      end do
      return
      end
c
c Calculate the vector product of vectors a and b
c
        subroutine vecp(a,b,c)
        implicit real*8 (a-h,o-z)
        dimension a(3), b(3), c(3)
        c(1) = a(2)*b(3) - a(3)*b(2)
        c(2) = -a(1)*b(3) + a(3)*b(1)
        c(3) = a(1)*b(2) - a(2)*b(1)
        return
        end
c
c ----- Multiply vetor v by the matrix a. Store in v again.
c
       subroutine matvec(a,v)
       implicit real*8 (a-h,o-z)
       dimension a(3,3),v(3),u(3)
c
       n = 3
       do i=1,n
        u(i) = 0.d0
        do j=1,n
         u(i) = u(i) + a(i,j)*v(j)
        end do
       end do
       do i=1,n
        v(i) = u(i)
       end do
       return
       end
c
c ----- Multiply vetor v by the transposed matrix a. Store in v again.
c
       subroutine matvec2(a,v)
       implicit real*8 (a-h,o-z)
       dimension a(3,3),v(3),u(3)
c
       n = 3
       do i=1,n
        u(i) = 0.d0
        do j=1,n
         u(i) = u(i) + a(j,i)*v(j)
        end do
       end do
       do i=1,n
        v(i) = u(i)
       end do
       return
       end
c
        function scalp(a,b,n)
        implicit real*8 (a-h,o-z)
        dimension a(n),b(n)
        pom = 0.d0
        do i=1,n
         pom = pom + a(i)*b(i)
        end do
        scalp = pom
        return
        end
c
c A simple procedure to return the file suffix...
c RB, 2/2/2000. Return (in suff) a string containing
c an integer number (up to 3 digits, e.g., 001, 023, 678).
c
        SUBROUTINE get_suff(me,suff)
        INTEGER me,ihun,idec,iuni
        CHARACTER*3 suff
c
c Get the base for ASCII characters
c
        ibase = ichar('0')
c
        ihun = me/100
        idec = (me - ihun*100)/10
        iuni = me - 10*idec - 100*ihun
c
        suff = char(ibase + iuni)
        if(idec.ne.0.or.ihun.ne.0) suff = char(ibase+idec)//suff
        if(ihun.ne.0) suff = char(ibase+ihun)//suff
c
        if(me.lt.10) suff = '00'//suff
        if(me.ge.10.and.me.lt.100) suff = '0'//suff
        return
        end
c
      subroutine r8zero(a,n)
      implicit real*8 (a-h,o-z)
      dimension a(1)
      do i=1,n
       a(i) = 0.d0
      end do
      return
      end
