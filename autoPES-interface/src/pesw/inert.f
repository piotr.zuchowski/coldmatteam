c
c  Calculate the inertia tensor of any molecule,
c  diagonalize it and rotate the molecule around its
c  center of mass that the x, y, and z axes coincide
c  with the principal axes of the smallest, medium,
c  and largest moment of inertia, respectively.
c
       subroutine inert(c,dmass,n,com,din,v)
       implicit real*8 (a-h,o-z)
       dimension c(3,1),dmass(1) 
       dimension com(3), din(3,3)
       dimension s(3,3), e(3), v(3)
       sc2cm = 16.86226d0
c
c Calculate the total mass of the system...
c
       dmtot = 0.0d0
       do i=1,n
        dmtot = dmtot + dmass(i)
       end do
c
c Compute the centre of mass
c
       do j=1,3
        com(j) = 0.d0
        do i=1,n
         com(j) = com(j) + dmass(i)*c(j,i)
        end do
        com(j) = com(j)/dmtot
       end do
c  
c       write(6,*)'COM coordinates...'
c       write(6,*)(com(i),i=1,3)
c
c   Shift the coordinates....
c
       do j=1,3
        do i=1,n
         c(j,i) = c(j,i) - com(j)
        end do
       end do
c
c
c  Compute the inertia tensor....
c
       do i=1,3
       do j=1,i
        din(i,j) = 0.d0
        do k=1,n
         rr = 0.d0
         if(i.eq.j) then
          do l=1,3
           rr = rr + c(l,k)*c(l,k)
          end do
         endif
         din(i,j) = din(i,j) + dmass(k)*(rr - c(i,k)*c(j,k))
        end do
        din(j,i) = din(i,j)
       end do
       end do
c
c       write(6,*)'The original inertia tensor...'
c       do i=1,3
c        write(6,*)(din(i,j),j=1,3)
c       end do
c
c  Diagonalize the inertia tensor....
c
       do i=1,3
       do j=1,3
        s(i,j) = 0.d0
       end do
       end do
       do i=1,3
        s(i,i) = 1.d0
       end do
       nr = 3
c
       call DIAGON(din,S,NR,nr,E,V,DIA)
c
       write(6,*)
       write(6,*)' Moments of inertia [a.u. of mass*A**2]'
       write(6,'(3e20.7)') (v(i), i=1,3)
       write(6,*)' Rotational constants [cm^-1]'
       write(6,'(3e20.7)') (sc2cm/v(i), i=1,3)
       write(6,*)'Principal axes in the LAB frame'
       do i=1,3
        write(6,'(3e20.7)')(din(i,j),j=1,3)
       end do
       write(6,*)
c Rotate the system into the principal axis system
       do i=1,n
        call matvec2(din,c(1,i))   ! multiplies c by a transposed matrix din
       end do 
       return
       end
