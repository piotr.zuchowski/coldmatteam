        subroutine eulerloc(ao,bo,rr,aol,bol,r)
        implicit real*8 (a-h,o-z)
c
c Given the sets of Euler angles ao(3) and bo(3) (in the LAB frame), 
c and the vector rr pointing from A to B, calculate the Euler angles
c aol(3) and bol(3) corresponding to the same orientation of
c A and B expressed in the system in which the z axis is along rr
c (this system is rotated with respect to LAB by alpha=phi, beta=theta
c gamma = 0, where theta and phi are spherical polar coordinates
c of rr in LAB). All angles in radians.
c
        dimension ao(3),bo(3),aol(3),bol(3),rr(3)
        dimension co(2), upom1(3,3), ud(3,3), uc(3,3)
        data zero /0.d0/

c        write (*,*) 'eulerloc start'
c
        pi = dacos(-1.d0)
c
c First calculate the length r of rr
c
        r = 0.d0
        do i=1,3
         r = r + rr(i)*rr(i)
        end do
        r = dsqrt(r)
c        write (*,*) 'r=',r
c
c Calculate theta [co(1)] and phi [co(2)]
c
c theta angle...
        ct = rr(3)/r
        if(ct.gt.1.d0) ct = 1.d0
        if(ct.lt.-1.d0) ct = -1.d0
        co(1) = dacos(ct)
        st = dsin(co(1))
c phi angle
        if(dabs(ct).eq.1.d0) then
cwc1
c           ph = 0.d0
           co(2)=0.d0
cwc0
           cp = 1.d0
           sp = 0.d0
        else
           cp = rr(1)/(r*st)
           sp = rr(2)/(r*st)
           if(cp.gt.1.d0) cp = 1.d0
           if(cp.lt.-1.d0) cp = -1.d0
           if(sp.ge.0.d0) then
              co(2) = dacos(cp)
           else
              co(2) = 2.d0*pi - dacos(cp)
           endif
        endif
c        write (*,*) 'sp,ct,cp,co=',sp,ct,cp,co(1),co(2)
c
c Create the transpose of R rotation matrix (new coord system versors
c in rows)
c
         call rotmatA(zero,-co(1),-co(2),upom1)
c
c Create the rotation matrix for A (first and third column needed)
c
         call rotmatA(ao(1),ao(2),ao(3),ud) ! not needed if ud supplied

c
c Transform ud versors to the rotated system
c
         call mulmat3(upom1,ud,ud)
c
c Retrieve angles in the rotated system (with z axis along R)
c
        call angles(ud(1,3),ud(1,1),aol(1),aol(2),aol(3))
c        write(6,*)'ao angles',(aol(i),i=1,3)
c
c Create the rotation matrix for B (first and third column needed)
c
        call rotmatA(bo(1),bo(2),bo(3),uc) ! not needed if uc supplied
c
c Transform uc versors to the rotated system
c
        call mulmat3(upom1,uc,uc)
c
c Retrieve angles in the rotated system (with z axis along R)
c
        call angles(uc(1,3),uc(1,1),bol(1),bol(2),bol(3))
c        write(6,*)'bo angles',(bol(i),i=1,3)
c        write (*,*) 'eulerloc end'
       return
       end
c
c An alternative procedure for generating Euler angles.
c vec1 - unit vector along the body-fixed z axis expressed in the LAB frame
c vec2 - unit vector along the body-fixed x axis expressed in the LAB frame
c
        subroutine angles(vec1,vec2,al,be,ga)
        implicit real*8 (a-h,o-z)
        dimension vec1(3), vec2(3), halfrot(3,3)
        data eps /1.d-12/
c
        pi = dacos(-1.d0)
        zero = 0.d0
c beta angle
        cb = vec1(3)
        if(cb.gt.1.d0) cb = 1.d0
        if(cb.lt.-1.d0) cb = -1.d0
        be = dacos(cb)
        sb = dsin(be)
c alpha angle
        if(dabs(dabs(cb)-1.d0).lt.eps) then  ! almost 0 or 180 degrees
         al = 0.d0
         ca = 1.d0
         sa = 0.d0
        else
        ca = vec1(1)/sb
        sa = vec1(2)/sb
        if(ca.gt.1.d0) ca = 1.d0
        if(ca.lt.-1.d0) ca = -1.d0
c Take care whether al less or more than 180 degrees..
        if(sa.ge.0.d0) then
         al = dacos(ca)
        else
         al = 2.d0*pi - dacos(ca)
        endif
        endif
c gamma angle 
c Generate the versors of the system rotated by (al,be,0)
c
        call rotmatA(al,be,zero,halfrot)
c
c cos(ga) and sin(ga) are equal to the directional cosines 
c of vec2 (which is along the body-fixed x axis) with the x and y
c versors, respectively, of the "half-rotated" (gamma=0) frame.
c
        cg = 0.d0
        sg = 0.d0
        do i=1,3
         cg = cg + vec2(i)*halfrot(i,1)
         sg = sg + vec2(i)*halfrot(i,2)
        end do
        if(cg.gt.1.d0) cg = 1.d0
        if(cg.lt.-1.d0) cg = -1.d0
c Take care whether gamma less or more than 180 degrees...
        if(sg.ge.0.d0) then
         ga = dacos(cg)
        else
         ga = 2.d0*pi - dacos(cg)
        endif
       return
       end
c
        subroutine rotmatA(ad,bd,gd,u)
c
c Construct the rotation matrix corresponding to
c the Euler angles ad,bd,gd (in radians).
c
        implicit real*8 (a-h,o-z)
        dimension u(3,3)
        sad = dsin(ad)
        sbd = dsin(bd)
        sgd = dsin(gd)
        cad = dcos(ad)
        cbd = dcos(bd)
        cgd = dcos(gd)
c----- construct the transformation matrix
        u(1,1) = cad*cbd*cgd - sad*sgd
        u(1,2) = -cad*cbd*sgd - sad*cgd
        u(1,3) = cad*sbd
        u(2,1) = sad*cbd*cgd + cad*sgd
        u(2,2) = -sad*cbd*sgd + cad*cgd
        u(2,3) = sad*sbd
        u(3,1) = -sbd*cgd
        u(3,2) = sbd*sgd
        u(3,3) = cbd
        return
        end
c
        subroutine mulmat3(a,b,c)
c
c c = a*b matrix product...
c
        implicit real*8 (a-h,o-z)
        dimension a(3,3),b(3,3),c(3,3),d(3,3)
        do i=1,3
         do j=1,3
          d(i,j) = 0.d0
          do k=1,3
           d(i,j) = d(i,j) + a(i,k)*b(k,j)
          end do
         end do
        end do
        do i=1,3
         do j=1,3
          c(i,j) = d(i,j)
         end do
        end do
        return
        end
c
        subroutine rotmatd(ad,bd,gd,ua,ub,ug)
c
c Construct the derivatives of rotation matrix corresponding to
c the Euler angles ad,bd,gd (in radians).
c
        implicit real*8 (a-h,o-z)
        dimension ua(3,3),ub(3,3),ug(3,3)
        sad = dsin(ad)
        sbd = dsin(bd)
        sgd = dsin(gd)
        cad = dcos(ad)
        cbd = dcos(bd)
        cgd = dcos(gd)
c----- construct the alpha-derivative of the transformation matrix
        ua(1,1) = -sad*cbd*cgd - cad*sgd
        ua(1,2) = sad*cbd*sgd - cad*cgd
        ua(1,3) = -sad*sbd
        ua(2,1) = cad*cbd*cgd - sad*sgd
        ua(2,2) = -cad*cbd*sgd - sad*cgd
        ua(2,3) = cad*sbd
        ua(3,1) = 0.d0
        ua(3,2) = 0.d0
        ua(3,3) = 0.d0
c----- construct the beta-derivative of the transformation matrix
        ub(1,1) = -cad*sbd*cgd 
        ub(1,2) = cad*sbd*sgd 
        ub(1,3) = cad*cbd
        ub(2,1) = -sad*sbd*cgd 
        ub(2,2) = sad*sbd*sgd 
        ub(2,3) = sad*cbd
        ub(3,1) = -cbd*cgd
        ub(3,2) = cbd*sgd
        ub(3,3) = -sbd
c----- construct the gamma-derivative of the transformation matrix
        ug(1,1) = -cad*cbd*sgd - sad*cgd
c        ug(1,2) = -cad*cbd*sgd + sad*sgd  ! Error!!!
        ug(1,2) = -cad*cbd*cgd + sad*sgd
        ug(1,3) = 0.d0
        ug(2,1) = -sad*cbd*sgd + cad*cgd
        ug(2,2) = -sad*cbd*cgd - cad*sgd
        ug(2,3) = 0.d0
        ug(3,1) = sbd*sgd
        ug(3,2) = sbd*cgd
        ug(3,3) = 0.d0
        return
        end
c
        subroutine euler_rot(ao,upom1,aol)
        implicit real*8 (a-h,o-z)
c
c Given the set of Euler angles ao(3) in some frame
c calculate the Euler angles aol(3) corresponding to the same orientation of
c the molecule expressed in the frame rotated by matrix (upom1)^t
c with respect to the original one. Note that the matrix upom1
c on the parameter list is NOT transposed. All angles in radians.
c
        dimension ao(3),aol(3)
        dimension upom1(3,3), ud(3,3)
        data zero /0.d0/
c
c upom1 is the transpose of the coord system rotation matrix 
c (new coord system versors in rows)
c
c Create the rotation matrix for A (first and third column needed)
c
         call rotmatA(ao(1),ao(2),ao(3),ud) ! not needed if ud supplied
c
c Transform ud versors to the rotated system
c
         call mulmat3(upom1,ud,ud)
c
c Retrieve angles in the rotated system (with z axis along R)
c
        call angles(ud(1,3),ud(1,1),aol(1),aol(2),aol(3))
c        write(6,*)'ao angles',(aol(i)/deg2rad,i=1,3)
       return
       end
