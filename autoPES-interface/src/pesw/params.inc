c --- maxmol is the maximum number of molecules in the cluster
c --- maxb is the maximum number of nonlinear parameters
c --- nsmax is the maximum number of atoms allowed in a molecule
c --- ncmax is the maximum number of molecule types
      parameter (maxmol=20,natommax=50,ncmax=10,maxb=6*maxmol)
	PARAMETER (icount_max=300)
      character*2 iitypa
