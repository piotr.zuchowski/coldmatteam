
program gen_report
  use const
  use dimer
  use force_field
  use utility
  use dgrid
  implicit none
  
  integer, parameter :: FIT_DAT = 1, TEST_DAT = 2
  !integer, parameter :: Rind=1, BAind=2, GAind=3, ABind=4, BBind=5, GBind=6
  integer, parameter :: Nener = 8, n_RMSE = 4
  integer, parameter :: nptBeforeMin = 6, nptAfterMin = 6
  real(8), parameter :: minScanRRatio = 1.2d0
  
  character(200) :: name, input_file, parms_file, intra_parms_file, min_file
  integer :: i, j, k
  real(8), allocatable, target :: fitgrid(:,:), testgrid(:,:)
  real(8), pointer :: grid(:,:)
  real(8) :: gpoint(MAX_NCOOR)
  real(8), allocatable :: minima(:,:)
  integer :: ngrid(2), nmin
  integer :: ff_type, comRuleExp, comRuleDmp, comRuleLin, comRuleVdw
  real(8) :: minWallHeight, meanWallHeight, minWallDist, maxWallDist
  logical :: same, exists, incVDWDmp, incDeltaHF, startFound
  character(1) :: Csupscr
  character(100) :: elsDmpFact, vdwDmpFact
  real(8) :: rmse, Emin, minScanR0
  real(8) :: components(n_FF_comp)
  real(8) :: RMSE_pts(n_RMSE)
  integer :: npt
  
  character(300) :: arg(20)
  character(1000) :: line
  integer :: ival
  real(8) :: val
  real(8) :: rbuf(20)
  logical :: flag
  
  
  !****************************************************************************
  !******************************** READ INPUT ********************************
  !****************************************************************************
  
  do i = 1,20
     call getarg(i,arg(i))
  enddo
  name = arg(1)
  parms_file = arg(2)
  read(arg(3),*) ff_type
  intra_parms_file = arg(4)
  min_file = arg(5)
  read(arg(6),*) comRuleExp
  read(arg(7),*) comRuleDmp
  read(arg(8),*) comRuleLin
  read(arg(9),*) comRuleVdw
  read(arg(10),*) minWallHeight
  read(arg(11),*) meanWallHeight
  read(arg(12),*) minWallDist
  read(arg(13),*) maxWallDist
  
  input_file = trim(name)//'.input_long'
  call read_dimer(input_file)
  
  inquire(file=intra_parms_file,exist=exists)
  !if (exists) then
  !   call ff_init(parms_file, ff_type, intra_parms_file)
  !else
     call ff_init(parms_file, ff_type)
  !endif
  
  call initGrid(min_file, minima, nmin, 1)
  call initGrid(trim(name)//'.ener.fit', fitgrid, ngrid(FIT_DAT), nener)
  call initGrid(trim(name)//'.ener.test1', testgrid, ngrid(TEST_DAT), nener)
  
  Emin = 1.0d100
  incDeltaHF = .false.
  do i = 1,2
     if (i == FIT_DAT) then
        grid => fitgrid
     else
        grid => testgrid
     endif
     
     do j = 1,ngrid(i)
        Emin = min(Emin, grid(j,C_Etot))
        if (abs(grid(j,C_ener+E_dhf)) > 1.0d-20) incDeltaHF = .true.
     enddo
  enddo
  
  write(*,*)
  write(*,*) '-----------------------------------------------------------------'
  write(*,*) '                     ',trim(name)
  write(*,*) '-----------------------------------------------------------------'
  
  write(*,'(A)') ''
  do i = 1,2
     if (homog) then
        write(*,'(A)') '------------------------------------------------------------------'
     else
        write(*,'(3A)') '--------------------------- MONOMER ',monlabelU(i),' ----------------------------'
     endif
     write(*,'(A)') ' Site         x (A)        y (A)        z (A)      Symmetry'
     write(*,'(A)') '------------------------------------------------------------------'
     
     do j = 1,nsite(i)
        write(*,'(A,A,A,F12.7,A,F12.7,A,F12.7,A,I4)') &
             '  ',alabel(i,j),'  ',sitOA(i,j,1)*bohr2A,' ',sitOA(i,j,2)*bohr2A,' ',sitOA(i,j,3)*bohr2A,'    ',stype(i,j)
     enddo
     
     if (homog) exit
  enddo
  
  write(*,'(A)') ''
  write(*,'(A)') '------------------------------------------------------------------'
  
  !****************************************************************************
  !************************* WRITE FUNCTIONAL FORM ****************************
  !****************************************************************************
  
  write(*,*)
  write(*,*)
  write(*,*) '----------------------- FUNCTIONAL FORM -------------------------'
  write(*,*)
  
  if (incElsDmp) then
     if (comRuleDmp == PC_glb) then
        elsDmpFact = 'f_1(\delta_1, r_{ab})'
     else
        elsDmpFact = 'f_1(\delta_1^{ab}, r_{ab})'
     endif
  else
     elsDmpFact = ''
  endif
  
  if (deltan(1,1) > 1.0d5) then
     incVDWDmp = .false.
     vdwDmpFact = ''
  else
     incVDWDmp = .true.
     flag = .true.
     do i = 2,nsptype
        flag = flag .and. deltan(i,1) == deltan(i-1,1)
     enddo
     if (flag) then
        if (ndisp == 1) then
           write(vdwDmpFact,'(A,I0,A,I0,A)') 'f_',cStart,'(\delta_',cStart,'^{ab}, r_{ab})'
        else
           vdwDmpFact = 'f_n(\delta_{\rm dsp}, r_{ab})'
        endif
     else
        if (ndisp == 1) then
           write(vdwDmpFact,'(A,I0,A,I0,A)') 'f_',cStart,'(\delta_',cStart,'^{ab}, r_{ab})'
        else
           vdwDmpFact = 'f_n(\delta_n^{ab}, r_{ab})'
        endif
     endif
  endif
  
  if (ndisp == 1) then
     write(Csupscr,'(I1)') cStart
  else
     Csupscr = 'n'
  endif
  
  write(*,'(A)',advance='no') 'V = '
  if (incPol) write(*,'(A)',advance='no') 'V_{\rm ind}(A,B) + '
  write(*,'(A,I0,A,I0,A)',advance='no') &
       '\sum\limits_{a = 1}^{',nsite(1),'} \sum\limits_{b = 1}^{',nsite(2),'} u_{ab}'
  write(*,'(A)') '\\'
  
  write(*,'(A)',advance='no') 'u_{ab} = '
  if (oPoly > 0) then
     write(*,'(A)',advance='no') '\left[1'
     do i = 1,oPoly
        write(*,'(A,I0,A,I0)',advance='no') ' + a_{ab}^',i,' (r_{ab})^',i
     enddo
     write(*,'(A)',advance='no') '\right]'
  endif
  write(*,'(A)',advance='no') 'A_{ab}e^{-\beta_{ab} r_{ab}}'
  write(*,'(A)') ''
  
  write(*,'(3A)',advance='no') '+ ',trim(elsDmpFact),'\frac{q_a q_b}{r_{ab}}'
  
  if (ndisp > 1) then
     write(*,'(A)',advance='no') '+ \sum\limits_{n='
     do i = 1,ndisp
        write(*,'(I0)',advance='no') cStart + 2*(i-1)
        if (i < ndisp) write(*,'(A)',advance='no') ','
     enddo
     write(*,'(A)',advance='no') '}'
  endif
  write(*,'(6A)',advance='no') &
       trim(vdwDmpFact),'\frac{C_{ab}^',Csupscr,'}{(r_{ab})^',Csupscr,'}'
  write(*,'(A)') '\\'
  
  if (incCore) then
     write(*,'(A)') '+ \frac{A_{ab}^12}{(r_{ab})^12}\\'
  endif
  
  if (incPol) then
     write(*,'(A,I0,A)',advance='no') &
          'V_{\rm ind} = -\frac{1}{2}\sum\limits_{a=1}^{',nsite(1),'} {\bm E}_a \cdot {\bm \mu}_a^{\rm ind}'
     write(*,'(A,I0,A)',advance='no') &
          '-\frac{1}{2}\sum\limits_{b=1}^{',nsite(2),'} {\bm E}_b \cdot {\bm \mu}_b^{\rm ind}'
     write(*,'(A)') '\\'
     write(*,'(A,I0,A)',advance='no') &
          '{\bm \mu}_a^{\rm ind} = \alpha_a\left[\bm E_a \rm + \sum\limits_{b=1}^{',nsite(2), &
          '} {\bm T}_{ab} {\bm \mu}_b^{\rm ind} \right]'
     write(*,'(A)') '\\'
     write(*,'(A,I0,A,A,A)',advance='no') &
          '{\bm E_a} = \sum\limits_{b = 1}^{',nsite(2),'} ',trim(elsDmpFact),'\frac{q_{\rm b}{\bm r_{ab}}}{r_{ab}^3}'
     write(*,'(A)') '\\'
     write(*,'(A)',advance='no') &
          '{\bm T_{ab}} = f_3(\delta_{\rm p}^{ab}, r_{ab})'
     write(*,'(A)',advance='no') &
          '\frac{{\bm r}_{ab} \otimes {\bm r}_{ab}}{r_{ab}^{5}} - f_3(\delta_{\rm p}^{ab}, r_{ab})\frac{1}{r_{ij}^{3}}'
     write(*,'(A)') '\\'
  endif
  
  if (comRuleExp == PC_s) then
     write(*,'(A)',advance='no') &
          'A_{ab} = (A_a A_b)^{\frac{1}{2}}'
     write(*,'(A)') '\\'
     write(*,'(A)',advance='no') &
          '\beta_{ab} = \frac{1}{2}(\beta_a + \beta_b)'
     write(*,'(A)') '\\'
  endif
  if (comRuleVdw == PC_s) then
     write(*,'(7A)',advance='no') &
          'C_{ab}^',Csupscr,' = (C_{a}^',Csupscr,' C_{b}^',Csupscr,')^{\frac{1}{2}} '
     write(*,'(A)') '\\'
  endif
  if (comRuleDmp == PC_s) then
     write(*,'(A)',advance='no') &
          '\delta_n^{ab} = (\delta_n^a \delta_n^b)^{\frac{1}{2}}'
     write(*,'(A)') '\\'
  endif
  if (comRuleLin == PC_s) then
     write(*,'(6A)',advance='no') &
          'a_{ab}^n = \frac{1}{2}(a_{a}^n + a_{b}^n) '
     write(*,'(A)') '\\'
  endif
  
  
  !****************************************************************************
  !**************************** WRITE PARAMETERS ******************************
  !****************************************************************************
  
  write(*,*)
  write(*,*)  
  write(*,*) '-------------- INTERMOLECULAR PES PARAMETER VALUES ---------------'
  
  if (comRuleDmp == PC_glb) then
     if (incElsDmp) then
        write(*,'(A,F8.5,A)',advance='no') &
             '\delta_1 = ',delta1(1)/bohr2A,' A^-1'
        write(*,'(A)') ''
     endif
     if (incVDWDmp) then
        write(*,'(A,F8.5,A)',advance='no') &
             '\delta_{\rm dsp} = ',deltan(1,1)/bohr2A,' A^-1'
        write(*,'(A)') ''
     endif
  endif
  
  ! Site specific parameters
  write(*,'(A)') ''
  do i = 1,2
     if (homog) then
        write(*,'(2A)') '-------------------------------------------------------------------------------', &
             '-------------------------------------------------------------------------------'
     else
        write(*,'(3A)') '-------------------------------------------------------------- MONOMER ',monlabelU(i), &
             ' ----------------------------------------------------------------'
     endif
     call writeLabelStr(PC_s)
     
     write(*,'(2A)') '-------------------------------------------------------------------------------', &
          '-------------------------------------------------------------------------------'
     
     do j = 1,nsite(i)
        write(*,'(3A)',advance='no') '  ',alabel(i,j),' '
        call writeParmStr(PC_s, stype(i,j), stype(i,j))
     enddo
     
     write(*,'(A)') ''
     if (homog) exit
  enddo
  
  ! Site pair specific parameters
  if (comRuleExp == PC_sp .or. comRuleDmp == PC_sp .or. comRuleLin == PC_sp .or. comRuleVdw == PC_sp) then
     
     write(*,'(A)') ''
     write(*,'(2A)') '-------------------------------------------------------------------------------', &
          '-------------------------------------------------------------------------------'
     
     call writeLabelStr(PC_sp)
     
     write(*,'(2A)') '-------------------------------------------------------------------------------', &
          '-------------------------------------------------------------------------------'
     
     if (homog) then
        do i = 1,nsite(1)
           do j = i,nsite(2)
              write(*,'(5A)',advance='no') '  ',alabel(1,i),'   ',alabel(2,j),'   '
              call writeParmStr(PC_sp, stype(1,i), stype(1,j))
           enddo
        enddo
     else
        do i = 1,nsite(1)
           do j = 1,nsite(2)
              write(*,'(5A)',advance='no') '  ',alabel(1,i),'   ',alabel(2,j),'   '
              call writeParmStr(PC_sp, stype(1,i), stype(2,j))
           enddo
        enddo
     endif
     
  endif
  
  
  !****************************************************************************
  !*************************** WRITE GLOBAL INFO ******************************
  !****************************************************************************
  
  write(*,*)
  write(*,*)
  write(*,*) '------------------------- FIT QUALITY ---------------------------'
  write(*,*)
  
  do i = 1,2
     if (i == FIT_DAT) then
        write(*,'(A)') 'Fit data'
        grid => fitgrid
     else
        if (ngrid(TEST_DAT) > 0) then
           write(*,'(A)') 'Test data'
           grid => testgrid
        else
           exit
        endif
     endif
     
     RMSE_pts(1) = Emin/2.0d0
     RMSE_pts(2) = 0.0d0
     RMSE_pts(3) = 10.0d0
     RMSE_pts(4) = 100.0d0
     
     do j = 1,n_RMSE
        rmse = 0.0d0
        npt = 0
        do k = 1,ngrid(i)
           if (grid(k,C_Etot) < RMSE_pts(j)) then
              npt = npt + 1
              call moveDimerGP(grid(k,:))
              rmse = rmse + (grid(k,C_Etot) - energy_ff())**2
           endif
        enddo
        rmse = sqrt(rmse/real(npt))
        
        write(*,'(A,I5,A,F5.1,A,F8.5,A)') &
             'RMSE on ',npt,' points with E < ',RMSE_pts(j),' kcal/mol = ',rmse,' kcal/mol'
     enddo
  enddo
  
  write(*,*)
  write(*,'(A,F10.4,A)') 'Minimum wall height =   ',minWallHeight,' kcal/mol'
  write(*,'(A,F10.4,A)') 'Mean wall height =      ',meanWallHeight,' kcal/mol'
  write(*,'(A,F10.4,A)') 'Minimum wall distance = ',minWallDist,' Angstrom between closest contact atoms'
  write(*,'(A,F10.4,A)') 'Maximum wall distance = ',maxWallDist,' Angstrom between closest contact atoms'
  
  
  !****************************************************************************
  !************************** WRITE LOCAL MINIMA ******************************
  !****************************************************************************
  
  if (nmin > 0) then
     write(*,*)
     write(*,*)
     write(*,*) '------------------------ LOCAL MINIMA ---------------------------'
     
     do i = 1,nmin
        call moveDimerGP(minima(i,:))
        Emin = energy_ff()
        
        write(*,*)
        write(*,'(A,I3,A)') '------------ Minimum ',i,' ------------'
        write(*,'(A,F15.8,A)') 'E =',Emin,' kcal/mol'
        write(*,'(A,F15.8,A)') 'R_com =',minima(i,C_rcom)*bohr2A,' Angstrom'
        write(*,'(A)') 'beta_A, gamma_A, alpha_B, beta_B, gamma_B = '
        write(*,'(5F12.5)') minima(i,C_betaA)*rad2d,minima(i,C_gammaA)*rad2d, &
             minima(i,C_alphaB)*rad2d,minima(i,C_betaB)*rad2d,minima(i,C_gammaB)*rad2d
        
        write(*,*)
        do j = 1,2
           do k = 1,nsite(j)
              write(*,'(I2,A,3F12.7,A,A)') &
                   sanum(j,k),' ',sit(j,k,1:3)*bohr2A,'   ',alabel(j,k)
           enddo
        enddo
        
        ! Compute radial scan
        gpoint(1:ncoor) = minima(i,1:ncoor)
        startFound = .true.
        do while (energy_ff() < 2.0d0*abs(Emin) .and. minCovDist() > 0.8d0)
           gpoint(C_rcom) = gpoint(C_rcom) - 0.1d0
           call moveDimerGP(gpoint)
           if (gpoint(C_rcom) < 0.0d0) then
              startFound = .false.
              exit
           endif
        enddo
        
        if (startFound) then
           val = 0.0d0
           do j = 1,nptBeforeMin
              val = val + minScanRRatio**j
           enddo
           minScanR0 = (minima(i,C_rcom) - gpoint(C_rcom))/val
           
           write(*,*)
           write(*,'(A)',advance='no') '    R_com        Fit total        Fit exp        Fit els          Fit Cn '
           if (incPol) write(*,'(A)',advance='no') '         Fit pol'
           write(*,'(A)') ''
           
           do j = 1,nptBeforeMin+1+nptAfterMin
              call moveDimerGP(gpoint)
              components = energy_ff_comp()
              
              write(*,'(F12.8,A,E15.8,A,E15.8,A,E15.8,A,E15.8)',advance='no') &
                   gpoint(C_rcom)*bohr2A,' ',energy_ff(),' ', &
                   components(F_exp),' ',components(F_els),' ',components(F_vdw)
              if (incPol) write(*,'(A,E15.8)',advance='no') ' ',components(F_pol)
              write(*,'(A)') ''
              
              gpoint(C_rcom) = gpoint(C_rcom) + minScanR0*minScanRRatio**j
           enddo
        endif
        
     enddo
  endif
  
  
  !****************************************************************************
  !************************** WRITE GRID POINTS *******************************
  !****************************************************************************
  
  write(*,*)
  write(*,*)
  write(*,*) '------------------------- GRID POINTS ---------------------------'
  write(*,*) 
  
  do i = 1,2
     do j = 1,2
        if (j == FIT_DAT) then
           write(*,'(A)') 'Fit data'
           grid => fitgrid
        else
           if (ngrid(TEST_DAT) > 0) then
              write(*,'(A)') 'Test data'
              grid => testgrid
           else
              exit
           endif
        endif
        
        if (i == 1) then
           write(*,'(2A)') '-----------------------------------------------------------------------------------------------', &
                '--------------------------------------------------------------------------------'
           write(*,'(A7,8A14)',advance='no') '   n   ','  SAPT total  ','   Fit total ','   SAPT els   ', &
                '   SAPT exch  ','   SAPT ind   ',' SAPT exch-ind','   SAPT disp  ','SAPT exch-disp'
           if (incDeltaHF) write(*,'(A14)',advance='no') '  SAPT DeltaHF'
           write(*,'(3A14)',advance='no') '    Fit els   ','    Fit exp   ','    Fit Cn    '
           if (incPol) write(*,'(A14)',advance='no') '    Fit pol   '
           write(*,'(A)') ''
           write(*,'(2A)') '-----------------------------------------------------------------------------------------------', &
                '--------------------------------------------------------------------------------'
           
           do k = 1,ngrid(j)
              call moveDimerGP(grid(k,:))
              components = energy_ff_comp()

              write(*,'(I6,A,8E14.6)',advance='no') &
                   k,' ',grid(k,C_ener+E_tot),energy_ff(),grid(k,C_ener+E_els),grid(k,C_ener+E_exh), &
                   grid(k,C_ener+E_ind),grid(k,C_ener+E_exhind),grid(k,C_ener+E_dsp),grid(k,C_ener+E_exhdsp)
              if (incDeltaHF) write(*,'(E14.6)',advance='no') grid(k,C_ener+E_dhf)
              write(*,'(3E14.6)',advance='no') &
                   components(F_els),components(F_exp),components(F_vdw)
              if (incPol) write(*,'(E14.6)',advance='no') components(F_pol)
              write(*,'(A)') ''
           enddo
        else
           write(*,'(A)') '-----------------------------------------------------------------------------------------------'
           write(*,'(A7,6A12)') '   n   ','  R_com   ','  beta_A  ',' gamma_A  ',' alpha_B  ','  beta_B  ',' gamma_B  '
           write(*,'(A)') '-----------------------------------------------------------------------------------------------'
           
           do k = 1,ngrid(j)
              write(*,'(I6,A,6F12.5)') k,' ',grid(k,C_Rcom)*bohr2A, &
                   grid(k,C_betaA)*rad2d,grid(k,C_gammaA)*rad2d, &
                   grid(k,C_alphaB)*rad2d,grid(k,C_betaB)*rad2d,grid(k,C_gammaB)*rad2d
           enddo
        endif
        
        write(*,*)
        write(*,*)
     enddo
  enddo
  
contains
  
  subroutine writeLabelStr(parmClass)
    integer :: parmClass
    integer :: i, j

    if (parmClass == PC_s) then
       write(*,'(A)',advance='no') ' Site   '
       write(*,'(A)',advance='no') '    charge    '
       if (incPol) write(*,'(A)',advance='no') ' polarizability '
    else
       write(*,'(A)',advance='no') ' Site 1   Site 2  '
    endif
    
    if (comRuleExp == parmClass) then
       write(*,'(A)',advance='no') '      A       '
       write(*,'(A)',advance='no') '    \beta     '
    endif
    if (comRuleLin == parmClass) then
       do i = 1,oPoly
          write(*,'(A,I1,A)',advance='no') '     a^',i,'      '
       enddo
    endif
    if (comRuleVdw == parmClass) then
       ival = cstart
       do i = 1,ndisp
          write(*,'(A,I2.2,A)',advance='no') '      C^',ival,'       '
          ival = ival + 2
       enddo
    endif
    if (incCore .and. parmClass == PC_sp) then
       write(*,'(A,I2.2,A)',advance='no') '      A^',12,'       '
    endif
    if (comRuleDmp == parmClass) then
       if (incElsDmp) then
          write(*,'(A)',advance='no') '  \delta_1  '
       endif
       if (incVDWDmp) then
          ival = cstart
          do i = 1,ndisp
             write(*,'(A,I2.2,A)',advance='no') ' \delta_',ival,'  '
             ival = ival + 2
          enddo
       endif
       if (incPol) then
          write(*,'(A)',advance='no') '  \delta_p  '
       endif
    endif
    write(*,'(A)') ''
    
    if (parmClass == PC_sp) then
       write(*,'(A)',advance='no') '                  '
    else
       write(*,'(A)',advance='no') '        '
       write(*,'(A)',advance='no') '    (a.u.)    '
       if (incPol) write(*,'(A)',advance='no') '     (a.u.)     '
    endif
    if (comRuleExp == parmClass) then
       write(*,'(A)',advance='no') '  (kcal/mol)  '
       write(*,'(A)',advance='no') '    (A^-1)    '
    endif
    if (comRuleLin == parmClass) then
       do i = 1,oPoly
          write(*,'(A,I1,A)',advance='no') '    (A^-',i,')    '
       enddo
    endif
    if (comRuleVdw == parmClass) then
       ival = cstart
       do i = 1,ndisp
          write(*,'(A,I2.2,A)',advance='no') ' (kcal/mol*A^',ival,') '
          ival = ival + 2
       enddo
    endif
    if (incCore .and. parmClass == PC_sp) then
       write(*,'(A,I2.2,A)',advance='no') ' (kcal/mol*A^',12,') '
    endif
    if (comRuleDmp == parmClass) then
       if (incElsDmp) then
          write(*,'(A)',advance='no') '   (A^-1)   '
       endif
       if (incVDWDmp) then
          ival = cstart
          do i = 1,ndisp
             ival = cstart
             write(*,'(A)',advance='no') '   (A^-1)   '
          enddo
       endif
       if (incPol) then
          write(*,'(A)',advance='no') '   (A^-1)   '
       endif
    endif
    
    write(*,'(A)') ''
  end subroutine writeLabelStr
  
  subroutine writeParmStr(parmClass, st1, st2)
    integer :: parmClass, st1, st2
    integer :: sptype
    integer :: i, j
    
    sptype = typemap(st1,st2)

    if (parmClass == PC_s) then
       if (incInter(st1,F_els)) then
          write(*,'(A,F12.7,A)',advance='no') ' ',stcharge(st1),' '
       else
          write(*,'(A)',advance='no') '              '
       endif
       if (incPol) then
          if (incInter(st1,F_pol)) then
             write(*,'(A,F12.7,A)',advance='no') '  ',pz(st1),'  '
          else
             write(*,'(A)',advance='no') '                '
          endif
       endif
    endif
    
    if (comRuleExp == parmClass) then
       if (incInter(st1,F_exp) .and. incInter(st2,F_exp)) then
          write(*,'(A,E12.5,A)',advance='no') ' ',exp(alpha(sptype))*h2kcal,' '
          write(*,'(A,F12.7,A)',advance='no') ' ',beta(sptype)*A2bohr,' '
       else
          write(*,'(A)',advance='no') '              '
          write(*,'(A)',advance='no') '              '
       endif
    endif
    if (comRuleLin == parmClass) then
       do i = 1,oPoly
          if (incInter(st1,F_exp) .and. incInter(st2,F_exp)) then
             write(*,'(A,E12.5,A)',advance='no') ' ',a(i,sptype)*A2bohr**i,' '
          else
             write(*,'(A)',advance='no') '              '
          endif
       enddo
    endif
    if (comRuleVdw == parmClass) then
       ival = cstart
       do i = 1,ndisp
          if (incInter(st1,F_vdw) .and. incInter(st2,F_vdw)) then
             write(*,'(A,E12.5,A)',advance='no') '  ',C(sptype,i)*h2kcal*bohr2A**ival,'   '
          else
             write(*,'(A)',advance='no') '                 '
          endif
          ival = ival + 2
       enddo
    endif
    if (incCore .and. parmClass == PC_sp) then
       write(*,'(A,E12.5,A)',advance='no') '  ',A12(sptype)*h2kcal*bohr2A**12,'   '
    endif
    if (comRuleDmp == parmClass) then
       if (incElsDmp) then
          if ((incInter(st1,F_els) .and. (incInter(st2,F_els) .or. incInter(st2,F_pol))) .or. &
             (incInter(st2,F_els) .and. (incInter(st1,F_els) .or. incInter(st1,F_pol)))) then
             write(*,'(A,F10.7,A)',advance='no') ' ',delta1(sptype)/bohr2A,' '
          else
             write(*,'(A)',advance='no') '              '
          endif
       endif
       if (incVDWDmp) then
          do i = 1,ndisp
             if (incInter(st1,F_vdw) .and. incInter(st2,F_vdw)) then
                write(*,'(A,F10.7,A)',advance='no') ' ',deltan(sptype,i)/bohr2A,' '
             else
                write(*,'(A)',advance='no') '              '
             endif
          enddo
       endif
       if (incPol) then
          if (incInter(st1,F_pol) .and. incInter(st2,F_pol)) then
             write(*,'(A,F10.7,A)',advance='no') ' ',deltap(sptype)/bohr2A,' '
          else
             write(*,'(A)',advance='no') '              '
          endif
       endif
    endif
    
    write(*,'(A)') ''
  end subroutine
  
end program gen_report

