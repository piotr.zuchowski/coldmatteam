
program get_nfreeparm
  use const
  use dimer
  use force_field_gen
  
  integer :: i
  character(200) :: cbuf(10)
  character(200) :: name, input_file
  integer :: comModeExp, comModeLin
  
  do i = 1,10
     call getarg(i,cbuf(i))
  enddo
  name = cbuf(1)
  read(cbuf(2),*) comModeExp
  read(cbuf(3),*) comModeLin
  read(cbuf(4),*) opoly
  
  input_file = trim(name)//'.input_long'
  call read_dimer(input_file)
  
  write(*,*) initFreeParm(comModeExp, comModeLin)
  
end program get_nfreeparm
