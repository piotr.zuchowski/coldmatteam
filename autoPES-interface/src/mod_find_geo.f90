
! Module for determining standard coordinates given Cartesian coordinates
module find_geo
  use const
  use dimer
  use utility
  use dgrid
  use powell
  implicit none
  
  integer, private :: vectLen
  real(8), private :: s1(2,MAX_NSITE,3)
  real(8), private :: dist
  real(8), private :: gpoint(MAX_NCOOR)
  
contains
  
  ! Function called by the optimizer for dimer orientation
  function errFunc(x, n)
    integer :: n
    real(8) :: x(n)
    real(8) :: errFunc
    
    integer :: i, j, k
    integer :: index
    
    gpoint(C_rcom) = dist
    gpoint(C_betaA:C_gammaB) = x(1:5)
    index = 6
    do i = 1,2
       do j = 1,nmcPhys(i)
          if (mctype(i,j) /= MC_CHI) then
             gpoint(C_mon(i) + j) = x(index)
             index = index + 1
          endif
       enddo
    enddo
    call moveDimerGP(gpoint)
    
    errFunc = 0.0d0
    do i = 1,natom(1)
       do j = 1,natom(2)
          errFunc = errFunc + abs(r12(sit(1,i,:), sit(2,j,:)) - &
               r12(s1(1,i,:), s1(2,j,:)))**2.0d0
       enddo
    enddo
    
    errFunc = sqrt(errFunc/(natom(1)*natom(2)))
  end function errFunc
  
  
  function r12(r1, r2)
    real(8) :: r1(3), r2(3)
    real(8) :: r12
    
    r12 = sqrt((r1(1) - r2(1))**2 + (r1(2) - r2(2))**2 + (r1(3) - r2(3))**2)
  end function r12
  
  
  ! Performs least-square error matching to the geometery given in xyz.
  ! The resulting 'geo' contains standard coordinates of the matched geometry.
  ! Returns the RMSD value between the two geometries in bohr.
  function findGeo(xyz, geo)
    real(8) :: findGeo
    real(8), intent(in) :: xyz(2,MAX_NSITE,3)
    real(8), intent(out) :: geo(ncoor)
    
    integer :: i, j, k
    real(8) :: mass(2), COM(2,3)
    real(8) :: val, err, minErr
    real(8), allocatable :: vect(:)
    integer :: chiral(2)
    
    vectLen = ncoor - 1
    do i = 1,2
       chiral(i) = 0
       do j = 1,nmcPhys(i)
          if (mctype(i,j) == MC_CHI) chiral(i) = j
       enddo
       if (chiral(i) /= 0) vectLen = vectLen - 1
    enddo
    
    allocate( vect(vectLen) )
    
    s1 = xyz
    
    mass(:) = 0.0d0
    COM(:,:) = 0.0d0
    do i = 1,2
       do j = 1,natom(i)
          val = atomMass(sanum(i,j))
          mass(i) = mass(i) + val
          do k = 1,3
             COM(i,k) = COM(i,k) + val*s1(i,j,k)
          enddo
       enddo
       COM(i,:) = COM(i,:)/mass(i)
    enddo
    
    dist = r12(COM(1,:), COM(2,:))
    
    minErr = 1.0d100
    
    ! Loop over various starting configurations of Euler angles
    do i = 0,31
       
       ! Loop over possible combinations of chirality
       do j = 0,1
          do k = 0,1
             if ((j == 0 .or. chiral(1) /= 0) .and. (k == 0 .or. chiral(2) /= 0)) then
                
                if (chiral(1) /= 0) gpoint(C_mon(1)+chiral(1)) = real(j)
                if (chiral(2) /= 0) gpoint(C_mon(2)+chiral(2)) = real(k)
                
                vect(:) = 0.0d0
                vect(1) = pi/3 + pi/3*modulo(i/2**0, 2)
                vect(2) = pi*modulo(i/2**1, 2)
                vect(3) = pi*modulo(i/2**2, 2)
                vect(4) = pi/3 + pi/3*modulo(i/2**3, 2)
                vect(5) = pi*modulo(i/2**4, 2)
                
                call praxis(30, 0.0d0, 1.0d-1, vectLen, 0, vect, errFunc, 0, .false.)
                err = errFunc(vect,vectLen)
                if (err < minErr) then
                   minErr = err
                   geo(1:ncoor) = gpoint(1:ncoor)
                endif
                
             endif
          enddo
       enddo
       
    enddo
    
    call fixDimerAngles(0.0d0, geo(C_betaA), geo(C_gammaA), geo(C_alphaB), geo(C_betaB), geo(C_gammaB))
    findGeo = minErr
    
    deallocate( vect )
  end function findGeo
  
end module find_geo

