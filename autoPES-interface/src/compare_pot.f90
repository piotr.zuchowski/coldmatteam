
! Computes cross RMSE of two potentials on a randomly generated set of 10000 points,
!    with energies < 0 according to the first potential

program compare
  use const
  use dimer
  use force_field
  use dgrid
  implicit none
  
  character(200) :: name, input_file, fit1_file, fit2_file
  real(8) :: sum
  real(8), allocatable :: grid(:,:), grid0(:,:)
  integer :: ngrid, ngrid0
  integer :: i, j
  
  call getarg(1, name)
  call getarg(2, fit1_file)
  call getarg(3, fit2_file)
  
  input_file=trim(name)//'.input_long'
  
  call read_dimer(input_file)
  call ff_init(fit1_file, 2)
  
  call initgrid('', grid0, ngrid0, 1)
  ngrid = 10000
  allocate( grid(ngrid,ngind) )
  call make_grid(grid0, ngrid0, grid, ngrid, 0.0d0, 0.0d0, .false., 0.0d0, -1.0d100, 0.0d0, 0.0d0)
  
  do i = 1,ngrid
     call moveDimerGP(grid(i,:))
     grid(i,C_Etot) = energy_ff()
  enddo
  call ff_init(fit2_file, 2)
  sum = 0.0d0
  do i = 1,ngrid
     call moveDimerGP(grid(i,:))
     sum = sum + (grid(i,C_Etot) - energy_ff())**2
  enddo
  sum = sqrt(sum/ngrid)
  
  write(*,'(F12.6)') sum
  
end program compare
