
program make_orca_inp
 use file_dir
 use dimer
 use dgrid
 use make_inp
 use basis
 implicit none
 
 integer, parameter :: MCBS_OFFSET = 50
 integer :: i, j
 character(len=1000) :: bas_dir
 character(200) :: arg(10)
 character(100) :: name_in, inpmode, bs_type, aux_bs_type
 character(100) :: mol_file_A, mol_file_B, mol_file_MA, mol_file_MB, mol_file_D
 character(100) :: constr_file, aux_file, aux_fileA, aux_fileB, inp_file, geo_file, input_file
 logical :: hybrid, dft, deltahf, distr, supmol, incMB
 integer :: ncores, maxmem, ngrid
 real(8) :: mbxyz(3)
 real(8) :: ksi, mbcutoff
 real(8), allocatable :: grid(:,:)
 
 do i = 1,10
    call getarg(i, arg(i))
 enddo
 read(arg(1),*) name_in
 call getarg(2, bas_dir)
 call getarg(3, bs_type)
 call getarg(4, aux_bs_type)
 read(arg(5),*) hybrid
 read(arg(6),*) inpmode
 read(arg(7),*) mbcutoff
 read(arg(8),*) ncores
 read(arg(9),*) maxmem
 
 call initBasis(bas_dir, bs_type)
 
 dft = .false.
 deltahf = .false.
 distr = .false.
 supmol = .false.
 if (inpmode == 'dft') then
    dft = .true.
 elseif (inpmode == 'deltahf') then
    deltahf = .true.
 elseif (inpmode == 'distr') then
    distr = .true.
 elseif (inpmode == 'supmol') then
    supmol = .true.
 else
    write(*,*) 'ERROR: Invalid input mode'
    stop
 endif
!
 mol_file_A   = trim(name_in)//'A'//'.data'
 mol_file_B   = trim(name_in)//'B'//'.data'
 mol_file_MA  = trim(name_in)//'MA'//'.data'
 mol_file_MB  = trim(name_in)//'MB'//'.data'
 mol_file_D   = trim(name_in)//'.data'
 P_file       = trim(name_in)//'P.data'
 dispinp_file = trim(name_in)//'.dispinp'
 aux_file     = trim(name_in)//'.aux'
 aux_fileA    = trim(name_in)//'A.aux'
 aux_fileB    = trim(name_in)//'B.aux'
 inp_file     = trim(name_in)//'.inp'
 constr_file  = 'constr.para'
 input_file   = trim(name_in)//'.input_long'
 geo_file     = trim(name_in)//'.geo'
 
 call read_dimer(input_file)
 
 if (dft .or. deltahf .or. supmol) then
    call initGrid(geo_file, grid, ngrid, 0)
    call moveDimerGP(grid(1,:))
    incMB = placemb(mbxyz, MBCutoff)
 else
    incMB = .false.
 endif

 if (supmol) then
    open(41,file=inp_file)
    call write_supmol_file(41, ncores, incMB, mbxyz, maxmem)
    close(41)
    stop
 endif
 
! FILES GENERATION
 if (distr) then
    open(unit=41,file=constr_file)
    call write_constr_file(41)
    close(41)
 endif
 
 ! ORCA .data
 open(unit=31,file=trim(mol_file_A))
 call write_file_data(31, 1, ncores, deltaHF, hybrid, .false., distr, incMB, mbxyz)
 close(31)
 if (.not. homog .or. .not. distr) then
    open(unit=32,file=trim(mol_file_B))
    call write_file_data(32, 2, ncores, deltaHF, hybrid, .false., distr, incMB, mbxyz)
    close(32)
 endif
 if (deltahf) then
    open(unit=33,file=trim(mol_file_D))
    call write_file_data(33, 0, ncores, .true., hybrid, .false., .false., incMB, mbxyz)
    close(33)
 elseif (dft) then
    open(unit=33,file=trim(mol_file_MA))
    call write_file_data(33, 1, ncores, .false., hybrid, .true., .false., incMB, mbxyz)
    close(33)
    open(unit=34,file=trim(mol_file_MB))
    call write_file_data(34, 2, ncores, .false., hybrid, .true., .false., incMB, mbxyz)
    close(34)
 endif
 
 ! SAPT main input
 open(unit=42,file=trim(P_file))
 call write_P(42, hybrid, dft, deltahf, distr, incMB, maxMem)
 close(42)

 ! Auxiliary basis
 if (dft .or. deltahf) then
    call initBasis(bas_dir, aux_bs_type)
    open(unit=43,file=aux_file)
    call write_aux(43, 43, 43, bs_type, incMB, mbxyz)
    close(43)
 elseif (distr) then
    call initBasis(bas_dir, aux_bs_type)
    open(unit=43,file=aux_fileA)
    open(unit=44,file=aux_fileB)
    call write_aux(43, 44, 0, bs_type, incMB, mbxyz)
    close(43)
    close(44)
 endif
 
 ! geoparm.d
 if (distr) then
    call initGrid(geo_file, grid, ngrid, 0)
    open(unit=43,file='geoparm.d')
    call write_geoparm(grid, ngrid, 43)
    close(43)
 endif
 
 ! dimer.cnf
 if (distr) then
    open(unit=43,file='dimer.cnf')
    call write_dimer_file(43)
    close(43)
 endif
 
contains

subroutine write_file_data(ifile, iX, &
     ncores, isHF, isHybrid, isMonomer, isPureMon, incMB, mbxyz)
 use dimer
 
 implicit none

 integer, intent(in) :: ifile, iX, ncores
 logical, intent(in) :: isHF, isHybrid, isMonomer, isPureMon, incMB
 real(8), intent(in) :: mbxyz(3)
 integer :: i, j, k, name_len, ibasis, natomtot, moncharge
 real(8) :: atomCharge
 logical :: exists(300)
 logical :: cut
 character*8 :: xx
 character*100 :: method
 
 ! Print header
 if (isHF) then
    method = 'HF RIJK AutoAux'
 else
    if (isHybrid) then
       method = 'PBE0 RIJK AutoAux'
    else
       method = 'PBE RI AutoAux'
    endif
 endif
 write(ifile,*) '!  TightSCF '//trim(BS_TYPE)//' '//trim(method)//' bohrs '
 write(ifile,*)
 if (.not. isHF) then
    write(ifile,*) '%method'
    write(ifile,*) 'Grid 4 FinalGrid 4'
    write(ifile,*) 'gracLB true'
    write(ifile,*) 'ip ', IP(iX)
    write(ifile,*) 'end'
    write(ifile,*)
 endif
 if (ncores > 1) then
    write(ifile,'(A,I2)') '%pal nprocs ',ncores
    write(ifile,*) 'end'
    write(ifile,*)
 endif
 write(ifile,'(A4)') 'META'
 
 ! Print geometry
 if (isPureMon) then
    natomtot = natom(iX)
 else
    natomtot = natom(1)+natom(2)
 endif
 if (incMB) natomtot = natomtot + 1
 if (iX == 0) then
    moncharge = molchar(1) + molchar(2)
 else
    moncharge = molchar(iX)
 endif
 write(ifile,*) natomtot, moncharge, 1
 do i = 1,2
    if (.not. isPureMon .or. iX == i) then
       do j = 1,natom(i)
          if (isMonomer .and. iX /= i) then
             ibasis = sanum(i,j) + MCBS_OFFSET
          else
             ibasis = sanum(i,j)
          endif
          xx = '  '//trim(alabel(i,j))//monlabelL(i)
          write(ifile,'(I4,3F25.16,I3,A,L,A)') &
               ibasis, (sit(i,j,k),k=1,3), sanum(i,j), ' ', iX == i .or. iX == 0, xx ! atoms_A(i)
       enddo
    endif
    if (incMB .and. i == 1) then
       write(ifile,'(I4,3F25.16,I3,A)') MCBS_OFFSET, (mbxyz(k),k=1,3), 1, ' F  Mb'
    endif
 enddo
 
 ! Print basis
 exists(:) = .false.
 do i = 1,2
    if (.not. isPureMon .or. iX == i) then
       do j = 1,natom(i)
          if (isMonomer .and. iX /= i) then
             ibasis = sanum(i,j) + MCBS_OFFSET
             cut = .true.
          else
             ibasis = sanum(i,j)
             cut = .false.
          endif
          if (.not.exists(ibasis)) then
             call write_bas(sanum(i,j), ifile, cut)
             exists(ibasis) = .true.
          end if
       enddo
       
       if (i == 1 .and. incMB) then
          call write_bas(0, ifile, .false.)
       endif
    endif
 enddo
 
 return
end subroutine write_file_data


subroutine write_bas(qq, iout, cut)
  use dimer
  use basis
  implicit none
  
  integer, intent(in) :: qq, iout
  logical, intent(in) :: cut
  integer :: ibasis, nincContr
  integer :: i, j
  
  call setBasis(qq)
  
  nincContr = 0
  do i = 1,ncontr
     if (.not. cut .or. incMCBS(i)) nincContr = nincContr + 1
  enddo
  
  ibasis = qq
  if (cut .or. qq == 0) ibasis = ibasis + MCBS_OFFSET
  
  write(iout,'(A,I5)') 'BAS ',ibasis
  write(iout,'(I5)') nincContr
  do i = 1,ncontr
     if (.not. cut .or. incMCBS(i)) then
        write(iout,'(I5,I5)') lcontr(i),nprim(i)
        do j = 1,nprim(i)
           write(iout,'(E15.8,A,E15.8)') basexp(i,j),' ',bascoef(i,j)
        enddo
     endif
  enddo
end subroutine


subroutine write_P(iout, hybrid, dft, deltahf, distr, incMB, maxMem)
 use dimer
 implicit none
 
 integer, intent(in) :: iout
 logical, intent(in) :: hybrid, dft, deltahf, distr, incMB
 integer, intent(in) :: maxMem
 
 character(1000) :: basisstr, tagsstr
 integer :: ncontrTot
 integer :: bsf(2), occ(2)
 integer :: i, j, k
 
 bsf(:) = 0
 occ(:) = 0
 basisstr = ''
 tagsstr = ''
 do i = 1,2
    do j = 1,natom(i)
       occ(i) = occ(i) + sanum(i,j)
       
       call setBasis(sanum(i,j))
       do k = 1,ncontr
          bsf(i) = bsf(i) + 2*lcontr(k) + 1
          
          basisstr = trim(basisstr)//orbSymbol(lcontr(k))
          if (incMCBS(k)) then
             tagsstr = trim(tagsstr)//'m'
          else
             tagsstr = trim(tagsstr)//monlabelL(i)
          endif
       enddo
    enddo
    occ(i) = (occ(i) + 1)/2
    
    if (i == 1 .and. incMB) then
       call setBasis(0)
       do k = 1,ncontr
          basisstr = trim(basisstr)//orbSymbol(lcontr(k))
          tagsstr = trim(tagsstr)//'m'
       enddo
    endif
 enddo
 
 write(iout,*) bsf(1)+bsf(2), bsf(1), occ(1), bsf(2), occ(2)
 write(iout,*)
 write(iout,*) '&TRN'
 write(iout,*) ' ISITG03=F, ISITORCA=T,'
 write(iout,*) ' ISITALCH=F, ISITG88=F, ISITG90=F, ISITHNDO=F, ISITMICR=F,'
 write(iout,*) ' ISITATM=F, ISITCADP=F, ISITGAMS=F, ISITDALT=F, T2EL=F,'
 if (distr) then
    write(iout,*) ' BLKMB=T,'
 else
    write(iout,*) ' BLKMB=F,'
 endif
 if (deltahf) then
    write(iout,*) ' DIMER=T,'
 else
    write(iout,*) ' DIMER=F,'
 endif
 write(iout,'(A,I0,A)') ' OUT=F, TOLER=15, SPHG=T, MEMTRAN=',maxMem,'M,'
 if (dft) then
    write (iout,'(A)') "  BASIS='"//trim(basisstr)//"+'"
    write (iout,'(A)') "   TAGS='"//trim(tagsstr)//"+'"
 endif
 write(iout,*) '&END'
 write(iout,*)
 write(iout,*) '&INPUTCOR'
 if (deltahf) then
    write(iout,*) ' DELTASCF=T, CKSIND=T,'
 else
    write(iout,*) ' SAPTKS=T,'
    if (.not. hybrid) write(iout,*) ' FASTDISP=T,'
 endif
 write(iout,'(A,I0,A)') ' PRINT=F, TIMEREP=T, MEMSAPT=',maxMem,'M'
 write(iout,*) '&END'
 write(iout,*)
 if (dft) then
    write(iout,*) '&SAPTDFT'
    write(iout,*) ' CKSDISP=T, CKSIND=T'
    write(iout,'(A,I0,A)') ' MAXMEM=',maxMem,'M'
    write(iout,*) '&END'
    write(iout,*)
 endif
 write(iout,*) '&DF'
 if (distr) then
    write(iout,'(A,I0,A)') '  mode=25, MEMTRAN=',maxMem,'M, SKIPPUREMON=F'
 endif
 write(iout,*) '&END'
 
 ! kernel.data
 open(unit=51,file='kernel.data')
 write(51,'(3I12)') 40, 40, 200*(occ(1) + occ(2))**3
 if (deltahf) then
    ksi = 1.0d0
 elseif (hybrid) then
    ksi = 0.25d0
 else
    ksi = 0.0d0
 endif
 write(51,'(F12.5)') ksi
 close(51)
 
 return
end subroutine write_P

subroutine write_supmol_file(iout, ncores, incMB, mbxyz, maxmem)
  integer, intent(in) :: iout, ncores, maxmem
  logical, intent(in) :: incMB
  real(8), intent(in) :: mbxyz(3)
  
  integer :: i, j, k
  character(1) :: ghostChar(2)
  character(40) :: idstr
  integer :: molCharge
  
  do i = 1,3
     if (i == 1) then
        ghostChar = [' ',':']
        molCharge = molchar(1)
        idstr = 'monomerA'
     elseif (i == 2) then
        ghostChar = [':',' ']
        molCharge = molchar(2)
        idstr = 'monomerB'
     elseif (i == 3) then
        ghostChar = [' ',' ']
        molCharge = molchar(1) + molchar(2)
        idstr = 'dimer'
     endif
     
     write(iout,'(A)') '! TightSCF bohrs PMODEL NOAUTOSTART MO-CCSD(T) RIJK '//trim(bs_type)
     write(iout,'(A)') '%id "'//trim(idstr)//'"'
     if (maxmem > 0) write(iout,'(A,I10)') '%maxcore ',maxmem*8
     if (ncores > 1) then
        write(iout,'(A,I2)') '%pal nprocs ',ncores
        write(iout,*) 'end'
     endif
     write(iout,'(A,2I5)') '* xyz ',molCharge,1
     do j = 1,2
        do k = 1,natom(j)
           write(iout,'(A,A,A,3F18.10)') asymbol(sanum(j,k)),ghostchar(j),' ',sit(j,k,1:3)
        enddo
        if (j == 1 .and. incMB) then
           write(iout,'(A,3F18.10)') 'H : ',mbxyz(1:3)
        endif
     enddo
     write(iout,'(A)') '*'
     
     if (i == 1 .or. i == 2) then
        write(iout,'(A)') ''
        write(iout,'(A)') '$new_job'
     endif
  enddo
end subroutine write_supmol_file

end program make_orca_inp

