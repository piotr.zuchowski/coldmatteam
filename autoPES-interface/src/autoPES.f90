program AUTOPES
  use const
  implicit none
 
 namelist /general/ test_pct, test_conv, min_gridgen_iter
 namelist /ab_initio/ method, basis, auxbasis, hybrid, deltahf, mb_cutoff
 namelist /grid_generation/ force_field, grid_size, grid_minima_pct, grid_minima_pct_add, &
      grid_add_pct, grid_weight_scale, probgrid, grid_mode, frac_r0
 namelist /asymptotics/ polarizable, max_cn, distr, com_rule_cn
 namelist /main_fit/ poly_order, fit_weight_scale, com_rule_damp, &
      com_rule_exp, els_dmp, cn_dmp, fit_A12, auto_reduce, &
      com_rule_lin
 namelist /hole_fixing/ wall_height, wall_min_dist, hole_add_pct
 namelist /sys_settings/ max_sim_pt, ncores, ncores_scf, ncores_sapt, &
      npart_per_job, sapt_timeout, sapt_queue_timeout, sapt_mem
 namelist /fit_intra/ inc_intra
 
 integer :: i
 character(1000) :: line
 integer :: na, nb
 character(100) :: outArgs(100)
 character(100) :: input, path, geo_input
 integer :: pathlen
 real(8) :: val
 logical :: same
 character(20) :: jobname, run_mode
 
 ! GENERAL
 real(8) :: test_conv = 0.2d0
 integer :: test_pct = 20
 integer :: min_gridgen_iter = 2
 ! AB_INITIO
 character(20) :: method = 'SAPTDFT'
 character(30) :: basis = 'aug-cc-pVDZ'
 character(30) :: auxbasis = ''
 logical :: hybrid = .false.
 character(10) :: deltahf = 'AUTO'
 real(8) :: mb_cutoff = 0.8d0
 ! GRID_GENERATION
 character(20) :: force_field = 'oplsaa'
 integer :: grid_size = 0
 integer :: grid_minima_pct = 10
 integer :: grid_minima_pct_add = 20
 integer :: grid_add_pct = 30
 real(8) :: grid_weight_scale = 0.8d0
 logical :: probgrid = .false.
 character(20) :: grid_mode = 'metric'
 integer :: frac_r0 = 0.2
! ASYMPTOTICS
 integer :: max_cn = 8
 logical :: polarizable = .false.
 logical :: distr = .false.
 integer :: com_rule_cn = 1
! MAIN_FIT
 integer :: poly_order = 2
 real(8) :: fit_weight_scale = 5.0d0
 integer :: com_rule_damp = 1
 integer :: com_rule_exp = 1
 integer :: com_rule_lin = 0
 logical :: els_dmp = .false.
 logical :: cn_dmp = .true.
 logical :: fit_A12 = .false.
 real(8) :: auto_reduce = 2.0d0
! HOLE_FIXING
 real(8) :: wall_height = 15.0d0
 integer :: hole_add_pct = 10
 character(20) :: wall_min_dist = 'AUTO'
! SYS_SETTINGS
 integer :: max_sim_pt = 1
 integer :: ncores = 1
 integer :: ncores_sapt = 1
 integer :: ncores_scf = 0
 integer :: npart_per_job = 1
 integer :: sapt_timeout = 0
 integer :: sapt_queue_timeout = 0
 integer :: sapt_mem = 0
! FIT_INTRA
 logical :: inc_intra = .false.
 
 
 call getarg(0,path)
 pathlen = len(trim(path)) - 8
 path = path(1:pathlen)
 
 call getarg(1,jobname)
 input = trim(jobname) // '.ctrl'
 geo_input = trim(jobname) // '.input'
 
 call getarg(2,run_mode)
 if (trim(run_mode) == '') run_mode = 'normal'
 
 call read_geo(geo_input)
 
 open(unit=10,file=input)
 rewind(10)
 read(10,NML=general,end=1001)
1001 continue
 rewind(10)
 read(10,NML=ab_initio,end=1002)
1002 continue
 rewind(10)
 read(10,NML=grid_generation,end=1003)
1003 continue
 rewind(10)
 read(10,NML=asymptotics,end=1004)
1004 continue
 rewind(10)
 read(10,NML=main_fit,end=1005)
1005 continue
 rewind(10)
 read(10,NML=hole_fixing,end=1006)
1006 continue
 rewind(10)
 read(10,NML=sys_settings,end=1007)
1007 continue
 rewind(10)
 read(10,NML=fit_intra,end=1008)
1008 continue
 close(10)

 
 if (auxbasis == '') then
    auxbasis = trim(basis)//'-RI'
 endif
 
 if (method /= 'SAPTDFT' .and. method /= 'CCSDT') then
    write(*,*) 'Error: Invalid method'
    stop
 endif
 
 if (max_cn /= 6 .and. max_cn /= 8 .and. max_cn /= 10) then
    write(*,*) 'ERROR: The value of max_cn =',max_cn,' is not supported'
    write(*,*) 'Supported values are: 6, 8, 10'
    stop
 end if
 
 if (deltahf /= 'T' .and. deltahf /= 'F' .and. deltahf /= 'AUTO') then
    write(*,*) "deltaHF must be set to 'T', 'F', or 'AUTO'"
    stop
 endif
 
 if (wall_min_dist /= 'AUTO') then
    read(wall_min_dist,*,ERR=100) val
    if (val <= 0.0d0) goto 100
    goto 200
    100 continue
    write(*,*) "WALL_MIN_DIST must be either a positive real number or 'AUTO'"
    stop
 endif
 200 continue
 
 if (probgrid) min_gridgen_iter = max(min_gridgen_iter, 3)
 
 if (na == 1 .or. nb == 1) then
    com_rule_exp = PC_sp
    com_rule_lin = PC_sp
    com_rule_damp = PC_sp
    com_rule_cn = PC_sp
 endif
 
 if (sapt_timeout == 0) sapt_timeout = 999999
 if (sapt_queue_timeout == 0) sapt_queue_timeout = 999999
 !if (max_mem == 0) max_mem = 999999
 
 !if (ncores_sapt == 0) ncores_sapt = ncores/npart_per_job
 if (ncores_scf == 0) ncores_scf = ncores_sapt
 
 if (test_conv > 0.0d0) then
    test_conv = test_conv + 1.0d0
 else
    test_conv = 0.0d0
 endif

 outArgs(:) = ''
 write(outArgs( 1),*) jobname
 write(outArgs( 2),*) run_mode
 write(outArgs( 3),*) same
 write(outArgs( 4),*) na
 write(outArgs( 5),*) nb
 write(outArgs( 6),*) basis
 write(outArgs( 7),*) max_cn
 write(outArgs( 8),*) grid_size
 write(outArgs( 9),*) force_field
 write(outArgs(10),*) max_sim_pt
 write(outArgs(11),*) ncores
 write(outArgs(12),*) ncores_scf
 write(outArgs(13),*) sapt_timeout
 write(outArgs(14),*) sapt_queue_timeout
 write(outArgs(15),*) sapt_mem
 write(outArgs(16),*) wall_height
 write(outArgs(17),*) npart_per_job
 write(outArgs(18),*) test_pct
 write(outArgs(19),*) poly_order
 write(outArgs(20),*) grid_minima_pct
 write(outArgs(21),*) grid_add_pct
 write(outArgs(22),*) hole_add_pct
 write(outArgs(23),*) fit_weight_scale
 write(outArgs(24),*) test_conv
 write(outArgs(25),*) com_rule_damp
 write(outArgs(26),*) hybrid
 write(outArgs(27),*) com_rule_exp
 write(outArgs(28),*) grid_minima_pct_add
 write(outArgs(29),*) min_gridgen_iter
 write(outArgs(30),*) distr
 write(outArgs(31),*) polarizable
 write(outArgs(32),*) deltahf
 write(outArgs(33),*) grid_weight_scale
 write(outArgs(34),*) wall_min_dist
 write(outArgs(35),*) els_dmp
 write(outArgs(36),*) cn_dmp
 write(outArgs(37),*) ncores_sapt
 write(outArgs(38),*) fit_A12
 write(outArgs(39),*) auto_reduce
 write(outArgs(40),*) probgrid
 write(outArgs(41),*) com_rule_lin
 write(outArgs(42),*) com_rule_cn
 write(outArgs(43),*) mb_cutoff
 write(outArgs(44),*) method
 write(outArgs(45),*) grid_mode
 write(outArgs(46),*) inc_intra
 write(outargs(47),*) auxbasis
 write(outargs(48),*) frac_r0
 
 line = trim(path) // '/master.sh'
 do i = 1,100
    if (outArgs(i) /= '') then
       line = trim(line) // ' ' // trim(outArgs(i))
    endif
 enddo
 call system(trim(line))
 
contains
  
  subroutine read_geo(geo_input)
    character(100) :: geo_input
    
    character(1000) :: line
    integer :: i
    
    open(100,file=geo_input,status='old')
    
    line = ''
    do while (line /= '*MONOA')
       read(100,'(A)',end=100,err=100) line
    enddo
    read(100,*) na
    
    line = ''
    do while (line /= '*MONOB')
       read(100,'(A)',end=100,err=100) line
    enddo
    read(100,'(A)') line
    
    same = (trim(line) == 'SAME')
    if (same) then
       nb = na
    else
       read(line,*) nb
    endif
    
    close(100)
    return
    
100 continue
    write(*,*) 'ERROR: Unable to find monomer input section in input file'
    stop
  end subroutine read_geo

end program AUTOPES
