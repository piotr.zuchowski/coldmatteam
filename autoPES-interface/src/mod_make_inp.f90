
! Contains functionality common to input generation for all interfaces
module make_inp
 implicit none
 
 real(8), parameter :: eps_scf = 5.0d-8
 real(8), parameter :: r_grid = 1.0d-13   ! RADINT - quality of the radial part of the grid (default = 1.0d-13)
 integer, parameter :: a_grid = 35        ! ANGINT - quality of the angular Lebedev grid (default = 35)
 real(8), parameter :: eps_safe = 0.01d0  ! DFTELS - safety threshold - stop if the charge integration gives too large error (no default)
 real(8), parameter :: eps_ao = 1.0d-10   ! AO DELETE - limit for basis set numerical linear dependences (default = 1.0D-6)
 real(8), parameter :: cmomax = 5.0d06    ! CMOMAX - abort calculations if the absolute value of any initial MO coefficient
                                          !          is greater than CMOMAX (default = 1000)
 
contains
  
  
  subroutine write_constr_file(unit)
    use dimer
    
    integer :: unit
    
    write(unit,*) " &CNTR"
    write(unit,*) "  eta = 0.002,"
    write(unit,*) "  lambda = 1000.0,"
    write(unit,*) "  gamma = 1.0"
    write(unit,*) " &END"
    write(unit,*) " &SLV"
    write(unit,*) "  solver_op=12"
    write(unit,*) " &END"
    write(unit,*) " &PROP"
    write(unit,*) "  NQUAD=10,"
    write(unit,*) "  isDistAsym=T"
    write(unit,*) " &END"
    write(unit,*) " &AUX_BASIS"
    write(unit,*) "  aux_type='PMCBS'"
    write(unit,*) " &END"
    write(unit,*) " &MULTIPOLE"
    write(unit,*) "  rank=2"
    write(unit,'(A,I3)') "  chargeA=",molchar(1)
    write(unit,'(A,I3)') "  chargeB=",molchar(2)
    write(unit,*) " &END"
  end subroutine write_constr_file
  
  
  subroutine write_aux(ioutA, ioutB, ioutM, bs_type, incMB, mbxyz)
    use dimer
    use const
    use utility
    use basis
    
    integer, intent(in) :: ioutA, ioutB, ioutM
    logical, intent(in) :: incMB
    real(8), intent(in) :: mbxyz(3)
    character(*), intent(in) :: bs_type
    integer :: i, j, k, l
    integer :: iout, nline
    character(1000) :: line
    
    do i = 1,2
       if (i == 1) then
          iout = ioutA
       else
          iout = ioutB
       endif
       
       do j = 1,natom(i)
          write(iout,'(A)') '0 z'
          write(iout,'(I3,3(A,F18.10))') &
               sanum(i,j),' ',sit(i,j,1),' ',sit(i,j,2),' ',sit(i,j,3)
          
          call setBasis(sanum(i,j))
          do k = 1,ncontr
             write(iout,'(I5,A,A)') nprim(k),' ',orbSymbol(lcontr(k))
             do l = 1,nprim(k)
                write(iout,'(E15.8,A,E15.8)') basexp(k,l),' ',bascoef(k,l)
             enddo
          enddo
       enddo
       
       if (i == 1 .and. incMB) then
          write(ioutA,'(A)') '0 z'
          write(ioutA,'(I3,3(A,F18.10))') &
               0,' ',mbxyz(1),' ',mbxyz(2),' ',mbxyz(3)
          
          call setBasis(0)
          do k = 1,ncontr
             write(ioutM,'(I5,A,A)') nprim(k),' ',orbSymbol(lcontr(k))
             do l = 1,nprim(k)
                write(ioutM,'(E15.8,A,E15.8)') basexp(k,l),' ',bascoef(k,l)
             enddo
          enddo
       endif
    enddo
  end subroutine write_aux
  
  
  subroutine write_dimer_file(unit)
    use dimer
    
    integer :: unit
    integer :: i, j
    
    do i = 1,2
       write(unit,*) natom(i)
       do j = 1,natom(i)
          write(unit,'(3F15.10,I5,F15.8,A5)') &
               sit(i,j,1:3),sanum(i,j),atomMass(sanum(i,j)),trim(alabel(i,j))
       enddo
    enddo
  end subroutine write_dimer_file
  
  
  subroutine write_geoparm(grid, ngrid, unit)
    use dgrid
    
    real(8), intent(in) :: grid(:,:)
    integer, intent(in) :: ngrid, unit
    integer :: i
    
    do i = 1,ngrid
       write(unit,'(6F15.5,A)') &
            grid(i,C_rcom)*bohr2A, grid(i,C_betaA)*rad2d, grid(i,C_gammaA)*rad2d, &
            grid(i,C_alphaB)*rad2d, grid(i,C_betaB)*rad2d, grid(i,C_gammaB)*rad2d," 'DOIT'"
    enddo
  end subroutine write_geoparm
  
  
  ! Compute location of MB funciton using 1/R^6 weighting.
  ! Returns false if MB location is within MBCutoff times
  ! covalent distance of one of the atoms.
  logical function placeMB(coor, MBCutoff)
    use dimer
    use const
    
    real(8) :: coor(3)
    real(8) :: MBCutoff
    
    integer :: i, j, k
    real(8) :: sum, rab, minDiff
    real(8) :: weight(MAX_NSITE,MAX_NSITE)
    
    ! Generate the 1/r^6 weights for computation of the
    ! position of the bond functions....
    sum = 0.0d0
    do i = 1,natom(1)
       do j = 1,natom(2)
          rab = 0.0d0
          do k = 1,3
             rab = rab + (sit(1,i,k) - sit(2,j,k))**2
          end do
          weight(i,j) = 1.0d0/rab**3
          sum = sum + weight(i,j)
       end do
    end do
    ! Normalize weights
    weight(:,:) = weight(:,:)/sum
    ! Calculate the bond functions position:
    do k = 1,3
       coor(k) = 0.0d0
       do i = 1,natom(1)
          do j = 1,natom(2)
             coor(k) = coor(k) + weight(i,j)*0.5d0*(sit(1,i,k) + sit(2,j,k))
          end do
       end do
    end do
    
    ! Find closest (distance/r_cov) between MB function and atom
    minDiff = 1.0d100
    do i = 1,2
       do j = 1,natom(i)
          rab = 0.0d0
          do k = 1,3
             rab = rab + (sit(i,j,k) - coor(k))**2
          end do
          rab = sqrt(rab)
          minDiff = min(minDiff, rab/covRad(sanum(i,j)))
       enddo
    enddo
    
    placeMB = minDiff >= MBCutoff
  end function placeMB
  
end module make_inp

