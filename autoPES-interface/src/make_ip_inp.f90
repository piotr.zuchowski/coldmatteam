program IP_INPUT
 use dimer
 use const
 use file_dir
 implicit none
 character(len=*), parameter ::  orcaA0_file = 'A0.inp', orcaA1_file = 'A1.inp', &
                                 orcaB0_file = 'B0.inp', orcaB1_file = 'B1.inp'
 integer :: i, j, k
 character(100) :: name_in, input_file
 character(len=100) :: line, bs_type, ch_polarizable
 logical :: polarizable
 character(len=2) :: symbol
 character(len=1) :: monlabel
 
 call getarg(1,name_in)
 call getarg(2,bs_type)
 call getarg(3,ch_polarizable)
 read(ch_polarizable,*) polarizable
 
 input_file = trim(name_in)//'.input_long'
 call read_dimer(input_file)
 
 do i = 1,2
    if (i == 1) then
       monlabel = 'A'
    else
       if (homog) exit
       monlabel = 'B'
    endif
    
    open(unit=21,file=monlabel//'0.inp')
    write(21,*) '# Monomer '//monlabel//' neutral'
    write(21,*) '! RKS B3LYP ',trim(BS_TYPE),' Grid4 bohrs'
    if (polarizable) then
       write(21,*) '%elprop Dipole true'
       write(21,*) '        Quadrupole true'
       write(21,*) '        Polar 2'
       write(21,*) 'end'
    endif
    write(21,*) '%output'
    write(21,*) '  Print[ P_Mulliken ] 1'
    write(21,*) '  Print[ P_AtCharges_M ] 1'
    write(21,*) 'end'
    write(21,'(A,I3,A)') '* xyz ',molchar(i),' 1'
    do j = 1,natom(i)
       write(21,'(A4,3F24.16)') asymbol(sanum(i,j)), (sit0(i,j,k),k=1,3)
    end do
    write(21,*) '*'
    close(21)
    !
    open(unit=21,file=monlabel//'1.inp')
    write(21,*) '# Monomer '//monlabel//' ion'
    write(21,*) '! UKS B3LYP ',trim(BS_TYPE),' Grid4 bohrs'
    write(21,'(A,I3,A)') '* xyz ',molchar(i) + 1,' 2'
    do j = 1,natom(i)
       write(21,'(A4,3F24.16)') asymbol(sanum(i,j)), (sit0(i,j,k),k=1,3)
    end do
    write(21,*) '*'
    close(21)
 enddo
 !
end program IP_INPUT
