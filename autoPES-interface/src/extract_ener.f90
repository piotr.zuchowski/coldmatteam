
program extract
  use const
  use dimer
  use force_field
  use dgrid
  implicit none
  
  character(200) :: args(10)
  character(200) :: name, mode
  character(200) :: input_file, out_file, geo_file, ener_file
  character(200) :: enerstr(n_ener)
  logical :: hybrid
  real(8), allocatable :: grid(:,:)
  integer :: ngrid
  real(8) :: molener(3)
  integer :: i, j
  
  do i = 1,10
     call getarg(i, args(i))
  enddo
  name = args(1)
  out_file = args(2)
  mode = args(3)
  read(args(4),*) hybrid
  
  input_file=trim(name)//'.input_long'
  geo_file=trim(name)//'.geo'
  ener_file=trim(name)//'.ener'
  
  call read_dimer(input_file)
  
  call initGrid(geo_file, grid, ngrid, n_ener)
  if (ngrid == 0) then
     write(*,*) 'ERROR: Unable to read file ',trim(geo_file)
     stop
  endif
  grid(1:ngrid,C_ener+1:C_ener+n_ener) = 0.0d0
  
  if (mode == 'dft') then
     
     enerstr(:) = ''
     enerstr(E_tot)    = 'SAPT(DFT)total:'
     enerstr(E_els)    = 'Electrostatics:'
     enerstr(E_exh)    = 'Exchange:'
     enerstr(E_ind)    = 'Induction(CKS):'
     enerstr(E_exhind) = 'Exchange-induction(CKS):'
     enerstr(E_dsp)    = 'Dispersion(CKS):'
     if (hybrid) then
        enerstr(E_exhdsp) = 'Exchange-dispersion(CKS):'
     else
        enerstr(E_exhdsp) = 'Exch-disp(CKS/scaled)*:'
     endif
     do i = 1,n_ener
        if (enerstr(i) /= '') then
           call extractVals(enerstr(i), 3, 1, grid(1:1,C_ener+i), out_file)
        endif
     enddo
     
     call extractVals('FINAL SINGLE POINT ENERGY', 5, 2, molener, out_file)
     grid(1,C_ener+E_mon(1)) = molener(1)
     grid(1,C_ener+E_mon(2)) = molener(2)
     
  elseif (mode == 'deltahf') then
     
     call extractVals('\delta^{HF}_{int,r}', 3, 1, grid(1:1,C_ener+E_dhf), out_file)
     
  elseif (mode == 'supmol') then
     
     call extractVals('FINAL SINGLE POINT ENERGY', 5, 3, molener, out_file)
     grid(1,C_Etot) = h2kcal*(molener(3) - molener(1) - molener(2))
     grid(1,C_ener+E_mon(1)) = molener(1)
     grid(1,C_ener+E_mon(2)) = molener(2)
     
  elseif (mode == 'asym') then
     
     call extractVals('Electrostatic=', 3, ngrid, grid(1:ngrid,C_ener+E_els), out_file)
     call extractVals('Induction=', 3, ngrid, grid(1:ngrid,C_ener+E_ind), out_file)
     call extractVals('Dispersion=', 3, ngrid, grid(1:ngrid,C_ener+E_dsp), out_file)
     grid(1:ngrid,C_Etot) = grid(1:ngrid,C_ener+E_els) + grid(1:ngrid,C_ener+E_ind) + grid(1:ngrid,C_ener+E_dsp)
     
  else
     write(*,*) 'ERROR: invalid mode: ',trim(mode)
     stop
  endif
  
  open(102, file=ener_file, status='replace')
  do i = 1,ngrid
     call writeCoors(grid(i,:), n_ener, 102)
  enddo
  close(102)
    
end program extract


! Extracts values from a file
subroutine extractVals(str, icol, nrow, values, file)
  character(*), parameter :: tmp_file = 'extract.tmp'
  character(*), intent(in) :: str, file
  integer, intent(in) :: icol, nrow
  real(8), intent(out) :: values(nrow)
  character(500) :: sysstr
  integer :: i
  
  write(sysstr,'(A,A,A,A,A,I0,A,A)') &
       "grep -F '",trim(str),"' ",trim(file)," | awk '{print $",icol,"}' > ",tmp_file
  call system(sysstr)
  
  open(105, file=tmp_file, status='old')
  do i = 1,nrow
     read(105,*,err=101,end=101) values(i)
  enddo
  close(105)
  
  return
  
101 continue
  write(*,*) "ERROR: Unable to extract '",trim(str),"' from ",trim(file)
  stop
end subroutine extractVals

