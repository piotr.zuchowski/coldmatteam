
! Program to evaluate crystal interaction energy given unit cell and translation vectors
program crys_ener
  use const
  use utility
  use dimer
  use force_field
  use basis
  use polarization
  implicit none
  
  character(200) :: args(10)
  character(200) :: name, input_file, crys_file, inter_file
  real(8) :: cutoff
  real(8) :: crysvect(3,3), offset(3)
  real(8), allocatable :: ucell(:,:,:)
  real(8) :: ener
  logical :: only2B, incPol0
  integer :: ncell, maxcell1, maxcell2, maxcell3, moltype(20), mt(2)
  integer :: i, j, k, l, m
  
  do i = 1,10
     call getarg(i, args(i))
  enddo
  read(args(1),*) name
  read(args(2),*) inter_file
  read(args(3),*) crys_file
  read(args(4),*) cutoff
  cutoff = cutoff*A2bohr
  
  only2B = .false.
  
  input_file=trim(name)//'.input_long'
  call read_dimer(input_file)  
  
  call ff_init(inter_file, 2)
  
  ! Read in crystal info, translation vectors first then unit cell
  open(101,file=crys_file)
  do i = 1,3
     read(101,*) crysvect(i,1:3)
  enddo
  crysvect(:,:) = crysvect(:,:)*A2bohr
  read(101,*) ncell
  allocate( ucell(ncell,nsitemax,3) )
  do i = 1,ncell
     read(101,*) moltype(i)
     do j = 1,nsite(moltype(i))
        read(101,*) ucell(i,j,1:3)
     enddo
  enddo
  ucell(:,:,:) = ucell(:,:,:)*A2bohr
  close(101)
  
  ener = 0.0d0
  
  ! Evaluate 2-body contribution
  maxcell1 = int(cutoff/cartnorm(crysvect(1,:)))
  maxcell2 = int(cutoff/cartnorm(crysvect(2,:)))
  maxcell3 = int(cutoff/cartnorm(crysvect(3,:)))
  incPol0 = incPol
  incPol = only2B
  do i = -maxcell1,maxcell1
     do j = -maxcell2,maxcell2
        do k = -maxcell3,maxcell3
           offset(1:3) = crysvect(1,1:3)*i + crysvect(2,1:3)*j + crysvect(3,1:3)*k
           if (cartnorm(offset) <= cutoff) then
              
              do l = 1,ncell
                 mt(1) = moltype(l)
                 do m = 1,nsite(mt(1))
                    sit(1,m,1:3) = ucell(l,m,1:3) + offset(1:3)
                 enddo
                 do m = 1,ncell
                    if (i /= 0 .or. j /= 0 .or. k /= 0 .or. m /= l) then
                       mt(2) = moltype(m)
                       sit(2,1:nsite(mt(2)),1:3) = ucell(m,1:nsite(mt(2)),1:3)
                       ener = ener + sum(energy_ff_gen(mt))
                    endif
                 enddo
              enddo
              
           endif
        enddo
     enddo
  enddo
  incPol = incPol0
  ener = ener/2
  
  ! Evaluate N-body polarization contribution
  if (incPol .and. .not. only2B) then
     ener = ener + polEnergy(ucell, ncell, moltype, MAX_POL_ITER, stcharge, pz, delta1, deltap)
  endif
  
  write(*,*) ener/ncell
  
end program crys_ener

