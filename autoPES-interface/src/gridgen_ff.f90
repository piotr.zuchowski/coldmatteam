
!############################################################################################
!--------------------------------------------------------------------------------------------
!                                   GRIDGEN  -----  MAIN
!--------------------------------------------------------------------------------------------
!############################################################################################
program gridgen_ff
 use double
 use force_field
 use dimer
 use randnum
 use utility
 use const
 use dgrid
 implicit none
 
 character(200) :: in_name, gridmode
 character(200) :: input_file, parms_file, intra_file, grid0_file, geo_file
 character(200) :: args(20)
 integer :: ff_type, ngrid, ngrid0
 real(8), allocatable :: grid1(:,:), grid0(:,:)
 real(8) :: weightScale, epsRatio, wallHeight, fracr0
 logical :: exists, useSobol
 integer :: i, j
 
 do i = 1,20
    call getarg(i,args(i))
 enddo
 read(args(1),*) in_name
 read(args(2),*) ngrid
 read(args(3),*) parms_file
 read(args(4),*) ff_type
 read(args(5),*) intra_file
 read(args(6),*) weightScale
 read(args(7),*) wallHeight
 read(args(8),*) gridmode
 read(args(9),*) fracr0
 
 input_file = trim(in_name)//'.input_long'
 grid0_file = trim(in_name)//'.ener'
 geo_file = trim(in_name)//'.geo_main'
 
 call read_dimer(input_file)
 
 inquire(file=intra_file,exist=exists)
 if (exists) then
    call ff_init(parms_file, ff_type, intra_file)
 else
    call ff_init(parms_file, ff_type)
 endif
 
 call initGrid(grid0_file, grid0, ngrid0, 1)
 
 if (gridmode == 'rand') then
    epsRatio = 0.0d0
    useSobol = .false.
 elseif (gridmode == 'sobol') then
    epsRatio = 0.0d0
    useSobol = .true.
 elseif (gridmode == 'metric') then
    epsRatio = 3.0d0
    useSobol = .false.
 else
    write(*,*) 'Error: Invalid mode',gridmode
    stop
 endif
 
 write(*,*) 'ngrid=',ngrid
 write(*,*) 'ngrid0=',ngrid0
 write(*,*) 'mode=',trim(gridmode)
 
 allocate(grid1(ngrid,ngind))
 call make_grid(grid0, ngrid0, grid1, ngrid, weightScale, epsRatio, useSobol, wallHeight, &
      -1.0d100, 1.0d100, fracr0)
 
 open(unit=102,file=geo_file)
 do i = 1, ngrid
    call writeCoors(grid1(i,:), 1, 102)
 enddo
 close(102)
 
contains

end program gridgen_ff
