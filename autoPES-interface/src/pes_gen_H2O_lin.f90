
program pess     
  use const
  use dimer
  use force_field
! molscat  use physical_constants
  implicit none
  real(8) :: RR,COSTH
  real(8) :: V0, V30, V60, V90, V120,V150, V180
  real(8) :: dr,extpot                    
  integer :: i,j 
  call extint()
  RR=5.0
  dr=0.25

  do i=1,60
     write(*,'(F10.5,3F15.4)')RR,EXTPOT(RR,cos(0.d0)),EXTPOT(RR,cos(pi/2.d0)), EXTPOT(RR,cos(pi))
     RR=RR+dr
  enddo  
  
end program pess  
!--------------------------moving atom+linear molecule which is fixed-----------------------
 double precision function  extpot(R,COSTH)
  use dimer   
  use force_field
  real*8 R,COSTH,SINTH
  SINTH=SQRT(1.0-COSTH**2)
  sit(1,1,1)=0d0
  sit(1,1,2)=R*SINTH    
  sit(1,1,3)=R*COSTH
  extpot=energy_ff()*349.75509
end function extpot
!--------------------------moving atom+linear molecule which is fixed-----------------------
 double precision function  extpot(R,COSTH)
  use dimer   
  use force_field
  real*8 R,COSTH,SINTH
  SINTH=SQRT(1.0-COSTH**2)
  sit(1,1,1)=0d0
  sit(1,1,2)=R*SINTH    
  sit(1,1,3)=R*COSTH
  extpot=energy_ff()*349.75509
end function extpot
!-------------------------------------------------
subroutine extint
  use const
  use dimer
  use force_field
! molscat use physical_constants
 
  implicit real*8 (a-h,o-z)
  character(200) :: str
  character(200) :: inter_file, intra_file
  real(8) :: rbuf(10)
  integer :: geo_type
  integer :: nstot
  integer :: i,j
 
  call read_dimer('HCN-He.input_long')
  call ff_init('fit_inter.dat', 2)
  
   
end 
