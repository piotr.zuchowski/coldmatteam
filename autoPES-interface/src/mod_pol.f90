
! This module implements the many-body polarizable model 

module polarization
  implicit none
  
  integer, parameter :: MAX_POL_ITER = 20
  
contains
  
  ! Calculate polarization energy for given configuration
  ! site: 3-index array of site coordinates. 1st index is monomer, 2nd is site, 3rd is xyz
  ! nmol: number of nomoners in the configuration
  ! moltype: array of length nmol, each value is 1 or 2, specifying the nomomer type A or B
  ! niter : maximum number of iterations
  ! charge and pz: arrays of partial charges and polarizabilities of site types
  ! delta1 and deltap (optional): arrays of damping parameters of site pair types
  ! cryscutoff (optional): cutoff radius for crystal
  ! crysvect (optional): set of 3 crystal translational symmetry vectors
  real(8) function polEnergy(site, nmol, moltype, nIter, charge, pz, delta1, deltap, cryscutoff, crysvect)
    use dimer
    use utility
    
    real(8), intent(in) :: site(:,:,:)
    integer, intent(in) :: nmol
    integer, intent(in) :: moltype(nmol)
    integer, intent(in) :: niter
    real(8), intent(in) :: charge(:), pz(:)
    real(8), intent(in), optional :: delta1(:), deltap(:)
    real(8), intent(in), optional :: cryscutoff, crysvect(3,3)
    logical :: crys
    real(8) :: T(nmol,nsitemax,nmol,nsitemax,3,3)
    real(8) :: Efield(nmol,nsitemax,3), fieldij(3)
    real(8) :: mu(nmol,nsitemax,3), muNext(nmol,nsitemax,3)
    real(8) :: rVect(3), r
    real(8) :: val, iterChange, diff(3), pz1, pz2, norm, dampFact
    real(8) :: crystrans(3), crystransNorm, crysrad
    integer :: st1, st2, sptype
    integer :: imon1, imon2, icrys1, icrys2, icrys3
    integer :: cryslim1, cryslim2, cryslim3
    integer :: i, j, k, l, m, n
    
    crys = present(cryscutoff)
    if (crys) then
       cryslim1 = int(cryscutoff/cartnorm(crysvect(1,:)))
       cryslim2 = int(cryscutoff/cartnorm(crysvect(2,:)))
       cryslim3 = int(cryscutoff/cartnorm(crysvect(3,:)))
       crysrad = cryscutoff
    else
       cryslim1 = 0
       cryslim2 = 0
       cryslim3 = 0
       crysrad = 0.0d0
    endif
    
    ! Compute E field and dipole tensor
    ! In non-crys case, the outer loops are not necessary
    crystransNorm = 0.0d0
    Efield(:,:,:) = 0.0d0
    do icrys1 = -cryslim1,cryslim1
       do icrys2 = -cryslim2,cryslim2
          do icrys3 = -cryslim3,cryslim3
             if (crys) then
                crystrans(:) = crysvect(1,:)*icrys1 + crysvect(2,:)*icrys2 + crysvect(3,:)*icrys3
                crystransNorm = cartnorm(crystrans)
             endif
             if (.not. crys .or. crystransNorm <= crysrad) then ! If unit cell is within crystal cutoff
                
                do i = 1,nmol
                   do j = 1,nsite(moltype(i))
                      do k = 1,nmol
                         
                         ! Compute E field and dipole tensor at monomer i from monomer k (and k <- i in non-crys case)
                         if ((.not. crys .and. k < i) .or. &
                              (crys .and. (k /= i .or. icrys1 /= 0 .or. icrys2 /= 0 .or. icrys3 /= 0))) then
                            
                            do l = 1,nsite(moltype(k))
                               st1 = stype(moltype(i),j)
                               st2 = stype(moltype(k),l)
                               sptype = typeMap(st1,st2)
                               
                               rVect = site(i,j,:) - site(k,l,:)
                               if (crys) rVect(:) = rVect(:) - crystrans(:)
                               r = cartnorm(rVect)
                               
                               ! Calculate E field due to point charges
                               if (present(delta1)) then
                                  dampFact = TT(1, delta1(sptype), r)
                               else
                                  dampFact = 1.0d0
                               endif
                               fieldij = rVect(:)*dampFact/r**3
                               Efield(i,j,:) = Efield(i,j,:) + fieldij*charge(st2)
                               if (.not. crys) Efield(k,l,:) = Efield(k,l,:) - fieldij*charge(st1)
                               
                               ! Calculate dipole tensor
                               if (pz(st1) > 1.0d-20 .and. pz(st2) > 1.0d-20) then
                                  if (present(deltap)) then
                                     dampFact = TT(3, deltap(sptype), r)
                                  else
                                     dampFact = 1.0d0
                                  endif
                                  do m = 1,3
                                     do n = 1,m
                                        val = 3.0d0*dampFact*rVect(m)*rVect(n)/r**5
                                        if (m == n) then
                                           val = val - dampFact/r**3
                                        endif
                                        T(i,j,k,l,m,n) = val
                                        T(i,j,k,l,n,m) = val
                                     enddo
                                  enddo
                                  if (.not. crys) T(k,l,i,j,:,:) = T(i,j,k,l,:,:)
                               endif
                               
                            enddo
                            
                         endif
                      enddo
                   enddo
                enddo
                
             endif ! End if unit cell is within crystal cutoff
          enddo
       enddo
    enddo
    
    ! Solve for induced dipoles iteratively
    mu(:,:,:) = 0.0d0
    do i = 1,niter
       iterChange = 0.0d0
       
       do imon1 = 1,nmol
          do imon2 = 1,nmol
             if (imon1 /= imon2) then
                
                do j = 1,nsite(moltype(imon1))
                   st1 = stype(moltype(imon1),j)
                   if (pz(st1) > 1.0d-20) then
                      muNext(imon1,j,:) = Efield(imon1,j,:)
                      do k = 1,nsite(moltype(imon2))
                         st2 = stype(moltype(imon2),k)
                         if (pz(st2) > 1.0d-20) then
                            muNext(imon1,j,:) = muNext(imon1,j,:) + &
                                 matmul(T(imon1,j,imon2,k,:,:), mu(imon2,k,:))
                         endif
                      enddo
                      
                      muNext(imon1,j,:) = muNext(imon1,j,:)*pz(st1)
                      diff = muNext(imon1,j,:) - mu(imon1,j,:)
                      iterChange = iterChange + dot_product(diff, diff)
                      mu(imon1,j,:) = muNext(imon1,j,:)
                   endif
                enddo
                
             endif
          enddo
       enddo
       
       if (iterChange < nsitemax*1.0d-14) exit
    end do
    
    ! Compute interaction energy
    polEnergy = 0.0
    do i = 1,nmol
       do j = 1,nsite(moltype(i))
          polEnergy = polEnergy - 0.5d0*dot_product(Efield(i,j,:), mu(i,j,:))
       end do
    end do
    polEnergy = polEnergy*h2kcal
    
  end function polEnergy
  
  
  ! Wrapper routine for polEnergy which evaluates the 2-body energy at current dimer geometry
  real(8) function polEnerDim(niter, charge, pz, delta1, deltap)
    use dimer
    
    integer, parameter :: moltypeAB(2) = [1,2]
    integer, intent(in) :: niter
    real(8), intent(in) :: charge(:), pz(:)
    real(8), intent(in), optional :: delta1(:), deltap(:)
    
    if (present(delta1)) then
       polEnerDim = polEnergy(sit, 2, moltypeAB, niter, charge, pz, delta1, deltap)
    else
       polEnerDim = polEnergy(sit, 2, moltypeAB, niter, charge, pz)
    endif
  end function polEnerDim
  
  
end module polarization

