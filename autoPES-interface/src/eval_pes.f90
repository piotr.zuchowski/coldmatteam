
program eval_pes
  use const
  use dimer
  use force_field
  implicit none
  
  character(200) :: str
  character(200) :: inter_file, intra_file
  real(8) :: rbuf(10)
  integer :: geo_type
  integer :: nstot
  integer :: i,j
  
  call getarg(1, inter_file)
  call getarg(2, intra_file)
  open(101,file=trim(inter_file))
  read(101,'(A)') str
  read(101,*) nsite(1), nsite(2)
  do i = 1,2
     read(101,*) stype(i,1:nsite(i))
  enddo
  close(101)
  
  write(*,*) ' ---------------------------------- '
  write(*,*) inter_file 
  write(*,*) ' ---------------------------------- '
  write(*,*) intra_file 
  write(*,*) ' ---------------------------------- '

  read(*,*) nstot
  if (nstot /= nsite(1) + nsite(2)) then
     write(*,*) 'Error: number of sites does not match'
     stop
  endif
  read(*,'(A)') str
  do i = 1,2
     do j = 1,nsite(i)
        read(*,*) str,rbuf(1:3)
        sit(i,j,1:3) = rbuf(1:3)*A2bohr
        sanum(i,j) = getanum(str)
     enddo
  enddo
  
  call read_dimer()
  
  if (intra_file == '') then
     call ff_init(inter_file, 2)
  else
     call ff_init(inter_file, 2, intra_file)
  endif
  
  rbuf(1) = energy_ff()
  write(*,'(A,E15.8)') 'E_tot= ',rbuf(1)
  rbuf(1:n_FF_comp) = energy_ff_comp()
  write(*,'(A,E15.8)') 'E_exp= ',rbuf(F_exp)
  write(*,'(A,E15.8)') 'E_els= ',rbuf(F_els)
  write(*,'(A,E15.8)') 'E_Cn = ',rbuf(F_vdw)
  write(*,'(A,E15.8)') 'E_pol= ',rbuf(F_pol)
  
end program eval_pes

