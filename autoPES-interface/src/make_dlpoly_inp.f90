
! Generates input files for DLPOLY
! Energies in kcal/mol, distances in Angstrom
program make_dlpoly_inp
  use const
  use dimer
  use force_field
  use dgrid
  implicit none
  
  real(8), parameter :: dr = 5.0d-3
  
  character(200) :: args(10)
  character(200) :: name, input_file, ff_file
  real(8) :: gpoint(MAX_NGIND)
  integer :: nmol, ngrid, nspair, nshell
  real(8) :: rinc, rcutoff
  integer :: sptype, irec, isit
  integer :: shmap(MAX_NSITE)
  real(8) :: qcore(MAX_NSITE), qshell(MAX_NSITE)
  real(8), allocatable :: vr(:), dvr(:)
  real(8) :: r, ener, enerp, enerm, enerComp(n_FF_comp)
  integer :: i, j, k
  
  do i = 1,10
     call getarg(i, args(i))
  enddo
  name = args(1)
  ff_file = args(2)
  read(args(3),*) rcutoff
  
  input_file=trim(name)//'.input_long'
  
  call read_dimer(input_file)  
  
  call ff_init(ff_file, 2)
  
  if (incPol) then
     write(*,*) 'WARNING: DL_POLY does not support the "mathematical" polarization model,'
     write(*,*) '         only the "physical" one. Creating equivalent inputs, but there'
     write(*,*) '         will be some difference in the polarization model.'
     if (incPolDmp) then
        write(*,*) 'WARNING: The fit uses polarization damping, but DL_POLY does not support it.'
        write(*,*) '         There will be some difference in the polarization model.'
     endif
  endif
  
  rinc = 1.0d-3
  ngrid = nint(rcutoff/rinc) + 4
  !nrec = int((dble(ngrid) + 3.0d0)/4.0d0)
  
  allocate( vr(ngrid), dvr(ngrid) )
  
  ! Create FIELD file
  open(101,file='FIELD',status='new')
  write(101,'(A)') 'DL_POLY Rigid molecule simulations'
  write(101,'(A)') 'units kcal'
  
  if (homog) then
     nmol = 1
  else
     nmol = 2
  endif
  write(101,'(A,I2)') 'molecules ',nmol
  do i = 1,nmol
     qcore(:) = 0.0d0
     qshell(:) = 0.0d0
     nshell = 0
     do j = 1,nsite(i)
        if (incPol .and. incInter(stype(i,j),F_pol)) nshell = nshell + 1
     enddo
     
     write(101,'(A)') trim(name)
     write(101,'(A)') 'nummols 1'
     write(101,'(A,I5)') 'atoms ',nsite(i)+nshell
     do j = 1,nsite(i) ! Write sites
        if (incPol .and. incInter(stype(i,j),F_pol)) then
           qcore(j) = sanum(i,j)
        else
           qcore(j) = stcharge(stype(i,j))
        endif
        write(101,'(A12,F12.8,F15.10,I2,I5,I5)') &
             alabel(i,j), atomMass(sanum(i,j)), qcore(j), 1, 0, 1
     enddo
     nshell = 0
     do j = 1,nsite(i) ! Write polarization shells
        if (incPol .and. incInter(stype(i,j),F_pol)) then
           nshell = nshell + 1
           shmap(nshell) = j
           qshell(j) = stcharge(stype(i,j)) - qcore(j)
           write(101,'(A12,F12.8,F15.10,I2,I5,I5)') &
                'SH_'//trim(alabel(i,j)), 0.0d0, qshell(j), 1, 0, 1
        endif
     enddo
     write(101,'(A)') 'rigid 1'
     write(101,'(I5)',advance='no') nsite(i)
     irec = 1
     do j = 1,nsite(i) ! Write rigid contraints
        if (irec == 15) then
           irec = 0
           write(101,'(A)') ''
        endif
        write(101,'(I5)',advance='no') j
        irec = irec + 1
     enddo
     write(101,'(A)') ''
     if (nshell > 0) then
        write(101,'(A,I5)') 'shell ',nshell
        do j = 1,nshell ! Write core-shell pairs
           isit = shmap(j)
           write(101,'(I5,A,I5,A,E15.8,A,E15.8,A,E15.8)') &
                isit,' ',nsite(i)+j,' ',qshell(isit),' ', &
                h2kcal*qshell(isit)**2/(pz(isit)*bohr2A**2),' ',0.0d0
        enddo
     endif
     write(101,'(A)') 'finish'
  enddo
  
  nspair = 0
  do i = 1,nsite(1)
     do j = 1,nsite(2)
        if (.not. homog .or. j >= i) then
           nspair = nspair + 1
        endif
     enddo
  enddo
  write(101,'(A,I8)') 'VDW ',nspair
  do i = 1,nsite(1)
     do j = 1,nsite(2)
        if (.not. homog .or. j >= i) then
           write(101,'(A8,A8,A)') alabel(1,i),alabel(2,j),'tab'
        endif
     enddo
  enddo
  write(101,'(A)') 'CLOSE'
  close(101)
  
  ! Create TABLE file
  open(101,file='TABLE',status='new')
  write(101,'(2A)') trim(name),'  TABLE file'
  write(101,'(E12.5,A,E12.5,A,I10)') rinc,' ',rcutoff,' ',ngrid
  
  do i = 1,nsite(1)
     do j = 1,nsite(2)
        if (.not. homog .or. j >= i) then
           sptype = typeMap(stype(1,i),stype(2,j))
           
           ! Compute potential and force tables
           r = 0.0d0
           do k = 1,ngrid
              if (r < 1.0d-2) then
                 vr(k) = 0.0d0
                 dvr(k) = 0.0d0
              else
                 enerComp = energy_ff_gen_ij(sptype, r*A2bohr)
                 ener = enerComp(F_exp) + enerComp(F_vdw)
                 enerComp = energy_ff_gen_ij(sptype, (r - dr)*A2bohr)
                 enerm = enerComp(F_exp) + enerComp(F_vdw)
                 enerComp = energy_ff_gen_ij(sptype, (r + dr)*A2bohr)
                 enerp = enerComp(F_exp) + enerComp(F_vdw)
                 vr(k) = ener
                 dvr(k) = -1.0d0*r*(enerp - enerm)/(2.0d0*dr)
              endif
              r = r + rinc
           enddo
           
           ! Output tables
           write(101,'(2A8)') alabel(1,i),alabel(2,j)
           irec = 0
           do k = 1,ngrid
              if (irec == 4) then
                 irec = 0
                 write(101,'(A)') ''
              endif
              write(101,'(A,E15.8)',advance='no') ' ',vr(k)
              irec = irec + 1
           enddo
           write(101,'(A)') ''
           irec = 0
           do k = 1,ngrid
              if (irec == 4) then
                 irec = 0
                 write(101,'(A)') ''
              endif
              write(101,'(A,E15.8)',advance='no') ' ',dvr(k)
              irec = irec + 1
           enddo
           write(101,'(A)') ''
           
        endif
     enddo
  enddo
  close(101)
  
  
end program make_dlpoly_inp

