program make_coords
 use double
 use dimer
 use const
 use utility
 use randnum
 use sobol
 implicit none
 
 integer(8), parameter :: dim_num = 6
 real :: r
 character(len=100) :: in_name, input, ch_npart, ch_ncoord, coords_file
 real(8) :: rand(6), minr, maxr
 real(8) :: beta_A, gamma_A, alpha_B, beta_B, gamma_B
 integer :: N_beta_A, N_gamma_A, N_alpha_B, N_beta_B, N_gamma_B
 integer :: ncoord, npart, ipart, nang
 integer(8) :: sobolSeed
 integer :: i, j
 
 call getarg(1,in_name)
 call getarg(2,ch_npart)
 call getarg(3,ch_ncoord)
 read(ch_npart,*) npart
 read(ch_ncoord,*) ncoord
 input = trim(in_name) // '.input_long'
 call read_dimer(input)
 call init_random_seed()
 nang = ncoord/npart
 sobolSeed = 64
 
 !
 ! coords.dat
 !
 do ipart = 1,npart
    write(coords_file,'(A,I0)') 'coords.dat.',ipart
    open(unit=101,file=coords_file)
    write(101,*) '  x    y    x   alpha    beta   gamma'
    
    do i = 1,nang
       call i8_sobol(dim_num, sobolSeed, rand)
       beta_A = dacos(1.998d0*rand(1) - 0.999d0)
       gamma_A = 2.0d0*pi*rand(2)
       alpha_B = 2.0d0*pi*rand(3)
       beta_B = dacos(1.998d0*rand(4) - 0.999d0)
       gamma_B = 2.0d0*pi*rand(5)
       
       call moveDimer(0.0d0, beta_A, gamma_A, alpha_B, beta_B, gamma_B)
       minr = covdist(1.4d0)
       maxr = covdist(4.0d0)
       r = minr + rand(6)*(maxr - minr)
       
       write(101,'(6F8.2)') 0.0, 0.0, 0.0, 0.0, beta_A*rad2d, gamma_A*rad2d
       write(101,'(6F8.2)') 0.0, 0.0, r, alpha_B*rad2d, beta_B*rad2d, gamma_B*rad2d
       write(101,*) ' k'
    enddo
    close(101)
 enddo
 
 !
 ! control.dat
 !
 open(unit=101,file='control.dat')
 write(101,*) '&CNTR'
 write(101,*) '  neig = 0,'
 write(101,*) '  nprt = 100,'
 write(101,*) '  dirs = 0.0d0,'
 write(101,*) '  dirm = 0.0d0,'
 write(101,*) '  dmax = 0.1d0,'
 write(101,*) '  hh = 0.001d0,'
 write(101,*) '  eps = 1.0d-10,'
 write(101,*) '  c_freq = F,'
 write(101,*) '  c_alc = F,'
 write(101,*) '  r_trudge = F,'
 write(101,*) '  r_saddle = T,'
 write(101,*) '  o_full = T,'
 write(101,*) '  rot_incr = 45.d0,'
 write(101,*) '  e_three = F,'
 write(101,*) '  TOLF = 0.10d-8,'
 write(101,*) '  TOLR = 0.20d-4,'
 write(101,*) '  ANOISE = 0.10d-8,'
 write(101,*) '  ALFST = 1.0d-4,'
 write(101,*) '  IPR = 1,'
 write(101,*) '  maxcyc = 10'
 write(101,*) '/'
 close(101)
 
 !
 ! cluster.dat
 !
 open(unit=101,file='cluster.dat')
 write(101,*) 'Number of molecules'
 write(101,*) '   2'
 write(101,*) 'Molecule types'
 if (homog) then
  write(101,*) '  1  1'
 else
  write(101,*) '  1  2'
 end if
 write(101,*) 'Molecules atomic specification'
 write(101,*) '1'
 write(101,*) natom(1), '  3'
 do i = 1, natom(1)
  write(101,'(3F25.15,F15.8,A10)') (sit0(1,i,j),j=1,3), atomMass(sanum(1,i)), '''X'''
 end do
 if (.not.homog) then
  write(101,*) '2'
  write(101,*) natom(2), '  3'
  do i = 1, natom(2)
   write(101,'(3F25.15,F15.8,A10)') (sit0(2,i,j),j=1,3), atomMass(sanum(2,i)), '''X'''
  end do
 end if
 close(101)
 
end program make_coords
