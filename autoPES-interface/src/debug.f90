
program debug
  use const
  use dimer
  use force_field
  use dgrid
  use basis
  implicit none
  
  character(200) :: str
  character(200) :: name, input_file, ener_file
  real(8) :: rbuf(10)
  real(8) :: val, val2, emin, rmin, rb, raasym
  real(8), allocatable :: grid(:,:)
  real(8) :: gpoint(MAX_NGIND)
  integer :: ival, ival2, imin
  integer :: ngrid
  real(8) :: crysvect(3,3), offset(3)
  real(8), allocatable :: ucell0(:,:,:)
  integer :: ncell, maxcell
  integer :: i, j, k, l, m
  
  call getarg(1, name)
  
  input_file=trim(name)//'.input_long'
  ener_file=trim(name)//'.ener'
  
  call read_dimer(input_file)  
  
  call ff_init('fit_inter.dat', 2)
  
  call initGrid(ener_file, grid, ngrid, n_ener)
  
  goto 102

100 continue  
  emin = 1.0d100
  do i = 1,ngrid
     if (grid(i,C_Etot) < emin) then
        emin = grid(i,C_Etot)
        imin = i
     endif
  enddo
  rmin = grid(imin,C_Rcom)
  
  do i = 1,ngrid
     call moveDimerGP(grid(i,:))
     write(*,'(2F15.6)') closestContact(), energy_ff_inter()!, energy_ff_comp()
     !write(*,*) minCovDist(), grid(i,C_etot), energy_ff()
     !rbuf(1:n_ff_comp) = energy_ff_comp()
     !write(*,*) grid(i,C_rcom)*bohr2A, rbuf(1:n_ff_comp)
     !val = rbuf(F_els) + rbuf(F_vdw) + rbuf(F_pol)
     !val2 = grid(i,C_Etot) !grid(i,C_ener+E_els) + grid(i,C_ener+E_ind) + grid(i,C_ener+E_dsp)
     !write(*,*) grid(i,C_rcom)/rmin, (val - val2)/val2, val, val2
  enddo
  stop
  
101 continue
  val = 0.0d0
  ival = 0
  val2 = 0.0d0
  do i = 1,ngrid
     call moveDimerGP(grid(i,:))
     if (grid(i,C_Etot) < 0.0d0) then
        ival = ival + 1
        val = val + (grid(i,C_Etot) - energy_ff_inter())**2
     endif
     !val2 = val2 + (grid(i,C_Etot) - energy_ff_inter())**2
     rbuf(1:n_ff_comp) = energy_ff_comp()
     val2 = val2 + 2.0d0*abs(grid(i,C_ener+E_els) - rbuf(F_els))/ &
          (abs(grid(i,C_ener+E_els)) + abs(rbuf(F_els)))
  enddo
  write(*,*) sqrt(val/ival), 100.0d0*val2/ngrid
  stop
  
102 continue
  
  stop
  
end program debug

