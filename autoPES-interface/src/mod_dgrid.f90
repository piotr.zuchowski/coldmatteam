
! Module for handling arrays of grid points. Each grid point contains at least
!     6 coordinates, more if monomers have IDOFs. Grid points may also contain
!     energy values depending on how they are initialized. In the case that
!     energy values are ab-inito data, E_tot is the intermolecular energy only

module dgrid
  use dimer
  use randnum
  use utility
  use const
  use weight
  implicit none
  
  ! Indices into grid array. Must be initialized after reading in dimer file.
  integer, parameter :: C_rcom = 1, &
       C_betaA = 2, C_gammaA = 3, C_alphaB = 4, C_betaB = 5, C_gammaB = 6
  integer, parameter :: MAX_NCOOR = 6 + MAX_IDOF
  integer, parameter :: MAX_NGIND = MAX_NCOOR + n_ener
  
  integer :: ncoor, ngind
  integer :: C_ener, C_Etot
  integer :: C_mon(2)
  logical :: inc_dhf
  
contains
  
  ! Generate a grid of dimer configurations
  !
  ! g0 : Pre-existing grid of points. Needed for metric and for sobol to locate current seed value
  ! ngrid0 : Size of g0
  ! g1 : Output grid of new points
  ! ngrid : Number of points to generate
  ! weightScale : Parameter specifying strength of guiding function weight. Zero means do not use
  !       guiding function rejection criterion
  ! epsRatio : Strength of metric rejection criterion. Zero means do not use metric rejection criterion
  ! useSobol : Whether to use sobol sequence in place of pseudo random number generation
  ! wallHeight : Used to determine rmin using grid0
  ! minEner : Minimum value of guiding function for point acceptance
  ! maxEner : Maximum value of guiding function for point acceptance
  ! fracr0 : For flexible monomers, the proportion of points taken at undeformed geometry r0
  subroutine make_grid(g0, ngrid0, g1, ngrid, &
       weightScale, epsRatio, useSobol, wallHeight, minEner, maxEner, fracr0)
    use force_field
    use sobol
    use powell
    
    real(8), parameter :: logn_std = 1.0d0
    integer(8), parameter :: dim_num = 6
    
    integer, intent(in) :: ngrid0, ngrid
    real(8), intent(in) :: g0(ngrid0,ngind)
    real(8), intent(out) :: g1(ngrid,ngind)
    real(8), intent(in) :: weightScale, epsRatio
    logical, intent(in) :: useSobol
    real(8), intent(in) :: wallHeight, maxEner, minEner, fracr0
    real(8) :: minVect(ncoor)
    real(8) :: siti(2,MAX_NSITE,3)
    real(8) :: w, ener, e_min, epsCutoff, thresh
    real(8) :: rmin, rmax, rmin_cov, rmax_vdw
    logical :: rejected, zeromon
    real(8) :: rand, randVect(dim_num)
    integer(8) :: sobolSeed
    integer :: N1
    real(8) :: minIDOF(2,MAX_IDOF), maxIDOF(2,MAX_IDOF)
    real(8) :: gpoint(MAX_NGIND)
    integer :: i, j
    
    call init_random_seed()
    !call init_fixed_seed()
    
    e_min = 1.0d100
    do i = 1,ngrid0
       call moveDimerGP(g0(i,:))
       e_min = min(e_min, energy_ff())
    enddo
    
    ! Find minimum distance as ratio of covalent radii
    if (ngrid0 == 0) then
       ! Detrmine minimum distance based on covalend radii, adjusting for ionic systems
       if (molchar(1)*molchar(2) < 0) then
          rmin_cov = 1.00d0
       elseif (molchar(1)*molchar(2) > 0) then
          rmin_cov = 2.00d0
       else
          rmin_cov = 1.30d0
       endif
    else
       ! Determine minimum distance using grid0
       rmin_cov = 0.85d0*repulDist(g0, ngrid0, wallHeight, 0.90d0)
    endif
    
    ! Find maximum distance as ratio of VDW radii
    if (molchar(1)*molchar(2) < 0) then
       rmax_vdw = 1.00d0
    elseif (molchar(1)*molchar(2) > 0) then
       rmax_vdw = 2.00d0
    else
       rmax_vdw = 1.40d0
    endif
    
    write(*,'(A,F6.3,A,F6.3)') 'rmin_cov=',rmin_cov,'     rmax_vdw=',rmax_vdw
    write(*,'(A)') ''
    
    ! Find limits of IDOFs
    if (incIntra) then
       thresh = 0.8d0*abs(e_min)
    else
       thresh = 0.08d0
    endif
    do i = 1,2
       do j = 1,nmcPhys(i)
          call IDOF_limits(i, j, minIDOF(i,j), maxIDOF(i,j), incIntra, thresh)
          write(*,'(A,I3,I3,A,F10.4,A,F10.4)') &
               'IDOF limits ',i,j,'    min=',minIDOF(i,j),'   max=',maxIDOF(i,j)
       enddo
    enddo
    
    ! Generate the grid
    epsCutoff = 0.05d0
    sobolSeed = 64
    N1 = 0
    do while (N1 < ngrid)
       
       if (useSobol) then
          call i8_sobol(dim_num, sobolSeed, randVect)
       else
          do i = 1,dim_num
             call random_number(randVect(i))
          enddo
       endif
       
       gpoint(:) = 0.0d0
       gpoint(C_betaA) = dacos(2.0d0*randVect(1) - 1.0d0)
       gpoint(C_gammaA) = randVect(2)*2.0d0*PI
       gpoint(C_alphaB) = randVect(3)*2.0d0*PI
       gpoint(C_betaB) = dacos(2.0d0*randVect(4) - 1.0d0)
       gpoint(C_gammaB) = randVect(5)*2.0d0*PI
       
       ! Skip already generated points. Needed to continue sobol sequence where it left off
       rejected = .false.
       if (N1 == 0) then
          do i = 1,ngrid0
             if (abs(gpoint(C_betaA) - g0(i,C_betaA)) < 1.0d-6 .and. &
                  abs(gpoint(C_gammaA) - g0(i,C_gammaA)) < 1.0d-6 .and. &
                  abs(gpoint(C_alphaB) - g0(i,C_alphaB)) < 1.0d-6) then
                rejected = .true.
                exit
             endif
          enddo
       endif
       if (rejected) cycle     
       
       ! Set monomer coordinates
       call random_number(rand)
       zeromon = rand < fracr0
       do i = 1,2
          do j = 1,nmcPhys(i)
             if (mctype(i,j) == MC_ANG .or. mctype(i,j) == MC_LIN) then
                if (zeromon) then
                   gpoint(C_mon(i)+j) = 0.0d0
                else
                   rand = 1.0d100
                   do while (rand > maxIDOF(i,j) .or. rand < minIDOF(i,j))
                      call normal_random(rand)
                      rand = rand*0.5d0*(maxIDOF(i,j) - minIDOF(i,j))
                   enddo
                   gpoint(C_mon(i)+j) = rand
                endif
             elseif (mctype(i,j) == MC_CHI) then
                call random_number(rand)
                if (rand > 0.5d0) then
                   gpoint(C_mon(i)+j) = 1.0d0
                else
                   gpoint(C_mon(i)+j) = 0.0d0
                endif
             endif
          enddo
       enddo
       
       ! Set R_com
       gpoint(C_rcom) = 0.0d0
       call moveDimerGP(gpoint)
       rMin = covDist(rmin_cov)
       rMax = vdwDist(rmax_vdw)
       
       rand = 0.5d0*randVect(6)  ! Uniform distribution in range (0,0.5)
       call uni2norm(rand)       ! Normal distribution in range (-inf,0)
       rand = exp(logn_std*rand) ! Log-normal distribution in range (0,1)
       gpoint(C_rcom) = rMin + rand*(rMax - rMin)
       
       call moveDimerGP(gpoint)
       
       !! Possibly minimize using praxis
       !call random_number(rand)
       !if (rand < pctMin) then
       !   minVect(1:ncoor) = g1(N1,1:ncoor)
       !   call praxis(10, 0.0d0, 1.0d-1, ncoor, 0, minVect, coorEner, 0, .false.)
       !   g1(N1,1:ncoor) = minVect(1:ncoor)
       !   call moveDimerGP(g1(N1,:))
       !   
       !   ! Minimization out of limits rejection criterion
       !   if (minCovDist() < rmin_cov) cycle
       !endif
       
       ! Calculate PES energy
       ener = energy_ff()
       gpoint(C_Etot) = ener
       e_min = min(e_min, ener)
       
       ! PES energy limits rejection criterion
       if (ener < minEner .or. ener > maxEner) cycle
       
       ! Guiding function rejection criterion
       if (weightScale > 0.0d0) then
          w = gweight(ener, e_min, weightScale)
          call random_number(rand)
          if (rand > w) cycle
       endif
       
       ! Rejection criterion based on dimer distance metric
       if (epsRatio > 0.0d0) then
          rejected = .false.
          call moveDimerGP(gpoint)
          siti = sit
          
          ! Check newly added grid points for redundant point
          do i = 1,N1
             call moveDimerGP(g1(i,:))
             if (epsMetric(siti, sit) < epsCutoff) then
                rejected = .true.
                exit
             endif
          enddo
          
          if (.not. rejected) then
             ! Check old grid points for redundant point
             do i = 1,ngrid0
                call moveDimerGP(g0(i,:))
                if (epsMetric(siti, sit) < epsCutoff) then
                   rejected = .true.
                   exit
                endif
             enddo
          endif
          
          if (rejected) then
             epsCutoff = epsCutoff*(1.0d0 - 1.0d-2)
             cycle
          else
             epsCutoff = epsCutoff*(1.0d0 + 1.0d-2*epsRatio)
          endif
       endif
       
       ! Point was not rejected for any reason, so add it
       N1 = N1 + 1
       g1(N1,1:ngind) = gpoint(1:ngind)
    enddo
    
    return
  end subroutine make_grid
  
  
  !! Compute PES energy at given coordinate vector
  !real(8) function coorEner(vect, n)
  !  use force_field
  !  integer, intent(in) :: n
  !  real(8), intent(in) :: vect(n)
  !  
  !  call moveDimerGP(vect)
  !  coorEner = energy_ff()
  !end function coorEner
  
  
  ! Finds the maximum covalent ratio such that of the points closer than that ratio,
  !      at least frac have energy greater than ethresh
  real(8) function repulDist(grid, ngrid, ethresh, frac)
    integer, intent(in) :: ngrid
    real(8), intent(in) :: grid(ngrid,ngind)
    real(8), intent(in) :: ethresh, frac
    integer :: nAbove, nBelow
    real(8) :: dist, fracAbove
    integer :: i
    
    nAbove = 0
    nBelow = 1
    dist = 3.0d0
    
    do while (dble(nAbove) < frac*dble(nAbove + nBelow))
       dist = dist - 0.01d0
       
       nAbove = 0
       nBelow = 0
       do i = 1,ngrid
          call moveDimerGP(grid(i,:))
          
          if (minCovDist() < dist) then
             if (grid(i,C_Etot) > ethresh) then
                nAbove = nAbove + 1
             else
                nBelow = nBelow + 1
             endif
          endif
       enddo
    enddo
    
    if (dist < 0.0d0) then
       write(*,*) 'ERROR: Could not find repulsive dist ',ethresh,frac
       stop
    endif
    
    repulDist = dist
  end function repulDist
  
  
  ! Reads in a given grid file and initializes variables 
  subroutine initGrid(fname, grid, ngrid, nener)
    implicit none
    character(*), intent(in) :: fname
    integer, intent(out) :: ngrid
    real(8), allocatable, intent(out) :: grid(:,:)
    integer, intent(in) :: nener
    character(2000) :: line
    real(8) :: factor
    integer :: i, j, k
    
    inc_dhf = .false.
    call initGridCoors(nener)
    
    ngrid = nlines(fname)
    allocate( grid(ngrid,ngind) )
    
    if (ngrid > 0) then
       open(199,status='old',file=fname)
       do i = 1,ngrid
          read(199,'(A2000)') line
          
          call str2gpoint(line, grid(i,1:ngind))
          
          if (nener >= E_dhf) then
             if (abs(grid(i,C_ener+E_dhf)) > 1.0d-5) then
                inc_dhf = .true.
             endif
          endif
          
          if (nener >= E_mon(1)) grid(i,C_ener+E_mon(1)) = grid(i,C_ener+E_mon(1))*h2kcal
          if (nener >= E_mon(2)) grid(i,C_ener+E_mon(2)) = grid(i,C_ener+E_mon(2))*h2kcal
       enddo
       close(199)
    endif
  end subroutine initGrid
  
  
  ! Convert a string to a grid point
  subroutine str2gpoint(str, gpoint)
    character(*), intent(in) :: str
    real(8), intent(out) :: gpoint(ngind)
    real(8) :: rbuf(40)
    real(8) :: factor
    integer :: i, j
    
    call splitLineReal(str, rbuf)
    gpoint(1:ngind) = rbuf(1:ngind)
    gpoint(C_rcom) = gpoint(C_rcom)*A2bohr
    gpoint(C_betaA:C_gammaB) = gpoint(C_betaA:C_gammaB)*d2rad
    do i = 1,2
       do j = 1,nmcPhys(i)
          factor = 1.0d0
          if (mctype(i,j) == MC_ANG) then
             factor = d2rad
          elseif (mctype(i,j) == MC_LIN) then
             factor = A2bohr
          endif
          gpoint(C_mon(i)+j) = gpoint(C_mon(i)+j)*factor
       enddo
    enddo
  end subroutine str2gpoint
  
  
  ! Initialize indices into grid array
  subroutine initGridCoors(nener)
    implicit none
    integer, intent(in) :: nener
    
    ncoor = 6 + nmcPhys(1) + nmcPhys(2)
    ngind = ncoor + nener
    C_mon(1) = 6
    C_mon(2) = C_mon(1) + nmcPhys(1)
    C_ener = C_mon(2) + nmcPhys(2)
    C_Etot = C_ener + E_tot
  end subroutine initGridCoors
  
  
  ! Wrapper routine for moveDimer
  subroutine moveDimerGP(gp)
    implicit none
    real(8) :: gp(ncoor)
    real(8) :: moncoor(2,MAX_IDOF)
    integer :: i, j
    
    moncoor(:,:) = 0.0d0
    do i = 1,2
       do j = 1,nmcPhys(i)
          moncoor(i,j) = gp(C_mon(i)+j)
       enddo
    enddo
    call moveDimer(gp(C_rcom), gp(C_betaA), gp(C_gammaA), &
         gp(C_alphaB), gp(C_betaB), gp(C_gammaB), moncoor)
  end subroutine moveDimerGP
  
  
  ! Outputs grid point to given unit
  subroutine writeCoors(gpoint, nener, unit)
    implicit none
    real(8), intent(in) :: gpoint(ncoor+nener)
    integer, intent(in) :: nener, unit
    character(1000) :: line
    character(100) :: str
    real(8) :: factor
    integer :: i, j
    
    write(line,'(6(A,F18.10))') ' ',gpoint(C_rcom)*bohr2A,' ', &
         gpoint(C_betaA)*rad2d,' ',gpoint(C_gammaA)*rad2d,' ', &
         gpoint(C_alphaB)*rad2d,' ',gpoint(C_betaB)*rad2d,' ',gpoint(C_gammaB)*rad2d
    do i = 1,2
       do j = 1,nmcPhys(i)
          factor = 1.0d0
          if (mctype(i,j) == MC_ANG) then
             factor = rad2d
          elseif (mctype(i,j) == MC_LIN) then
             factor = bohr2A
          endif
          write(str,'(F18.10)') gpoint(C_mon(i)+j)*factor
          line = trim(line)//' '//trim(str)
       enddo
    enddo
    do i = 1,nener
       if (i == E_mon(1) .or. i == E_mon(2)) then
          factor = kcal2h
       else
          factor = 1.0d0
       endif
       write(str,'(F18.10)') factor*gpoint(C_ener+i)
       line = trim(line)//' '//trim(str)
    enddo
    
    write(unit,'(A)') trim(line)
  end subroutine writeCoors
  
end module dgrid

