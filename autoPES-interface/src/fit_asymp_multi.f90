!
!************************ COMMAND LINE PARAMETERS **************************
!
! verbose (logical): true to print all output, false to print only final error
!     and fit parameters
! comRuleMode (integer): 0 to fit each site pair separately,
!     1 to use geometric mean combination rule
!
!**************************** STANDARD INPUT *******************************
!
!
!************************* REQUIRED INPUT FILES ****************************
!
! asymp.in : Contains atom symmetries and site types.
!     The first lines contain atom type indices: If monomer A has N atoms and
!     monomer B had M atoms, then there are N + M lines, with each line containing
!     one integer.  The indexing starts at 1, and repeated indices indicate symmetric
!     atoms, so that the same fit parameters are used for each atom of that type.
!     The next part of asymp.in is one line per site type, with each line contining
!     the atomic number of that site type, with 0 for off-atomic sites, followed by
!     four logical values indicating which types of interactions that site type includes.
!     The first is for electrostatic, then polarization, then Cn, then exponential.
!     The next line contains an integer from 1 to 5 specifying the number of dispersion
!     coefficients in each site-site pair, starting with C6, then C8, etc.
!     The final section contains monomer polarizabilities in AU, one per line.
! dimer.cnf : Contains monomer geometries in bohr.
! out.out.asymp : Contains set of long-range dimer configurations and energies.
!
!***************************** OUTPUT FILES ********************************
!
! asymp.dat : Input file for final fitting
!
!***************************************************************************
!


!***************************************************************************
!***************************** DEFINITIONS *********************************
!***************************************************************************

module fitAsympDef
use utility
use const
use powell
implicit none

integer, parameter :: x=1, y=2, z=3 ! Coordinate indices
integer, parameter :: a=1, b=2 ! Monomer indices
!integer, parameter :: Etot = 1, Eels = 2, Eexh = 3, Eind = 4, Eexhind = 5, Edsp = 6, Eexhdsp = 7 ! Energy component indices
integer, parameter :: maxSites = 256, maxCparm = 5, maxmon = 4
integer, parameter :: maxdim = maxmon*(maxmon + 1)/2
integer, parameter :: input_long = 1, asymp_in = 2, asymp_dat = 3, existing_parms = 4 ! File handles
real(8), parameter :: cParmRatio = 30.0d0

! Used for starting parameters of C6, C8 coefficients.
! See http://www.physics.udel.edu/~szalewic/dldf/dispersion/disp4dldf.py
real(8), parameter :: disp6(18) = 1.7345d1*[ &
     0.1726,0.0832,0.0,0.0,0.0,1.1501,0.9918,0.6463,0.4109,0.3159,0.0,0.0,0.0,0.0,0.0,5.8531,5.2305,3.0751]
real(8), parameter :: disp8(18) = 6.1941d3*[ &
     0.0000,0.0037,0.0,0.0,0.0,0.2505,0.2522,0.2222,0.0589,0.0272,0.0,0.0,0.0,0.0,0.0,3.9314,1.1213,0.6296]

!real(8), parameter :: disp6(40) = 1.7345d1*[ &
!     0.2044,6.1438,0.4117,0.3242,0.0794,0.1792,0.1863,0.0939,5.3056,0.7772, &
!     0.6309,0.5608,0.1477,0.1272,0.1965,0.0692,39.7169,0.0727,0.5970,12.2461, &
!     5.1374,1.2402,0.4142,1.5763,1.0182,0.6889,0.5181,0.2459,45.7198,0.0000, &
!     4.5841,27.5251,25.8102,2.7405,1.7066,1.1809,3.2334,7.9959,5.1041,3.0519]
!real(8), parameter :: disp8(40) = 6.1941d3*[ &
!     0.0097,0.0000,0.1598,0.0729,0.0132,0.0040,0.0017,0.0000,0.0013,0.6239, &
!     0.1697,0.1011,0.5304,0.0674,0.0009,0.0045,37.6958,0.0000,0.0000,4.1540, &
!     3.8813,0.0408,0.0020,0.1613,0.1481,0.0445,0.1202,0.0312,64.1860,21.2101, &
!     0.0000,18.3248,34.2080,0.0458,0.1561,0.0000,0.8047,0.3412,1.0469,0.6312]

! Used for calculating starting parameters of C10, C12, C14.
! See J. Chem. Phys. 89, 2092 (1988)
real(8), parameter :: xi4 = 1.225, omega4 = 1.0281, omega5 = 0.9749

type point
   real(8) :: shift(2,3)
   real(8) :: dist
   real(8) :: alpha(2), beta(2), gamma(2)
   real(8) :: ener(7)
   real(8) :: calcPol
   real(8) :: weight
end type point

type site
   integer :: type
   real(8) :: r(3)
   integer :: atomNum
end type site

type site_type
   logical :: optimizePol
   logical :: incCharge, incPol, incCparm, incExp
   real(8) :: charge, pz
   integer :: atomNum
   real(8) :: C(maxCparm)
end type site_type

type dimer
   integer :: mon(2)
end type dimer

!type site_pair_type
!   logical :: incCparm
!   real(8) :: C(maxCparm)
!end type site_pair_type

!***************************************************************************
!*************************** GLOBAL VARIABLES ******************************
!***************************************************************************

logical :: verbose
integer :: praxisPrint
integer :: existingFitMode
integer :: comRuleMode
character(100) :: sysname
character(100) :: enerfile

!real(8), allocatable :: rAB(:,:)
integer :: ndat, nstype, npolsite(maxmon), ncterm
integer :: nchargeparm, npolparm, ncparm, nmon, ndim
integer :: nsite(maxmon)
type(point), allocatable :: p(:,:)
type(site) :: s(maxmon,maxSites)
type(site_type), allocatable :: stype(:)
type(dimer), allocatable :: dmr(:)
!type(site_pair_type), allocatable :: sptype(:,:)
!integer, allocatable :: smap(:,:)
integer, allocatable :: dimmap(:,:)
integer, allocatable :: siteCount(:,:), polSiteCount(:)
!integer :: cTerms(maxCparm)
integer :: chargeBalStype(maxmon)
!real(8) :: elsDmp, dipDmp
real(8) :: comPz(maxmon)
real(8) :: totCharge(maxmon)
real(8) :: elsDecay(maxdim), vdwDecay(maxdim)
integer :: cstart(maxdim)

real(8), allocatable :: T(:,:,:,:,:,:,:)
real(8), allocatable :: Efield(:,:,:,:)

real(8) :: rBuf(20)
integer :: iBuf(10)
character(100) :: cBuf(10)
logical :: lBuf(20)

real(8), allocatable :: rABinv(:,:,:,:)
logical :: optimizeCOM
real(8) :: rA(3), rB(3)
real(8) :: Cparm(20)
real(8) :: CparmFitScale(maxdim), polParmFitScale(maxdim)
real(8) :: praxisScale, avgEN, indDispRatio
real(8) :: val, val2, polE, errSum, magSum
integer :: iVal, index, count
logical :: lval

real(8), allocatable :: optimVectPol(:), optimVectCharge(:), optimVectCparm(:)
character(200) :: str

contains

!***************************************************************************
!****************************** ROUTINES ***********************************
!***************************************************************************

! Read data points from given files
function readPoints(nEnergy)
  logical :: readPoints
  integer :: nEnergy
  
  character(200) :: fileName
  integer, parameter :: rdptUnit = 10
  integer :: ii, jj, kk, isys
  integer :: maxnsite
  
  readPoints = .true.
  do isys = 1,ndim
     
     write(fileName,'(I2.2,A,I2.2)') dmr(isys)%mon(1),'-',dmr(isys)%mon(2)
     fileName = trim(sysname) // '.ener_asym_' // trim(filename)
     write(*,*) 'Reading ',trim(fileName)
     
     ndat = nlines(fileName)
     
     if (ndat == 0) then
        readPoints = .false.
        return
     else
        open(rdptUnit, file=trim(fileName), status='old')
        
        if (isys == 1) then
           if (allocated(p)) deallocate( p )
           allocate( p(ndim,ndat) )
           
           if (allocated( rABinv )) deallocate( rABinv )
           maxnsite = 0
           do ii = 1,nmon
              maxnsite = max(maxnsite,nsite(ii))
           enddo
           allocate( rABinv(ndim,ndat,maxnsite,maxnsite) )
        endif
        
        do ii = 1,ndat
           
           read(rdptUnit,*) rBuf(1:6+nEnergy)
           
           ! Input in angstroms, degrees, kcal/mol
           p(isys,ii) =  point(reshape([0.0d0,0.0d0, 0.0d0,0.0d0, 0.0d0,rBuf(1)],[2,3])/bohr2A, &
                rBuf(1)/bohr2A, &
                [0.0d0, rBuf(4)]*d2rad, [rBuf(2), rBuf(5)]*d2rad, [rBuf(3), rBuf(6)]*d2rad, &
                [rBuf(7:6+nEnergy)]/h2kcal, &
                0.0d0, &
                1.0d+00)
           
           if (p(isys,ii)%ener(E_ind) == 0.0d0 .or. p(isys,ii)%ener(E_dsp) == 0.0d0) then
              readPoints = .false.
              return
           endif
           
        end do
        
        close(rdptUnit)
        
        do ii = 1,ndat
           do jj = 1,nsite(dmr(isys)%mon(1))
              do kk = 1,nsite(dmr(isys)%mon(2))
                 rABinv(isys,ii,jj,kk) = 1.0d0/norm(r12(isys,ii,1,jj,2,kk))
              enddo
           enddo
        enddo
        
        !call initializePol()
        
        do ii = 1,ndat
           p(isys,ii)%weight = 1.0d0
        end do
        
        readPoints = .true.
     endif
  enddo
  
end function


! Calculate vector between given sites for given configuration
function r12(isys,idat, mon1, site1, mon2, site2)
  real(8) :: r12(3)
  integer :: isys,idat, mon1, site1, mon2, site2
  
  real(8) :: r1(3), r2(3)
  
  r1 = p(isys,idat)%shift(mon1,:) + &
       matmul(zyzRotMat(p(isys,idat)%alpha(mon1), p(isys,idat)%beta(mon1), p(isys,idat)%gamma(mon1)), &
       s(dmr(isys)%mon(mon1),site1)%r)
  r2 = p(isys,idat)%shift(mon2,:) + &
       matmul(zyzRotMat(p(isys,idat)%alpha(mon2), p(isys,idat)%beta(mon2), p(isys,idat)%gamma(mon2)), &
       s(dmr(isys)%mon(mon2),site2)%r)
  
  r12 = r1 - r2
end function


!! Set Efield and dipole tensor
!subroutine initializePol()
!  
!  integer :: idat, ii, jj, kk, ll, mm, nn
!  real(8) :: rVect(3), r
!  real(8) :: val
!  !real(8) :: pz, dmpParm
!  
!  if (allocated( T )) deallocate( T )
!  allocate( T(ndat,nmol,maxnsite,nmol,maxnsite,3,3) )
!  
!  if (allocated( Efield )) deallocate( Efield )
!  allocate( Efield(ndat,nmol,maxnsite,3) )
!  
!  ! Calculate E field due to point charges
!  Efield = 0.0d0
!  do idat = 1,ndat
!     do ii = 1,nmol
!        do jj = 1,nsite(ii)
!           
!           do kk = 1,nmol
!              if (ii /= kk) then
!                 do ll = 1,nsite(kk)
!                    
!                    rVect = r12(idat, ii, jj, kk, ll)
!                    r = norm(rVect)
!                    
!                    !pz = sqrt(stype(s(ii,jj)%type)%pz*stype(s(kk,ll)%type)%pz)
!                    !dmpParm = elsDmp/pz
!                    
!                    Efield(idat,ii,jj,:) = Efield(idat,ii,jj,:) + &
!                         rVect(:)*stype(s(kk,ll)%type)%charge/r**3
!                         !TD3(r, dmpParm)*rVect(:)*stype(s(kk,ll)%type)%charge/r**3
!                 enddo
!              endif
!           enddo
!           
!        enddo
!     enddo
!  enddo
!  
!  ! Calculate dipole tensor
!  T = 0.0d0
!  do idat = 1,ndat
!     do ii = 1,nmol
!        do jj = 1,nmol
!           
!           do kk = 1,nsite(ii)
!              do ll = 1,nsite(jj)
!                 if (ii /= jj .or. kk /= ll) then
!                    
!                    rVect = r12(idat, ii, kk, jj, ll)
!                    r = norm(rVect)
!                    
!                    !pz = sqrt(stype(s(ii,kk)%type)%pz*stype(s(jj,ll)%type)%pz)
!                    !dmpParm = dipDmp
!                    
!                    do mm = x,z
!                       do nn = x,z
!                          
!                          !val = 3.0d0*TD5(r, dmpParm)*rVect(mm)*rVect(nn)/r**5
!                          val = 3.0d0*rVect(mm)*rVect(nn)/r**5
!                          if (mm == nn) then
!                             !val = val - TD3(r, dmpParm)*1.0d0/r**3
!                             val = val - 1.0d0/r**3
!                          endif
!                          
!                          T(idat,ii,kk,jj,ll,mm,nn) = val
!                          
!                       enddo
!                    enddo
!                    
!                 endif
!              enddo
!           enddo
!           
!        enddo
!     enddo
!  enddo
!  
!end subroutine


! Calculate electrostatic energy for given configuration
function elsEnergy(isys,idat)
  integer :: isys,idat
  real(8) :: elsEnergy
  
  integer :: ii, jj
  integer :: stype1, stype2
  !real(8) :: r
  
  elsEnergy = 0.0d0
  do ii = 1,nsite(dmr(isys)%mon(1))
     do jj = 1,nsite(dmr(isys)%mon(2))
        stype1 = s(dmr(isys)%mon(1),ii)%type
        stype2 = s(dmr(isys)%mon(2),jj)%type
        
        if (stype(stype1)%incCharge .and. stype(stype2)%incCharge) then
           
           elsEnergy = elsEnergy + stype(stype1)%charge*stype(stype2)%charge*rABinv(isys,idat,ii,jj)
           
        endif
     enddo
  enddo
  
end function


! Calculate VDW energy for given configuration
function CEnergy(isys,idat)
  integer :: isys,idat
  real(8) :: CEnergy
  
  integer :: ii, jj, kk
  real(8) :: rInv2, rInvN, Cval
  integer :: stype1, stype2
  
  CEnergy = 0.0d0
  do ii = 1,nsite(dmr(isys)%mon(1))
     do jj = 1,nsite(dmr(isys)%mon(2))
        stype1 = s(dmr(isys)%mon(1),ii)%type
        stype2 = s(dmr(isys)%mon(2),jj)%type
        
        if (stype(stype1)%incCparm .and. stype(stype2)%incCparm) then
           
           rInv2 = rABinv(isys,idat,ii,jj)**2
           rInvN = rInv2**((cStart(isys) - 2)/2)
           do kk = 1,ncterm
              rInvN = rInvN*rInv2
              if (comRuleMode == 0) then
                 !Cval = sptype(isys,smap(isys,ii,jj))%C(kk)
                 Cval = 0.0d0
              elseif(comRuleMode == 1) then
                 !write(*,*) stype1,stype2,stype(stype1)%C(kk),stype(stype2)%C(kk),rInv2,rInvN
                 Cval = sqrt(stype(stype1)%C(kk)*stype(stype2)%C(kk))
              endif
              CEnergy = CEnergy - Cval*rInvN
           enddo
           
        endif
     enddo
  enddo
  
end function


function polEnergy(isys,idat)
  real(8) :: polEnergy
  integer :: isys,idat
  polEnergy = 0.0d0
end function

!! Calculate polarization energy for given configuration
!function polEnergy(idat)
!  integer :: idat
!  real(8) :: polEnergy
!  
!  integer :: ii, jj, kk, ll, mm
!  real(8) :: Ti(nmol,maxnsite,nmol,maxnsite,3,3)
!  real(8) :: Efieldi(nmol,maxnsite,3)
!  real(8) :: mu(nmol,maxnsite,3), muNext(nmol,maxnsite,3)
!  real(8) :: diff(3), iterChange
!  
!  Ti = T(idat,:,:,:,:,:,:)
!  Efieldi = Efield(idat,:,:,:)
!  
!  ! Solve for induced dipoles self-consistently
!  mu(:,:,:) = 0.0
!  muNext(:,:,:) = 0.0
!  do ii = 1,10
!     iterChange = 0.0d0
!     
!     do jj = 1,nmol
!        do kk = 1,nsite(jj)
!           if (stype(s(jj,kk)%type)%incPol) then
!              
!              muNext(jj,kk,:) = Efieldi(jj,kk,:)
!              !if (idat == 1 .and. jj==1 .and. kk ==1) write(*,*)'z start ',stype(s(1,1)%type)%pz*Efieldi(jj,kk,z)
!              
!              do ll = 1,nmol
!                 do mm = 1,nsite(ll)
!                    if ((jj /= ll) .and. stype(s(ll,mm)%type)%incPol) then
!                       muNext(jj,kk,:) = muNext(jj,kk,:) + matmul(Ti(jj,kk,ll,mm,:,:), mu(ll,mm,:))
!                    endif
!                 enddo
!              enddo
!              
!              muNext(jj,kk,:) = muNext(jj,kk,:)*stype(s(jj,kk)%type)%pz
!              
!              diff = muNext(jj,kk,:) - mu(jj,kk,:)
!              iterChange = iterChange + dot_product(diff, diff)
!              
!              mu(jj,kk,:) = muNext(jj,kk,:)
!              
!              !if (idat == 1 .and. jj == 1 .and. kk == 1) write(*,*) mu(jj,kk,:)
!           endif
!        enddo
!     enddo
!     
!     if (iterChange < npolsite*1.0d-16) exit
!  end do
!  
!  ! Find final polarization energy
!  polEnergy = 0.0
!  do ii = 1,nmol
!     do jj = 1,nsite(ii)
!        
!        polEnergy = polEnergy - 0.5d0*dot_product(Efieldi(ii,jj,:), mu(ii,jj,:))
!        
!     end do
!  end do
!  
!  !write(*,*) idat,polEnergy
!  
!end function
!
!
!! Scale polarizabilities so they sum to COM polarizability
!subroutine normalizePz
!  
!  integer :: ii, jj
!  real(8) :: sum
!  
!  do ii = 1,nmol
!     sum = 0.0d0
!     do jj = 1,nstype
!        sum = sum + siteCount(ii,jj)*stype(jj)%pz
!     enddo
!     
!     if (sum > 0.0d0) then
!        sum = comPz(ii)/sum
!        do jj = 1,nstype
!           if (siteCount(ii,jj) > 0) then
!              stype(jj)%pz = stype(jj)%pz*sum
!           endif
!        enddo
!     endif
!     
!  enddo
!  
!end subroutine


! Prepare given parameter vector with program values for charge optimization
subroutine loadParmVectCharge(parmVect)
  real(8) :: parmVect(nchargeparm)
  
  integer :: ii,isys
  integer :: index
  logical :: isOptimized
  real(8) :: sum
  
  index = 0
  !do isys = 1,nmon
  !   do ii = 1,nsite(isys)
  !      if (stype(s(isys,ii)%type)%incCharge .and. chargeBalStype(isys) /= ii) then
  !         index = index + 1
  !         parmVect(index) = stype(s(isys,ii)%type)%charge
  !      endif
  !   enddo
  !enddo
  do ii = 1,nstype
     isOptimized = .true.
     do isys = 1,nmon
        if (chargeBalStype(isys) == ii) isOptimized = .false.
     enddo
     
     if (isOptimized) then
        index = index + 1
        parmVect(index) = stype(ii)%charge
     endif
  enddo
  
  do isys = 1,ndim
     sum = 0.0d0
     do ii = 1,ndat
        !p(ii)%weight = p(ii)%dist**3
        !p(ii)%weight = 1.0d0/(1.0d0/h2kcal + abs(p(ii)%ener(Eels)))**2
        p(isys,ii)%weight = 1.0d0/sumInvDist(isys,ii,elsDecay(isys))
        sum = sum + p(isys,ii)%weight
     enddo
     
     do ii = 1,ndat
        p(isys,ii)%weight = p(isys,ii)%weight/sum
     enddo
  enddo
  
end subroutine


! Set program values to ones from parameter vector for charges
! chargeBalStype for each monomer set so that charge sums to zero
subroutine unloadParmVectCharge(parmVect)
  real(8) :: parmVect(nchargeparm)
  
  integer :: ii, jj,isys
  integer :: index
  real(8) :: chargeSum(nmon)
  logical :: isOptimized
  
  index = 0
  !do isys = 1,mon
  !   chargeSum = 0.0d0
  !   do ii = 1,nstype(isys)
  !      if (stype(s(isys,ii)%type)%incCharge) then
  !         
  !         isBalanceStype = .false.
  !         do jj = 1,2
  !            if (chargeBalStype(isys,jj) == ii) then
  !               stype(isys,ii)%charge = (totCharge(isys,jj) - chargeSum(jj))/siteCount(isys,jj,ii)
  !               isBalanceStype = .true.
  !            endif
  !         enddo
  !         
  !         if (.not. isBalanceStype) then
  !            index = index + 1
  !            stype(isys,ii)%charge = parmVect(index)
  !            
  !            do jj = 1,2
  !               chargeSum(jj) = chargeSum(jj) + siteCount(isys,jj,ii)*stype(isys,ii)%charge
  !            enddo
  !         endif
  !         
  !      endif
  !   enddo
  !enddo
  chargeSum(:) = 0.0d0
  do ii = 1,nstype
     isOptimized = .true.
     do isys = 1,nmon
        if (chargeBalStype(isys) == ii) isOptimized = .false.
     enddo
     
     if (isOptimized) then
        index = index + 1
        stype(ii)%charge = parmVect(index)
        do isys = 1,nmon
           chargeSum(isys) = chargeSum(isys) + siteCount(isys,ii)*stype(ii)%charge
        enddo
     endif
  enddo
  
  do isys = 1,nmon
     do ii = 1,nstype
        if (chargeBalStype(isys) == ii) then
           stype(ii)%charge = (totCharge(isys) - chargeSum(isys))/siteCount(isys,ii)
        endif
     enddo
  enddo
  
end subroutine


!! Prepare given parameter vector with program values for polarizability optimization
!subroutine loadParmVectPol(parmVect)
!  real(8) :: parmVect(npolparm)
!  
!  integer :: ii
!  integer :: index
!  
!  if (optimizeCOM) parmVect(npolparm) = encode(comPz(1))
!  
!  index = 0
!  do ii = 1,nstype
!     if (stype(ii)%optimizePol) then
!        index = index + 1
!        parmVect(index) = encode(stype(ii)%pz)
!     endif
!  enddo
!  
!  do ii = 1,ndat
!     !p(ii)%weight = p(ii)%dist**6
!     p(ii)%weight = 1.0d0/sumInvDist(ii, vdwDecay)
!  enddo
!  
!  polParmFitScale = 0.0d0
!  do ii = 1,ndat
!     polParmFitScale = polParmFitScale + p(ii)%weight*p(ii)%ener(E_ind)**2
!  enddo
!  polParmFitScale = 1.0d-2*polParmFitScale/npolparm
!  
!end subroutine


!! Set program values to ones from parameter vector for polarizability
!subroutine unloadParmVectPol(parmVect)
!  real(8) :: parmVect(npolparm)
!  
!  integer :: ii
!  integer :: index
!  
!  if (optimizeCOM) comPz = decode(parmVect(npolparm))
!  
!  index = 0
!  do ii = 1,nstype
!     if (stype(ii)%optimizePol) then
!        index = index + 1
!        stype(ii)%pz = decode(parmVect(index))
!     endif
!  enddo
!  
!  call normalizePz
!  
!end subroutine


! Prepare given parameter vector with program values for C-parm optimization
subroutine loadParmVectCparm(parmVect)
  real(8) :: parmVect(ncparm)
  
  integer :: ii, jj,isys
  integer :: index
  real(8) :: factor
  real(8) :: sum
  
  index = 0
  do ii = 1,nstype
     if (stype(ii)%incCparm) then
        factor = 1.0d0
        do jj = 1,ncterm
           index = index + 1
           parmVect(index) = encode(stype(ii)%C(jj)/factor)
           factor = factor*cParmRatio
        enddo
     endif
  enddo
  
  do isys = 1,ndim
     sum = 0.0d0
     do ii = 1,ndat
        !p(ii)%weight = p(ii)%dist**6
        !p(ii)%weight = 1.0d0/(1.0d0/h2kcal + abs(p(ii)%ener(E_ind) + p(ii)%ener(E_dsp)))**2
        p(isys,ii)%weight = 1.0d0/sumInvDist(isys,ii, vdwDecay(isys))
        sum = sum + p(isys,ii)%weight
     enddo
     
     do ii = 1,ndat
        p(isys,ii)%weight = p(isys,ii)%weight/sum
     enddo
  enddo
  
  !do isys = 1,ndim
  !   CParmFitScale(isys) = 0.0d0
  !   do ii = 1,ndat
  !      CParmFitScale(isys) = CParmFitScale(isys) + &
  !           p(isys,ii)%weight*(p(isys,ii)%ener(E_ind) + p(isys,ii)%ener(E_dsp))**2
  !   enddo
  !enddo
  !CparmFitScale(isys) = 4.0d-4*CParmFitScale(isys)/dble(ncparm)/dble(ndim)
  
end subroutine


! Set program values to ones from parameter vector for C-parms
subroutine unloadParmVectCparm(parmVect)
  real(8) :: parmVect(ncparm)
  
  integer :: ii, jj,isys
  integer :: index
  real(8) :: factor
  
  index = 0
  do ii = 1,nstype
     if (stype(ii)%incCparm) then
        factor = 1.0d0
        do jj = 1,ncterm
           index = index + 1
           stype(ii)%C(jj) = decode(parmVect(index))*factor
           factor = factor*cParmRatio
        enddo
     endif
  enddo
end subroutine


! Function called by the optimizer for charge optimization
function errFuncCharge(x, n)
  real(8) :: errFuncCharge
  real(8) :: x(n)
  integer :: n
  
  integer :: ii,isys
  integer :: index
  real(8) :: sum, magSum
  
  call unloadParmVectCharge(x)
  
  errFuncCharge = 0.0d0
  do isys = 1,ndim
     sum = 0.0d0
     magSum = 0.0d0
     do ii = 1,ndat
        magSum = magSum + abs(p(isys,ii)%ener(E_els))
        sum = sum + p(isys,ii)%weight*(p(isys,ii)%ener(E_els) - elsEnergy(isys,ii))**2
     enddo
     errFuncCharge = errFuncCharge + sqrt(sum/ndat)/(magSum/ndat)
  enddo
  
end function


!! Function called by the optimizer for polarizability optimization
!function errFuncPol(x, n)
!  real(8) :: errFuncPol
!  real(8) :: x(n)
!  integer :: n
!  
!  integer :: ii
!  integer :: index
!  
!  call unloadParmVectPol(x)
!  
!  errFuncPol = 0.0
!  do ii = 1,ndat
!     errFuncPol = errFuncPol + p(ii)%weight*(p(ii)%ener(E_ind) - polEnergy(ii))**2
!  enddo
!  
!  if (.not. optimizeCOM) then
!     do ii = 1,nstype
!        if (stype(ii)%incPol) then
!           errFuncPol = errFuncPol - polParmFitScale*log(stype(ii)%pz)
!        endif
!     enddo
!  endif
!  
!end function


!! Function called by the optimizer for pol damping optimization
!function errFuncDmp(x, n)
!  real(8) :: errFuncDmp
!  real(8) :: x(n)
!  integer :: n
!  
!  integer :: ii
!  integer :: index
!  
!  call unloadParmVectDmp(x)
!  
!  errFuncDmp = 0.0d0
!  do ii = 1,ndat
!     errFuncDmp = errFuncDmp + p(ii)%weight*(p(ii)%ener(E_ind) - polEnergy(ii))**2
!  enddo
!  
!end function


! Function called by the optimizer for C-parm optimization
function errFuncCparm(x, n)
  real(8) :: errFuncCparm
  real(8) :: x(n)
  integer :: n
  
  integer :: ii, jj, isys
  integer :: index
  real(8) :: ener, sum, magSum
  
  call unloadParmVectCparm(x)
  
  errFuncCparm = 0.0d0
  do isys = 1,ndim
     sum = 0.0d0
     magSum = 0.0d0
     do ii = 1,ndat
        ener = p(isys,ii)%ener(E_ind) + p(isys,ii)%ener(E_dsp)
        magSum = magSum + abs(ener)
        sum = sum + p(isys,ii)%weight*((ener - p(isys,ii)%calcPol) - CEnergy(isys,ii))**2
        !write(*,*)p(isys,ii)%weight, CEnergy(isys,ii), errFuncCparm
     enddo
     errFuncCparm = errFuncCparm + sqrt(sum/ndat)/(magSum/ndat)
  enddo
  
  ! Penalty function to use larger C-parm values
  do ii = 1,ncterm
     do jj = 1,nstype
        if (stype(jj)%incCparm) then
           do isys = 1,ndim
              !errFuncCparm = errFuncCparm - &
              !     CParmFitScale(isys)*dble(siteCount(dmr(isys)%mon(1),jj) + siteCount(dmr(isys)%mon(2),jj))* &
              !     log(stype(jj)%C(ii)/cParmRatio**(ii - 1))
              errFuncCparm = errFuncCparm - 1.0d-7*ndim/ncparm*log(stype(jj)%C(ii)/cParmRatio**(ii - 1))
           enddo
        endif
     enddo
  enddo
  
end function


! Gives sum of site-site distances to power -pow
function sumInvDist(isys, ipt, pow)
  real(8) :: sumInvDist
  integer :: ipt, isys
  real(8) :: pow
  
  integer :: ii, jj
  real(8) :: sum
  
  sum = 0.0d0
  do ii = 1,nsite(dmr(isys)%mon(1))
     do jj = 1,nsite(dmr(isys)%mon(2))
        sum = sum + rABinv(isys,ipt,ii,jj)**pow
     enddo
  enddo
  
  sumInvDist = sum
end function



!*************************** PURE  FUNCTIONS *******************************


! Matrix to rotate vector by euler angles
pure function zyzRotMat(aa, ab, ag)
  real(8), intent(in) :: aa, ab, ag
  real(8) :: zyzRotMat(3,3)

  zyzRotMat(1,1) = cos(aa)*cos(ab)*cos(ag) - sin(aa)*sin(ag)
  zyzRotMat(1,2) = -cos(aa)*cos(ab)*sin(ag) - sin(aa)*cos(ag)
  zyzRotMat(1,3) = cos(aa)*sin(ab)
  zyzRotMat(2,1) = sin(aa)*cos(ab)*cos(ag) + cos(aa)*sin(ag)
  zyzRotMat(2,2) = -sin(aa)*cos(ab)*sin(ag) + cos(aa)*cos(ag)
  zyzRotMat(2,3) = sin(aa)*sin(ab)
  zyzRotMat(3,1) = -sin(ab)*cos(ag)
  zyzRotMat(3,2) = sin(ab)*sin(ag)
  zyzRotMat(3,3) = cos(ab)
end function


! Encode parameter for fitting procedure
pure function encode(x)
  real(8), intent(in) :: x
  real(8) :: encode
  
  real(8) :: xPos
  
  xPos = max(x, 1.0d-30)
  
  if (xPos < 1.0d0) then
     encode = 10.0d0*log(xPos)
     !encode = -sqrt(1.0d0/xPos - 1.0d0)
  else
     encode = sqrt(xPos) - 1.0d0
     !encode = xPos - 1.0d0
     !encode = (xPos**2 - 1)/2
  end if
  
  encode = encode
  
end function


! Decode fitting result to parameter value
pure function decode(x)
  real(8), intent(in) :: x
  real(8) :: decode
  
  real(8) :: xPos
  
  xPos = x
  
  if (xPos < 0.0d0) then
     decode = exp(0.1d0*xPos)
     !decode = 1.0d0/(1.0d0 + x**2)
  else
     decode = (xPos + 1.0d0)**2
     !decode = x + 1.0d0
     !decode = sqrt(2*x + 1)
  end if
  
  decode = max(decode, 1.0d-30)
  
end function


! Norm of vector
function norm(rVect)
  real(8) :: norm
  real(8) :: rVect(3)

  norm = sqrt(rVect(x)**2 + rVect(y)**2 + rVect(z)**2)
end function


end module


!***************************************************************************
!**************************** MAIN PROGRAM *********************************
!***************************************************************************

program fitAsymp
use fitAsympDef
use const
use utility
use omp_lib
implicit none

integer :: i,j,k,l,isys

!***************************************************************************
!*************************** INITIALIZATION ********************************
!***************************************************************************

optimizeCOM = .false.

! Read command line parameters
do i = 1,10
   call getarg(i, cbuf(i))
enddo

read(cbuf(1),*) sysname
!read(cbuf(2),*) comRuleMode
read(cbuf(2),*) ncterm

verbose = .true.
comRuleMode = 1

!enerfile = trim(sysname)//'.ener_asym'

if (verbose) then
   praxisPrint = 1
else
   praxisPrint = 0
endif

! Initialize sites
open(input_long, file = trim(sysname)//'.input', status = 'old')

read(input_long,*) nmon

allocate( dmr(nmon*(nmon+1)/2) )
allocate( stype(maxSites) )
allocate( siteCount(nmon,maxSites), polSiteCount(nmon) )
allocate( dimmap(nmon,nmon) )

do i = 1,nmon
   do j = i,nmon
      ndim = ndim + 1
      dimmap(i,j) = ndim
      dimmap(j,i) = ndim
      dmr(ndim)%mon(1) = i
      dmr(ndim)%mon(2) = j
   enddo
enddo

nstype = 0
do isys = 1,nmon
   
   read(input_long,*) val, totCharge(isys), comPz(isys)
   read(input_long,*) nsite(isys)
   do i = 1,nsite(isys)
      read(input_long,*) cbuf(1), rBuf(1), rBuf(2), rBuf(3), ibuf(1), ibuf(2)
      
      ! Input in bohr
      s(isys,i) = site(ibuf(2), [rBuf(1), rBuf(2), rBuf(3)], ibuf(1))
      nstype = max(nstype, s(isys,i)%type)
   enddo
enddo

! Initialize site types
do i = 1,nstype
   read(input_long,*) lBuf(1), lBuf(2), lBuf(3), lBuf(4)
   
   stype(i)%optimizePol = .false.
   stype(i)%incCharge = lBuf(1)
   stype(i)%incPol = lBuf(2)
   stype(i)%incCparm = lBuf(3)
   stype(i)%incExp = lBuf(4)
   stype(i)%charge = 0.0d0
   stype(i)%pz = 0.0d0
   do isys = 1,nmon
      do j = 1,nsite(isys)
         if (s(isys,j)%type == i) stype(i)%atomNum = s(isys,j)%atomNum
      enddo
   enddo
   
   stype(i)%C(:) = 0.0d0
   if (stype(i)%incCparm) then
      stype(i)%C(1) = 1.0d0
      do j = 2,ncterm
         stype(i)%C(j) = cParmRatio*stype(i)%C(j-1)
      enddo
   endif
end do

close(input_long)   

! Calculate additional information about the sites and set initial pz
npolsite = 0
do isys = 1,nmon
   polSiteCount(isys) = 0
   
   do i = 1,nstype
      siteCount(isys,i) = 0
      
      do j = 1,nsite(isys)
         if (s(isys,j)%type == i) then
            siteCount(isys,i) = siteCount(isys,i) + 1
            
            if (stype(i)%incPol) then
               polSiteCount(isys) = polSiteCount(isys) + 1
               npolsite = npolsite + 1
               
               stype(i)%pz = atomPol(stype(i)%atomNum) !comPz(i)
            endif
         endif
      enddo
      
   enddo
enddo

!call normalizePz

! Set starting charges
do isys = 1,nmon
   avgEN = 0.0d0
   do i = 1,nsite(isys)
      if (stype(s(isys,i)%type)%atomNum > 0) then
         avgEN = avgEN + electroNeg(stype(s(isys,i)%type)%atomNum)
      endif
   enddo
   avgEN = avgEN/nsite(isys)
   
   do i = 1,nstype
      if (stype(i)%atomNum > 0) then
         if (siteCount(isys,i) > 0) then
            stype(i)%charge = 1.0d0*(avgEN - electroNeg(stype(i)%atomNum)) + totCharge(isys)/nsite(isys)
         endif
      else
         stype(i)%charge = 0.0d0
      endif
   enddo
enddo

! Set special site types for balancing charge of each monomer
chargeBalStype(:) = -1
do isys = 1,nmon
   do i = 1,nstype
      if (siteCount(isys,i) > 0) then
         lval = .true.
         do j = 1,nmon
            if (j /= isys .and. siteCount(j,i) > 0) lval = .false.
         enddo
         if (lval) chargeBalStype(isys) = i
      endif
   enddo
enddo

   ! Initialize site pair types
   !nsptype = nstype*(nstype+1)/2
   
   !index = 1
   !do i = 1,nstype(isys)
   !   do j = 1,nstype(isys)
   !      if (siteCount(isys,a,i) > 0 .and. siteCount(isys,b,j) > 0 .and. &
   !           (j >= i .or. siteCount(isys,a,j) == 0)) then
   !         
   !         sptype(isys,index)%incCparm = stype(isys,i)%incCparm .and. stype(isys,j)%incCparm
   !         
   !         do k = 1,maxCParm
   !            sptype(isys,index)%C(k) = sqrt(stype(isys,i)%C(k)*stype(isys,j)%C(k))
   !         enddo
   !         !write(*,*) index,sptype(index)%C(1)
   !         
   !         do k = 1,nsite(isys,a)
   !            do l = 1,nsite(isys,b)
   !               if ((s(isys,a,k)%type == i .and. s(isys,b,l)%type == j) .or. &
   !                    (s(isys,a,k)%type == j .and. s(isys,b,l)%type == i)) smap(isys,k,l) = index
   !            enddo
   !         enddo
   !         
   !         index = index + 1
   !         
   !      endif
   !   enddo
   !enddo
   !
   !nsptype(isys) = index - 1

! Initialize parameter optimization info
if (optimizeCOM) then
   npolparm = 1
else
   npolparm = 0
endif
nchargeparm = 0
ncparm = 0


do i = 1,nstype
   if (stype(i)%incPol) then
      stype(i)%optimizePol = .true.
      do isys = 1,nmon
         if (siteCount(isys,i) == polSiteCount(isys)) stype(i)%optimizePol = .false.
      enddo
   endif
   
   if (stype(i)%optimizePol) npolparm = npolparm + 1
enddo

do i = 1,nstype
   if (stype(i)%incCharge) then
      lval = .true.
      do isys = 1,nmon
         if (chargeBalStype(isys) == i) lval = .false.
      enddo
      if (lval) nchargeparm = nchargeparm + 1 
   endif
enddo

do i = 1,nstype
   if (stype(i)%incCparm) ncparm = ncparm + ncterm
enddo

allocate( optimVectPol(npolparm) )
allocate( optimVectCharge(nchargeparm) )
allocate( optimVectCparm(ncparm) )

do isys = 1,ndim
   ! Determine how fast energies decay
   if (abs(totCharge(dmr(isys)%mon(1))) < 1.0d-10 .and. abs(totCharge(dmr(isys)%mon(2))) < 1.0d-10) then
      elsDecay(isys) = 3.0d0
      vdwDecay(isys) = 6.0d0
      cStart(isys) = 6
   elseif (abs(totCharge(dmr(isys)%mon(1))) > 1.0d-10 .and. abs(totCharge(dmr(isys)%mon(2))) > 1.0d-10) then
      elsDecay(isys) = 1.0d0
      vdwDecay(isys) = 4.0d0
      cStart(isys) = 4
   else
      elsDecay(isys) = 2.0d0
      vdwDecay(isys) = 4.0d0
      cStart(isys) = 4
   endif
enddo

! Initialize grid points
if (.not. readPoints(7)) then
   write(*,*) 'ERROR reading energy file'
   stop
end if

write(*,*) nsite(1),nsite(2),nsite(3)
write(*,*) ndat, nstype, nchargeparm, nmon, ndim
   
!***************************************************************************
!******************************* OPTIMIZE **********************************
!***************************************************************************

!write(*,*) ' Initial els error:'
!write(*,'(E12.5,A,F12.6,A)') rmsErrEls(),' kcal/mol   ',pctErrEls(),'%'

if (nchargeparm > 0) then
   do isys = 1,nmon
      write(*,*) ''
      write(*,'(A,I3,A)') 'System ',isys,' starting charges:'
      val = 0.0d0
      do i = 1,nsite(isys)
         write(*,'(F15.10)') stype(s(isys,i)%type)%charge
         val = val + stype(s(isys,i)%type)%charge
      enddo
      write(*,'(A,F12.8)') 'Total charge = ',val
   enddo
endif

if (nchargeparm > 0) then
   write(*,*)
   write(*,*) 'BEGIN OPTIMIZING CHARGES ', nchargeparm
   praxisScale = sqrt(real(nchargeparm))
   call loadParmVectCharge(optimVectCharge)
   call praxis(200, 1.0d-4, 2.0d0*praxisScale, nchargeparm, praxisPrint, optimVectCharge, errFuncCharge, 0)
   call unloadParmVectCharge(optimVectCharge)
endif

write(*,*) ''
write(*,*) ' Final els results (kcal/mol):'
do isys = 1,ndim
   errSum = 0.0d0
   magSum = 0.0d0
   do i = 1,ndat
      errSum = errSum + (p(isys,i)%ener(E_els) - elsEnergy(isys,i))**2
      magSum = magSum + abs(p(isys,i)%ener(E_els))
   enddo
   write(*,'(A,I2.2,A,I2.2,A,E12.5,A,E12.5)') &
        'System ',dmr(isys)%mon(1),'-',dmr(isys)%mon(2),': rmse=',sqrt(errSum/ndat),'   mean magnitude=',magsum/ndat
enddo
!call initializePol()

!write(*,*) ''
!write(*,*) ' Initial VDW error:'
!write(*,'(E12.5,A,F12.6,A)') rmsErrVDW(),' kcal/mol   ',pctErrVDW(),'%'

!if (npolparm > 0) then
!   write(*,*)
!   write(*,*) 'BEGIN OPTIMIZING POLARIZABILITIES ', npolparm
!   praxisScale = sqrt(real(npolparm))
!   call loadParmVectPol(optimVectPol)
!   call praxis(20, 1.0d-2, 1.0d0*praxisScale, npolparm, praxisPrint, optimVectPol, errFuncPol, 0)
!   call unloadParmVectPol(optimVectPol)
!   
!   do i = 1,ndat
!      p(i)%calcPol = polEnergy(i)
!   enddo
!endif

if (ncparm > 0) then
   write(*,*)
   write(*,*) 'BEGIN OPTIMIZING C-PARAMETERS ', ncparm
   praxisScale = sqrt(real(ncparm))
   call loadParmVectCparm(optimVectCparm)
   call praxis(200, 1.0d-4, 0.5d0*praxisScale, ncparm, praxisPrint, optimVectCparm, errFuncCparm, 0)
!   CParmFitScale = 0.0d0
!   call praxis(200, 2.0d-4*praxisScale, 0.05d0*praxisScale, ncparm, praxisPrint, optimVectCparm, errFuncCparm, 0)
   call unloadParmVectCparm(optimVectCparm)
endif

write(*,*) ''
write(*,*) ' Final VDW results (kcal/mol):'
do isys = 1,ndim
   errSum = 0.0d0
   magSum = 0.0d0
   do i = 1,ndat
      val = p(isys,i)%ener(E_ind) + p(isys,i)%ener(E_dsp)
      errSum = errSum + (val - (CEnergy(isys,i) + polEnergy(isys,i)))**2
      magSum = magSum + abs(val)
   enddo
   write(*,'(A,I2.2,A,I2.2,A,E12.5,A,E12.5)') &
        'System ',dmr(isys)%mon(1),'-',dmr(isys)%mon(2),': rmse=',sqrt(errSum/ndat),'   mean magnitude=',magsum/ndat
enddo
!write(*,'(E12.5,A,F12.6,A)') rmsErrVDW(),' kcal/mol   ',pctErrVDW(),'%'

!write(*,*)
!do i = 1,ndat
!   write(*,*) i,CEnergy(i)*h2kcal,(p(i)%ener(E_ind)+p(i)%ener(E_dsp))*h2kcal
!enddo

!if (npolsite > 0) then
!   if (.not. readPoints('out.out.close', 7)) then
!      write(*,*) 'Error reading out.out.close'
!   end if
!   
!   write(*,*)
!   write(*,*) 'BEGIN OPTIMIZING POLARIZATION DAMPING PARAMETERS'
!   praxisScale = 1.0
!   call loadParmVectDmp(optimVectDmp)
!   call praxis(20, 1.0d-2, praxisScale, 2, praxisPrint, optimVectDmp, errFuncDmp, 0)
!   call unloadParmVectDmp(optimVectDmp)
!   
!endif

!***************************************************************************
!******************************** OUTPUT ***********************************
!***************************************************************************

write(*,*) ''
if (.not. readPoints(7)) then
   write(*,*) 'Error reading energy file'
end if

100 continue

if (optimizeCOM) then
   write(*,*) ''
   write(*,*) 'COM polarizabilities (AU):',comPz(1)!*bohr2A**3
endif

if (nchargeparm > 0) then
   do isys = 1,nmon
      write(*,*) ''
      write(*,'(A,I3,A)') 'System ',isys,'  charges:'
      val = 0.0d0
      do i = 1,nsite(isys)
         write(*,'(F15.10)') stype(s(isys,i)%type)%charge
         val = val + stype(s(isys,i)%type)%charge
      enddo
      write(*,'(A,F12.8)') 'Total charge = ',val
   enddo
endif

!if (npolparm > 0) then
!   write(*,*) ''
!   write(*,*) 'Polarizabilities (AU):'
!   do i = 1,nstype
!      write(*,'(I4,F15.10)') i, stype(i)%pz!*bohr2A**3
!   enddo
!   
!!   write(*,*) ''
!!   write(*,*) 'Electrostatic damping: ', elsDmp
!!   write(*,*) 'Dipole damping: ', dipDmp
!endif

val = 0.0d0
do isys = 1,ndim
   do i = 1,ndat
      val = val + p(i,isys)%ener(E_ind)/p(i,isys)%ener(E_dsp)
   enddo
enddo
val = val/(ndim*ndat)
write(*,*)
write(*,'(A,F20.6)') 'Mean ratio of E_ind/E_dsp = ',val

do isys = 1,ndim
   write(enerfile,'(I0,A,I0)') dmr(isys)%mon(1),'-',dmr(isys)%mon(2)
   open(asymp_dat, file = 'asymp.dat.' // trim(enerfile), status = 'new')
   
   do i = 1,nstype
      if (siteCount(dmr(isys)%mon(1),i) > 0 .or. siteCount(dmr(isys)%mon(2),i) > 0) then
         write(asymp_dat,'(I4,A,E20.12,A,E20.12,A,L)') &
              stype(i)%atomNum,' ',stype(i)%charge,' ',stype(i)%pz,' ',stype(i)%incExp
      endif
   enddo
   
   write(asymp_dat,'(I5,A,I5,A,I5)') ncterm,' ',comRuleMode,' ',cStart(isys)
   
   do i = 1,nstype
      if (siteCount(dmr(isys)%mon(1),i) > 0 .or. siteCount(dmr(isys)%mon(2),i) > 0) then
         iVal = cStart(isys) !6
         do j = 1,ncterm
            write(asymp_dat,'(E15.8,A)',advance='no') &
                 stype(i)%C(j)*h2kcal*bohr2A**iVal,' '
            iVal = iVal + 2
         enddo
         write(asymp_dat,'()')
      endif
   enddo
   
   close(asymp_dat)
enddo

end program

