program make_asym_grid
  use const
  use dimer
  use randnum
  use dgrid
  implicit none
  
  real(8), parameter :: rGrid(3) = [1.0d0, 1.4d0, 1.8d0]
  character(100) :: input_file, geo_file, in_name, ch_distr
  logical :: distr
  real(8) :: ratio, rStart, metric
  real(8) :: beta_A, gamma_A, alpha_B, beta_B, gamma_B
  real :: rand
  real(8) :: gpoint(MAX_NGIND)
  integer :: i, j
  
  call getarg(1,in_name)
  call getarg(2,ch_distr)
  read(ch_distr,*) distr
  input_file = trim(in_name)//'.input_long'
  geo_file = trim(in_name)//'.geo'
  call read_dimer(input_file)
  call initGridCoors(n_ener)
  
  open(unit=11,file=geo_file)
  call init_random_seed()
  
  do i = 1,4000
     call random_number(rand)
     gpoint(C_betaA) = dacos(1.998d0*rand - 0.999d0)
     call random_number(rand)
     gpoint(C_gammaA) = 2.0d0*pi*rand
     call random_number(rand)
     gpoint(C_alphaB) = 2.0d0*pi*rand
     call random_number(rand)
     gpoint(C_betaB) = dacos(1.998d0*rand - 0.999d0)
     call random_number(rand)
     gpoint(C_gammaB) = 2.0d0*pi*rand
     
     gpoint(C_rcom) = 0.0d0
     call moveDimerGP(gpoint)
     do j = 1,3
        if (distr) then
           ratio = 0.0d0
        else
           ratio = 0.70d0
        endif
        gpoint(C_rcom) = minDist(rGrid(j)*(4.5d0*A2bohr + ratio*max(molsize(1),molsize(2))))
        
        call writeCoors(gpoint, 0, 11)
        !write(11,'(6F12.3,A12)') &
        !     r*bohr2A, beta_A*rad2d, gamma_A*rad2d, alpha_B*rad2d, beta_B*rad2d, gamma_B*rad2d, '''DOIT'''
     enddo
  enddo
  
  close(11)
  
end program make_asym_grid
