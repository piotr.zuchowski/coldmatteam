
! This program is an implementation of 'variance minimizing grid' generation

module probmod
  use const
  use dimer
  use force_field
  implicit none
  
  integer, parameter :: prec = quadp
  
  integer :: ngrid, ngrid0, nparm
  
contains
  
  function sig(J0, I0, K0, H0, incDelta)
    real(8) :: sig
    real(kind=prec) :: J0(nparm,nparm), I0(nparm,nparm)
    real(kind=prec) :: K0(nparm), H0(nparm)
    logical :: incDelta
    
    integer :: i, j, k, l
    real(kind=prec) :: L0
    
#ifdef QBLAS
    real(8) :: J0_1(nparm,nparm), J0_2(nparm,nparm), J0_1_l(nparm), J0_2_l(nparm)
    real(8) :: I0_1(nparm,nparm), I0_2(nparm,nparm), I0_1_l(nparm), I0_2_l(nparm)
    real(8) :: K0_1(nparm), K0_2(nparm)
    real(8) :: H0_1(nparm), H0_2(nparm)
    
    do i = 1,nparm
       if (incDelta) then
          K0_1(i) = real(K0(i), kind=dblep)
          K0_2(i) = real(K0(i) - real(K0_1(i), kind=prec), kind=dblep)
          H0_1(i) = real(H0(i), kind=dblep)
          H0_2(i) = real(H0(i) - real(H0_1(i), kind=prec), kind=dblep)
       endif
       do j = 1,nparm
          J0_1(i,j) = real(J0(i,j), kind=dblep)
          J0_2(i,j) = real(J0(i,j) - real(J0_1(i,j), kind=prec), kind=dblep)  
          I0_1(i,j) = real(I0(i,j), kind=dblep)
          I0_2(i,j) = real(I0(i,j) - real(I0_1(i,j), kind=prec), kind=dblep)
       enddo
    enddo
#endif
    
    L0 = 0.0d0
    
    do l = nparm,1,-1
       
#ifdef QBLAS
       L0 = L0 + abs(I0_1(l,l)/J0_1(l,l)) 
       if (incDelta) then
          L0 = L0 + I0_1(l,l)*K0_1(l)**2/J0_1(l,l)**2 + 2*K0_1(l)*H0_1(l)/J0_1(l,l)
       endif
#else
       L0 = L0 + abs(I0(l,l)/J0(l,l))
       if (incDelta) then
          L0 = L0 + I0(l,l)*K0(l)**2/J0(l,l)**2 + 2*K0(l)*H0(l)/J0(l,l)
       endif
#endif
       
       if (l > 1) then
          
#ifdef QBLAS
          J0_1_l(1:l) = J0_1(l,1:l)
          J0_2_l(1:l) = J0_2(l,1:l)
          I0_1_l(1:l) = I0_1(l,1:l)
          I0_2_l(1:l) = I0_2(l,1:l)
          if (incDelta) then
             call ddaxpy(l-1, -1.0d0*K0_1(l)/J0_1(l,l), 0.0d0, J0_1_l, J0_2_l, 1, K0_1, K0_2, 1)
             call ddaxpy(l-1, K0_1(l)/J0_1(l,l), 0.0d0, I0_1_l, I0_2_l, 1, H0_1, H0_2, 1)
             call ddaxpy(l-1, -1.0d0*K0_1(l)*I0_1(l,l)/J0_1(l,l)**2 - H0_1(l)/J0_1(l,l), 0.0d0, &
                  J0_1_l, J0_2_l, 1, H0_1, H0_2, 1)
          endif
          call ddsyr('L', l-1, -1.0d0/J0_1(l,l), 0.0d0, J0_1_l, J0_2_l, 1, J0_1, J0_2, nparm)
          call ddsyr2('L', l-1, -1.0d0/J0_1(l,l), 0.0d0, &
               J0_1_l, J0_2_l, 1, I0_1_l, I0_2_l, 1, I0_1, I0_2, nparm)
          call ddsyr('L', l-1, I0_1(l,l)/J0_1(l,l)**2, 0.0d0, J0_1_l, J0_2_l, 1, I0_1, I0_2, nparm)
#else
          if (incDelta) then
             do i = 1,l-1
                K0(i) = K0(i) - J0(l,i)*K0(l)/J0(l,l)
             enddo
             do i = 1,l-1
                H0(i) = H0(i) + I0(l,i)*K0(l)/J0(l,l) - &
                     J0(l,i)*(K0(l)*I0(l,l)/J0(l,l)**2 + H0(l)/J0(l,l))
             enddo
          endif
          do i = 1,l-1
             do j = 1,i
                J0(i,j) = J0(i,j) - J0(l,i)*J0(l,j)/J0(l,l)
             enddo
          enddo
          do i = 1,l-1
             do j = 1,i
                I0(i,j) = I0(i,j) - (J0(l,i)*I0(l,j) + J0(l,j)*I0(l,i))/J0(l,l) + &
                     J0(l,i)*J0(l,j)*I0(l,l)/J0(l,l)**2
             enddo
          enddo
#endif
          
       endif
    enddo
    
    if (L0 /= L0 .or. L0 < 0.0d0) then
       sig = 1.0d10
    else
       sig = sqrt(L0/ngrid0)
    endif
  end function sig
  
end module probmod

program probgrid
  use probmod
  use utility
  use weight
  use dgrid
  use qsort
  implicit none
  
  real(8), parameter :: e_cutoff = 12.0d0
  real(8), parameter :: stdMult = 0.50d0 ! 0.35d0 -> water  0.70 -> RDX
  integer, parameter :: nsearch_min = 2000, scount_max = 2, nsearch_step = 2
  
  integer :: i, j, k
  character(200) :: cbuf(20)
  real(8) :: rbuf(100)
  real(8) :: minEner, weightScale, fracr0
  real(8) :: eta0, bestSig, curSig, delta
  real(8), allocatable :: grid0(:,:), grid(:,:), grid_add(:,:), minima(:,:)
  real(8), allocatable :: dudp(:,:), dudp0(:,:)
  real(8), allocatable :: weight0(:), enerff(:)
  !real(8), allocatable :: enerAI(:), ener(:), ener0(:)
  real(kind=prec), allocatable :: I_start(:,:), J_start(:,:), J_tmp(:,:), I_tmp(:,:)
  real(kind=prec), allocatable :: K_start(:), H_start(:), K_tmp(:), H_tmp(:)
  real(8), allocatable :: ptstd(:)
  logical, allocatable :: chosen(:)
  logical :: exists, incDelta
  real(8), allocatable :: newSig(:) 
  integer :: ncutoff
  integer :: ngrid_add, iadd, nminima
  integer :: nInc, nDec
  integer :: nsearch, isearch, scount(100)
  integer :: comModeExp, comModeLin
  integer, allocatable :: g0order(:)
  character(200) :: name, parms_file, intra_file, input_file, ener_file, geo_file
  
  do i = 1,20
     call getarg(i,cbuf(i))
  enddo
  name = cbuf(1)
  parms_file = cbuf(2)
  intra_file = cbuf(3)
  read(cbuf(4),*) ngrid_add
  read(cbuf(5),*) ngrid0
  read(cbuf(6),*) incDelta
  read(cbuf(7),*) weightScale
  read(cbuf(8),*) comModeExp
  read(cbuf(9),*) comModeLin
  read(cbuf(10),*) fracr0
  
  input_file = trim(name)//'.input_long'
  ener_file = trim(name)//'.ener'
  geo_file = trim(name)//'.geo_prob'
  
  call read_dimer(input_file)
  
  inquire(file=intra_file,exist=exists)
  if (exists) then
     call ff_init(parms_file, 2, intra_file)
  else
     call ff_init(parms_file, 2)
  endif
  
  nparm = initFreeParm(comModeExp, comModeLin)
  
  call initGrid('minima.dat', minima, nminima, 1)
  call initGrid(ener_file, grid, ngrid, 1)
  
  write(*,*) 'ngrid=',ngrid,'  ngrid_add=',ngrid_add,'  ngrid0=',ngrid0
  write(*,*) 'nparm=',nparm,'  incDelta=',incDelta
#ifdef QBLAS
  write(*,*) 'Using double-double presicion BLAS library'
#endif
  
  write(*,*) 'Initializing grid'
  
  allocate( enerff(ngrid), dudp(ngrid,nparm) )
  
  minEner = 1.0d100
  do i = 1,ngrid
     call moveDimerGP(grid(i,:))
     
     minEner = min(grid(i,C_Etot), minEner)
     enerff(i) = energy_ff()
     do j = 1,nparm
        dudp(i,j) = getdudp(j)
     enddo   
  enddo
  
  eta0 = 0.0d0
  ncutoff = 0
  do i = 1,ngrid
     if (grid(i,C_Etot) < e_cutoff) then
        ncutoff = ncutoff + 1
        eta0 = eta0 + gweight(enerff(i), minEner, weightScale)*(enerff(i) - grid(i,C_Etot))**2
     endif
  enddo
  eta0 = eta0/ncutoff
  
  allocate( ptstd(ngrid) )
  do i = 1,ngrid
     ptstd(i) = stdMult*sqrt(pi*eta0/(2.0d0*gweight(grid(i,C_Etot), minEner, weightScale)))
  enddo
  
  write(*,*) 'Initializing grid0'
  
  allocate( grid0(ngrid0,ngind) )
  call make_grid(grid, ngrid, grid0, ngrid0, 0.0d0, 0.0d0, .false., e_cutoff, 2*minEner, &
       1.5d0*e_cutoff, fracr0)
  do i = 1,nminima
     grid0(i,1:ngind) = minima(i,1:ngind)
  enddo
  
  allocate( dudp0(ngrid0,nparm), chosen(ngrid0) )
  do i = 1,ngrid0
     call moveDimerGP(grid0(i,:))
     !ener0(i) = energy_ff()
     do j = 1,nparm
        dudp0(i,j) = getdudp(j)
     enddo
     chosen(i) = .false.
  enddo
  
  allocate( weight0(ngrid0) )
  do i = 1,ngrid0
     weight0(i) = gweight(grid0(i,C_Etot), minEner, weightScale)
  enddo
  
  write(*,*) 'Initializing starting matrices'
  
  allocate( I_start(nparm,nparm), J_start(nparm,nparm), K_start(nparm), H_start(nparm) )
  I_start = 0.0d0
  do i = 1,nparm
     do j = 1,i
        do k = 1,ngrid0
           I_start(i,j) = I_start(i,j) + weight0(k)*dudp0(k,i)*dudp0(k,j)
        enddo
     enddo
  enddo
  J_start = 0.0d0
  do i = 1,nparm
     do j = 1,i
        do k = 1,ngrid
           J_start(i,j) = J_start(i,j) + dudp(k,i)*dudp(k,j)/ptstd(k)**2
        enddo
     enddo
  enddo
  K_start = 0.0d0
  do i = 1,nparm
     do j = 1,ngrid
        delta = (grid(j,C_Etot) - enerff(j))/2.0d0
        if (delta > ptstd(j)) delta = ptstd(j)*delta/abs(delta)
        K_start(i) = K_start(i) + dudp(j,i)*delta/ptstd(j)**2
     enddo
  enddo
  H_start = 0.0d0
  
  allocate( J_tmp(nparm,nparm), I_tmp(nparm,nparm), K_tmp(nparm), H_tmp(nparm) )
  J_tmp = J_start
  I_tmp = I_start
  K_tmp = K_start
  H_tmp = H_start
  curSig = sig(J_tmp, I_tmp, K_tmp, H_tmp, incDelta)
  deallocate( J_tmp, I_tmp, K_tmp, H_tmp )
  
  write(*,'(3(A,E12.5))') &
       'min energy=',minEner,'  initial sig=',curSig,'  eta0=',eta0
  write(*,*)
  write(*,*) 'Beginning grid selection'
  
  allocate( newSig(ngrid0), g0order(ngrid0) )
  do i = 1,ngrid0
     g0order(i) = i
  enddo

  iadd = 1
  allocate( grid_add(ngrid_add,ngind) )
  
  nsearch = ngrid0
  isearch = 1
  scount(:) = 0
  
  do while(iadd <= ngrid_add)
     
     !$OMP PARALLEL DEFAULT(SHARED) PRIVATE(J_tmp,I_tmp,K_tmp,H_tmp)
     allocate( J_tmp(nparm,nparm), I_tmp(nparm,nparm), K_tmp(nparm), H_tmp(nparm) )
     !$OMP DO
     do i = 1,nsearch
        if (chosen(g0order(i))) then
           newSig(i) = 1.0d100
        else
           J_tmp = J_start
           I_tmp = I_start
           K_tmp = K_start
           H_tmp = H_start
           call addGrid(g0order(i), J_tmp)
           newSig(i) = sig(J_tmp, I_tmp, K_tmp, H_tmp, incDelta)
        endif
     enddo
     !$OMP END DO
     deallocate( J_tmp, I_tmp, K_tmp, H_tmp )
     !$OMP END PARALLEL
     
     nInc = 0
     nDec = 0
     do i = 1,nsearch
        if (.not. chosen(g0order(i))) then
           if (newSig(i) > curSig) then
              nInc = nInc + 1
           elseif (newSig(i) < curSig) then
              nDec = nDec + 1
           endif
        endif
     enddo
     
     if (nDec > 0) then
        call quick_sort(newSig, g0order, nsearch)
        
        chosen(g0order(1)) = .true.
        call addGrid(g0order(1), J_start)
        grid_add(iadd,:) = grid0(g0order(1),:)
        curSig = newSig(1)
        iadd = iadd + 1
        
        write(*,'(A,F16.8,A,F15.8,A,I10,A,I10,A,I10)') &
             'sig=', curSig, '  add energy=', grid0(g0order(1),C_Etot), &
             '  nsearch=', nsearch, ' nDec=', nDec, ' nInc=', nInc
        
        ! Determine the number of points to compute in the next iteration
        scount(isearch) = scount(isearch) + 1
        if (nsearch >= nsearch_step*nsearch_min) then
           isearch = isearch + 1
           nsearch = nsearch/nsearch_step
        else
           do while (scount(isearch) >= scount_max .and. isearch > 1)
              scount(isearch) = 0
              isearch = isearch - 1
              nsearch = nsearch*nsearch_step
           enddo
        endif
     else
        if (isearch == 1) then
           write(*,*) 'Terminating grid selection'
           exit
        else
           write(*,*) 'No point added'
           scount(:) = 0
           isearch = 1
           nsearch = ngrid0
        endif
     endif
  enddo
  
  open(101,file=geo_file,status='new')
  do i = 1,iadd-1
     call writeCoors(grid_add(i,:), 1, 101)
  enddo
  close(101)
  
contains
  
  ! Add the given point from grid0 to the given J matrix
  subroutine addGrid(igrid0, J_add)
    integer :: igrid0
    real(kind=prec) :: J_add(nparm,nparm)
    
    integer :: i, j
    real(8) :: std
    
    std = stdMult*sqrt(pi*eta0/(2.0d0*weight0(igrid0)))
    do i = 1,nparm
       do j = 1,i
          J_add(i,j) = J_add(i,j) + dudp0(igrid0,i)*dudp0(igrid0,j)/std**2
       enddo
    enddo
  end subroutine addGrid
  
end program probgrid
