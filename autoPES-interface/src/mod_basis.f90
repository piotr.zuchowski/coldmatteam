
! Module for reading in Gaussian basis sets
module basis
  use const
  use utility
  implicit none
  
  integer, parameter :: MAX_CONTR = 100, MAX_PRIM = 40
  character(1000), private :: basisdir, basisname, basfile
  integer :: ncontr
  integer :: lcontr(MAX_CONTR), nprim(MAX_CONTR)
  logical :: incMCBS(MAX_CONTR)
  real(8) :: basexp(MAX_CONTR,MAX_PRIM), bascoef(MAX_CONTR,MAX_PRIM)
  
contains
  
  ! Initializes basis info
  subroutine initBasis(dir, name)
    character(*), intent(in) :: dir, name
    logical :: exists
    
    basisdir = dir
    basisname = name
    basfile = trim(dir)//'/'//trim(name)//'.bas'
    
    inquire(file=basfile,exist=exists)
    if (.not. exists) then
       write(*,*) 'ERROR: Basis file ',trim(basfile),' does not exist'
       stop
    endif
  end subroutine
  
  
  ! Sets the basis to the given element, where 0 = Mb
  subroutine setBasis(inum)
    integer, intent(in) :: inum
    character(1000) :: line
    character(2) :: asymb, lsymb
    logical :: found
    integer :: i, j
    
    open(101,file=basfile,status='old')
    
    found = .false.
    do while(.not. found)
       read(101,'(A)',err=1001,end=1001) line
       if (line(1:1) == '*') then
          read(101,'(A)',err=1001,end=1001) asymb
          if (asymb == asymbol(inum)) found = .true.
       endif
    enddo
    
    ncontr = 0
    do i=1,MAX_CONTR
       read(101,'(A)') line
       lcontr(i) = getl(line(1:1))
       if (lcontr(i) == -1) then
          exit
       else
          ncontr = i
          read(line,*) lsymb, nprim(i)
          do j = 1,nprim(i)
             read(101,*) basexp(i,j), bascoef(i,j)
          enddo
       endif
    enddo
    
    close(101)
    
    incMCBS(:) = .false.
    do i = 1,ncontr
       if (inum == 0) then
          incMCBS(i) = .true.
       else
          incMCBS(i) = lcontr(i) <= maxOccL(inum)
       endif
    enddo
    
    return
    
1001 continue
    write(*,*) 'ERROR: Unable to locate basis element ',inum
    stop
  end subroutine
  
  integer function getl(lsymb)
    character(*) :: lsymb
    integer :: i
    
    getl = -1
    do i = 0,MAX_L
       if (to_upper(orbSymbol(i)) == to_upper(lsymb(1:1))) then
          getl = i
       endif
    enddo
  end function getl
  
  
end module basis
