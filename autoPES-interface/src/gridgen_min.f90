
program gridgen_min
 use double
 use randnum
 use const
 use utility
 use dimer
 use dgrid
 implicit none
 
 real(8), parameter :: dAngle = 10.0d0*d2rad, dRad = 0.5d0*A2bohr
 integer, parameter :: Mmax = 30
 integer :: i, j
 real(8) :: Esum, w(Mmax), rand
 real(8), allocatable :: addgrid(:,:), minima(:,:)
 integer :: nmin, MM, nn(Mmax), Nadd
 character(200) :: name, min_file, input_file, geo_file, Nadd_ch
 
 call getarg(1,name)
 call getarg(2,min_file)
 call getarg(3,Nadd_ch)
 read(Nadd_ch,*) Nadd
 
 input_file = trim(name)//'.input_long'
 geo_file = trim(name)//'.geo_min'
 
 call read_dimer(input_file)
 
 ! Read in minima
 call initGrid(min_file, minima, nmin, 1)
 
 if (Nadd == 0) then
    Nadd = min(nmin, 10)
 endif
 
 allocate( addgrid(Nadd,ngind) )
 
 ! Add grid points
 if (Nadd > 0) then
    call init_random_seed()
    MM = min(Nadd, min(nmin, Mmax))
    Esum = 0.0d0
    do i = 1, MM
       Esum = Esum + minima(i,C_Etot)
    end do
    
    do i = 1, MM
       w(i) = minima(i,C_Etot)/Esum
       nn(i) = 1 + w(i)*(Nadd - MM)
    end do
    
    j = Nadd
    do i = 2, MM
       j = j - nn(i)
    end do
    nn(1) = j
    
    open(103,file=geo_file,status='replace')
    do i = 1, MM
       if (nn(i) > 0) then
          
          addgrid(1,:) = minima(i,:)
          do j = 2, nn(i)
             addgrid(j,C_Etot) =  minima(i,C_Etot)
             call random_number(rand)
             addgrid(j,C_rcom) = minima(i,C_rcom) + (2.0d0*rand - 1.0d0)*dRad
             call random_number(rand)
             addgrid(j,C_betaA) = minima(i,C_betaA) + (2.0d0*rand - 1.0d0)*dAngle
             call random_number(rand)
             addgrid(j,C_gammaA) = minima(i,C_gammaA) + (2.0d0*rand - 1.0d0)*dAngle
             call random_number(rand)
             addgrid(j,C_alphaB) = minima(i,C_alphaB) + (2.0d0*rand - 1.0d0)*dAngle
             call random_number(rand)
             addgrid(j,C_betaB) = minima(i,C_betaB) + (2.0d0*rand - 1.0d0)*dAngle
             call random_number(rand)
             addgrid(j,C_gammaB) = minima(i,C_gammaB) + (2.0d0*rand - 1.0d0)*dAngle
             call fixDimerAngles(0.0d0, addgrid(j,C_betaA), addgrid(j,C_gammaA), &
                  addgrid(j,C_alphaB), addgrid(j,C_betaB), addgrid(j,C_gammaB))   
          end do
          
          do j = 1, nn(i)
             call writeCoors(addgrid(j,:), 1, 103)
          end do
          
       end if
    end do
    close(103)
    
 endif
 
end program gridgen_min

