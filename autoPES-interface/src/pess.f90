
program pess     
  use const
  use dimer
  use force_field
! molscat  use physical_constants
  implicit none
  real(8) :: RR,COSTH, RR4,RR6,RR8, thetaa, thetab,zero
  real(8) :: V0, V30, V60, V90, V120,V150, V180
  real(8) :: dr,extpot,extpot2                    
  integer :: i,j,ii,jj 
  call extint()
  RR4=4.0
  RR6=6.0
  RR8=8.0

  write(*,*) ' ------ > ',MAX_NSITE 
  zero=0d0 
  j=1

  do ii=1,2
        do jj=1,nsite(ii)
                write(*,*) 'monomer: ',ii,' site: ',jj,' coords: ',sit(ii,jj,1),sit(ii,jj,2),sit(ii,jj,3)
        enddo
  enddo
 write(*,*) '------------------------------------------'
  do i=1,38
!  do j=1,19
  thetaa=(i-1)*10.0       
  thetab=(j-1)*10.0     
     write(*,*) RR6,thetaa,thetab,EXTPOT2(RR6,thetab,zero,zero,thetaa,zero)
!  enddo  
  enddo
    
end program pess  
!--------------------------moving atom+linear molecule which is fixed-----------------------
 double precision function  extpot(R,COSTH)
  use dimer   
  use force_field
  real*8 R,COSTH,SINTH
  SINTH=SQRT(1.0-COSTH**2)
  sit(1,1,1)=0d0
  sit(1,1,2)=R*SINTH    
  sit(1,1,3)=R*COSTH
  extpot=energy_ff()*349.75509
end function extpot
!--------------------------2x molecule -----------------------
 double precision function  extpot2(R, xbetaA, xgammaA, xalphaB, xbetaB, xgammaB)
  use dimer   
  use utility
  use force_field
  real*8 R, xbetaA, xgammaA, xalphaB, xbetaB, xgammaB

  call moveDimer(R,xbetaA, xgammaA, xalphaB, xbetaB, xgammaB)
  
   
  extpot2=energy_ff()*349.75509
end function extpot2
!-------------------------------------------------
subroutine extint
  use const
  use dimer
  use force_field
! molscat use physical_constants
 
  implicit real*8 (a-h,o-z)
  character(200) :: str
  character(200) :: inter_file, intra_file
  real(8) :: rbuf(10)
  integer :: geo_type
  integer :: nstot
  integer :: i,j
 
  call read_dimer('hcn-h2o.input_long')
  call ff_init('fit_inter.dat', 2)
  
   
end 
