#!/bin/bash

gfortran-mp-9 -c mod_common.f90
gfortran-mp-9 -c mod_dimer.f90
gfortran-mp-9 -c mod_pol.f90
gfortran-mp-9 -c mod_ff.f90
gfortran-mp-9 -c mod_ff_gen.f90

