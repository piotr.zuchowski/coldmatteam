
program make_inputs
 use make_inp
 use file_dir
 use dimer
 use dgrid
 use basis
 implicit none
 
 integer :: i, j
 character(len=1000) :: bas_dir
 character(len=100) :: name_in, command, ch_mode, ch_hyb, ch_maxmem
 character(len=100) :: constr_file, aux_file, aux_fileA, aux_fileB, geo_file, input_file
 character(len=100) :: bs_type, aux_bs_type, ch_mbcutoff
 character(len=len_ch) :: dal_file_A, dal_file_B, dal_file_MA, dal_file_MB, dal_file_D
 character(len=len_ch) :: mol_file_A, mol_file_B, mol_file_MA, mol_file_MB, mol_file_D
 logical :: asymp, distr, deltahf, hyb, incMB
 real(8) :: mbcutoff
 real(8) :: mbxyz(3)
 real(8), allocatable :: grid(:,:)
 integer :: maxmem, ngrid
 
 call getarg(1,name_in)
 call getarg(2,bas_dir)
 call getarg(3,bs_type)
 call getarg(4,aux_bs_type)
 call getarg(5,ch_hyb)
 call getarg(6,ch_mode)
 call getarg(7,ch_mbcutoff)
 call getarg(8,ch_maxmem)
 read(ch_hyb,*) hyb
 read(ch_mbcutoff,*) mbcutoff
 read(ch_maxmem,*) maxmem
 
 call initBasis(bas_dir, bs_type)
 
 asymp = .false.
 distr = .false.
 deltahf = .false.
 if (ch_mode == 'asymp') then
    asymp = .true.
 elseif (ch_mode == 'distr') then
    distr = .true.
 elseif (ch_mode == 'deltahf') then
    deltahf = .true.
 else
    write(*,*) 'ERROR: Invalid mode'
    stop
 endif
 
 dal_file_A   = trim(name_in)//'A'//'.dal'
 dal_file_B   = trim(name_in)//'B'//'.dal'
 dal_file_MA  = trim(name_in)//'MA'//'.dal'
 dal_file_MB  = trim(name_in)//'MB'//'.dal'
 dal_file_D   = trim(name_in)//'.dal'
 mol_file_A   = trim(name_in)//'A'//'.mol'
 mol_file_B   = trim(name_in)//'B'//'.mol'
 mol_file_MA  = trim(name_in)//'MA'//'.mol'
 mol_file_MB  = trim(name_in)//'MB'//'.mol'
 mol_file_D   = trim(name_in)//'.mol'
 P_file       = trim(name_in)//'P.data'
 dispinp_file = trim(name_in)//'.dispinp'
 aux_file     = trim(name_in)//'.aux'
 aux_fileA    = trim(name_in)//'A.aux'
 aux_fileB    = trim(name_in)//'B.aux'
 constr_file  = 'constr.para'
 input_file   = trim(name_in)//'.input_long'
 geo_file     = trim(name_in)//'.geo'
 
 call read_dimer(input_file)
 
 if (deltahf) then
    call initGrid(geo_file, grid, ngrid, 0)
    call moveDimerGP(grid(1,:))
    incMB = placemb(mbxyz, MBCutoff)
 else
    incMB = .false.
 endif
 
 !
! FILES GENERATION
!
! .dispinp or constr.para
!
 if (asymp) then
    open(unit=41,file=trim(dispinp_file))
    write(41,*) " &disper"
    write(41,*) "  title = 'Asymptotic calculations',"
    write(41,*) "  groupa = 'CINFV',"
    write(41,*) "  groupb = 'CINFV',"
    write(41,*) "  nmax = 12,"
    write(41,*) "  ngrid = 10,"
    write(41,*) "  nfpola = 1,"
    write(41,*) "  nfpolb = 2,"
    write(41,*) "  itypea = 0,"
    write(41,*) "  itypeb = 0,"
    write(41,*) "  prpol = t,"
    write(41,*) "  prcop = t,"
    write(41,*) "  tabul = t"
    write(41,*)"  &end"
    close(41)
 endif
 if (distr) then
    open(unit=41,file=trim(constr_file))
    call write_constr_file(41)
    close(41)
 endif
!
! Dalton .dal
!
 open(unit=21,file=trim(dal_file_A))
 call write_fileX_dal(21, .false., .not. deltahf)
 close(21)
 if (.not. homog .or. .not. distr) then
    open(unit=22,file=trim(dal_file_B))
    call write_fileX_dal(22, .not. deltahf, .not. deltahf)
    close(22)
 endif
 if (deltahf) then
    open(unit=23,file=trim(dal_file_D))
    call write_fileX_dal(23, .true., .false.)
    close(23)
 else
    open(unit=23,file=trim(dal_file_MA))
    call write_fileMX_dal(23,1,distr)
    close(23)
    if (.not. homog .or. .not. distr) then
       open(unit=24,file=trim(dal_file_MB))
       call write_fileMX_dal(24,2,distr)
       close(24)
    endif
 endif
!
! Dalton .mol
!
 if (asymp) then
    open(unit=33,file=trim(mol_file_MA))
    call write_file_mol_MX(33,mol_file_MA,1)
    close(33)
    open(unit=34,file=trim(mol_file_MB))
    call write_file_mol_MX(34,mol_file_MB,2)
    close(34)
 endif
 
 if (asymp .or. deltahf) then
    if (asymp) then
       call moveDimer(100.0d0,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0)
    endif
    open(unit=31,file=trim(mol_file_A))
    call write_file_mol_X(31,mol_file_A,1)
    close(31)
    open(unit=32,file=trim(mol_file_B))
    call write_file_mol_X(32,mol_file_B,2)
    close(32)
    if (deltahf) then
       open(unit=33,file=trim(mol_file_D))
       call write_file_mol_X(33,mol_file_D,0)
       close(33)
    endif
 endif
 call moveDimer(0.0d0,0.0d0,0.0d0,0.0d0,0.0d0,0.0d0)
 
 if (distr) then
    open(unit=31,file=trim(mol_file_A))
    call write_file_mol_MX(31,mol_file_A,1)
    close(31)
    if (.not. homog) then
       open(unit=32,file=trim(mol_file_B))
       call write_file_mol_MX(32,mol_file_B,2)
       close(32)
    endif
 endif
 
!
! SAPT main input
!
 open(unit=42,file=trim(P_file))
 write(42,*) " "
 write(42,*) "&TRN"
 write(42,*) " ISITALCH=F ,ISITG90=F, ISITG88=F, ISITHNDO=F, ISITMICR=F, ISITATM=F,"
 write(42,*) " ISITANEW=F, ISITDALT=T,OUT=F, TOLER=15, SPHG=T,"
 if (deltahf) then
    write(42,*) " DIMER=T,"
 else
    write(42,*) " DIMER=F,"
 endif
 if (distr) write(42,*) " T2EL=F, BLKMB=T,"
 write(42,'(A,I0,A)') " MEMTRAN=",maxmem,'M'
 write(42,*) "&END"
 write(42,*)
 write(42,*) "&INPUTCOR"
 if (deltahf) then
    write(42,*) " DELTASCF=T, CKSIND=T,"
 else
    write(42,*) " SAPTKS=T,"
 endif
 write(42,'(A,I0,A)') " PRINT=F, TIMEREP=T, MEMSAPT=",maxmem,'M'
 write(42,*) "&END"
 write(42,*)
 if (asymp) then
    write(42,*) "&SAPTDFT"
    write(42,*) " CKSDISP=T, CKSIND=T, IQUADTYP=4, NQUAD=10,"
    write(42,*) " AONLY=",homog,","
    write(42,'(A,I0,A)') " MAXMEM=",maxmem,'M'
    write(42,*) "&END"
 endif
 if (distr) then
    write(42,*) " &DF"
    write(42,'(A,I0,A)') "  mode=25, SKIPPUREMON=F, MEMTRAN=",maxmem,'M'
    write(42,*) " &END"
 endif
 close(42)
 
 ! dimer.cnf
 if (asymp .or. distr) then
    open(unit=43,file='dimer.cnf')
    call write_dimer_file(43)
    close(43)
 endif
 
 ! Auxiliary basis
 if (distr) then
    call initBasis(bas_dir, aux_bs_type)
    open(unit=43,file=aux_fileA)
    open(unit=44,file=aux_fileB)
    call write_aux(43, 44, 0, bs_type, incMB, mbxyz)
    close(43)
    close(44)
 endif
 
 ! geoparm.d
 if (asymp .or. distr) then
    call initGrid(geo_file, grid, ngrid, 0)
    open(unit=43,file='geoparm.d')
    call write_geoparm(grid, ngrid, 43)
    close(43)
 endif
 
 stop
contains

subroutine write_fileX_dal(ifile, twoint, noHF)
 implicit none
 integer, intent(in) :: ifile
 logical, intent(in) :: twoint, noHF
 write(ifile,'(A)') '**DALTON INPUT'
 write(ifile,'(A)') '.RUN WAVE FUNCTION'
 write(ifile,'(A)') '**INTEGRALS'
 write(ifile,'(A)') '.NOSUP'
 write(ifile,'(A)') '.PRINT'
 write(ifile,'(I5)') 1
 if(.not. twoint) then
    write(ifile,'(A)') '.NOTWO'
 else
    write(ifile,'(A)') '*TWOINT'
    write(ifile,'(A)') '.IFTHRS'
    write(ifile,'(A)') '14'
 endif
 write(ifile,'(A)') '**WAVE FUNCTIONS'
 write(ifile,'(A)') '.HF'
 write(ifile,'(A)') '.INTERFACE'
 if (noHF) then
    write(ifile,'(A)') '.STOP'
    write(ifile,'(A)') 'AFTER MO-ORTHNORMALIZATION'
 endif
 write(ifile,'(A)') '*AUXILLIARY INPUT'
 write(ifile,'(A)') '.NOSUPMAT'
 write(ifile,'(A)') '*ORBITALS'
 write(ifile,'(A)') '.NOSUPSYM'
 write(ifile,'(A)') '*HF INPUT'
 write(ifile,'(A)') '.THRESH'
 write(ifile,'(A)') '2.D-6'
 write(ifile,'(A)') '*ORBITAL INPUT'
 write(ifile,'(A)') '.AO DELETE'
 write(ifile,'(D10.3)') eps_ao
 write(ifile,'(A)') '.CMOMAX'
 write(ifile,'(D10.3)') cmomax
 !if (asymp .or. distr) then
 write(ifile,'(A)') '.MOSTART'
 write(ifile,'(A)') 'H1DIAG'
 !endif
 write(ifile,'(A)') '*END OF INPUT'
 return
end subroutine write_fileX_dal

subroutine write_fileMX_dal(ifile,iX,distr)
 use dimer
 use const
 use make_inp
 implicit none
 
 integer, intent(in) :: ifile, iX
 logical, intent(in) :: distr
 
 write(ifile,'(A)') '**DALTON INPUT'
 write(ifile,'(A)') '.RUN WAVE FUNCTION'
 write(ifile,'(A)') '**INTEGRALS'
 write(ifile,'(A)') '.NOSUP'
 write(ifile,'(A)') '.PRINT'
 write(ifile,'(I5)') 1
 write(ifile,'(A)') '**WAVE FUNCTIONS'
 write(ifile,'(A)') '.DFT'
 if (hyb) then
    write(ifile,'(A)') 'PBE0'
 else
    write(ifile,'(A)') 'PBE'
 endif
 write(ifile,'(A)') '.INTERFACE'
 write(ifile,'(A)') '*POPULATION ANALYSIS'
 write(ifile,'(A)') '.MULLIKEN'
 write(ifile,'(A)') '*AUXILLIARY INPUT'
 write(ifile,'(A)') '.NOSUPMAT'
 write(ifile,'(A)') '*ORBITALS'
 write(ifile,'(A)') '.NOSUPSYM'
 write(ifile,'(A)') '*DFT INPUT'
 if (.not. distr) then
    write(ifile,'(A)') '.CKS'
 else
    write(ifile,'(A)') '.CKSAUX'
 endif
 write(ifile,'(A)') '.DFTELS'
 write(ifile,'(D10.3)') eps_safe
 write(ifile,'(A)') '.DFTGRAC'
 write(ifile,"(F7.4,F5.1,2I4)") IP(iX)/h2ev, 0.5, 40, 0
 write(ifile,'(A)') '.RADINT'
 write(ifile,'(D10.3)') r_grid
 write(ifile,'(A)') '.ANGINT'
 write(ifile,'(I5)') a_grid
 write(ifile,'(A)') '*HF INPUT'
 write(ifile,'(A)') '.THRESH'
 write(ifile,'(D10.3)') eps_scf
 write(ifile,'(A)') '*ORBITAL INPUT'
 write(ifile,'(A)') '.AO DELETE'
 write(ifile,'(D10.3)') eps_ao
 write(ifile,'(A)') '.CMOMAX'
 write(ifile,'(D10.3)') cmomax
 write(ifile,'(A)') '*END OF INPUT'
 return
end subroutine write_fileMX_dal


subroutine write_file_mol_MX(ifile,filename,iX)
 use dimer
 implicit none
 
 integer, intent(in) :: ifile, iX
 character(len=100), intent(in) :: filename
 integer :: i, j, name_len
 
 write(ifile,'(A)') 'INTGRL'
 write(ifile,'(A)') 'Energy calculation without symmetry'
 write(ifile,'(A)') 'Basis set specified with ATOMBASIS'
 write(ifile,'(I5,I3,I2)') natom(iX), molchar(iX), 0
 do i = 1, natom(iX)
    call write_basis(iX,i,sanum(iX,i),ifile)
 enddo
 return
end subroutine write_file_mol_MX


subroutine write_file_mol_X(ifile,filename,iX)
 use dimer
 implicit none
 
 integer, intent(in) :: ifile, iX
 character(len=100), intent(in) :: filename
 integer :: qq, i, j, molcharX
 
 write(ifile,'(A)') 'INTGRL'
 write(ifile,'(A)') 'Energy calculation without symmetry'
 write(ifile,'(A)') 'Basis set specified with ATOMBASIS'
 
 if (iX == 0) then
    molcharX = molchar(1) + molchar(2)
 else
    molcharX = molchar(iX)
 endif
 write(ifile,'(I5,I3,I2)') natom(1)+natom(2), molcharX, 0
 
 do i = 1,2
    do j = 1,natom(i)
       if (iX == i .or. iX == 0) then
          qq = sanum(i,j)
       else
          qq = 0
       endif
       call write_basis(i,j,qq,ifile)
    enddo
 enddo
 
 return
end subroutine write_file_mol_X


! Convert to the stupid format that Dalton uses and print to ifile
subroutine write_basis(imon,iatom,qq,ifile)
  use basis
  use dimer
  implicit none
  
  integer, intent(in) :: imon, iatom, qq, ifile
  character(50) :: fmt1
  integer :: lmax, blockInd, ncontrl(0:MAX_L), nblockl(0:MAX_L)
  real(8) :: blockcoef(0:MAX_L,MAX_CONTR,MAX_CONTR), blockExp(0:MAX_L,MAX_CONTR)
  integer :: i, j, k, l
  
  call setBasis(sanum(imon,iatom))
  
  ! Primitives organized into blocks with the same exponent
  blockCoef(:,:,:) = 0.0d0
  lmax = 0
  do i = 0,MAX_L
     ncontrl(i) = 0
     nblockl(i) = 0
     do j = 1,ncontr
        if (lcontr(j) == i) then
           lmax = max(lmax, i)
           ncontrl(i) = ncontrl(i) + 1
           
           do k = 1,nprim(j)
              blockInd = 0
              do l = 1,nblockl(i)
                 if (abs(basexp(j,k) - blockExp(i,l)) < 1.0d-5) then
                    blockInd = l
                    exit
                 endif
              enddo
              if (blockInd == 0) then
                 nblockl(i) = nblockl(i) + 1
                 blockInd = nblockl(i)
                 blockExp(i,blockInd) = basexp(j,k)
              endif
              blockCoef(i,blockInd,ncontrl(i)) = bascoef(j,k)
           enddo
           
        endif
     enddo
  enddo
  
  write(ifile,'(I10,9I5)') qq, 1, lmax+1, (min(1,nblockl(i)), i=0,lmax)
  write(ifile,'(A,3F20.10)') trim(alabel(imon,iatom))//monlabelL(imon), (sit(imon,iatom,i),i=1,3)
  
  do i = 0,MAX_L
     if (nblockl(i) > 0) then
        write(ifile,'(A1,I4,I5)') 'H', nblockl(i), ncontrl(i)
        if (ncontrl(i) <= 3) then
           fmt1='(F20.10,3F20.10)'
        else if (ncontrl(i) <= 6) then
           fmt1='(F20.10,3F20.10,/,F40.10,2F20.10)'
        else
           fmt1='(F20.10,3F20.10,/,F40.10,2F20.10,/,F40.10,2F20.10)'
        endif
        do j = 1,nblockl(i)
           write(ifile,fmt1) blockExp(i,j), (blockCoef(i,j,k), k=1,ncontrl(i))
        enddo
     endif
  enddo
  
end subroutine

end program make_inputs
