
program sort_minima
 use const
 use utility
 use dimer
 use force_field
 use dgrid
 implicit none
 
 integer :: i, j, k
 real(8) :: ener, coor(1:6)
 real(8) :: elem(7)
 character(200) :: name, min_file, input_file, parms_file, ff_type_ch
 integer N, MM, M, least, ireplace, ff_type
 logical exists
 real(8) :: sit1(2,MAX_NSITE,3)
 real(8), allocatable :: tab(:,:)  ! tab in units of kcal/mol, bohr, radian
 real(8) :: gpoint(MAX_NGIND)
 
 call getarg(1,name)
 call getarg(2,min_file)
 call getarg(3,parms_file)
 call getarg(4,ff_type_ch)
 read(ff_type_ch,*) ff_type
 
 input_file = trim(name)//'.input_long'
 call read_dimer(input_file)
 call ff_init(parms_file, ff_type)
 call initGridCoors(1)
 
 ! Read in and filter duplicate minima
 N = nlines(min_file)
 open(unit=101,file=min_file)
 allocate(tab(N,7))
 M = 0
 do i = 1, N
    ! Minima input file in bohrs, degrees
    read(101,*) coor(1:6), ener
    !coor(1) = coor(1)*A2bohr
    coor(2:6) = coor(2:6)*d2rad
    
    call fixDimerAngles(0.0d0, coor(2), coor(3), coor(4), coor(5), coor(6))
    
    call moveDimer(coor(1), coor(2), coor(3), coor(4), coor(5), coor(6))
    sit1 = sit
    
    if (ener < 0.0d0 .and. ener > -5.0d2 .and. minCovDist() > 1.0d0) then
       
       ireplace = 0
       exists = .false.
       do j = 1, M
          call moveDimer(tab(j,2), &
               tab(j,3), tab(j,4), tab(j,5), tab(j,6), tab(j,7))
          if (epsMetric(sit1, sit) < 0.012d0) then
             exists = .true.
             if (ener < tab(j,1)) then
                ireplace = j
             endif
          endif
       end do
       
       if (.not.exists) then
          M = M + 1
          tab(M,1) = ener
          tab(M,2:7) = coor(1:6)
       elseif (ireplace > 0) then
          tab(ireplace,1) = ener
          tab(ireplace,2:7) = coor(1:6)
       end if
    end if
 end do
 close(101)
 
 ! Sort minima
 do i = 1,M
    least = i
    do j = i,M
       if (tab(j,1) < tab(least,1)) least = j
    enddo
    elem(:) = tab(i,:)
    tab(i,:) = tab(least,:)
    tab(least,:) = elem(:)
 enddo
 
 ! Remove 'false' minima
 do i = 1, M
    if (.not. testmin(tab(i,2:7))) then
       do j = i+1,M
          tab(j-1,:) = tab(j,:)
       enddo
       M = M - 1
    endif
 enddo
 
 ! Output
 gpoint(:) = 0.0d0
 do i = 1, M
    gpoint(C_Etot) = tab(i,1)
    gpoint(C_rcom) = tab(i,2)
    gpoint(C_betaA:C_gammaB) = tab(i,3:7)
    call writeCoors(gpoint, 1, STDOUT)
 end do
 
contains

! Tests if a minimum is extremely small
logical function testmin(coords0)
  use const
  use dimer
  use force_field
  implicit none
  
  real(8), parameter :: dmax = 3.0d0*d2rad, rmax = 0.05d0*A2bohr
  
  real(8) :: coords0(1:6)
  
  real(8) :: coords(1:6)
  real(8) :: ener0
  real(8) :: cstep, cmax
  integer :: i, j
  
  call moveDimer(coords0(1), coords0(2), coords0(3), &
       coords0(4), coords0(5), coords0(6))
  ener0 = energy_ff()
  
  do i = 1,6
     if (i == 1) then
        cstep = rmax/20
        cmax = rmax
     else
        cstep = dmax/20
        cmax = dmax
     endif
     
     do j = 1,2 ! 1 = positive direction, 2 = negative
        if (j == 2) cstep = -1.0d0*cstep
        
        coords(:) = coords0(:)
        do while (abs(coords(i) - coords0(i)) < cmax)
           coords(i) = coords(i) + cstep
           call moveDimer(coords(1), coords(2), coords(3), &
                coords(4), coords(5), coords(6))
           !write(*,*) i,j,energy_ff(),ener0
           if (energy_ff() < ener0 - 1.0d-5) then
              testmin = .false.
              return
           endif
        enddo
     enddo
  enddo
  
  testmin = .true.
end function testmin
  
end program sort_minima
