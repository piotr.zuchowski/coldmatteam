
! Wrapper module for reading potential parameters from a file and evaluating the energy
module force_field
 use const
 use double
 use force_field_guid
 use force_field_gen
 use force_field_intra_gen
 implicit none
 
 integer, private :: ff_type
 logical :: incIntra
 
contains

! Gives total energy of FF in kcal/mol for current values of sit
real(8) function energy_ff()
 implicit none
 integer :: i
 
 energy_ff = energy_ff_inter()
 do i = 1,2
    energy_ff = energy_ff + energy_ff_intra(i)
 enddo
end function

! Read in and initialize the FF
subroutine ff_init(parms_file, ff_type_in, parms_file_intra)
 implicit none
 character(*), intent(in) :: parms_file
 integer, intent(in) :: ff_type_in
 character(*), intent(in), optional :: parms_file_intra
 
 ff_type = ff_type_in
 
 if (ff_type == 1) then
    call ff_init_guid(parms_file)
 elseif (ff_type == 2) then
    call read_fit_gen(parms_file)
    if (present(parms_file_intra)) then
       call read_fit_intra_gen(parms_file_intra)
       incIntra = .true.
    else
       call initIntraFF(0)
       incIntra = .false.
    endif
 else
    write(*,*) 'ERROR: Bad FF type'
    stop
 endif
 
 return
end subroutine

! Gives component-wise interaction energy of FF in kcal/mol for current values of sit
function energy_ff_comp()
 implicit none
 real(8) :: energy_ff_comp(n_FF_comp)
 
 energy_ff_comp(:) = 0.0d0
 if (ff_type == 1) then
    energy_ff_comp(F_els) = energy_ff_qq()
    energy_ff_comp(F_vdw) = energy_ff_vdw()
 elseif (ff_type == 2) then
    energy_ff_comp = energy_ff_gen()
 endif
end function energy_ff_comp


! Gives intermolecular energy of FF in kcal/mol for current values of sit
real(8) function energy_ff_inter()
 implicit none
 real(8) :: components(n_FF_comp)
 integer :: i
 
 energy_ff_inter = 0.0d0
 components = energy_ff_comp()
 do i = 1,n_FF_comp
    energy_ff_inter = energy_ff_inter + components(i)
 enddo
end function


! Compute intramonomer FF for current values of sit
real(8) function energy_ff_intra(imon)
  implicit none
  integer, intent(in) :: imon
  
  if (incIntra) then
     if (ff_type == 1) then
        energy_ff_intra = 0.0d0
     else
        energy_ff_intra = energy_ff_intra_gen(imon)
     endif
  else
     energy_ff_intra = 0.0d0
  endif
end function energy_ff_intra


end module force_field

