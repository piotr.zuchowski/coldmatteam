! Energies in kcal/mol, distances in bohrs

module mod_hole_scan
  use const
  use dimer
  use force_field
  use utility
  use dgrid
  use powell
  implicit none
  
  integer, parameter :: MAX_NHOLES = 10000
  integer, parameter :: nRInc = 160, dStep = 20
  integer, parameter :: NbetaA = 180/dStep
  integer, parameter :: NbetaB = NbetaA
  integer, parameter :: NgammaA = 360/dStep
  integer, parameter :: NgammaB = NgammaA
  integer, parameter :: NalphaB = NgammaA
  real(8), parameter :: pressureConst = 0.85d0
  real(8), parameter :: S2Start = 0.20d0, S2End = 0.90d0
  integer, parameter :: S2NIter = 10
  character(10), parameter :: typeDesc(4) = ['min range ','wall start','infinity  ','stage 2   ']
  
  character(200) :: name, parms_file, intra_file, ch_ff_type, ch_wallHeight, ch_minEnergy, ch_addDist, input_file
  real(8) :: wallHeight, minEnergy, radAddDist, radMinLimit, pctAboveWall
  real(8) :: minWallDist, maxWallDist, minWallHeight, sumWallHeight
  real(8) :: minWallDisti(NgammaA), maxWallDisti(NgammaA), minWallHeighti(NgammaA), sumWallHeighti(NgammaA), nWallFoundi(NgammaA)
  real(8) :: betaA, gammaA, alphaB, betaB, gammaB
  integer :: ff_type
  real(8) :: rStart, rInc, wallMaxE, wallMaxR
  integer :: nscan, nholes, nMinRange, nWallFound, nWallStart, nInf, nRedundant
  real(8), allocatable :: hole(:), holes(:,:)
  integer :: holeType(MAX_NHOLES)
  integer :: worstHole, VDWAddPt, htype, ndat, nAbove, nBelow
  real(8) :: pntR, pntE, rAdd, eAdd, rStartS2
  integer :: nholesi(0:NgammaA)
  real(8), allocatable :: holesi(:,:,:)
  integer :: holeTypei(0:NgammaA,MAX_NHOLES)
  logical :: exists
  real(8) :: coordVect(6)
  real(8) :: pressureScale, minEnerS2
  real(8) :: sit_add(2,MAX_NSITE,3)
  real(8), allocatable :: grid0(:,:)
  integer :: i, j, k, l, m, n
  !$OMP THREADPRIVATE(minEnerS2, pressureScale)
  
contains
  
  ! Error function called by nonlinear optimizer for 'stage 2' search
  function S2ErrFunc(x, len)
    real(8) :: S2ErrFunc
    integer :: len
    real(8) :: x(len)
    
    real(8) :: dist
    
    call moveDimer(x(1), x(2), x(3), x(4), x(5), x(6))
    S2errFunc = max(energy_ff(), minEnerS2) + pressureScale*distpen()
    !errFuncS2 = energy_ff()! + pressureScale*distpen()
    
    dist = minCovDist()
    if (dist < radMinLimit) then
       S2errFunc = S2errFunc + 200.0d0*(radMinLimit - dist)
    endif
    
  end function
  
  ! Weak attractive potential added for stage 2 hole search
  function distpen()
    use const
    use dimer
    implicit none
    real(8) :: distpen
    
    integer :: i, j
    real(8) :: sum, distScale
    
    sum = 0.0d0
    do i = 1,nsite(1)
       do j = 1,nsite(2)
          if (sanum(1,i) > 0 .and. sanum(2,j) > 0) then
             distScale = covRad(sanum(1,i)) + covRad(sanum(2,j))
             sum = sum + log(pressureConst + cDist(1,i,2,j)/distScale)
          endif
       enddo
    enddo
    
    distpen = sum
  end function
  
end module

program hole_scan
  use mod_hole_scan
  implicit none
  
  write(*,*)
  
  call getarg(1,name)
  call getarg(2,parms_file)
  call getarg(3,ch_ff_type)
  call getarg(4,intra_file)
  call getArg(5,ch_wallHeight)
  call getArg(6,ch_minEnergy)
  call getArg(7,ch_addDist)
  read(ch_wallHeight,*) wallHeight
  read(ch_minEnergy,*) minEnergy
  read(ch_ff_type,*) ff_type
  
  input_file = trim(name)//'.input_long'
  call read_dimer(input_file)
  
  inquire(file=intra_file,exist=exists)
  if (exists) then
     call ff_init(parms_file, ff_type, intra_file)
  else
     call ff_init(parms_file, ff_type)
  endif
  
  call initGridCoors(1)
  allocate( hole(ngind), holes(MAX_NHOLES,ngind) )
  allocate( holesi(0:NgammaA,MAX_NHOLES/2,ngind) )
  
  if (ch_addDist == 'AUTO') then
     write(*,*) 'Finding wall add distance'
     
     input_file = trim(name)//'.ener'
     call initGrid(input_file, grid0, ndat, 1)
     radAddDist = 0.9d0*repulDist(grid0, ndat, wallHeight, 0.80d0)
  else
     read(ch_addDist,*) radAddDist
  endif
  
  radMinLimit = 0.85d0*radAddDist
  
  write(*,*) 'Wall add dist = ',radAddDist
  write(*,*) 'Wall min dist = ',radMinLimit
  write(*,*)
  
  nscan = NbetaA*NgammaA*NalphaB*NbetaB*NgammaB
  nholes = 0
  nMinRange = 0
  nInf = 0
  nWallFound = 0
  nWallStart = 0
  nRedundant = 0
  minWallDist = 1.0d100
  maxWallDist = 0.0d0
  minWallHeight = 1.0d100
  sumWallHeight = 0.0d0
  
  do i = 1, NbetaA
     write(*,'(A,I4,A,I10,A)') 'Hole scanning',100*(i - 1)/NbetaA,'% ',nholes,' holes found'

     ! These arrays are needed to allow for openmp parallelization
     nholesi(:) = 0
     nWallFoundi(:) = 0
     minWallDisti(:) = 1.0d100
     maxWallDisti(:) = 0.0d0
     minWallHeighti(:) = 1.0d100
     sumWallHeighti(:) = 0.0d0
     
     !$OMP PARALLEL DEFAULT(FIRSTPRIVATE) &
     !$OMP SHARED(holesi,nholesi,holeTypei,nWallFoundi,minWallDisti,maxWallDisti,minWallHeighti,sumWallHeighti)
     !$OMP DO
     do j = 1, NgammaA
        do k = 1, NalphaB
           do l = 1, NbetaB
              do m = 1, NgammaB
                 betaA = acos(i*1.99d0/NbetaA - 0.99d0)
                 gammaA = j*dStep/rad2d
                 alphaB = dStep*k/rad2d
                 betaB = acos(l*1.99d0/NbetaB - 0.99d0)
                 gammaB = dStep*m/rad2d
                 
                 call moveDimer(0.0d0, betaA, gammaA, alphaB, betaB, gammaB)
                 rStart = covDist(radMinLimit)
                 rInc = (minDist(7.0d0*A2bohr) - rStart)/nRInc
                 
                 htype = 0
                 wallMaxE = -1.0d100
                 rStartS2 = 0.0d0
                 
                 ! Radial scan
                 do n = 1,nRInc
                    
                    pntR = rStart + (nRInc - n)*rInc
                    call moveDimer(pntR, betaA, gammaA, alphaB, betaB, gammaB)
                    pntE = energy_ff()
                    
                    if (pntE > wallMaxE) then
                       wallMaxE = pntE
                       wallMaxR = pntR
                    endif
                    
                    if (pntE > S2Start*wallHeight .and. rStartS2 <= 0.0d0) rStartS2 = pntR
                    
                    ! If wall has not yet been found
                    if (wallMaxE < wallHeight) then
                       
                       ! Check if minimum distance reached
                       if (n == nRInc) then
                          eAdd = pntE
                          htype = 1
                          exit
                       endif
                       
                       ! If start of wall found, check if it is increasing
                       if (wallMaxE > 0.5d0*wallHeight .and. pntE < 0.5d0*wallMaxE) then
                          eAdd = wallMaxE
                          htype = 2
                          exit
                       endif
                       
                       ! Check if function is going to -infinity
                       if (pntE < 2.0d0*minEnergy) then
                          eAdd = pntE
                          htype = 3
                          exit
                       endif
                    endif
                    
                 enddo !n
                 
                 ! Non-radial scan 'stage 2'
                 if (htype <= 0) then
                    call moveDimer(rStartS2, betaA, gammaA, alphaB, betaB, gammaB)
                    pressureScale = 0.05d0*S2Start*wallHeight/distpen()
                    minEnerS2 = energy_ff()
                    
                    coordVect(1) = rStartS2
                    coordVect(2) = betaA
                    coordVect(3) = gammaA
                    coordVect(4) = alphaB
                    coordVect(5) = betaB
                    coordVect(6) = gammaB
                    
                    do n = 1,S2NIter
                       call praxis(1, 0.0d0, 1.0d-2, 6, 0, coordVect, S2ErrFunc, 0, .false.)
                       call moveDimer(coordVect(1), coordVect(2), &
                            coordVect(3), coordVect(4), coordVect(5), coordVect(6))
                       pntE = energy_ff()
                       
                       if (minCovDist() < radMinLimit .and. pntE < wallHeight) then
                          eAdd = pntE
                          rAdd = coordVect(1)
                          betaA = coordVect(2)
                          gammaA = coordVect(3)
                          alphaB = coordVect(4)
                          betaB = coordVect(5)
                          gammaB = coordVect(6)
                          htype = 4
                          
                          exit
                       endif
                    enddo ! stage 2 iterations
                 endif
                 
                 if (htype > 0 .and. nholesi(j) < MAX_NHOLES) then
                    
                    call moveDimer(0.0d0, betaA, gammaA, alphaB, betaB, gammaB)
                    rAdd = max(pntR,covDist(radAddDist))
                    
                    nholesi(j) = nholesi(j) + 1
                    holesi(j,nholesi(j),:) = 0.0d0
                    holesi(j,nholesi(j),C_rcom) = rAdd
                    holesi(j,nholesi(j),C_betaA) = betaA
                    holesi(j,nholesi(j),C_gammaA) = gammaA
                    holesi(j,nholesi(j),C_alphaB) = alphaB
                    holesi(j,nholesi(j),C_betaB) = betaB
                    holesi(j,nholesi(j),C_gammaB) = gammaB
                    holesi(j,nholesi(j),C_Etot) = eAdd
                    holeTypei(j,nholesi(j)) = htype
                    
                 else
                    
                    call moveDimer(wallMaxR, betaA, gammaA, alphaB, betaB, gammaB)
                    nWallFoundi(j) = nWallFoundi(j) + 1
                    minWallDisti(j) = min(minWallDisti(j), closestContact()) !minCovDist())
                    maxWallDisti(j) = max(maxWallDisti(j), closestContact()) !minCovDist())
                    minWallHeighti(j) = min(minWallHeighti(j), wallMaxE)
                    sumWallHeighti(j) = sumWallHeighti(j) + wallMaxE
                    
                 endif
                 
              enddo !m
           enddo !l
        enddo !k
     enddo !j
     !$OMP END PARALLEL
     
     do j = 1,NgammaA
        nWallFound = nWallFound + nWallFoundi(j)
        minWallDist = min(minWallDisti(j), minWallDist)
        maxWallDist = max(maxWallDisti(j), maxWallDist)
        minWallHeight = min(minWallHeighti(j), minWallHeight)
        if (minWallHeight /= minWallHeight) minWallHeight = -100.0d0
        sumWallHeight = sumWallHeighti(j) + sumWallHeight
        
        do k = 1,nholesi(j)
           select case(holeTypei(j,k))
              case (1)
                 nMinRange = nMinRange + 1
              case (2)
                 nWallStart = nWallStart + 1
              case (3)
                 nInf = nInf + 1
           end select
           
           ! Check if similar hole has already been added
           exists = .false.
           call moveDimer(holesi(j,k,C_rcom), &
                holesi(j,k,C_betaA), holesi(j,k,C_gammaA), &
                holesi(j,k,C_alphaB), holesi(j,k,C_betaB), holesi(j,k,C_gammaB))
           sit_add = sit
           do n = 1,nholes
              call moveDimer(holes(n,C_rcom), &
                   holes(n,C_betaA), holes(n,C_gammaA), &
                   holes(n,C_alphaB), holes(n,C_betaB), holes(n,C_gammaB))
              if (epsMetric(sit_add, sit) < 0.05d0) then
                 nRedundant = nRedundant + 1
                 exists = .true.
                 exit
              endif
           enddo
           
           if (.not. exists .and. nholes < MAX_NHOLES) then
              nholes = nholes + 1
              holes(nholes,:) = holesi(j,k,:)
              holeType(nholes) = holeTypei(j,k)
           endif
        enddo
     enddo
     
  enddo !i
  
  ! Sort holes
  do i = 1,nholes
     worstHole = i
     do j = i,nholes
        if (holes(j,C_Etot) < holes(worstHole,C_Etot)) worstHole = j
     enddo
     
     hole(:) = holes(i,:)
     htype = holeType(i)
     holes(i,:) = holes(worstHole,:)
     holeType(i) = holeType(worstHole)
     holes(worstHole,:) = hole(:)
     holeType(worstHole) = htype
  enddo
  
  ! Output
  write(*,*)
  write(*,*) 'Hole scanning complete'
  write(*,'(A,I10,A,I10)') &
       'nHoles:',nholes,'   nWallFound:',nWallFound
  write(*,'(A,I10,A,I10,A,I10,A,I10)') &
       'nWallStart:',nWallStart,'   nMinRange:',nMinRange,'   nInf',nInf,'   nRedundant',nRedundant
  write(*,*) 'Minimum wall height=',minWallHeight
  write(*,*) 'Minimum wall distance=',minWallDist*bohr2A
  write(*,*) 'Maximum wall distance=',maxWallDist*bohr2A
  write(*,*) 'Mean wall height=',sumWallHeight/nWallFound*bohr2A
  if (nholes > 0) then
     write(*,*) 'Minimum hole energy=',holes(1,C_Etot)
     write(*,*)
     write(*,*) 'Holes (i, E, R, type):'
     do i = 1,min(nholes,1000)
        write(*,'(I5,A,F12.5,A,F12.5,A,A)') &
             i,' ',holes(i,C_Etot),' ',holes(i,C_rcom)*bohr2A,'     ',typeDesc(holeType(i))
     enddo
  endif
  open(100,file='holes.out',status='new')
  do i = 1,nholes
     call writeCoors(holes(i,:), 0, 100)
  enddo
  close(100)
  
end program hole_scan

