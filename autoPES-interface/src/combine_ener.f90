
! Utility program which combines energy files of main SAPT runs with deltaHF runs

program combine_ener
  use utility
  use dimer
  use dgrid
  implicit none
  character(100) :: name, ch_incDelta
  character(200) :: input_file, dft_file, deltahf_file
  real(8), allocatable :: dftgrid(:,:), dhfgrid(:,:)
  integer :: npt_dft, npt_deltahf
  logical :: incDelta, isSame
  character(3000) :: line
  real(8) :: arr(30)
  integer :: i, j, k
  
  call getarg(1,name)
  call getarg(2,dft_file)
  call getarg(3,deltahf_file)
  call getarg(4,ch_incDelta)
  read(ch_incDelta,*) incDelta
  
  input_file = trim(name)//'.input_long'
  call read_dimer(input_file)
  if (incDelta) call initGrid(deltahf_file, dhfgrid, npt_deltahf, n_ener)
  call initGrid(dft_file, dftgrid, npt_dft, n_ener)
  
  do i = 1,npt_dft
     if (incDelta) then
        
        do j = 1,npt_deltahf
           if (dhfgrid(j,C_ener+E_dhf) == dhfgrid(j,C_ener+E_dhf)) then  ! Check for NaN
              isSame = .true.
              do k = 1,ncoor
                 if (k == C_gammaA .or. k == C_alphaB .or. k == C_gammaB) then
                    if (angleDiff(dftgrid(i,k), dhfgrid(j,k)) > 1.0d-5) then
                       isSame = .false.
                       exit
                    endif
                 else
                    if (abs(dftgrid(i,k) - dhfgrid(j,k)) > 1.0d-5) then
                       isSame = .false.
                       exit
                    endif
                 endif
              enddo
              
              if (isSame) then
                 dftgrid(i,C_Etot) = dftgrid(i,C_Etot) + dhfgrid(j,C_ener+E_dhf)
                 dftgrid(i,C_ener+E_dhf) = dftgrid(i,C_ener+E_dhf) + dhfgrid(j,C_ener+E_dhf)
                 call writeCoors(dftgrid(i,:), n_ener, 6)
                 exit
              endif
           endif
        enddo
        
     else
        
        call writeCoors(dftgrid(i,:), n_ener, 6)
        
     endif
  enddo
  
end program combine_ener
