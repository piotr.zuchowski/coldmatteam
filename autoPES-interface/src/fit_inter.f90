
!
! Program for performing intermolecular PES fitting
! All internal qunatities are in atomic units
!
!************************ COMMAND LINE PARAMETERS **************************
!
! NAME (string): Name of the system, used to open NAME.ener and NAME.input_long
!
!**************************** STANDARD INPUT *******************************
!
! fit (namelist) : Fortran namelist containing all the fitting parameters 
!
!************************* REQUIRED INPUT FILES ****************************
!
! fit_asymp.dat : Contains parameters of the asymptotic fit.
! NAME.input_long : Contains dimer info in standard autoPES format.
! NAME.ener : Contains set of dimer configurations and energies for fitting.
!
!************************* OPTIONAL INPUT FILES ****************************
!
! NAME.ener.test1, NAME.ener.test2, etc : Contain additional sets of dimer
!     configurations which are used only to evaluate the error of the fit.
!     Any of the files NAME.ener.test1 through NAME.ener.test9 will be used.
!
!***************************** OUTPUT FILES ********************************
!
! fit_inter.dat : Contains all parameters of the intermolecular fit. Units
!     are in powers of Angstrom and kcal/mol, except charges and
!     polarizabilities in atomic units.
!
!***************************************************************************

!***************************************************************************
!***************************** DEFINITIONS *********************************
!***************************************************************************

module fitInterDef
use const
use utility
use weight
use dgrid
use powell
use polarization
implicit none

real(8), parameter :: NOVAL = 1.23d123

integer, parameter :: maxDisp = 5, maxPoly = 5, maxVect = 20

! File IO units
integer, parameter :: input_long = 11, asymp_out = 12, parms_out = 13

! Parameter type indices
integer, parameter :: T_dp = 1, T_alpha = 2, T_beta = 3, T_vdw = 4, T_Cn = 5, T_d1 = 6, &
     T_dn = 7, T_core = 8, T_OA = 9

! Parameter vector indices
integer, parameter :: V_els = 1, V_vdw = 2, V_exppart = 3, V_expfull = 4, V_final = 5, &
     V_glb = 6, V_pol = 7

real(8), parameter :: minDamp = 0.80d0, maxDamp = 2.00d0, maxDampEls = 3.0d0
real(8), parameter :: minBeta = 0.90d0, maxBeta = 2.2d0, minAlpha = 6.0d0, maxAlpha = 13.5d0
real(8), parameter :: minCoreMult = 1.0d-4, maxCoreMult = 1.0d0
real(8), parameter :: minVDWFact = 0.95d0, maxVDWFact = 1.15d0
real(8), parameter :: ALPHA_START = 9.0d0, BETA_START = 1.5d0, DELTA_START = 1.0d0
real(8), parameter :: NO_DAMP = 1.0d10*bohr2A
real(8), parameter :: linParmPenStart = 1.0d4, linParmPenEnd = 1.0d-7
real(8), parameter :: enerDmp = 0.5d0 ! Damping fraction for damping between powell iterations for linear parameter wieghts
integer, parameter :: nExpIter = 80

type sp_type
   integer :: indx
   integer :: type1, type2
   real(8) :: alpha, beta
   real(8) :: chargeProd, radius
   real(8) :: C(maxDisp)
   real(8) :: d(maxDisp)
   real(8) :: d1, dP
   real(8) :: a(maxPoly)
   real(8) :: A12
   logical :: incExp, incEls, incVdw, incPolEls, incPolDip
   logical :: isUsed
end type sp_type

type param
   integer :: indx, indx2
   integer :: type, class
end type param

!***************************************************************************
!*************************** GLOBAL VARIABLES ******************************
!***************************************************************************

namelist /fit/ nIter, oPoly, weightScale, nrand, nrandFinal, wallHeight, &
     comModeDmp, incVDWFact, fitTotal, incCore, &
     comModeExp, fitVdw, excludeDHF, Ecap, incElsDmp, incVDWDmp, incPolDmp, &
     autoReduce, comModeLin, keepLinPen, weightEnerDep

! Number of iterations in the final randomized exponential parameter stage of fitting
integer, save :: nIter = 40
! Order of the polynomial in the exponential term of the fit
integer, save :: oPoly = 1
! Parameter determining how heavily low energy points are weighted
real(8), save :: weightScale = 5.0d0
! The number of starting randomized exponential parameter vectors
integer, save :: nrand = 64
! The number of randomized exponential parameter vectors to fully optimize
integer, save :: nrandFinal = 4
! The minimum height of the repulsive wall, in kcal/mol
real(8), save :: wallHeight = 15.0d0
! Combination rule mode to use for damping parameters: 0 for no rule, 1 for geometric, 2 for single value
integer, save :: comModeDmp = PC_s
! Whether or not to include the global factor multiplying the vdW energies
logical, save :: incVDWFact = .false.
! Whether or not to optimize all parameters in the final randomized stage of fitting
logical, save :: fitTotal = .false.
! Whether or not to include the repulsize 1/r^12 term
logical, save :: incCore = .false.
! Combination rule mode to use for exponential parameters: 0 for no rule, 1 for arithmetic, 2 for single value
integer, save :: comModeExp = PC_s
! Whether or not to optimize the distributed vdW coefficients to close-range data
logical, save :: fitVdw = .false.
! Whether or not to exclude the delta_HF component of the energy (if present) 
logical, save :: excludeDHF = .false.
! Whether or not to include the damping factor in the polarization model (if used)
logical, save :: incPolDmp = .true.
! Whether or not to include damping factor in the vdW expansion
logical, save :: incVDWDmp = .true.
! Whether or not to include damping factor in the electrostatic component
logical, save :: incElsDmp = .true.
! All data points with E_tot > Ecap kcal/mol will be discarded from the fit
real(8), save :: Ecap = 200.0d0
! The program will reduce the parameter space such that Ngrid/Nparm > autoReduce
real(8), save :: autoReduce = 0.0d0
! Combination rule mode to use for linear parameters: 0 for no rule, 1 for arithmetic
integer, save :: comModeLin = PC_sp
! Whether or not to reatin small linear parameter penalty at final optimization
logical, save :: keepLinPen = .true.
! How strongly weight increases when PES value is less than ab initio value
real(8), save :: weightEnerDep = 0.0d0

logical, save :: incPol
integer, save :: nPolIter, nDisp
integer, save :: praxisPrint
integer, save :: dispTerms(maxDisp)
character(100), save :: sysname, enerfile
logical, save :: verbose, incComponents
integer, save :: comModeVDW
real(8), save, allocatable :: rAB(:,:)
real(8), save, allocatable :: rVect(:,:,:,:)
integer, save :: ndat, npair, nsptypeUsed, nlinparm
real(8), save, allocatable :: grid(:,:)
real(8), save, allocatable :: fite(:,:)
real(8), save, allocatable :: baseWeight(:)!, linFitEner(:)
type(sp_type), save, allocatable :: spt(:)
real(8), save, allocatable :: charge(:), pz(:), strad(:)
!integer, save, allocatable :: typeMap(:,:) ! Map from pair of site types to site pair type
integer, save, allocatable :: spmap(:) ! Map from site pair to site pair type
integer, save, allocatable :: spsite(:,:) ! Map from site pair to pair of sites
integer, save, allocatable :: sitesp(:,:) ! Map from pair of sites to site pair
real(8), save :: rBuf(20)
integer, save :: ival
real(8), save :: val, minVal
logical, save :: reduced
integer, save :: indx, nIterPer, nAbove, nBelow
real(8), save :: minEnergy, maxEnergy
real(8), save :: bestErr, praxisScale, wallStartDist
character(2000), save :: str
real(8), save, allocatable :: globalParmVect(:), expPartParmVect(:), expFullParmVect(:)
real(8), save, allocatable :: elsParmVect(:), vdwParmVect(:), polParmVect(:)
real(8), save, allocatable :: finalParmVect(:), finalParmVectPrivate(:), finalParmVectRand(:,:)
real(8), save, allocatable :: randVectErr(:)
integer, save :: newNrand
real(8), save :: vdwFactor, coreMult
integer, save :: maxParm
real(8), save :: corePenScale
real(8), save :: linParmPenScale(maxPoly)
integer, save, allocatable :: rowMap(:,:)
integer, save :: vectLen(maxVect)
type(param), save, allocatable :: vectParm(:,:)
real(8), save :: OAcoor(2,MAX_IDOF)

contains


!***************************************************************************
!****************************** ROUTINES ***********************************
!***************************************************************************


! Initialize the array of parameter vectors, which tells which free parameters
!    are included in which optimization stages
subroutine initParmVect()
  
  integer :: i, j
  
  maxParm = 10 + (10 + ndisp)*nsptype + (5 + ndisp)*nstype
  if (allocated(vectParm)) deallocate( vectParm )
  allocate( vectParm(maxVect,maxParm) )
  vectLen(:) = 0
  
  call addParm(V_glb, param(1, -1, T_alpha, PC_glb))
  call addParm(V_glb, param(1, -1, T_beta, PC_glb))
  if (incCore) call addParm(V_glb, param(-1, -1, T_core, PC_glb))
  if (incElsDmp) call addParm(V_glb, param(-1, -1, T_d1, PC_glb))
  if (incPolDmp) call addParm(V_glb, param(-1, -1, T_dp, PC_glb))
  do i = 1,nDisp
     if (incVDWDmp) call addParm(V_glb, param(-1, i, T_dn, PC_glb))
  enddo
  
  if (incVDWFact) call addParm(V_vdw, param(-1, -1, T_vdw, PC_glb))
  
  if (incCore) call addParm(V_exppart, param(-1, -1, T_core, PC_glb))
  if (incCore) call addParm(V_expfull, param(-1, -1, T_core, PC_glb))
  
  if (incElsDmp .and. comModeDmp == 2) call addParm(V_els, param(1, -1, T_d1, PC_glb))
  do i = 1,nDisp
     if (incVDWDmp .and. comModeDmp == 2) call addParm(V_vdw, param(1, i, T_dn, PC_glb))
  enddo
  
  if (comModeExp == 2) then
     call addParm(V_exppart, param(1, -1, T_alpha, PC_glb))
     call addParm(V_exppart, param(1, -1, T_beta, PC_glb))
     call addParm(V_expfull, param(1, -1, T_alpha, PC_glb))
     call addParm(V_expfull, param(1, -1, T_beta, PC_glb))
  endif
  
  do i = 1,nstype
     
     if (comModeExp == 0 .or. comModeExp == 1) then
        if (incInter(i,F_exp)) then
           call addParm(V_exppart, param(i, -1, T_alpha, PC_s))
           call addParm(V_exppart, param(i, -1, T_beta, PC_s))
        endif
     endif
     
     if (comModeExp == 1) then
        if (incInter(i,F_exp)) then
           call addParm(V_expfull, param(i, -1, T_alpha, PC_s))
           call addParm(V_expfull, param(i, -1, T_beta, PC_s))
        endif
     endif
     
     if (comModeDmp == 1) then
        if (incElsDmp .and. (incInter(i,F_els) .or. incInter(i,F_pol))) then
           call addParm(V_els, param(i, -1, T_d1, PC_s))
        endif
        
        if (incVDWDmp .and. incInter(i,F_vdw)) then
           do j = 1,nDisp
              call addParm(V_vdw, param(i, j, T_dn, PC_s))
           enddo
        endif
        
        if (incPolDmp .and. incInter(i,F_pol)) then
           call addParm(V_pol, param(i, -1, T_dp, PC_s))
        endif
     endif
     
     if (comModeVdw == 1 .and. fitVdw) then
        if (incInter(i,F_vdw)) then
           do j = 1,nDisp
              call addParm(V_vdw, param(i, j, T_Cn, PC_s))
           enddo
        endif
     endif
     
  enddo
  
  do i = 1,nsptype
     if (spt(i)%isUsed) then
        if (comModeExp == 0) then
           if (spt(i)%incExp) then
              call addParm(V_expfull, param(i, -1, T_alpha, PC_sp))
              call addParm(V_expfull, param(i, -1, T_beta, PC_sp))
           endif
        endif
        
        if (comModeDmp == 0) then
           if (incElsDmp .and. (spt(i)%incEls .or. spt(i)%incPolEls)) then
              call addParm(V_els, param(i, -1, T_d1, PC_sp))
           endif
           
           if (incVDWDmp .and. spt(i)%incVDW) then
              do j = 1,nDisp
                 call addParm(V_vdw, param(i, j, T_dn, PC_sp))
              enddo
           endif
           
           if (incPolDmp .and. spt(i)%incPolDip) then
              call addParm(V_pol, param(i, -1, T_dp, PC_sp))
           endif
        endif
        
        if (comModeVdw == 0 .and. fitVdw) then
           if (spt(i)%incVDW) then
              do j = 1,nDisp
                 call addParm(V_vdw, param(i, j, T_Cn, PC_sp))
              enddo
           endif
        endif
     endif
  enddo
  
  do i = 1,2
     if (i == 1 .or. .not. homog) then
        do j = 1,nmcgroupOptim(i)
           !call addParm(V_glb, param(i, j, T_OA, PC_glb))
           !call addParm(V_exppart, param(i, j, T_OA, PC_glb))
           call addParm(V_expfull, param(i, j, T_OA, PC_glb))
        enddo
     endif
  enddo
  
  do i = 1,vectLen(V_expfull)
     if (vectParm(V_expfull,i)%type /= T_OA) then
        call addParm(V_final, vectParm(V_expfull,i))
     endif
  enddo
  if (fitTotal) then
     do i = 1,vectLen(V_els)
        call addParm(V_final, vectParm(V_els,i))
     enddo
     do i = 1,vectLen(V_vdw)
        call addParm(V_final, vectParm(V_vdw,i))
     enddo
     do i = 1,vectLen(V_pol)
        call addParm(V_final, vectParm(V_pol,i))
     enddo
  endif
  
  call initRowMap()
end subroutine initParmVect

! Add parameter to given vector type
subroutine addParm(vectType, parm)
  integer :: vectType
  type(param) :: parm
  
  vectLen(vectType) = vectLen(vectType) + 1
  vectParm(vectType,vectLen(vectType)) = parm
end subroutine


! Construct map from exponential site pair or site and polynomial term to vector index
subroutine initRowMap()
  integer :: i, j
  
  if (allocated(rowMap)) deallocate( rowMap )
  if (comModeLin == 0) then
     allocate( rowMap(nsptype,oPoly) )
     rowMap(:,:) = 0
     nlinparm = 0
     do i = 1,nsptype
        if (spt(i)%incExp .and. spt(i)%isUsed) then
           do j = 1,oPoly
              nlinparm = nlinparm + 1
              rowMap(i,j) = nlinparm
           enddo
        endif
     enddo
  else
     allocate( rowMap(nstype,oPoly) )
     rowMap(:,:) = 0
     nlinparm = 0
     do i = 1,nstype
        if (incInter(i,F_exp)) then
           do j = 1,oPoly
              nlinparm = nlinparm + 1
              rowMap(i,j) = nlinparm
           enddo
        endif
     enddo
  endif
end subroutine initRowMap


! Fill vector with current values of parameters
subroutine fillParmVect(vect, vectType)
  integer :: vectType
  real(8) :: vect(vectLen(vectType))
  
  integer :: i, j
  integer :: type, spType, indx, indx2
  
  do i = 1,vectLen(vectType)
     type = vectParm(vectType,i)%type
     indx = vectParm(vectType,i)%indx
     indx2 = vectParm(vectType,i)%indx2
     select case (vectParm(vectType,i)%class)
     case (PC_sp)
        spType = indx
     case (PC_s)
        spType = typeMap(indx,indx)
     case (PC_glb)
        spType = 1
     end select
     
     if (type == T_alpha) then
        vect(i) = encodeSig(spt(spType)%alpha - alphaOffset(spType), minAlpha, maxAlpha)
     elseif (type == T_beta) then
        vect(i) = encodeSig(spt(spType)%beta, minBeta, maxBeta)
     elseif (type == T_core) then
        vect(i) = encodeSig(coreMult, minCoreMult, maxCoreMult)
     elseif (type == T_vdw) then
        vect(i) = encodeSig(vdwFactor, minVDWFact, maxVDWFact)
     elseif (type == T_Cn) then
        vect(i) = encodeLog(spt(spType)%C(indx2))
     elseif (type == T_dp) then
        vect(i) = encodeSig(spt(spType)%dP, minDamp, maxDampEls)
     elseif (type == T_d1) then
        vect(i) = encodeSig(spt(spType)%d1, minDamp, maxDampEls)
     elseif (type == T_dn) then
        vect(i) = encodeSig(spt(spType)%d(indx2), minDamp, maxDamp)
     elseif (type == T_OA) then
        vect(i) = OACoor(indx,indx2)
     endif
  enddo
  
end subroutine


! Atom-dependent difference in minimum alpha value
function alphaOffset(spType)
  real(8) :: alphaOffset
  integer :: spType
  
  alphaOffset = log(0.25d0 + spt(spType)%radius)
end function


! Read data points from given file
function readPoints(fileName)
  logical :: readPoints
  character(*) :: fileName
  
  integer :: i, j
  real(8) :: val
  
  if (allocated(grid)) then
     deallocate( grid )
  endif
  call initGrid(fileName, grid, ndat, n_ener)
  
  if (ndat <= 0) then
     readPoints = .false.
     return
  endif
  
  if (excludeDHF) then
     do i = 1,ndat
        grid(i,C_etot) = grid(i,C_etot) - grid(i,C_ener+E_dhf)
        grid(i,C_ener+E_dhf) = 0.0d0
     enddo
  endif
  
  ! Filter points with Etot > Ecap
  i = 1
  do while (i <= ndat)
     if (grid(i,C_Etot) > Ecap) then
        ndat = ndat - 1
        do j = i,ndat
           grid(j,:) = grid(j+1,:)
        enddo
     endif
     i = i + 1
  enddo
  
  if (allocated(fite)) then
     deallocate( fite, baseWeight )
  endif
  allocate( fite(ndat,n_FF_comp), baseWeight(ndat) )
  fite(:,:) = 0.0d0
  baseWeight(:) = 1.0d0
  
  incComponents = .false.
  do i = 1,ndat
     do j = 1,n_ener_inter
        if (j /= E_tot .and. abs(grid(i,C_ener+j)) > 1.0d-20) incComponents = .true.
     enddo
  enddo
  
  ! Find minimum and maximum energy
  minEnergy = 1.0d100
  maxEnergy = -1.0d100
  do i = 1,ndat
     minEnergy = min(minEnergy, grid(i,C_Etot))
     maxEnergy = max(maxEnergy, grid(i,C_Etot))
  end do
  
  call initializePoints()
  call initializePenaltyFunctions()
  val = calcEnergies(.false., .true., .false., .false., .false.)
  
  readPoints = .true.
end function


! Initialize data for current set of grid points
subroutine initializePoints()
  integer :: i
  
  ! Calculate site-site distances
  if (allocated(rAB)) then
     deallocate( rAB )
     deallocate( rVect )
  end if
  allocate( rAB(ndat,npair) )
  allocate( rVect(ndat,2,nsitemax,3) )
  
  call computeDistances()
  
  do i = 1,ndat
     !p(i)%weight = (p(i)%EAI(E_tot) - 1.10*minEnergy)**(-2.0d0)
     baseWeight(i) = ptweight(i, grid(i,C_Etot))
  enddo
  
end subroutine


! Fill rAB and rVect arrays
subroutine computeDistances()
  integer :: i, j, k
  real(8) :: sum
  
  do i = 1,ndat
     call setOACoor(OACoor)
     call moveDimerGP(grid(i,:))
     
     do j = 1,2
        do k = 1,nsite(j)
           rVect(i,j,k,:) = sit(j,k,:)
        enddo
     enddo
     
     do j = 1,npair
        sum = 0.0d0
        do k = 1,3
           sum = sum + (rVect(i,1,spsite(j,1),k) - rVect(i,2,spsite(j,2),k))**2
        enddo
        rAB(i,j) = sqrt(sum)
     enddo
  enddo
end subroutine


! Set point and weight dependent constants for use in penalty functions
subroutine initializePenaltyFunctions
  integer :: i
  real(8) :: sum
  
  ! Calculate scale factor for penalty functions
  sum = 0.0d0
  do i = 1,ndat
     sum = sum + baseWeight(i)*grid(i,C_Etot)**2
  enddo
  
  corePenScale = 1.0d-5*sum
  
  linParmPenScale(1) = linParmPenStart*sum/sqrt(dble(nlinparm)) ! 2.0d-11
  do i = 2,maxPoly
     linParmPenScale(i) = 1.0d0*linParmPenScale(i-1)
  enddo
  
end subroutine


! Percent error for all points given current energies
function pctErr(maxLim, minLim)
  real(8) :: maxLim, minLim
  real(8) :: pctErr
  
  integer :: i
  real(8) :: calculated, actual, sum
  integer :: count
  
  sum = 0.0
  count = 0
  do i = 1,ndat
     actual = grid(i,C_Etot)
     if (actual <= maxLim .and. actual >= minLim) then
        calculated = fittot(i)
        sum = sum + abs(calculated - actual)/abs(actual)
        count = count + 1
     end if
  end do
  
  if (count > 0) then
     pctErr = 100.0d0*sum/real(count)
  else
     pctErr = 0.0d0
  endif
end function


! RMS error for all points given current energies
function rmsErr(maxLim, minLim)
  real(8) :: maxLim, minLim
  real(8) :: rmsErr
  
  integer :: i
  real(8) :: sum
  integer :: count
  
  sum = 0.0
  count = 0
  do i = 1,ndat
     if (grid(i,C_Etot) <= maxLim .and. grid(i,C_Etot) >= minLim) then
        sum = sum + (fittot(i) - grid(i,C_Etot))**2
        count = count + 1
     end if
  end do
  
  if (count > 0) then
     rmsErr = sqrt(sum/real(count))
  else
     rmsErr = 0.0d0
  endif
end function


! Return the number of points in a given energy range
function nPtRange(minLim, maxLim)
  integer :: nPtRange
  real(8) :: minLim, maxLim
  
  integer :: i
  
  nPtRange = 0
  do i = 1,ndat
     if (grid(i,C_Etot) < maxLim .and. grid(i,C_Etot) >= minLim) then
        nPtRange = nPtRange + 1
     end if
  end do
end function


! Unloads vector, computes energies, and computes error function
! If setParms is true, will update parameter values
! If setEnergies is true, will update energy values
! If setLinParms is true, will optimize linear parameters, otherwise use existing ones
! If skipAsymp is true, use existing pol, vdw, els values, otherwise compute them
! If convLin is true, weighs will be iteratively converged for linear parameters
function calcEnergies(setParms, setEnergies, setLinParms, skipAsymp, convLin, vect, vectType)
  real(8) :: calcEnergies
  integer, optional :: vectType
  real(8), optional :: vect(:)
  logical :: setParms, setEnergies, setLinParms, skipAsymp, convLin
  
  real(8) :: fite1(ndat,n_FF_comp)
  
  real(8) :: beta(nsptype), alpha(nsptype), C(nsptype,ndisp)
  real(8) :: dp(nsptype), d1(nsptype), dn(nsptype,nDisp)
  real(8) :: alin(nsptype,opoly)
  real(8) :: coreMultiplier, vdwFact
  
  integer :: i, j, k, ilin
  real(8) :: val, sum, rjtok, Eint
  real(8) :: dmpFactor, dmpParm, Ediff, elsEner, vdwEner, expEner
  integer :: spType, ipt, index, index2, class
  integer :: nliniter
  logical :: calcr
  
  coreMultiplier = NOVAL
  vdwFact = NOVAL
  beta(:) = NOVAL
  alpha(:) = NOVAL
  d1(:) = NOVAL
  dP(:) = NOVAL
  C(:,:) = NOVAL
  dn(:,:) = NOVAL
  alin(:,:) = NOVAL
  
  calcr = .false.
  if (present(vect)) then
     ! Unload parameter vector
     do i = 1,vectLen(vectType)
        
        index = vectParm(vectType,i)%indx
        index2 = vectParm(vectType,i)%indx2
        class = vectParm(vectType,i)%class
        select case (class)
        case (PC_sp)
           spType = index
        case (PC_s)
           spType = typeMap(index,index)
        case (PC_glb)
           spType = 1
        end select
        
        select case(vectParm(vectType,i)%type)
        case (T_core)
           coreMultiplier = decodeSig(vect(i), minCoreMult, maxCoreMult)
        case (T_vdw)
           vdwFact = decodeSig(vect(i), minVDWFact, maxVDWFact)
        case (T_alpha)
           val = decodeSig(vect(i), minAlpha, maxAlpha) + alphaOffset(spType) 
           call fillSpVect(alpha, val, index, class, CR_arith)
        case (T_beta)
           val = decodeSig(vect(i), minBeta, maxBeta)
           call fillSpVect(beta, val, index, class, CR_arith)
        case (T_dp)
           val = decodeSig(vect(i), minDamp, maxDampEls)
           call fillSpVect(dP, val, index, class, CR_geo)
        case (T_d1)
           val = decodeSig(vect(i), minDamp, maxDampEls)
           call fillSpVect(d1, val, index, class, CR_geo)
        case (T_dn)
           val = decodeSig(vect(i), minDamp, maxDamp)
           call fillSpVect(dn(:,index2), val, index, class, CR_geo)
        case (T_Cn)
           val = decodeLog(vect(i))
           call fillSpVect(C(:,index2), val, index, class, CR_geo)
        case (T_OA)
           calcr = .true.
           OACoor(index,index2) = vect(i)
           if (homog) OACoor(2,index2) = vect(i)
        end select
     enddo
  endif
  
  ! Default to global program values if not set by the parameter vector
  if (coreMultiplier == NOVAL) coreMultiplier = coreMult
  if (vdwFact == NOVAL) vdwFact = vdwFactor
  do i = 1,nsptype
     if (beta(i) == NOVAL) beta(i) = spt(i)%beta
     if (alpha(i) == NOVAL) alpha(i) = spt(i)%alpha
     if (d1(i) == NOVAL) d1(i) = spt(i)%d1
     if (dp(i) == NOVAL) dp(i) = spt(i)%dp
     do j = 1,ndisp
        if (C(i,j) == NOVAL) C(i,j) = spt(i)%C(j)
        if (dn(i,j) == NOVAL) dn(i,j) = spt(i)%d(j)
     enddo
     if (spt(i)%incExp .and. spt(i)%isUsed) then
        do j = 1,oPoly
           if (alin(i,j) == NOVAL) alin(i,j) = spt(i)%a(j)
        end do
     endif
  enddo
  
  ! OA coordinates are being optimized so distance matrices need to be re-computed
  if (calcr) then
     call computeDistances()
  endif
  
  if (.not. skipAsymp) then
     ! Calculate polarization energies
     if (incPol) then
        do i = 1,ndat
           fite1(i,F_pol) = polEnerPoint(i, d1, dP)
        enddo
     else
        fite1(:,F_pol) = 0.0d0
     endif
     
     ! Calculate els and ind+disp energies
     do i = 1,ndat
        
        elsEner = 0.0d0
        do j = 1,npair
           spType = spmap(j)
           
           if (spt(spType)%incEls) then
              if (incElsDmp) then
                 dmpFactor = TT(1, d1(spType), rAB(i,j))
              else
                 dmpFactor = 1.0d0
              endif
              elsEner = elsEner + &
                   dmpFactor*spt(spType)%chargeProd/rAB(i,j)
           end if
        enddo
        fite1(i,F_els) = elsEner
        
        vdwEner = 0.0d0
        do j = 1,npair
           spType = spmap(j)
           
           do k = 1,nDisp
              if (spt(spType)%incVdw) then
                 if (incVDWDmp) then
                    dmpFactor = TT(dispTerms(k), dn(spType,k), rAB(i,j))
                 else
                    dmpFactor = 1.0d0
                 endif
                 vdwEner = vdwEner - &
                      dmpFactor*C(spType,k)/rAB(i,j)**dispTerms(k)
              end if
           end do
           
        end do
        fite1(i,F_vdw) = vdwEner*vdwFact
        fite1(i,F_exp) = fite(i,F_exp)
     end do
  else ! Use existing energy values
     do i = 1,ndat
        fite1(i,:) = fite(i,:)
     enddo
  endif
  
  if (convLin .and. setLinParms .and. weightEnerDep > 0.0d0) then
     nliniter = 3
  else
     nliniter = 1
  endif
  do ilin = 1,nliniter
     if (setLinParms .and. oPoly > 0) then
        alin = solveLin(alpha, beta, fite1)
     end if
     
     ! Calculate exponential energies
     do i = 1,ndat
        
        expEner = 0.0d0
        do j = 1,npair
           spType = spmap(j)
           
           if (spt(spType)%incExp) then
              
              sum = 1.0d0
              rjtok = 1.0d0
              do k = 1,oPoly
                 rjtok = rjtok*rAB(i,j)
                 sum = sum + rjtok*alin(spType,k)
              end do
              expEner = expEner + sum*exp(alpha(spType) - beta(spType)*rAB(i,j))
              
              if (incCore) then
                 expEner = expEner + coreMultiplier*spt(spType)%A12/rAB(i,j)**12
              endif
              
           endif
        end do
        
        if (ilin < nliniter) then
           fite1(i,F_exp) = enerDmp*fite1(i,F_exp) + (1.0d0 - enerDmp)*expEner
        else
           fite1(i,F_exp) = expEner
        endif
     end do
  enddo
  
  ! Calculate error
  calcEnergies = 0.0d0
  do i = 1,ndat
     Eint = fite1(i,F_els) + fite1(i,F_pol) + fite1(i,F_vdw) + fite1(i,F_exp)
     calcEnergies = calcEnergies + ptWeight(i, Eint)*(grid(i,C_Etot) - Eint)**2
  end do
  
  ! Hard core penalty function
  if (incCore) then
     calcEnergies = calcEnergies - corePenScale*log(coreMultiplier)
  endif
  
  calcEnergies = sqrt(calcEnergies)
  
  ! Set program variables to buffer values if necessary
  if (setParms) then
     do i = 1,nsptype
        if (spt(i)%incPolDip) spt(i)%dP = dP(i)
        if (spt(i)%incEls .or. spt(i)%incPolEls) spt(i)%d1 = d1(i)
        if (spt(i)%incVDW) then
           spt(i)%d(1:ndisp) = dn(i,1:ndisp)
           spt(i)%C(1:ndisp) = C(i,1:ndisp)
        endif
        
        if (spt(i)%incExp) then
           spt(i)%alpha = alpha(i)
           spt(i)%beta = beta(i)
           do j = 1,oPoly
              if (rowMap(i,j) > 0) spt(i)%a(j) = alin(i,j)
           end do
        endif
     end do
     
     coreMult = coreMultiplier
     vdwFactor = vdwFact
  endif
  
  if (setEnergies) then
     ! Set energy components
     do i = 1,ndat
        fite(i,:) = fite1(i,:)
     enddo
  end if
  
end function


! Sets values in site pair vector according to given combination mode
subroutine fillSpVect(spVect, val, index, class, rule)
  real(8) :: spVect(:)
  real(8) :: val
  integer :: index, class, rule
  
  integer :: i
  integer :: spType
  
  select case (class)
  case (PC_glb)
     spVect(1:nsptype) = val
  case (PC_s)
     do i = 1,nstype
        spType = typeMap(i,index)
        if (spVect(spType) == NOVAL) then
           spVect(spType) = val
        else
           spVect(spType) = combine(val, spVect(spType), rule)
        endif
     enddo
  case (PC_sp)
     spVect(index) = val
  end select
end subroutine


! Sovles for linear parameters
function solveLin(alpha, beta, fite1)
  real(8) :: solveLin(nsptype,opoly)
  real(8) :: alpha(nsptype), beta(nsptype)
  real(8) :: fite1(ndat,n_FF_comp)
  
  integer :: i, j, k
  integer :: row, col, spType, info
  real(8) :: expValSum, expVal, prod, rjtok, Eint, wener
  real(8) :: expMrPk(nlinparm)
  real(8) :: M(nlinparm,nlinparm)
  real(8) :: linVect(nlinparm)
  
  ! Construct M and b for matrix equation M*x = b
  M(:,:) = 0.0d0
  linVect(:) = 0.0d0
  
  do i = 1,ndat
     
     expValSum = 0.0d0
     expMrPk(:) = 0.0d0
     do j = 1,npair
        spType = spmap(j)
        
        if (spt(spType)%incExp) then
           expVal = exp(alpha(spType) - beta(spType)*rAB(i,j))
           expValSum = expValSum + expVal
           
           rjtok = 1.0
           do k = 1,oPoly
              rjtok = rjtok*rAB(i,j)
              
              if (comModeLin == 0) then
                 row = rowMap(spType,k)
                 expMrPk(row) = expMrPk(row) + expVal*rjtok
              else
                 prod = 0.5d0*expVal*rjtok
                 row = rowMap(spt(spType)%type1,k)
                 expMrPk(row) = expMrPk(row) + prod
                 row = rowMap(spt(spType)%type2,k)
                 expMrPk(row) = expMrPk(row) + prod
              endif
           end do
        endif
     end do
     
     do row = 1,nlinparm
        Eint = fite1(i,F_els) + fite1(i,F_vdw) + fite1(i,F_pol)
        prod = expMrPk(row)*ptWeight(i, Eint + fite1(i,F_exp))!baseWeight(i)
        linVect(row) = linVect(row) + prod*(grid(i,C_Etot) - Eint - expValSum)
        
        do col = row,nlinparm
           M(row,col) = M(row,col) + prod*expMrPk(col)
        end do
     end do
     
  end do
  
  ! Modify along diagonal for linear parameter penalty function
  do i = 1,nlinparm
     M(i,i) = M(i,i) + linParmPenScale(mod(i-1, oPoly) + 1)
  enddo
  
  ! Solve linear system
  info = 0
  call DPOSV('U', nlinparm, 1, M, nlinparm, linVect, nlinparm, info)
  
  do i = 1,nsptype
     if (spt(i)%isUsed .and. spt(i)%incExp) then
        do j = 1,opoly
           if (comModeLin == 0) then
              solveLin(i,j) = linVect(rowMap(i,j))
           else
              solveLin(i,j) = combine(linVect(rowMap(spt(i)%type1,j)), &
                   linVect(rowMap(spt(i)%type2,j)), CR_arith)
           endif
        enddo
     endif
  enddo
  
end function solveLin


! Calculate polarization energy for given configuration
real(8) function polEnerPoint(idat, d1, dp)
  integer, intent(in) :: idat
  real(8), intent(in) :: d1(nsptype), dP(nsptype)
  integer :: i, j
  
  do i = 1,2
     do j = 1,nsite(i)
        sit(i,j,:) = rvect(idat,i,j,:)
     enddo
  enddo
  
  polEnerPoint = polEnerDim(nPolIter, charge, pz, d1, dp)
  
  ! Check for NaN
  if (polEnerPoint /= polEnerPoint) then
     polEnerPoint = 1.0d10
  endif
end function


! Function called by the optimizer
function errFuncGlobal(x, n)
  real(8) :: errFuncGlobal
  integer :: n
  real(8) :: x(n)
  
  errFuncGlobal = calcEnergies(.false., .false., .false., .false., .false., x, V_glb)
end function


! Function called by the optimizer
function errFuncFinal(x, n)
  real(8) :: errFuncFinal
  integer :: n
  real(8) :: x(n)
  
  errFuncFinal = calcEnergies(.false., .false., .true., .not. fitTotal, .false., x, V_final)
end function


! Function called by the optimizer
function errFuncExpPart(x, n)
  real(8) :: errFuncExpPart
  integer :: n
  real(8) :: x(n)
  
  errFuncExpPart = calcEnergies(.false., .false., .false., .true., .false., x, V_exppart)
end function


! Function called by the optimizer
function errFuncExpFull(x, n)
  real(8) :: errFuncExpFull
  integer :: n
  real(8) :: x(n)
  
  errFuncExpFull = calcEnergies(.false., .false., .true., .true., .true., x, V_expfull)
end function


! Function called by the optimizer
function errFuncEls(x, n)
  real(8) :: errFuncEls
  integer :: n
  real(8) :: x(n)
  
  integer :: i
  
  errFuncEls = calcEnergies(.false., .true., .false., .false., .false., x, V_els)
  
  errFuncEls = 0.0d0
  do i = 1,ndat
     errFuncEls = errFuncEls + &
          baseWeight(i)*(fite(i,F_els) - grid(i,C_ener+E_els))**2
  enddo
end function


! Function called by the optimizer
function errFuncPol(x, n)
  real(8) :: errFuncPol
  integer :: n
  real(8) :: x(n)
  
  real(8) :: aiener
  integer :: i
  
  errFuncPol = calcEnergies(.false., .true., .false., .false., .false., x, V_pol)
  
  errFuncPol = 0.0d0
  do i = 1,ndat
     aiener = grid(i,C_ener+E_ind) + grid(i,C_ener+E_exhind) + grid(i,C_ener+E_dhf)
     errFuncPol = errFuncPol + baseWeight(i)*(fite(i,F_pol) - aiener)**2
  enddo
end function


! Function called by the optimizer
function errFuncVDW(x, n)
  real(8) :: errFuncVDW
  integer :: n
  real(8) :: x(n)
  
  integer :: i
  real(8) :: val, vdwweight
  
  errFuncVDW = calcEnergies(.false., .true., .false., .false., .false., x, V_vdw)
  
  errFuncVDW = 0.0d0
  do i = 1,ndat
     val = grid(i,C_ener+E_ind) + grid(i,C_ener+E_exhind) + grid(i,C_ener+E_dsp) + grid(i,C_ener+E_exhdsp) + &
          grid(i,C_ener+E_dhf) - fite(i,F_pol)
     if (val /= 0.0d0) then
        vdwweight = baseWeight(i) !(0.1d0*abs(minEnergy) + abs(grid(i,C_ener+E_ind)) + abs(grid(i,C_ener+E_dsp)))**(-1.0d0)
        errFuncVDW = errFuncVDW + vdwweight*(fite(i,F_vdw) - (val - fite(i,F_pol)))**2
     endif
  enddo
end function


! Find closest point of contact between molecules in units of pair radius
function minRadDist(idat)
  real(8) :: minRadDist
  integer :: idat
  
  integer :: i
  
  minRadDist = 1.0d100
  do i = 1,npair
     if (spt(spmap(i))%radius > 0.0d0) then
        minRadDist = min(minRadDist, rAB(idat,i)/spt(spmap(i))%radius)
     endif
  enddo
end function


! Returns the total fit energy of a point
function fittot(pnt)
  integer :: pnt
  real(8) :: fittot
  
  fittot = fite(pnt,F_els) + fite(pnt,F_vdw) + fite(pnt,F_pol) + fite(pnt,F_exp)
end function


! Fit weight of a point
real(8) function ptWeight(pnt, Eint)
  integer :: pnt
  real(8) :: Eint
  real(8) :: ptener
  
  !diff = max(0.0d0, (pnt%EAI(E_tot) - Eint)/weightEnerScale(minEnergy*h2kcal, weightScale))
  !ptWeight = pnt%weight*(1.0d0 + diff + 0.5d0*diff**2)
  ptener = grid(pnt,C_Etot)*(1.0d0 - weightEnerDep) + &
       min(grid(pnt,C_Etot), max(Eint, minEnergy))*weightEnerDep
  ptWeight = gweight(ptener, minEnergy, weightScale)
end function



!*************************** I/O ROUTINES *******************************


! Print error of fit on data set
subroutine writeError(title, maxLim, minLim)
  logical :: weighted
  real(8) :: maxLim, minLim
  character(*) :: title
  
  integer :: i
  
  write(*,'(A,A,I7,A,F10.3,A,F15.6)') &
       title,' (n, %, RMS in kcal/mol): ', &
       nPtRange(minLim, maxLim),' ',pctErr(maxLim, minLim),' ',rmsErr(maxLim, minLim)
end subroutine


! Print info on each grid point
subroutine writePoints()
  integer :: i
  real(8) :: sqErr, sum
  
  sum = 0.0
  do i = 1,ndat
     sqErr = (fittot(i) - grid(i,C_Etot))**2
     sum = sum + ptWeight(i, fittot(i))*sqErr
  end do
  
  do i = 1,ndat
     sqErr = (fittot(i) - grid(i,C_Etot))**2
     write(*,'(I6,A,F15.8,A,F6.3,A,F15.8,A,F15.8,A,F10.2,A,F15.7)') &
          i,' ',grid(i,C_rcom)*bohr2A,' ',minRadDist(i),' ',grid(i,C_Etot),' ',fittot(i),' ', &
          100*(fittot(i) - grid(i,C_Etot))/abs(grid(i,C_Etot)), ' ', &
          ndat*ptWeight(i, fittot(i))*sqErr/sum
  end do
end subroutine


! Print PES in transferrable form
subroutine writePES(iunit)
  integer :: iunit
  
  integer :: nsptypePrint
  integer :: i, j, k
  
  write(iunit,'(I5,I5,I5)') oPoly,nDisp,dispTerms(1)
  
  write(iunit,'(I5,I5)') nsite(1),nsite(2)
  do i = 1,2
     do j = 1,nsite(i)
        write(iunit,'(I5)',advance='no') stype(i,j)
     enddo
     write(iunit,*)
  enddo
  
  write(iunit,'(I5)') nmcgroupOptim(1) + nmcgroupOptim(2)
  do i = 1,2
     do j = 1,nmcgroupOptim(i)
        write(iunit,'(I5,I5,F15.8)') i,j,OACoor(i,j)
     enddo
  enddo
  
  do i = 1,nstype
     write(iunit,'(F20.14,A,F20.14)') charge(i),' ',pz(i)
  enddo
  
  nsptypePrint = 0
  do i = 1,nsptype
     if (spt(i)%isUsed .or. spt(i)%type1 == spt(i)%type2) then
        nsptypePrint = nsptypePrint + 1
     endif
  enddo
  
  write(iunit,'(I5,I5)') nsptypePrint
  do i = 1,nsptype
     if (spt(i)%isUsed .or. spt(i)%type1 == spt(i)%type2) then
        
        write(iunit,'(I3,A,I3,A,E18.10,A,E18.10,A)',advance='no') &
             spt(i)%type1,' ',spt(i)%type2,' ',spt(i)%beta*A2bohr,' ',spt(i)%alpha,' '
        do k = 1,oPoly
           write(iunit,'(E18.10,A)',advance='no') spt(i)%a(k)*A2bohr**k,' '
        enddo
        do k = 1,nDisp
           write(iunit,'(E18.10,A)',advance='no') vdwFactor*spt(i)%C(k)*bohr2A**dispTerms(k),' '
        enddo
        write(iunit,'(E18.10,A)',advance='no') spt(i)%A12*coreMult*bohr2A**12,' '
        write(iunit,'(E18.10,A)',advance='no') spt(i)%d1*A2bohr,' '
        do k = 1,nDisp
           write(iunit,'(E18.10,A)',advance='no') spt(i)%d(k)*A2bohr,' '
        enddo
        if (incPolDmp) then
           write(iunit,'(E18.10,A)',advance='no') spt(i)%dP*A2bohr,' '
        endif
        
        write(iunit,'()')
     endif
  enddo
end subroutine

end module

!***************************************************************************
!**************************** MAIN PROGRAM *********************************
!***************************************************************************

program fitInter
use fitInterDef
use const
use utility
implicit none

integer, save :: i, j, k, l

!***************************************************************************
!*************************** INITIALIZATION ********************************
!***************************************************************************


! Read command line parameters
call getarg(1, sysname)
enerfile = trim(sysname) // '.ener.fit'
verbose = .true.

read(*,NML=fit)

nPolIter = 1

if (verbose) then
   praxisPrint = 1
else
   praxisPrint = 0
endif

coreMult = 0.1d0
vdwFactor = 1.0d0

call read_dimer(trim(sysname)//'.input_long')

OACoor(:,:) = 0.0d0

if (verbose) write(*,*) 'Initializing site types'

open(asymp_out, file = 'fit_asymp.dat', status = 'old')

! Initialize site types
allocate( charge(nstype), pz(nstype), strad(nstype) )
do i = 1,nstype
   read(asymp_out,*) ival, charge(i), pz(i)
   
   if ((.not. incInter(i,F_exp)) .or. stanum(i) <= 0) then
      strad(i) = 0.0d0
   else
      strad(i) = covRad(stanum(i))
   endif
enddo

read(asymp_out,*) ndisp, comModeVDW, dispTerms(1)
do i = 2,maxDisp
   dispTerms(i) = dispTerms(i-1) + 2
enddo

if (verbose) write(*,*) 'Initializing site pair types'
! Initialize site pair types
allocate( spt(nstype**2) )

indx = 0
nsptypeUsed = 0
do i = 1,nstype
   do j = i,nstype
      
      indx = indx + 1
      
      spt(indx)%indx = indx
      spt(indx)%type1 = i
      spt(indx)%type2 = j
      
      spt(indx)%isUsed = (stcount(1,i) > 0 .and. stcount(2,j) > 0) .or. &
           (stcount(2,i) > 0 .and. stcount(1,j) > 0)
      if (spt(indx)%isUsed) nsptypeUsed = nsptypeUsed + 1
      
      spt(indx)%incEls = incInter(i,F_els) .and. incInter(j,F_els) &
           .and. abs(charge(i)) > 1.0d-10 .and. abs(charge(j)) > 1.0d-10 
      if (spt(indx)%incEls) then
         spt(indx)%chargeProd = h2kcal*charge(i)*charge(j)
      else
         spt(indx)%chargeProd = 0.0d0
      end if
      
      if (stanum(i) > 0 .and. stanum(j) > 0) then
         spt(indx)%radius = strad(i) + strad(j)
      else
         spt(indx)%radius = 0.0d0
      endif
      
      spt(indx)%incPolEls = (incInter(i,F_els) .and. incInter(j,F_pol)) &
           .or. (incInter(j,F_els) .and. incInter(i,F_pol))
      spt(indx)%incPolDip = incInter(i,F_pol) .and. incInter(j,F_pol)
      spt(indx)%incVDW = incInter(i,F_vdw) .and. incInter(j,F_vdw)
      spt(indx)%incExp = incInter(i,F_exp) .and. incInter(j,F_exp)
      
      if (spt(indx)%incExp) then
         spt(indx)%alpha = ALPHA_START
         spt(indx)%beta = BETA_START
      else
         spt(indx)%alpha = 0.0d0
         spt(indx)%beta = 0.0d0
      endif
      spt(indx)%a(:) = 0.0d0
      
      spt(indx)%A12 = 0.0d0   
      
      if (incPolDmp .and. spt(indx)%incPolDip) then
         spt(indx)%dP = DELTA_START
      else
         spt(indx)%dP = NO_DAMP
      endif
      if (incElsDmp .and. (spt(indx)%incEls .or. spt(indx)%incPolEls)) then
         spt(indx)%d1 = DELTA_START
      else
         spt(indx)%d1 = NO_DAMP
      endif
      if (incVDWDmp .and. spt(indx)%incVDW) then
         spt(indx)%d(:) = DELTA_START
      else
         spt(indx)%d(:) = NO_DAMP
      endif
      
      if (nDisp > 0) then
         if ((comModeVDW == 0 .and. spt(indx)%isUsed) .or. (comModeVDW == 1 .and. i == j)) then
            read(asymp_out,*) rBuf(1:nDisp)
            do k = 1,nDisp
               spt(indx)%C(k) = rBuf(k)*A2bohr**dispTerms(k)
            enddo
         else
            spt(indx)%C(:) = 0.0d0
         endif
      endif
      
   enddo
enddo

close(asymp_out)

if (comModeVDW == 1) then
   do i = 1,nsptype
      do j = 1,ndisp
         spt(i)%C(j) = combine(spt(typeMap(spt(i)%type1,spt(i)%type1))%C(j), &
              spt(typeMap(spt(i)%type2,spt(i)%type2))%C(j), CR_geo)
      enddo
   enddo
endif

incPol = .false.
do i = 1,nstype
   if (abs(pz(i)) > 1.0d-5) incPol = .true.
enddo

if (.not. incPol) incPolDmp = .false.

! Initialize site pairs
if (verbose) write(*,*) 'Initializing site pairs'

npair = nsite(1)*nsite(2)
allocate( spmap(npair), spsite(npair,2), sitesp(nsite(1),nsite(2)) )
indx = 1
do i = 1,nsite(1)
   do j = 1,nsite(2)
      spmap(indx) = typeMap(stype(1,i),stype(2,j))
      spsite(indx,:) = [i,j]
      sitesp(i,j) = indx
      indx = indx + 1
   enddo
enddo

call initRowMap()


! Initialize grid points
if (verbose) write(*,*) 'Initializing grid points'

if (.not. readPoints(enerfile)) then
   write(*,*) 'Error reading '//enerfile
   stop
endif

if (.not. incComponents) fitTotal = .true.


! Initialize parameter vector types
write(*,*) 'Initializing parameter vectors'

call initParmVect()

! Auto reduce
reduced = autoReduce > 0.0d0
do while (reduced)
   reduced = .false.
   
   if (ndat < autoReduce*vectLen(V_els) .or. ndat < autoReduce*vectLen(V_vdw)) then
      if (comModeDmp < 2) then
         comModeDmp = comModeDmp + 1
         reduced = .true.
         write(*,*) 'AUTO REDUCE: Increasing combination rule for damping parameters'
      endif
   endif
   
   if (ndat < autoReduce*(vectLen(V_expfull) + nlinparm)) then
      if (oPoly >= 2 .or. (oPoly == 1 .and. comModeExp >= 1)) then
         oPoly = oPoly - 1
         reduced = .true.
         write(*,*) 'AUTO REDUCE: Decreasing polynomial order'
      elseif (comModeExp < 1) then
         comModeExp = comModeExp + 1
         reduced = .true.
         write(*,*) 'AUTO REDUCE: Increasing combination rule for exponential parameters'
      elseif (comModeLin < 1) then
         comModeLin = comModeLin + 1
         reduced = .true.
         write(*,*) 'AUTO REDUCE: Increasing combination rule for linear parameters'
      endif
   endif
   
   if (reduced) call initParmVect()
enddo

allocate( globalParmVect(vectLen(V_glb)) )
allocate( finalParmVect(vectLen(V_final)) )
allocate( elsParmVect(vectLen(V_els)) )
allocate( polParmVect(vectLen(V_pol)) )
allocate( vdwParmVect(vectLen(V_vdw)) )
allocate( expPartParmVect(vectLen(V_exppart)) )
allocate( expFullParmVect(vectLen(V_expfull)) )
allocate( finalParmVectRand(nrand,vectLen(V_final)) )
allocate( randVectErr(nrand) )


if (verbose) then
   write(*,*)
   write(*,*) 'number of fitting points=',ndat
   write(*,*) 'number of randomized fits=',nrand
   write(*,*) 'number of fits to fully optimize=',nrandFinal
   write(*,*) 'number of Powell iterations=',nIter
   write(*,*) 'polynomial order=',oPoly
   write(*,*) 'weight scale=',weightScale
   write(*,*) 'minimum wall height (kcal/mol)=',wallHeight
   write(*,*) 'include VDW scale factor=',incVDWFact
   write(*,*) 'include els damping=',incElsDmp
   write(*,*) 'include vdw damping=',incVDWDmp
   write(*,*) 'include hard core=',incCore
   write(*,*) 'VDW combination rule mode=',comModeVDW
   write(*,*) 'Damping combination rule mode=',comModeDmp
   write(*,*) 'Exp combination rule mode=',comModeExp
   write(*,*) 'Lin combination rule mode=',comModeLin
   write(*,*) 'Fit all parms to total energy=',fitTotal
   write(*,*) 'Weight energy dependence=',weightEnerDep
   write(*,*) 'Energy of minimum=',minEnergy
endif


!***************************************************************************
!****************************** OPTIMIZATION *******************************
!***************************************************************************

! Set hard core coefficients
if (incCore) then
   !wallStartDist = max(0.8d0, repulDist(grid, ndat, wallHeight, 0.95d0)
   wallStartDist = 3.0d0
   do while (wallStartDist > 0.80d0)
      wallStartDist = wallStartDist - 0.01d0
      
      nAbove = 0
      nBelow = 0
      do i = 1,ndat
         if (minRadDist(i) < wallStartDist) then
            if (grid(i,C_Etot) > wallHeight) then
               nAbove = nAbove + 1
            else
               nBelow = nBelow + 1
            endif
         endif
      enddo
      
      if (nAbove >= 0.95d0*(nAbove + nBelow)) then
         exit
      endif
   enddo
   
   if (verbose) then
      write(*,*) ''
      write(*,'(A,F6.3)') 'Wall start location set to ',wallStartDist
   endif
   
   do i = 1,nsptype
      if (spt(i)%incExp) then
         spt(i)%A12 = wallHeight*(wallStartDist*spt(i)%radius)**12
      endif
   enddo
endif

! Fit global parameters
if (verbose) then
   write(*,*) ''
   write(*,'(A,I6)') 'Begin global parameter optimization', vectLen(V_glb)
endif

praxisScale = 5.0d-2
call fillParmVect(globalParmVect, V_glb)
call praxis(20, 5.0d-3, praxisScale, vectLen(V_glb), praxisPrint, globalParmVect, errFuncGlobal, 0, .false.)
val = calcEnergies(.true., .true., .false., .false., .false., globalParmVect, V_glb)

! Fit electrostatic parameters
if (vectLen(V_els) > 0 .and. incComponents) then
   if (verbose) then
      write(*,*) ''
      write(*,'(A,I6)') 'Begin electrostatic parameter optimization ', vectLen(V_els)
   end if
   
   call fillParmVect(elsParmVect, V_els)
   praxisScale = 1.0d0*sqrt(real(vectLen(V_els)))
   call praxis(40, 1.0d-2, praxisScale, vectLen(V_els), praxisPrint, elsParmVect, errFuncEls, 0, .false.)
   val = calcEnergies(.true., .true., .false., .false., .false., elsParmVect, V_els)
endif

! Fit polarization damping values
if (incPolDmp .and. incComponents) then
   if (verbose) then
      write(*,*) ''
      write(*,'(A,I6)') 'Begin polarization damping parameter optimization ', vectLen(V_pol)
   end if
   
   if (inc_dhf) then
      nPolIter = MAX_POL_ITER
   else
      nPolIter = 1
   endif
   call fillParmVect(polParmVect, V_pol)
   praxisScale = 5.0d0*sqrt(real(vectLen(V_pol)))
   call praxis(40, 1.0d-2, praxisScale, vectLen(V_pol), praxisPrint, polParmVect, errFuncPol, 0, .false.)
   val = calcEnergies(.true., .true., .false., .false., .false., polParmVect, V_pol)   
endif

! Optimize VDW parameters
if (vectLen(V_vdw) > 0 .and. incComponents) then
   if (verbose) then
      write(*,*) ''
      write(*,'(A,I6)') 'Begin VDW parameter optimization ', vectLen(V_vdw)
   end if
   
   call fillParmVect(vdwParmVect, V_vdw)
   praxisScale = 5.0d0*sqrt(real(vectLen(V_vdw)))
   call praxis(40, 1.0d-4, praxisScale, vectLen(V_vdw), praxisPrint, vdwParmVect, errFuncVDW, 0, .false.)
   val = calcEnergies(.true., .true., .false., .false., .false., vdwParmVect, V_vdw)
endif

nPolIter = MAX_POL_ITER

if (nIter <= 0) goto 1001

if (vectLen(V_exppart) > 0) then
   if (verbose) then
      write(*,*) ''
      write(*,'(A,I6)') 'Begin constrained exponential parameter optimization ', vectLen(V_exppart)
   end if
   
   call fillParmVect(expPartParmVect, V_exppart)
   praxisScale = 3.0d-2*sqrt(real(vectLen(V_exppart)))
   call praxis(40, 0.0d0, praxisScale, vectLen(V_exppart), praxisPrint, expPartParmVect, errFuncExpPart, 0, .false.)
   val = calcEnergies(.true., .true., .false., .false., .false., expPartParmVect, V_exppart)
endif

if (vectLen(V_expfull) > 0) then
   if (verbose) then
      write(*,*) ''
      write(*,'(A,I6)') 'Begin unconstrained exponential parameter optimization ', vectLen(V_expfull)
   end if
   
   call fillParmVect(expFullParmVect, V_expfull)
   praxisScale = 2.0d-1*sqrt(real(vectLen(V_expfull)))
   minVal = 1.0d100
   do i = 1,nExpIter
      linParmPenScale(:) = linParmPenScale(:)*(linParmPenEnd/linParmPenStart)**(1.0d0/dble(nExpIter))
      if (verbose) write(*,*) 'Lin parm pen scale =',linParmPenScale(1)
      do j = 1,10
         val = calcEnergies(.true., .true., .true., .false., .true., expFullParmVect, V_expfull)
      enddo
      val = 2.0d100
      do j = 1,50
         minVal = min(val, minVal)
         call praxis(1, 5.0d-3, praxisScale, vectLen(V_expfull), praxisPrint, expFullParmVect, errFuncExpFull, 0, j > 1)
         do k = 1,10
            val = calcEnergies(.true., .true., .true., .false., .true., expFullParmVect, V_expfull)
         enddo
         if (val < 1.00001d0*minVal .and. minVal - val < 0.01d0*minVal) exit
      enddo
      minVal = min(val, minVal)
   enddo
endif

if (.not. keepLinPen) linParmPenScale(:) = 0.0d0

if (vectLen(V_final) > 0) then
   newNrand = max(nrand, 1)
   praxisScale = 2.0d-1*sqrt(real(vectLen(V_final)))
   call fillParmVect(finalParmVect, V_final)
   
   finalParmVectRand(1,:) = finalParmVect(:)
   if (nrand == 0) then
      nrand = 1
   else
      if (verbose) then
         write(*,*) ''
         write(*,'(A)') 'Randomly perturbing parameters '
      end if
      
      call random_seed
      
      do i = 2,nRand
         val = calcEnergies(.true., .true., .false., .false., .false., finalParmVect, V_final)
         
         ! Randomly alter parameters
         do j = 1,nsptype
            call random_number(val)
            
            spt(j)%alpha = spt(j)%alpha + 0.4d0*(val*2.0d0 - 1.0d0)
            spt(j)%beta = spt(j)%beta*(0.90d0 + 0.20d0*val)
         end do
         
         call fillParmVect(finalParmVectRand(i,:), V_final)
      end do
   end if
   
   if (verbose) then
      write(*,*) ''
      write(*,'(A,I6)') 'Optimizing parameters ', vectLen(V_final)
   end if
   
   nIterPer = 3
   do while (nrand > nrandfinal)
      
      !$omp parallel default(firstprivate) shared(randVectErr,finalParmVectRand) private(j,k,val,finalParmVectPrivate)
      allocate( finalParmVectPrivate(vectLen(V_final)) )
      !$omp do
      do j = 1,nrand
         finalParmVectPrivate(:) = finalParmVectRand(j,:)
         call praxis(nIterPer, 0.0d0, praxisScale, vectLen(V_final), praxisPrint, finalParmVectPrivate, errFuncFinal, j, .false.)
         randVectErr(j) = errFuncFinal(finalParmVectPrivate, vectLen(V_final))
         finalParmVectRand(j,:) = finalParmVectPrivate(:)
      end do
      !$omp end do
      deallocate( finalParmVectPrivate )
      !$omp end parallel
      
      ! Discard worst vectors
      if (i == 1) then
         newNrand = nrandFinal
         do while(newNrand < nrand/2)
            newNrand = 2*newNrand
         end do
      else
         newNrand = max(nrand/2, nrandFinal)
      end if
      
      do j = 1,newNrand
         indx = j
         do k = j,nrand
            if (randVectErr(k) < randVectErr(indx)) indx = k
         end do
         
         finalParmVect(:) = finalParmVectRand(j,:)
         finalParmVectRand(j,:) = finalParmVectRand(indx,:)
         finalParmVectRand(indx,:) = finalParmVect(:)
         
         val = randVectErr(j)
         randVectErr(j) = randVectErr(indx)
         randVectErr(indx) = val
      end do
      
      write(*,*) 'nrand changed from ', nrand, 'to', newNrand
      nrand = newNrand
      
      nIter = max(1, nIter - nIterPer)
   end do
   
   ! Final optimization
   if (verbose) then
      write(*,*) ''
      write(*,*) 'Final optimization'
   end if
   
   !$omp parallel default(firstprivate) shared(randVectErr,finalParmVectRand) private(i,finalParmVectPrivate)
   allocate( finalParmVectPrivate(vectLen(V_final)) )
   !$omp do
   do i = 1,nrand
      finalParmVectPrivate(:) = finalParmVectRand(i,:)
      call praxis(nIter, 0.01d0, 2*praxisScale, vectLen(V_final), praxisPrint, finalParmVectPrivate, errFuncFinal, i, .false.)
      randVectErr(i) = errFuncFinal(finalParmVectPrivate, vectLen(V_final))
      finalParmVectRand(i,:) = finalParmVectPrivate(:)
   end do
   !$omp end do
   deallocate( finalParmVectPrivate )
   !$omp end parallel
   
   ! Determine best vector
   bestErr = 1.0d100
   do i = 1,nrand
      if (randVectErr(i) < bestErr) then
         finalParmVect(:) = finalParmVectRand(i,:)
         bestErr = randVectErr(i)
      end if
   end do
   
   val = calcEnergies(.true., .true., .true., .false., .false., finalParmVect, V_final)
endif

!***************************************************************************
!******************************** OUTPUT ***********************************
!***************************************************************************

1001 continue

write(*,*) ''
write(*,'(A,I8,A)') 'FIT DATA', ndat, ' points'
call writeError('Fit data final error E<Emin/2  ', minEnergy/2, -1.0d100)
call writeError('Fit data final error E<0       ', 0.0d00, -1.0d100)
call writeError('Fit data final error E<10      ', 10.0d00, -1.0d100)
call writeError('Fit data final error all points', 1.0d100, -1.0d100)

do i = 1,9
   str = ''
   write(str,'(A,A,I1)') trim(sysname),'.ener.test',i
   
   if (readPoints(str)) then
      write(*,*) ''
      write(*, '(A,I3,I8,A)') 'TEST DATA', i, ndat, ' points'
      call writeError('Test data final error E<Emin/2  ', minEnergy/2, -1.0d100)
      call writeError('Test data final error E<0       ', 0.0d0, -1.0d100)
      call writeError('Test data final error E<10      ', 10.0d0, -1.0d100)
      call writeError('Test data final error all points', 1.0d100, -1.0d100)
   end if
end do

if (verbose) then
   if (incCore) then
      write(*,*) ''
      write(*,'(A,F12.8)') 'A12 factor: ',coreMult
   endif
   if (incVDWFact) then
      write(*,*) ''
      write(*,'(A,F12.8)') 'vdwFactor: ',vdwFactor
   endif
endif

write(*,*) ''
write(*,*) 'Parameter values, all units in angstroms and kcal/mol'
write(*,*) 'i, j, beta, alpha, a(:), C(:), A_12, delta_1, delta_n(:), delta_p'
write(*,*) ''
call writePES(STDOUT)

open(parms_out, file='fit_inter.dat', status='new')
call writePES(parms_out)
close(parms_out)

if (.not. readPoints(enerfile)) write(*,*) 'Cannot read '//enerfile

if (verbose) then
   write(*,*) ''
   write(*,*) 'Points (i, R, minRad, E, E calculated, % error, weighted normalized sq err):'
   write(*,*) ''
   write(*,*) 'FIT DATA ', ndat, 'points'
   call writePoints()
   
   do i = 1,9
      str = ''
      write(str,'(A,A,I1)') trim(sysname),'.ener.test',i
      
      if (readPoints(str)) then
         write(*,*) ''
         write(*, '(A,I3,I8,A)') 'TEST DATA', i, ndat, ' points'
         call writePoints()
      end if
   end do
end if

end program

