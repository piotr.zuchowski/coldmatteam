
! Utility program to convert coordinates to .xyz format

program toxyz
  use dimer
  use dgrid
  use force_field
  implicit none
  
  character(200) :: str
  character(200) :: name, dimer_file, inter_file
  real(8) :: gpoint(MAX_NGIND)
  integer :: i, j
  
  call getarg(1, name)
  call getarg(2, inter_file)
  
  dimer_file = trim(name)//'.input_long'
  call read_dimer(dimer_file)
  call initGridCoors(0)
  
  if (inter_file /= '') then
     call ff_init(inter_file, 2)
  endif
  
  read(*,'(A)') str
  call str2gpoint(str, gpoint)
  
  call moveDimerGP(gpoint)
  
  write(*,'(I5)') nsite(1) + nsite(2)
  write(*,'(A)') trim(name)
  do i = 1,2
     do j = 1,nsite(i)
        write(*,'(I3,A,3F18.8)') sanum(i,j),' ',sit(i,j,1:3)*bohr2A
     enddo
  enddo
  
end program toxyz

