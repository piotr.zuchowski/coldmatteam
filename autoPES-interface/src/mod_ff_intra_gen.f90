
! Module for inputting and evaluating generated intramonomer potentials
! If dimer is homogeneous, nsptypeMon(2) == 0 and bn(2) is copied from bn(1)
module force_field_intra_gen
 use double
 use const
 use dimer
 implicit none
 
 integer, parameter :: NBTERM_MAX = 4
 
 real(8), allocatable :: bn(:,:,:)
 integer, allocatable :: typeMapMon(:,:) ! Map from pairs of site types to site pair types
 integer, allocatable :: typeMapInvMon(:,:,:) ! Map from site pair types to pairs of site types
 integer :: nbterm, nsptypeMon(2), maxnspt
 
 real(8), private :: rAB(MAX_NSITE,MAX_NSITE)
 real(8), private :: val
 !$OMP THREADPRIVATE(rAB)
 
contains

! Compute intramonomer potential at current geometry
function energy_ff_intra_gen(imon)
  use dimer
  implicit none
  
  real(8) :: energy_ff_intra_gen
  integer, intent(in) :: imon
  
  integer :: i, j, k
  integer :: sptype
  real(8) :: ener, dr, drPow
  
  do i = 1,nsite(imon)
     do j = 1,nsite(imon)
        rAB(i,j) = sqrt( &
             (sit(imon,i,1) - sit(imon,j,1))**2 + &
             (sit(imon,i,2) - sit(imon,j,2))**2 + &
             (sit(imon,i,3) - sit(imon,j,3))**2)
     enddo
  enddo
  
  ener = 0.0d0
  do i = 1,nsite(imon)
     do j = 1,nsite(imon)
        sptype = typeMapMon(stype(imon,i),stype(imon,j))
        if (sptype > 0) then
           dr = rAB(i,j) - rAB0(imon,i,j)
           
           drPow = dr
           do k = 1,nbterm
              ener = ener + bn(imon,sptype,k)*drPow
              drPow = drPow*dr
           enddo
        endif
     enddo
  enddo
  
  energy_ff_intra_gen = ener
  return
end function energy_ff_intra_gen


! Read in PES information from given file
! Ignores info already read from dimer file
subroutine read_fit_intra_gen(filename)
  use dimer 
  implicit none
  
  character(*), intent(in) :: filename
  
  integer :: i, j
  integer :: stype1, stype2, nspt, nbt
  real(8) :: rBuf(100)
  character(1000) :: line
  
  open(100,file=filename,status='old')
  
  read(100,*) nbt
  call initIntraFF(nbt)
  
  do i = 1,2
     read(100,*) nspt
     if (nspt /= nsptypeMon(i)) then
        write(*,*) 'ERROR: Inconsistent number of site pair types in intramolecular fit'
        stop
     endif
     
     do j = 1,nspt
        read(100,*) stype1, stype2, rbuf(1:nbt)
        bn(i,typeMapMon(stype1,stype2),1:nbt) = rbuf(1:nbt)
     enddo
  enddo
  
  close(100)
  
  if (homog) then
     bn(2,:,:) = bn(1,:,:)
  endif
  
end subroutine read_fit_intra_gen


! Initialize FF data, used whether or not reading FF from file
subroutine initIntraFF(nbt)
  integer, intent(in) :: nbt
  integer :: nspt
  integer :: i, j, k
  
  if (allocated(typeMapMon)) then
     deallocate(typeMapMon,typeMapInvMon,bn)
  endif
  
  allocate( typeMapMon(nstype,nstype) )
  allocate( typeMapInvMon(2,nstype*nstype,2) )
  
  typeMapMon(:,:) = 0
  typeMapInvMon(:,:,:) = 0
  nsptypeMon(:) = 0
  do i = 1,nstype
     do j = 1,nstype
        do k = 1,2
           if (stanum(i) > 0 .and. stanum(j) > 0 .and. typeMapMon(i,j) == 0) then
              if ((i /= j .and. stcount(k,i) > 0 .and. stcount(k,j) > 0) .or. &
                   (i == j .and. stcount(k,i) > 1)) then
                 nspt = nsptypeMon(k) + 1
                 nsptypeMon(k) = nspt
                 typeMapMon(i,j) = nspt
                 typeMapMon(j,i) = nspt
                 typeMapInvMon(k,nspt,1) = i
                 typeMapInvMon(k,nspt,2) = j
              endif
           endif
        enddo
     enddo
  enddo
  
  maxnspt = max(nsptypeMon(1), nsptypeMon(2))
  nbterm = nbt
  allocate( bn(2,maxnspt,NBTERM_MAX) )
  bn(:,:,:) = 0.0d0
  
end subroutine initIntraFF


! Finds range limits of IDOF using geometric threshold if mode == .false.
!      or energy threshold if mode == .true.
subroutine IDOF_limits(imon, iidof, minlim, maxlim, mode, thresh)
  integer, intent(in) :: imon, iidof
  logical, intent(in) :: mode
  real(8), intent(in) :: thresh
  real(8), intent(out) :: minlim, maxlim
  real(8) :: lim, dir
  real(8) :: mc(2,nmcmax)
  integer :: i, j, k
  
  if (mctype(imon,iidof) == MC_ANG .or. mctype(imon,iidof) == MC_LIN) then
     do i = 1,2
        if (i == 1) then
           dir = -1.0d0
        else
           dir = 1.0d0
        endif
        
        mc(:,:) = 0.0d0
        lim = 0.0d0
        do while (lim < pi)
           lim = min(pi, lim + 1.0d-2)
           
           mc(imon,iidof) = dir*lim
           call moveDimer(0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, mc)
           
           if (.not. mode) then  ! Geometric criterion
              do j = 1,natom(imon)
                 do k = 1,natom(imon)
                    if (j /= k) then
                       if (abs(cdist(imon,j,imon,k) - rAB0(imon,j,k))/rAB0(imon,j,k) > thresh) then
                          goto 101
                       endif
                    endif
                 enddo
              enddo
              !write(*,*) epsMetric(sit, sit0), thresh, mc(imon,iidof)
              !if (epsMetric(sit, sit0) > thresh) exit
           else ! Energy criterion using fit
              if (energy_ff_intra_gen(imon) > thresh) goto 101 !exit
           endif
        enddo
101     continue
        
        if (i == 1) then
           minlim = dir*lim
        else
           maxlim = dir*lim
        endif
     enddo
  elseif (mctype(imon,iidof) == MC_CHI) then
     minlim = 0.0d0
     maxlim = 0.0d0
  endif
end subroutine IDOF_limits


end module force_field_intra_gen

