#!/bin/bash

. $VARSDIR/vars

MODE=$1
WAIT=$2

MAINDIR=$(pwd)
PART='PART'
JOB='JOB'
OUTPUT=$NAME'.log'

if [ $RUN_MODE = 'clean' ]; then
    echo 'The directory '$(pwd)' will be cleaned of all failed or incomplete parts'
    echo 'Make sure that there are no running or queued jobs'
    echo -n 'Enter "y" to continue:'
    read cont
    if [ "$cont" = "y" ]; then
	for DIR in `find . -name 'PART*' -type d`; do
	    if [ ! -e $DIR/SUCCESS ]; then
		echo 'Removing '$DIR
		rm -r $DIR
	    fi
	done
	for DIR in `find . -name 'JOB*' -type d`; do
	    if [ ! -e $DIR/FINALIZED ]; then
		echo 'Removing '$DIR
		rm -r $DIR
	    fi
	done
    fi
fi

if [ ! -e SLAVE_SAPT_STARTED ]; then
    touch SLAVE_SAPT_STARTED
fi


if [ $MODE = 'dft' ]; then
    GOODSTR='Merging and scaling results'
    BADSTR='One or more corrections missing'
    BADSTR2='SCF NOT CONVERGED'
    RUNARG='noscfcp'
elif [ $MODE = 'deltahf' ]; then
    GOODSTR='\delta^{HF}_{int,r}'
    BADSTR='One or more corrections missing'
    BADSTR2='SCF NOT CONVERGED'
    RUNARG='scfcp'
elif [ $MODE = 'supmol' ]; then
    GOODSTR='FINAL SINGLE POINT ENERGY'
    BADSTR='ERROR'
    BADSTR2='SCF NOT CONVERGED'
    RUNARG=''
fi

if [ $NCORES_SCF == 1 ]; then
    ORCAEXE=$ORCADIR'/orca'
else
    ORCAEXE='mpirun '$ORCADIR'/orca'
fi

i=1
while read line; do
    DIRNAME=$PART$i
    
    if [ ! -d $DIRNAME ]; then
	mkdir $DIRNAME
	
	echo $line > $DIRNAME/$NAME.geo
	cp $INPUT2 $DIRNAME
	
	if [ -e $NAME'-elst.aux' ]; then
	    cp $NAME'-elst.aux' $DIRNAME
	fi
    fi
    
    let i=$i+1
done < $NAME.geo

while true; do
    
    NSTART=0
    pass1=1
    while [ $NSTART -eq 0 ]; do
        
        # Check for failue or success
	for DIR in `find . -name "$JOB*" -type d`; do
	    cd $DIR
	    if [ ! -e FINALIZED ]; then
		if [ -e SAPT_DONE ]; then
		    touch FINALIZED
		    
		    for PARTDIR in `find . -name "$PART*" -type l`; do
			if [ -e $PARTDIR/SUBMITTED ]; then
			    if [ -e $PARTDIR/$OUTPUT ]; then
				LINE1=$(grep -F "$GOODSTR" $PARTDIR/$OUTPUT)
				LINE2=$(grep -F "$BADSTR" $PARTDIR/$OUTPUT)
				LINE3=$(grep -F "$BADSTR2" $PARTDIR/$OUTPUT)
				if [ ! -n "$LINE1" ] || [ -n "$LINE2" ] || [ -n "$LINE3" ]; then
				    echo 'BAD OUTPUT' > $PARTDIR/FAILED
				else
				    cd $PARTDIR
				    $BINDIR/extract_ener $NAME $OUTPUT $MODE $HYBRID
				    cd $MAINDIR/$DIR
				    echo 'HOORAY' > $PARTDIR/SUCCESS
				fi
			    else
				echo 'NO OUTPUT' > $PARTDIR/FAILED
			    fi
			fi
		    done
		    
		elif [ -e SAPT_RUNNING ] && test $(find "SAPT_RUNNING" -mmin +$SAPT_TIMEOUT 2&>/dev/null); then
		    touch FINALIZED
		    
		    for PARTDIR in `find . -name "$PART*" -type l`; do
			if [ -e $PARTDIR/SUBMITTED ]; then
			    echo 'TIMEOUT' > $PARTDIR/FAILED
			fi
		    done
		else
		    if [ -e "submit_SAPT" ]; then
			if test $(find "submit_SAPT" -mmin +$SAPT_QUEUE_TIMEOUT); then
			    touch FINALIZED
			    
			    for PARTDIR in `find . -name "$PART*" -type l`; do
				if [ -e $PARTDIR/SUBMITTED ]; then
				    echo 'QUEUE TIMEOUT' > $PARTDIR/FAILED
				fi
			    done
			fi
		    else
			echo $DIR' has no submit_SAPT file'
		    fi
		fi
	    fi
	    cd $MAINDIR
	done
	
        # Collect status of jobs
	NRUNNING=0
	NQUEUE=0
	NSUCCESS=0
	NFAILED=0
	NREMAIN=0
	
	for DIR in `find . -name "$PART*" -type d`; do
	    if [ -e $DIR/SUBMITTED ]; then
		if [ -e $DIR/SUCCESS ]; then
		    let NSUCCESS=$NSUCCESS+1
		elif [ -e $DIR/FAILED ]; then
		    let NFAILED=$NFAILED+1
		fi
	    else
		let NREMAIN=$NREMAIN+1
	    fi
	done
	
	for DIR in `find . -name "$JOB*" -type d`; do
	    cd $DIR
	    if [ ! -e FINALIZED ]; then
		for PARTDIR in `find . -name "$PART*" -type l`; do
		    if [ -e SAPT_RUNNING ]; then
			let NRUNNING=$NRUNNING+1
		    else
			let NQUEUE=$NQUEUE+1
		    fi
		done
	    fi
	    cd $MAINDIR
	done
	
	let NSTART=$MAX_SIM_PT-$NRUNNING-$NQUEUE
	if [ $NSTART -lt 0 ]; then
	    NSTART=0
	else
	    if [ $NSTART -ge $NREMAIN ]; then
		NSTART=$NREMAIN
	    else
		let NSTART=($NSTART/$NPART_PER_JOB)*$NPART_PER_JOB
	    fi
	fi
	
	if [ $NRUNNING -eq 0 ] && [ $NQUEUE -eq 0 ] && [ $NREMAIN -eq 0 ]; then
	    break
	fi
	
	if [ $NSTART -eq 0 ]; then
	    if [ "$WAIT" = "F" ]; then
		exit
	    fi
	    
	    if [ $pass1 = '1' ]; then
                pass1=0
		echo "$NSUCCESS completed,  $NRUNNING running,  $NQUEUE queued,  $NREMAIN remaining,  $NFAILED failed"
                echo 'Awaiting completion of ab initio calculations'
            fi
	    sleep 1m
	fi
    done
    
    echo "$NSUCCESS completed,  $NRUNNING running,  $NQUEUE queued,  $NREMAIN remaining,  $NFAILED failed"
    
    if [ $NSTART -eq 0 ]; then
	echo 'SAPT calculations completed'
	break
    else
	echo
	echo 'Starting '$NSTART' new points' 
	
	npart=99999
	while [ $NSTART -gt 0 ]; do
	    let NSTART=$NSTART-1
	    
	    # Begin constructing new job
	    if [ $npart -ge $NPART_PER_JOB ]; then
		npart=0
                
		i=1
		while [ -d ./$JOB$i ]; do
		    let i=$i+1
		done
		JOBDIR=$JOB$i
		mkdir $JOBDIR
		
		JOBSTR=$JOBDIR' :'
                WORK=''
                INFILES=''
                OUTFILES=''
	    fi
	    
	    # Add a part to this job
	    for PARTDIR in `find $(pwd) -name "$PART*" -type d | sort`; do
		if [ ! -e $PARTDIR/SUBMITTED ]; then
		    touch $PARTDIR/SUBMITTED
		    
		    let npart=$npart+1
		    PARTNAME=$(basename $PARTDIR)
		    ln -s $PARTDIR $JOBDIR/$PARTNAME
		    JOBSTR=$JOBSTR' '$PARTNAME
		    
		    INFILES=$INFILES' '$PARTNAME
		    OUTFILES=$OUTFILES' '$PARTNAME'/'$OUTPUT
		    
		    if [ $MODE = dft ] || [ $MODE = deltahf ]; then
			WORK=$WORK' ( cd '$PARTNAME';
                               '$BINDIR'/make_orca_inp '$NAME' '$BASISDIR' '$BASIS' '$AUXBASIS' '$HYBRID' '$MODE' '$MB_CUTOFF' '$NCORES_SCF' '$SAPT_MEM';
                               '$SAPTDIR'/SAPTdf.orca '$NAME' '$RUNARG' > '$OUTPUT' 2>&1 ) &'
		    else
			WORK=$WORK' ( cd '$PARTNAME';
                               '$BINDIR'/make_orca_inp '$NAME' '$BASISDIR' '$BASIS' '$AUXBASIS' F supmol '$MB_CUTOFF' '$NCORES_SCF' '$SAPT_MEM';
                               '$ORCAEXE' '$NAME'.inp > '$OUTPUT' 2>&1 ) &'
		    fi
		    
		    break
		fi
	    done
	    
	    # If job has enough parts, run it
	    if [ $npart -eq $NPART_PER_JOB ] || [ $NSTART -eq 0 ]; then
		cd $JOBDIR
		NCORES_PER=$NCORES_SAPT
		if [ $NCORES_SCF -gt $NCORES_PER ]; then
		    NCORES_PER=$NCORES_SCF
		fi
		let JOBNCORES=$NCORES_PER*$npart
		WORK=$WORK' wait'
		echo 'Submitting '$JOBSTR
		$BINDIR/submit.sh $RUN_MODE $NAME$i 'SAPT' "$INFILES" "$OUTFILES" "$WORK" $NCORES_PER $JOBNCORES 0
		cd $MAINDIR
	    fi
	    
	done
	
	echo
    fi
    
done

OUTFILE=$MAINDIR/$NAME.ener
rm -f $OUTFILE

npart=$(cat $NAME.geo | wc -l)
#for DIR in `find . -name "$PART*" -type d`; do
for i in `seq 1 $npart`; do
    DIR=$PART$i
    if [ -e $DIR ]; then
	if [ -e $DIR/SUCCESS ]; then
	    cat $DIR/$NAME.ener >> $OUTFILE
	fi
    fi
done

touch SLAVE_SAPT_DONE

