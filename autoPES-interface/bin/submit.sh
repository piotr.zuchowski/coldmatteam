#!/bin/bash

# This is the optional second command line argument when calling autoPES
# after system name and may be ignored (see 'MODE' in the manual)
MODE=$1

# The name of the dimer
SYSTEM=$2

# This is a label indicating the type of job and will have one of the following values:
# A0, A1, B0, B1, ASYM, ASYMFIT, GRID, MINIMA, SAPT, INTER, SCAN
# This may be ignored unless different resources need to be allocated for different jobs
JOBNAME=$3

# This is a space-separated list of input files which must be copied from
# the original job directory to the work directory
INFILES=$4

# This is a space-separated list of output files which must be copied
# from the work directory to the original job directory
OUTFILES=$5

# This is a bash command which performs the necessary work
WORK=$6

# The number of OpenMP or MPI processes (threads) which will be used by each part
# of the job. In the case that this is a SAPT job and NPARTS_PER_JOB is greater
# than 1, this will differ from NCORES_TOT. In all other cases each job has only
# one 'part', so NCORES_PER and NCORES_TOT will be the same.
NCORES_PER=$7

# The maximum total number of cores (threads) which will be used simultaneously by the job
NCORES_TOT=$8

###########################################################################################
# MODIFY BELOW HERE FOR THE TARGET SYSTEM
# MAKE SURE YOU KNOW HOW TO RUN JOBS MANUALLY ON THE SYSTEM BEFORE MODIFYING THIS FILE
###########################################################################################

# Begin generating the job script ...
JOBFILE='submit_'$JOBNAME
echo '#!/bin/bash' > $JOBFILE

# Directives for the queueing system.
echo '#$ -N '$SYSTEM'_'$JOBNAME >> $JOBFILE
echo '#PBS -q grant' >> $JOBFILE
if [ "$NCORES_TOT" -ne "1" ]; then
    echo '#$ -pe threads '$NCORES_TOT >> $JOBFILE
    mem=$((5000*$NCORES_TOT)) 
    echo '#PBS -l walltime=120:00:00,nodes=1:ppn='$NCORES_TOT',pmem='$mem'mb' >>$JOBFILE
else
    echo '#PBS -l walltime=120:00:00,nodes=1:ppn=2,pmem=10000mb' >> $JOBFILE
fi

# Print some useful information
echo 'echo "Compute node: $HOSTNAME"' >> $JOBFILE
echo 'echo "Started at:"' >> $JOBFILE
echo 'date' >> $JOBFILE

# Set up environment
echo 'export OMP_NUM_THREADS='$NCORES_PER >> $JOBFILE
echo '.  /share/apps/intel/2016/compilers_and_libraries/linux/bin/compilervars.sh intel64      ' >> $JOBFILE
echo '. /share/apps/intel/2016/compilers_and_libraries/linux/mkl/bin/mklvars.sh intel64 ' >> $JOBFILE

# Change to the directory from which this script was run
echo 'LOCALDIR='$(pwd) >> $JOBFILE
echo 'cd $LOCALDIR' >> $JOBFILE

# Create the file $JOBNAME'_RUNNING'. This file is used as a flag by the
# master script and so must be handled correclty.
echo 'touch '$JOBNAME'_RUNNING' >> $JOBFILE

# Create the working directory. This will perform the work in $LOCALDIR.
# On many systems, this should instead be set to a specific scratch directory
# with a unique directory name assigned to the job.
echo 'WORKDIR='/scratch/$USER/$JOBNAME'_work.$PBS_JOBID' >> $JOBFILE
echo 'mkdir -p $WORKDIR' >> $JOBFILE

# Copy input files to the working directory. rsync is required so that linked
# directories are treated properly.
echo 'rsync -rRpk '$INFILES' $WORKDIR' >> $JOBFILE

# Move to the working directory and perform the work
echo 'cd $WORKDIR' >> $JOBFILE
echo $WORK | sed 's#/share/apps/AutoPES/AMD/#/share/apps/AutoPES/#g' >> $JOBFILE

echo 'if [ -d $LOCALDIR ]; then' >> $JOBFILE

# Copy output files and move back to original job directory. Again rsync is required.
echo '    rsync -rRpK '$OUTFILES' $LOCALDIR' >> $JOBFILE
echo '    cd $LOCALDIR' >> $JOBFILE

# Create the file $JOBNAME'_DONE' and remove the file $JOBNAME'_RUNNING'.
# These are used as flags by the master script and so must be handled correclty.
echo '    touch '$JOBNAME'_DONE' >> $JOBFILE
echo '    rm '$JOBNAME'_RUNNING' >> $JOBFILE

echo 'fi' >> $JOBFILE

# Clean up the working directory
echo 'rm -rf $WORKDIR' >> $JOBFILE

echo 'echo "End at:"' >> $JOBFILE
echo 'date' >> $JOBFILE

# Set excute permission on the jobfile
chmod 755 $JOBFILE

###########################################################################################
# THE NEXT COMMAND WILL EITHER RUN THE JOB DIRECTLY OR SUBMIT TO A QUEUING SYSTEM
# CHOOSE ONE, NOT BOTH
###########################################################################################

# This will run the job directly on the node that autoPES was run from. This is
# recommended only for small dimers on systems which do not use job schedulers.
# Usually this line should be commented out, and replaced with a command to submit
# $JOBFILE to a queuing system, see below.
# ./$JOBFILE &

# Uncomment this command if you want to submit the job script to the queuing
# system instead of running it directly
qsub $JOBFILE

