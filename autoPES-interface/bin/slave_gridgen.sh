#!/bin/bash

. $VARSDIR/vars

FF_TYPE=$1

if [ ! -e SLAVE_GRIDGEN_STARTED ]; then
    touch SLAVE_GRIDGEN_STARTED
fi

if [ $FF_TYPE = '1' ]; then
    FF_PARMS_FILE=$FORCE_FIELD.prm
    MINIMA_PCT=$GRID_MINIMA_PCT
elif [ $FF_TYPE = '2' ]; then
    FF_PARMS_FILE=fit_inter.dat
    MINIMA_PCT=$GRID_MINIMA_PCT_ADD
else
    echo 'ERROR: Unknown FF type'
fi

ITER_GRID_SIZE=$(cat npoint.info | head -1)

if [ $MINIMA_PCT = 0 ]; then
    INC_MIN_GRID=F
    GRID_SIZE_MIN=0
else
    INC_MIN_GRID=T
    if [ $PROBGRID = 'T' ]; then
	GRID_SIZE_MIN=0
    else
	GRID_SIZE_MIN=$(echo $ITER_GRID_SIZE'*'$MINIMA_PCT'/100' | bc)
    fi
fi
GRID_SIZE0=$(echo $ITER_GRID_SIZE'-'$GRID_SIZE_MIN | bc)

echo 'Main grid size    = ' $GRID_SIZE0
echo 'Minima grid size  = ' $GRID_SIZE_MIN
echo

if [ $INC_MIN_GRID = 'T' ] && [ ! -e MINIMA_RUNNING ] && [ ! -e MINIMA_DONE ]; then
   touch MINIMA_RUNNING
   
   echo
   echo '.......................................'
   echo '     BEGINNING MINIMA LOCALIZATION     '
   echo '.......................................'
   echo
   
   INFILES=$INPUT2' '$FF_PARMS_FILE
   OUTFILES='find_minima.log minima.dat'
   WORK=$BINDIR'/find_minima.sh '$NAME' '$FF_PARMS_FILE' '$FF_TYPE' '$NCORES' > find_minima.log 2>&1'
   $BINDIR/submit.sh $RUN_MODE $NAME 'MINIMA' "$INFILES" "$OUTFILES" "$WORK" $NCORES $NCORES 1000
fi

if [ ! -e GRID_RUNNING ] && [ ! -e GRID_DONE ]; then
    touch GRID_RUNNING
    
    echo
    echo '.......................................'
    echo '          MAIN GRID GENERATION         '
    echo '.......................................'
    echo
    
    if [ $PROBGRID = 'T' ] && [ $FF_TYPE = '2' ]; then
	INFILES=$INPUT2' '$FF_PARMS_FILE' '$NAME'.ener fit_intra.dat'
	OUTFILES='probgrid.log '$NAME'.geo_prob'
	WORK='time '$BINDIR'/probgrid '$NAME' '$FF_PARMS_FILE' fit_intra.dat '$GRID_SIZE0' 16000 F '$FIT_WEIGHT_SCALE' '$COM_RULE_EXP' '$COM_RULE_LIN' '$FRACR0' > probgrid.log 2>&1;'
	$BINDIR/submit.sh $RUN_MODE $NAME 'GRID' "$INFILES" "$OUTFILES" "$WORK" $NCORES $NCORES 2000
    else    	
	INFILES=$INPUT2' '$FF_PARMS_FILE' '$NAME'.ener fit_intra.dat'
	OUTFILES='gridgen.out '$NAME'.geo_main'
	WORK='time '$BINDIR'/gridgen_ff '$NAME' '$GRID_SIZE0' '$FF_PARMS_FILE' '$FF_TYPE' fit_intra.dat '$GRID_WEIGHT_SCALE' '$WALL_HEIGHT' '$GRID_MODE' '$FRACR0' > gridgen.out 2>&1'
	$BINDIR/submit.sh $RUN_MODE $NAME 'GRID' "$INFILES" "$OUTFILES" "$WORK" 1 1 1000
    fi
fi

if [ ! -e GRID_DONE ]; then
    echo 'Awaiting main grid generation - '$(date)
    while [ ! -e GRID_DONE ]; do
	sleep 1m
    done
fi
echo 'Main grid created'

echo

if [ $INC_MIN_GRID = 'T' ]; then
    if [ ! -e MINIMA_DONE ]; then
	echo 'Awaiting minima localization - '$(date)
	while [ ! -e MINIMA_DONE ]; do
            sleep 1m
	done
    fi
    echo 'Minima localization complete'
    
    $BINDIR/gridgen_min $NAME minima.dat $GRID_SIZE_MIN
fi

rm -f $NAME.geo
if [ -e $NAME.geo_main ]; then
    cat $NAME.geo_main >> ./$NAME.geo
fi
if [ -e $NAME.geo_prob ]; then
    cat $NAME.geo_prob >> ./$NAME.geo
fi
if [ -e $NAME.geo_min ]; then
    cat $NAME.geo_min >> ./$NAME.geo
fi

touch SLAVE_GRIDGEN_DONE
