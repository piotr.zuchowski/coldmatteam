#!/bin/bash

function isFloat {
    if [ ! -z $(echo "$1" | grep -E '^[-+]?[0-9]*\.?[0-9]+([eEdD][-+]?[0-9]+)?$') ]; then
        return 0
    fi
    return 1
}

AU2EV=27.211396132

. $VARSDIR/vars

if [ ! -e SLAVE_IP_STARTED ]; then
    touch SLAVE_IP_STARTED
    
    $BINDIR/make_input2 $NAME
    $BINDIR/make_ip_inp $NAME $BASIS $POLARIZABLE
fi

if [ ! -e $INPUT2 ]; then
    echo 'ERROR: problem creating input_long file'
    exit
fi

#A0
if [ ! -e A0_DONE ] && [ ! -e A0_RUNNING ]; then	
    if [ ! -e A0.inp ]; then
	echo 'ERROR: A0.inp not found!'
	exit 1
    fi
    
    touch A0_RUNNING
    echo 'Starting monomer calculations for neutral monomer A'
    
    INFILES='A0.inp'
    OUTFILES='A0.out'
    WORK='time '$ORCADIR'/orca A0.inp > A0.out 2>&1'
    $BINDIR/submit.sh $RUN_MODE $NAME 'A0' "$INFILES" "$OUTFILES" "$WORK" 1 1 4000
fi

#A1
if [ ! -e A1_DONE ] && [ ! -e A1_RUNNING ]; then
    if [ ! -e A1.inp ]; then
	echo 'ERROR: A1.inp not found!'
	exit 1
    fi
    
    touch A1_RUNNING
    echo 'Starting monomer calculations for ionized monomer A'
    
    INFILES='A1.inp'
    OUTFILES='A1.out'
    WORK='time '$ORCADIR'/orca A1.inp > A1.out 2>&1'
    $BINDIR/submit.sh $RUN_MODE $NAME 'A1' "$INFILES" "$OUTFILES" "$WORK" 1 1 4000
fi

if [ $SAME = 'F' ]; then
#B0
    if [ ! -e B0_DONE ] && [ ! -e B0_RUNNING ]; then
	touch B0_RUNNING
	echo 'Starting monomer calculations for neutral monomer B'
	
	INFILES='B0.inp'
	OUTFILES='B0.out'
	WORK='time '$ORCADIR'/orca B0.inp > B0.out 2>&1'
	$BINDIR/submit.sh $RUN_MODE $NAME 'B0' "$INFILES" "$OUTFILES" "$WORK" 1 1 4000
    fi

#B1
    if [ ! -e B1_DONE ] && [ ! -e B1_RUNNING ]; then
	touch B1_RUNNING
	echo 'Starting monomer calculations for ionized monomer B'
	
	INFILES='B1.inp'
	OUTFILES='B1.out'
	WORK='time '$ORCADIR'/orca B1.inp > B1.out 2>&1'
	$BINDIR/submit.sh $RUN_MODE $NAME 'B1' "$INFILES" "$OUTFILES" "$WORK" 1 1 4000
    fi
fi

if [ ! -e A0_DONE ]; then
    echo
    echo 'Awaiting monomer calculations for neutral monomer A - '$(date)
    while [ ! -e A0_DONE ]; do
	sleep 1m
    done
fi
echo 'Monomer calculations for neutral monomer A done'

if [ ! -e A1_DONE ]; then
    echo
    echo 'Awaiting monomer calculations for ionized monomer A - '$(date)
    while [ ! -e A1_DONE ]; do
        sleep 1m
    done
fi
echo 'Monomer calculations for ionized monomer A done'

if [ $SAME = 'F' ]; then
    if [ ! -e B0_DONE ]; then
	echo
	echo 'Awaiting monomer calculations for neutral monomer B - '$(date)
	while [ ! -e B0_DONE ]; do
	    sleep 1m
	done
    fi
    echo 'Monomer calculations for neutral monomer B done'
    
    if [ ! -e B1_DONE ]; then
	echo
	echo 'Awaiting IP calculations for ionized monomer B - '$(date)
	while [ ! -e B1_DONE ]; do
	    sleep 1m
	done
    fi
    echo 'Monomer calculations for ionized monomer B done'
fi

# Extract IP
PATTERN1='FINAL SINGLE POINT ENERGY'

EA0=`grep "$PATTERN1" A0.out | head -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 5`
EA1=`grep "$PATTERN1" A1.out | head -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 5`
if (isFloat "$EA0") && (isFloat "$EA1") && [ $(echo "$EA1 > $EA0" | bc -l ) -eq 1 ]; then
    IPA=$(echo "$AU2EV*($EA1 - $EA0)" | bc -l)
    echo 'IP for monomer A = '$IPA' eV'
else
    echo 'Unable to calculate IP for monomer A'
    exit
fi

if [ $SAME = 'F' ]; then
    EB0=`grep "$PATTERN1" B0.out | head -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 5`
    EB1=`grep "$PATTERN1" B1.out | head -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 5`
    if (isFloat "$EB0") && (isFloat "$EB1") && [ $(echo "$EB1 > $EB0" | bc -l ) -eq 1 ]; then
	IPB=$(echo "$AU2EV*($EB1 - $EB0)" | bc -l)
	echo 'IP for monomer B = '$IPB' eV'
    else
	echo 'Unable to calculate IP for monomer B'
	exit
    fi
else
    IPB=$IPA
fi

echo $IPA > IP.info
echo $IPB >> IP.info

# Extract dipole moment
PATTERN1='Magnitude (Debye)'

DMA=$(grep "$PATTERN1" A0.out | cut -d ':' -f 2 | tr -d '[[:space:]]')
if (isFloat "$DMA"); then
    echo 'Dipole moment for monomer A = '$DMA' Debye'
else
    echo 'Unable to calculate dipole moment for monomer A'
    exit
fi

if [ $SAME = 'F' ]; then
    DMB=$(grep "$PATTERN1" B0.out | cut -d ':' -f 2 | tr -d '[[:space:]]')
    if (isFloat "$DMB"); then
	echo 'Dipole moment for monomer B = '$DMB' Debye'
    else
	echo 'Unable to calculate dipole moment for monomer B'
	exit
    fi
else
    DMB=$DMA
fi

echo $DMA > DM.info
echo $DMB >> DM.info

# Extract isotropic polarizability
if [ $POLARIZABLE = 'T' ]; then
    
    PATTERN1='Isotropic polarizability'
    
    PZA=$(grep "$PATTERN1" A0.out | cut -d ':' -f 2 | tr -d '[[:space:]]')
    #echo "$PZA"
    if (isFloat "$PZA"); then
	echo 'Isotropic polarizability for monomer A = '$PZA' atomic units'
    else
	echo 'Unable to calculate polarizability for monomer A'
	exit
    fi
    
    if [ $SAME = 'F' ]; then
	PZB=$(grep "$PATTERN1" B0.out | cut -d ':' -f 2 | tr -d '[[:space:]]')
	if (isFloat "$PZB"); then
	    echo 'Isotropic polarizability for monomer B = '$PZB' atomic units'
	else
	    echo 'Unable to calculate polarizability for monomer B'
	    exit
	fi
    else
	PZB=$PZA
    fi
    
    echo $PZA > PZ.info
    echo $PZB >> PZ.info    
fi

# Exctract Loewdin partial charges
let nline=$NA+2
grep -A$nline 'LOEWDIN ATOMIC CHARGES' A0.out | grep ':' | cut -d ':' -f 2 > loewdinA.dat
if [ $SAME = 'F' ]; then
    let nline=$NB+2
    grep -A$nline 'LOEWDIN ATOMIC CHARGES' B0.out | grep ':' | cut -d ':' -f 2 > loewdinB.dat
else
    cp loewdinA.dat loewdinB.dat
fi

touch SLAVE_IP_DONE
