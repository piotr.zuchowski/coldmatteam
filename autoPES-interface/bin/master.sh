#!/bin/bash

export AUTOPES="$( cd "$( dirname "$0" )" && pwd )/.."
export VARSDIR=$AUTOPES/bin
. $VARSDIR/vars

export BINDIR=$AUTOPES/bin
export BASISDIR=$AUTOPES/basis
export FFDIR=$AUTOPES/ff
export ASYMSCRIPT=$SAPTDIR/asymp_com/Master_cmplx_asym.sh

export NAME=${1}
export RUN_MODE=$(echo ${2} | tr '[:upper:]' '[:lower:]')
export SAME=${3}
export NA=${4}
export NB=${5}
export BASIS=${6}
export MAX_CN=${7}
export GRID_SIZE=${8}
export FORCE_FIELD=${9}
export MAX_SIM_PT=${10}
export NCORES=${11} #1
export NCORES_SCF=${12} #1
export SAPT_TIMEOUT=${13} #600
export SAPT_QUEUE_TIMEOUT=${14} #3000
export SAPT_MEM=${15} #2000000000
export WALL_HEIGHT=${16} #15.0
export NPART_PER_JOB=${17} #1
export FIT_TEST_PCT=${18} #20
export POLY_ORDER=${19} #2
export GRID_MINIMA_PCT=${20} #20
export GRID_ADD_PCT=${21} #30
export HOLE_ADD_PCT=${22} #100
export FIT_WEIGHT_SCALE=${23} #0.15
export FIT_CONV_RATIO=${24} #0.2
export COM_RULE_DAMP=${25} #1
export HYBRID=${26} #F
export COM_RULE_EXP=${27} #0
export GRID_MINIMA_PCT_ADD=${28} #40
export MIN_GRIDGEN_ITER=${29} #1
export DISTR=${30} #T
export POLARIZABLE=${31} #T
export DELTAHF=${32} #F
export GRID_WEIGHT_SCALE=${33} #1.2
export WALL_MIN_DIST=${34} #1.35
export ELS_DMP=${35} #T
export VDW_DMP=${36} #T
export NCORES_SAPT=${37} #1
export FIT_A12=${38} #F
export FIT_AUTO_REDUCE=${39} #2.0
export PROBGRID=${40} #F
export COM_RULE_LIN=${41} #0
export COM_RULE_VDW=${42} #0
export MB_CUTOFF=${43} #0.8
export METHOD=${44} #SAPTDFT
export GRID_MODE=${45} #metric
export INTRA=${46} #F
export AUXBASIS=${47} 
export FRACR0=${48} #0.2

export INPUT1=$NAME'.input'
export INPUT2=$NAME'.input_long'
export AUXFILEA=$NAME.auxA
export AUXFILEB=$NAME.auxB
export AUXFILEM=$NAME.auxM

# Local directories
MASTERDIR=$(pwd)
IPSDIR=$MASTERDIR'/IP'
ASYMSDIR=$MASTERDIR'/ASYM'
MINIMASDIR=$MASTERDIR'/MINIMA'

MAXITER=999


echo
echo '==============================================================='
echo '==============================================================='
echo '           SAPT Automatic PES Generation Package'
echo '==============================================================='
echo '==============================================================='
echo

if [ ! -e $BINDIR/submit.sh ]; then
    echo 'Error: cannot find '$BINDIR'/submit.sh'
    echo 'Check that autoPES has been properly installed'
    exit
fi
if [ ! -e $SAPTDIR/sapt.x ]; then
    echo 'Error: cannot find '$SAPTDIR'/sapt.x'
    echo 'Check that SAPT has been properly installed'
    exit
fi
if [ ! -e $ORCADIR/orca ]; then
    echo 'Error: cannot find '$ORCADIR/orca
    echo 'Check that ORCA has been properly installed'
    exit
fi
if [ ! -e $BASISDIR/$BASIS.bas ]; then
    echo 'Error: cannot find '$BASISDIR/$BASIS.bas
    echo 'Check that the basis has been added to the autoPES repository'
    exit
fi
if [ ! -e $BASISDIR/$AUXBASIS.bas ]; then
    if [ $METHOD = 'SAPTDFT' ] || [ $DISTR = 'T' ]; then
	echo 'Error: cannot find '$BASISDIR/$AUXBASIS.bas
	echo 'Check that the auxiliary basis has been added to the autoPES repository'
	exit
    fi
fi

echo $NAME
if [ $SAME = 'T' ]; then
    echo 'Monomers are identical'
else
    echo 'Monomers are different'
fi
echo 'NA = '$NA
echo 'NB = '$NB
echo 'BASIS: '$BASIS


# Print fit table
FIRST=T
for n in $(seq 1 $MAXITER); do
    dir=$(pwd)'/FIT'$n
    if [ -e $dir/SLAVE_FIT_DONE ]; then
	if [ $FIRST = T ]; then
	    FIRST=F
	    echo ''
	    echo ' FIT   N grid       RMSE E<0 (test)      RMSE E<10 kcal/mol (test)   N holes '
	fi
	NGRID=$(cat $dir/$NAME.ener | wc -l)
	fitRMSE1=$(cat $dir/rmse.info | head -1 | awk '{print $1}')
	fitRMSE2=$(cat $dir/rmse.info | head -1 | awk '{print $2}')
	tstRMSE1=$(cat $dir/rmse.info | tail -1 | awk '{print $1}')
	tstRMSE2=$(cat $dir/rmse.info | tail -1 | awk '{print $2}')
	NHOLE=$(cat $dir/holes.info | head -1)
	printf '%4s %6s  %25s %25s %9s\n' $n $NGRID $fitRMSE1' ('$tstRMSE1')' $fitRMSE2' ('$tstRMSE2')' $NHOLE
    fi
done


###############################################################################################
#
#                              MONOMER PROPERTIES
#
###############################################################################################

mkdir -p $IPSDIR
cd $IPSDIR

if [ ! -e SLAVE_IP_DONE ]; then
    echo
    echo '=============================================================='
    echo '                    MONOMER PROPERTIES'
    echo '=============================================================='
    echo
    
    cp $MASTERDIR/$INPUT1 .
    $BINDIR/slave_ip.sh 2>&1 | tee -a slv_ip.out
fi

cd $MASTERDIR


if [ ! -e $IPSDIR/IP.info ]; then
    echo 'Error in IP calculations'
    exit
fi

if [ -e $IPSDIR/DM.info ]; then
    DMA=$(cat $IPSDIR/DM.info | head -1)
    DMB=$(cat $IPSDIR/DM.info | tail -1)
else
    echo 'Error in dipole moment calculations'
    exit
fi

if [ $POLARIZABLE = 'T' ]; then
    if [ ! -e $IPSDIR/PZ.info ]; then
	echo 'Error in polarizability calculations'
	exit
    fi
fi


#############################################################################################
#
#                                      CREATE INPUTS
#
#############################################################################################

echo ''
rm -f $INPUT2
$BINDIR/make_input2 $NAME $IPSDIR'/IP.info' $IPSDIR'/DM.info' $IPSDIR'/PZ.info'
if [ ! -e $INPUT2 ]; then
    echo 'Error creating input_long file'
    exit
fi

NFREEPARM=$($BINDIR/get_nfreeparm $NAME $COM_RULE_EXP $COM_RULE_LIN $POLY_ORDER)
if [ "$NFREEPARM" -eq "$NFREEPARM" ]; then
    echo 'Number of free parameters = '$NFREEPARM
else
    echo 'ERROR computing number of free parameters: '$NFREEPARM
    exit
fi

if [ $GRID_SIZE = 0 ]; then
    if [ $PROBGRID = 'T' ]; then
	ratio=3
    else
	ratio=6
    fi
    GRID_SIZE=$(echo $NFREEPARM'*'$ratio'*100/(100 - '$FIT_TEST_PCT')' | bc)
fi
GRID_SIZE_ADD=$(echo $GRID_SIZE'*'$GRID_ADD_PCT'/100' | bc)
GRID_SIZE_HOLE=$(echo $GRID_SIZE'*'$HOLE_ADD_PCT'/100' | bc)


##########################################################################################
#
#                                     SPECIAL RUN MODES
#
##########################################################################################

if [ $RUN_MODE == 'xyz' ]; then
    XYZDIR=$MASTERDIR'/XYZ'
    TEMPGEO=$XYZDIR'/'$NAME'.geo_xyz'
    echo ''
    echo 'Adding geometries from xyz files'
    echo ''
    
    for file in $(find $XYZDIR -name '*.xyz' -type f); do
	geofile=$file'.geo'
	rm -f $geofile
	$BINDIR/add_geo $NAME $file 0.0
	if [ ! -e $geofile ]; then
	    echo 'Unable to determine coordinates for '$file
	else
#	    dist=$(cat $geofile | awk '{print $1}')
#	    if [ -e $TEMPGEO ]; then
#		str=$(grep -F "$dist" $TEMPGEO)
#	    else
#		str=''
#	    fi
#	    if [ "$str" = "" ]; then
		echo 'Adding coordinates from '$file
		cat $geofile >> $TEMPGEO
#	    else
#		echo 'Ignoring duplicate structure in '$file
#	    fi
	fi
    done
    
    echo ''
    if [ -e $TEMPGEO ]; then
	nextiter=1
	for n in $(seq 1 $MAXITER); do
	    if [ -d GRID$n ] || [ -d SAPT$n ] || [ -d DELTAHF$n ]; then
		let nextiter=$n+1
	    fi
	done
	
	dir=$MASTERDIR/SAPT$nextiter
	echo 'Creating new batch of '$(cat $TEMPGEO | wc -l)' grid points at '$dir
	mkdir $dir
	mv $TEMPGEO $dir/$NAME.geo
    else
	echo 'Unable to add any .xyz files from the directory "'$XYZDIR'"'
    fi
    
    exit
fi

if [ $RUN_MODE == 'compress' ]; then
    echo 'Compressing all ab initio output files...'
    
    for n in $(seq 1 $MAXITER); do
        if [ -e SAPT$n/SLAVE_SAPT_DONE ]; then
	    cd SAPT$n
	    tar --remove-files -czf archive.tar.gz PART* JOB*
	    cd $MASTERDIR
	fi
	if [ -e DELTAHF$n/SLAVE_SAPT_DONE ]; then
            cd DELTAHF$n
            tar --remove-files -czf archive.tar.gz PART* JOB*
	    cd $MASTERDIR
        fi
    done
    
    exit
fi

#################################################################################################
#
#                                   ASYMPTOTICS
#
#################################################################################################

mkdir -p $ASYMSDIR
cd $ASYMSDIR

if [ ! -e SLAVE_ASYM_DONE ]; then
    
    echo
    echo '=============================================================='
    echo '                   ASYMPTOTIC CALCULATIONS'
    echo '=============================================================='
    echo
    
    cp $MASTERDIR/$INPUT2 .
    cp $IPSDIR/loewdinA.dat .
    cp $IPSDIR/loewdinB.dat .
    
    $BINDIR/slave_asym.sh F 2>&1 | tee -a slv_asym.out
fi

cd $MASTERDIR

####################################################################################################
#
#                                       MAIN LOOP BEGIN
#
####################################################################################################

WALL_THRESHOLD=$(echo 0.9*$WALL_HEIGHT | bc -l)

while true; do
    echo ''
    
##################################################################################################
#
#                                MAIN LOOP CONTROL LOGIC
#
##################################################################################################
    
    itype=''
    inum=0
    wait='T'
    
    # Set DELTAHF
    if [ ! $METHOD = 'SAPTDFT' ]; then
	DELTAHF='F'
    fi
    if [ $DELTAHF = 'AUTO' ]; then
	if [ -e $ASYMSDIR/SLAVE_ASYM_DONE ]; then
	    if [ ! -e $ASYMSDIR/fit_asymp.dat ]; then
		echo 'Error in asymptotic calculations'
		exit 1
            fi
	    ID_RATIO=$(grep 'Mean ratio of E_ind/E_dsp' $ASYMSDIR/fit_asymp.log | cut -d '=' -f 2 | tr -d '[[:space:]]')
	else
	    ID_RATIO='0.0'
	fi
	if [ $(echo "$ID_RATIO > 0.15" | bc -l) -eq 1 ]; then
	    DELTAHF='T'
	    echo 'deltaHF correction enabled because E_ind/E_disp ratio criterion met'
	elif [ $(echo "$DMA > 1.5" | bc -l) -eq 1 ]; then
	    DELTAHF='T'
	    echo 'deltaHF correction enabled because dipole moment criterion for monomer A met'
	elif [ $(echo "$DMB > 1.5" | bc -l) -eq 1 ]; then
	    DELTAHF='T'
	    echo 'deltaHF correction enabled because dipole moment criterion for monomer B met'
	else
	    DELTAHF='F'
	    echo 'deltaHF correction disabled'
	fi
    fi
    export DELTAHF
    
    # Check for fit in progress and identify latest complete fit
    FITSDIR=''
    CURR_FIT=''
    FIT_NEXT=1
    for n in $(seq 1 $MAXITER); do
	if [ -d FIT$n ]; then
	    FIT_NEXT=$(echo "$n + 1" | bc)
	    dir=$(pwd)'/FIT'$n
	    
	    if [ "$itype" = "" ] && [ ! -e $dir/SLAVE_FIT_DONE ]; then
		FITSDIR=$dir
		itype='FIT'
		inum=$n
	    fi
	    
	    if [ -e $dir/SLAVE_FIT_DONE ]; then
		CURR_FIT=$dir
	    fi
	fi
    done
    
    # Check for SAPT calculations in progress
    SAPTSDIR=''
    GRIDSDIR=''
    NGRIDGEN=0
    GRID_NEXT=1
    SAPT_NEXT=1
    for n in $(seq 1 $MAXITER); do
	if [ -d GRID$n ]; then
            GRID_NEXT=$(echo "$n + 1" | bc)
            dirGRID=$(pwd)'/GRID'$n
	    dirSAPT=$(pwd)'/SAPT'$n
	    
	    if [ "$itype" = "" ]; then
	        # If grid generation in progress
		if [ ! -e $dirGRID/SLAVE_GRIDGEN_DONE ]; then
                    GRIDSDIR=$dirGRID
                    itype='GRID'
                    inum=$n
		fi
                # If GRID$n done but no associated SAPT$n
		if [ -e $dirGRID/SLAVE_GRIDGEN_DONE ] && [ ! -e $dirSAPT/$NAME.geo ]; then
		    if [ ! -e $dirGRID/$NAME.geo ]; then
			echo 'Error in grid generation '$n
			exit
		    fi
                    mkdir -p $dirSAPT
		    cp $dirGRID/$NAME.geo $dirSAPT
		fi
	    fi
	    
            if [ -e $dirGRID/SLAVE_GRIDGEN_DONE ]; then
                let NGRIDGEN=$NGRIDGEN+1
            fi
        fi
	
	if [ -d SAPT$n ]; then
	    SAPT_NEXT=$(echo "$n + 1" | bc)
	    GRID_NEXT=$SAPT_NEXT
            dirSAPT=$(pwd)'/SAPT'$n
	    dirDHF=$(pwd)'/DELTAHF'$n

	    # Check for failure
	    if [ -e $dirSAPT/SLAVE_SAPT_DONE ] && [ $(cat $dirSAPT/$NAME.ener | wc -l) -eq 0 ]; then
		echo 'Error in '$dirSAPT
		exit
	    fi
	    if [ -e $dirDHF/SLAVE_SAPT_DONE ] && [ $(cat $dirDHF/$NAME.ener | wc -l) -eq 0 ]; then
		echo 'Error in '$dirDHF
		exit
	    fi
	    
	    if [ "$itype" = "" ]; then
	        # If SAPT in progress
		if [ ! -e $dirSAPT/SLAVE_SAPT_DONE ]; then
		    npt=$(cat $dirSAPT/$NAME.geo | wc -l)
		    # Start simultaneous DHF calcualtions if $MAX_SIM_PT is large enough, otherwise just run SAPT
		    if [ $DELTAHF = 'T' ] && [ ! -e $dirDHF/SLAVE_SAPT_STARTED ] && [ $(echo "2*$npt <= $MAX_SIM_PT" | bc -l) -eq 1 ]; then
			DELTAHFSDIR=$dirDHF
			itype='DELTAHF'
			inum=$n
			wait='F'
		    else
			SAPTSDIR=$dirSAPT
			itype='SAPT'
			inum=$n
		    fi
		fi
		# If SAPT is done but DHF needed
		if [ "$itype" = "" ] && [ $DELTAHF = 'T' ] && [ -e $dirSAPT/SLAVE_SAPT_DONE ] && [ ! -e $dirDHF/SLAVE_SAPT_DONE ]; then
		    DELTAHFSDIR=$dirDHF
		    itype='DELTAHF'
		    inum=$n
		fi
		# Create DHF directory if needed
		if [ "$itype" = 'DELTAHF' ] && [ ! -e $dirDHF/SLAVE_SAPT_STARTED ]; then
		    mkdir -p $DELTAHFSDIR
		    cp $dirSAPT/$NAME.geo $DELTAHFSDIR
		fi
	    fi
	    
        fi
    done
    
    # Read info on current fit
    converged=F
    incTest=F
    if [ ! "$CURR_FIT" = "" ]; then
	
	# Read holes info
	if [ -e $CURR_FIT/holes.info ]; then
	    nholes=$(cat $CURR_FIT/holes.info | head -1)
	    minheight=$(cat $CURR_FIT/holes.info | tail -1)
	    if [ $(echo "$minheight > $WALL_THRESHOLD" | bc -l) -eq 1 ]; then
		wall_good=T
	    else
		wall_good=F
	    fi
	else
	    echo 'Error in hole scanning in '$CURR_FIT
	    exit
	fi
	
        # Read RMSE info
	if [ -e $CURR_FIT/rmse.info ]; then
	    fitRMSE=$(cat $CURR_FIT/rmse.info | head -1 | awk '{print $1}')
	    tstRMSE=$(cat $CURR_FIT/rmse.info | tail -1 | awk '{print $1}')
	    
	    # Check if current fit uses test set
	    if [ $(echo "$tstRMSE > 0.0" | bc -l) -eq 1 ]; then
		incTest=T
	    else
		incTest=F
	    fi
	    
	    # Check if current fit meets convergence criteria
	    if [ $wall_good = 'T' ] && [ $NGRIDGEN -ge $MIN_GRIDGEN_ITER ]; then
		if [ $incTest = 'T' ]; then
		    if [ $(echo $tstRMSE' < '$FIT_CONV_RATIO'*'$fitRMSE | bc -l) -eq 1 ]; then
			converged=T
			#echo 'Convergence criteria met'
		    fi
		else
		    converged=T
		fi
	    fi
	else
	    echo 'Error in PES fitting in '$CURR_FIT
	    exit
	fi    
    fi
    
    # If nothing in progress then decide what to do
    if [ "$itype" = "" ]; then
	
	if [ "$CURR_FIT" = "" ]; then
	    if [ ! -d GRID1 ] && [ ! -d SAPT1 ]; then
                # Start the first grid generation
		mkdir GRID1
		echo $GRID_SIZE > GRID1/npoint.info
	    else
	        # Start the first fit
		mkdir FIT1
	    fi
	else
	    
	    # Collect existing grid points
	    enerfile=$NAME.ener
	    rm -f $enerfile
	    for n in $(seq 1 $MAXITER); do
		if [ -e SAPT$n/SLAVE_SAPT_DONE ]; then
		    if [ $DELTAHF = 'F' ] || [ -e DELTAHF$n/SLAVE_SAPT_DONE ]; then
			$BINDIR/combine_ener $NAME 'SAPT'$n/$NAME.ener 'DELTAHF'$n/$NAME.ener $DELTAHF >> $enerfile
		    fi
		fi
	    done
	    if [ -z "$(diff $enerfile $CURR_FIT/$NAME.ener)" ]; then
		newdat='F'
	    else
		newdat='T'
	    fi
	    rm -f $enerfile
	    
            # If latest fit did not use latest data then start a new fit
	    if [ $newdat = 'T' ]; then
		echo 'Starting new fit with latest data'
		mkdir 'FIT'$FIT_NEXT
	    else
		
		# There is nothing in progress, and the latest fit uses the latest data
	        # Decide what to do based on result of latest fit
		if [ $wall_good = 'F' ] && [ $GRID_SIZE_HOLE -gt 0 ]; then
		    dir='SAPT'$SAPT_NEXT
		    mkdir -p $dir
		    cat $CURR_FIT/holes.out | head -n $GRID_SIZE_HOLE > $dir/$NAME.geo
		    nadd=$(cat $dir/$NAME.geo | wc -l)
		    echo 'Patching holes with '$nadd' additional points'
		else
		    if [ $converged = 'T' ]; then
			if [ $incTest = 'F' ]; then
			    break
			else
			    echo 'Convergence criteria met.'
			    echo 'Performing PES fit to all grid points'
			    mkdir 'FIT'$FIT_NEXT
			fi
		    else
			dir='GRID'$GRID_NEXT
			echo 'Convergence criteria not met.'
			echo 'Adding '$GRID_SIZE_ADD' more points to the grid'
			
			mkdir -p $dir
			echo $GRID_SIZE_ADD > $dir/npoint.info
			if [ -e $CURR_FIT/minima.dat ]; then
			    cp $CURR_FIT/minima.dat $dir
			    touch $dir/MINIMA_DONE
			fi
		    fi
		fi
		
	    fi
	    
	fi
    fi
    
##################################################################################################
#
#                                  CALL APPROPRIATE SLAVE SCRIPT
#
##################################################################################################
    
    if [ "$itype" = "GRID" ]; then
	echo
	echo '=============================================================='
	echo '                  GRID POINT GENERATION '$inum
	echo '=============================================================='
	echo
	
	cd $GRIDSDIR
	
	if [ "$CURR_FIT" = "" ]; then
	    FF_TYPE=1
	else
	    FF_TYPE=2
	fi
	
	if [ ! -e SLAVE_GRIDGEN_STARTED ]; then
	    cp $MASTERDIR/$INPUT2 .
	    if [ $FF_TYPE = '1' ]; then
		cp $FFDIR/$FORCE_FIELD.prm .
	    elif [ $FF_TYPE = '2' ]; then
		cp $CURR_FIT/fit_inter.dat .
		if [ -e $CURR_FIT/minima.dat ]; then
		    cp $CURR_FIT/minima.dat .
		fi
		if [ $INTRA = 'T' ]; then
		    cp $CURR_FIT/fit_intra.dat .
		fi
	    fi
	    if [ -e $CURR_FIT/$NAME.ener ]; then
		cp $CURR_FIT/$NAME.ener .
	    fi
	fi
	
	$BINDIR/slave_gridgen.sh $FF_TYPE 2>&1 | tee -a slv_gridgen.out
	
	cd $MASTERDIR
    fi
    
    if [ "$itype" = "SAPT" ]; then
	echo
	echo '=============================================================='
	echo '                  AB INITIO CALCULATIONS '$inum
	echo '=============================================================='
	echo
	
	cd $SAPTSDIR
	
	if [ ! -e SLAVE_SAPT_STARTED ]; then
	    cp $MASTERDIR/$INPUT2 .
	fi
	
	if [ $METHOD = 'SAPTDFT' ]; then
	    mode='dft'
	elif [ $METHOD = 'CCSDT' ]; then
	    mode='supmol'
	fi
	$BINDIR/slave_sapt.sh $mode $wait 2>&1 | tee -a slv_sapt.out
        
	cd $MASTERDIR
    fi
    
    if [ "$itype" = "DELTAHF" ]; then
	echo
	echo '=============================================================='
	echo '                  DELTAHF CALCULATIONS '$inum
	echo '=============================================================='
	echo
	
	cd $DELTAHFSDIR
	
	if [ ! -e SLAVE_SAPT_STARTED ]; then
	    cp $MASTERDIR/$INPUT2 .
	fi
	
	$BINDIR/slave_sapt.sh 'deltahf' $wait 2>&1 | tee -a slv_sapt.out
	
	cd $MASTERDIR
    fi
    
    if [ "$itype" = "FIT" ]; then
	
        # Wait for asymptotic calculations before doing fit
	if [ ! -e $ASYMSDIR/SLAVE_ASYM_DONE ]; then
	    echo
	    cd $ASYMSDIR
	    $BINDIR/slave_asym.sh T 2>&1 | tee -a slv_asym.out
	    cd $MASTERDIR
	fi
	if [ ! -e $ASYMSDIR/fit_asymp.dat ]; then
	    echo 'Error in asymptotic calculations'
	    exit 1
	fi
	
	echo
	echo '=============================================================='
	echo '                        PES FIT '$inum
	echo '=============================================================='
	echo
	
	if [ ! -e $FITSDIR/SLAVE_FIT_STARTED ]; then
	    cp $MASTERDIR/$INPUT2 $FITSDIR
	    cp $ASYMSDIR/fit_asymp.dat $FITSDIR
	    
	    # Collect existing grid points
            enerfile=$FITSDIR/$NAME.ener
            rm -f $enerfile
            for n in $(seq 1 $MAXITER); do
                if [ -e SAPT$n/SLAVE_SAPT_DONE ]; then
		    if [ $DELTAHF = 'F' ] || [ -e DELTAHF$n/SLAVE_SAPT_DONE ]; then
                        $BINDIR/combine_ener $NAME 'SAPT'$n/$NAME.ener 'DELTAHF'$n/$NAME.ener $DELTAHF >> $enerfile
                    fi
                fi
            done
	    
	    if [ -e $MASTERDIR/$NAME.ener.test2 ]; then
		echo 'Using extra test set '$NAME.ener.test2
		cp $MASTERDIR/$NAME.ener.test2 $FITSDIR
	    fi
	fi
	
	cd $FITSDIR
	
	if [ $converged = 'T' ]; then
	    incTestSet='F'
	else
	    incTestSet='T'
	fi
	
	$BINDIR/slave_fit.sh $incTestSet 2>&1 | tee -a slv_fit.out
        
	cd $MASTERDIR
    fi
    
done

####################################################################################################
#
#                                       MAIN LOOP END
#
####################################################################################################

echo
echo 'PES generation complete'

exit

