#!/bin/bash

BINDIR="$( cd "$( dirname "$0" )" && pwd )"

NAME=$1
FF_FILE=$2
FF_TYPE=$3
NPAR=$4

MAINDIR=$(pwd)

$BINDIR/make_pesw_inp $NAME $NPAR 8000
for i in $(seq 1 $NPAR); do
    PARTDIR=$MAINDIR/PART$i
    mkdir $PARTDIR
    cp cluster.dat control.dat $FF_FILE $NAME.input_long $PARTDIR
    mv coords.dat.$i $PARTDIR/coords.dat
    (cd $PARTDIR; $BINDIR/pesw_ff $NAME $FF_FILE $FF_TYPE > pesw.out 2>&1) &
done
wait

rm -f minima_all.dat
for i in $(seq 1 $NPAR); do
    PARTDIR=$MAINDIR/PART$i
    cd $PARTDIR
    
    PATTERN1='Energy after minimization'
    PATTERN2='Relative coordinates'
    PATTERN3='   1   2'
    
    grep -A 12 "$PATTERN1" pesw.out > tmp
    
    N=`grep "$PATTERN1" tmp | wc -l`
    
    for i in `seq 1 $N`; do
	x1=`grep "$PATTERN1" tmp | head -$i | tail -1 | tr -s ' ' | cut -d ':' -f 2`
	x2=`grep -A 2 "$PATTERN2" tmp | grep "$PATTERN3" | head -$i | tail -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 3`
	x3=`grep -A 2 "$PATTERN2" tmp | grep "$PATTERN3" | head -$i | tail -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 4`
	x4=`grep -A 2 "$PATTERN2" tmp | grep "$PATTERN3" | head -$i | tail -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 5`
	x5=`grep -A 2 "$PATTERN2" tmp | grep "$PATTERN3" | head -$i | tail -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 6`
	x6=`grep -A 2 "$PATTERN2" tmp | grep "$PATTERN3" | head -$i | tail -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 7`
	x7=`grep -A 2 "$PATTERN2" tmp | grep "$PATTERN3" | head -$i | tail -1 | sed -e 's/^[ \t]*//' | tr -s ' ' | cut -d ' ' -f 8`
	if [ "$x2" != "NaN" ]; then
	    echo $x2 $x3 $x4 $x5 $x6 $x7 $x1 >> minima_all.dat
	fi
    done
    
    rm tmp
    
    cd $MAINDIR
    cat $PARTDIR/minima_all.dat >> ./minima_all.dat
done

$BINDIR/sort_minima $NAME minima_all.dat $FF_FILE $FF_TYPE > minima.dat
