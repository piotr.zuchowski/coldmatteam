#!/bin/bash

function isFloat {
    if [ ! -z $(echo "$1" | grep -E '^[-+]?[0-9]*\.?[0-9]+([eEdD][-+]?[0-9]+)?$') ]; then
        return 0
    fi
    return 1
}

#VARSDIR=`echo $0 | sed 's#/[^/]*$#/#g'`
. $VARSDIR/vars

incTestSet=$1

FIT_EFFORT='NORM'

if [ ! -e SLAVE_FIT_STARTED ]; then
    touch SLAVE_FIT_STARTED
fi

if [ $INTRA = 'T' ] && [ ! -f INTRA_RUNNING ] && [ ! -f INTRA_DONE ]; then
    touch INTRA_RUNNING
    
    echo
    echo '.......................................'
    echo '      FITTING INTRAMOLECULAR PES       '
    echo '.......................................'
    echo
    
    INFILES='fit_asymp.dat '$NAME'.ener '$INPUT2
    OUTFILES='fit_intra.log fit_intra.dat'
    WORK=$BINDIR'/fit_intra '$NAME' 4 100.0 fit_intra.dat.old '$FIT_WEIGHT_SCALE' > fit_intra.log 2>&1'
    $BINDIR/submit.sh $RUN_MODE $NAME 'INTRA' "$INFILES" "$OUTFILES" "$WORK" 1 1 2000
fi

if [ ! -f INTER_RUNNING ] && [ ! -f INTER_DONE ]; then
    touch INTER_RUNNING
    
    echo
    echo '.......................................'
    echo '      FITTING INTERMOLECULAR PES       '
    echo '.......................................'
    echo
    
    if [ $incTestSet = 'T' ]; then
	testSetCutoff=$FIT_TEST_PCT
    else
	testSetCutoff='0'
    fi
    
    # Split the SAPT data into fitting and test points
    while read line; do
	etot=$(echo $line | awk '{print $7}')
	RANDNUM=$RANDOM
	let "RANDNUM %= 100"
	if [ $RANDNUM -lt $testSetCutoff ] && [ $(echo "$etot < 10.0" | bc -l) -eq 1 ]; then
	    echo $line >> $NAME'.ener.test1'
	else
	    echo $line >> $NAME'.ener.fit'
	fi
    done < $NAME.ener

    if [ $FIT_EFFORT = 'HIGH' ]; then
	NFIT_START=128
    else
	let NFIT_START=8*$NCORES
	if [ $NFIT_START -gt 128 ]; then
            NFIT_START=128
	fi
    fi

    if [ $FIT_EFFORT = 'HIGH' ]; then
	NITER=60
    else
	# More iterations for final fit
	if [ $incTestSet = 'F' ]; then
	    NITER=50
	else
	    NITER=40
	fi
    fi
    
    echo '&FIT' > fit.in
    echo ' niter='$NITER >> fit.in
    echo ' opoly='$POLY_ORDER >> fit.in
    echo ' weightScale='$FIT_WEIGHT_SCALE >> fit.in
    echo ' nrand='$NFIT_START >> fit.in
    echo ' nrandFinal='$NCORES >> fit.in
    echo ' wallHeight='$WALL_HEIGHT >> fit.in
    echo ' comModeDmp='$COM_RULE_DAMP >> fit.in
    echo ' incElsDmp='$ELS_DMP >> fit.in
    echo ' incVDWDmp='$VDW_DMP >> fit.in
    echo ' comModeExp='$COM_RULE_EXP >> fit.in
    echo ' comModeLin='$COM_RULE_LIN >> fit.in
    echo ' incCore='$FIT_A12 >> fit.in
    echo ' autoReduce='$FIT_AUTO_REDUCE >> fit.in
    echo ' weightEnerDep=0.75' >> fit.in
    if [ $DELTAHF = 'F' ]; then
	echo ' excludeDHF=T' >> fit.in
    fi
    echo '/' >> fit.in
    
    INFILES='fit.in fit_asymp.dat '$NAME'.ener.fit '$INPUT2' '$NAME'.ener.test*'
    OUTFILES='fit_inter.log fit_inter.dat'
    WORK=$BINDIR'/fit_inter '$NAME' < fit.in > fit_inter.log 2>&1'
    $BINDIR/submit.sh $RUN_MODE $NAME 'INTER' "$INFILES" "$OUTFILES" "$WORK" $NCORES $NCORES 2000
fi

if [ $INTRA = 'T' ]; then
    if [ ! -e INTRA_DONE ]; then
	echo 'Awaiting intramolecular PES fitting - '$(date)
	while [ ! -e INTRA_DONE ]; do
	    sleep 1m
	done
    fi
    echo 'Intramolecular PES fit done'
fi

if [ ! -e INTER_DONE ]; then
    echo 'Awaiting intermolecular PES fitting - '$(date)
    while [ ! -e INTER_DONE ]; do
        sleep 1m
    done
fi
echo 'Intermolecular PES fit done'

fitRMSE1=$(grep 'Fit data final error E<0' fit_inter.log | tr -s ' ' | cut -d ':' -f 2 | cut -d ' ' -f 4)
fitRMSE2=$(grep 'Fit data final error E<10' fit_inter.log | tr -s ' ' | cut -d ':' -f 2 | cut -d ' ' -f 4)

# Check for errors before creating info file
if (isFloat "$fitRMSE1") && (isFloat "$fitRMSE2"); then
    
    echo "$fitRMSE1  $fitRMSE2" > rmse.info
    echo 'RMSE on fit data with E<0 is '$fitRMSE1' kcal/mol'
    echo 'RMSE on fit data with E<10 kcal/mol is '$fitRMSE2' kcal/mol'
    
    if [ -e $NAME.ener.test1 ] || [ -e $NAME.ener.test2 ]; then
        testRMSE1=$(grep 'Test data final error E<0' fit_inter.log | head -1 | tr -s ' ' | cut -d ':' -f 2 | cut -d ' ' -f 4)
        testRMSE2=$(grep 'Test data final error E<10' fit_inter.log | head -1 | tr -s ' ' | cut -d ':' -f 2 | cut -d ' ' -f 4)
        echo 'RMSE on test data with E<0 is '$testRMSE1' kcal/mol'
        echo 'RMSE on test data with E<10 kcal/mol is '$testRMSE2' kcal/mol'
    else
	testRMSE1='0.000000'
	testRMSE2='0.000000'
    fi
    echo "$testRMSE1  $testRMSE2" >> rmse.info
    
    echo
fi


if [ ! -f SCAN_RUNNING ] && [ ! -f SCAN_DONE ]; then
    touch SCAN_RUNNING
    
    echo
    echo '.......................................'
    echo '       SCANNING PES FOR HOLES          '
    echo '.......................................'
    echo
    
    minEnergy=`grep 'Energy of minimum' fit_inter.log | cut -d '=' -f 2`
    
    INFILES=$INPUT2' '$NAME'.ener fit_inter.dat fit_intra.dat'
    OUTFILES='holescan.log holes.out'
    WORK=$BINDIR'/hole_scan '$NAME' fit_inter.dat 2 fit_intra.dat '$WALL_HEIGHT' '$minEnergy' '$WALL_MIN_DIST' > holescan.log 2>&1'
    $BINDIR/submit.sh $RUN_MODE $NAME 'SCAN' "$INFILES" "$OUTFILES" "$WORK" $NCORES $NCORES 2000
fi

if [ ! -e MINIMA_RUNNING ] && [ ! -e MINIMA_DONE ]; then
    touch MINIMA_RUNNING

    echo
    echo '.......................................'
    echo '     BEGINNING MINIMA LOCALIZATION     '
    echo '.......................................'
    echo
    
    INFILES=$INPUT2' fit_inter.dat fit_intra.dat'
    OUTFILES='minima.dat find_minima.log'
    WORK=$BINDIR'/find_minima.sh '$NAME' fit_inter.dat 2 '$NCORES' > find_minima.log 2>&1'
    $BINDIR/submit.sh $RUN_MODE $NAME 'MINIMA' "$INFILES" "$OUTFILES" "$WORK" $NCORES $NCORES 2000
fi

if [ ! -e SCAN_DONE ]; then
    echo 'Awaiting hole scanning - '$(date)
    while [ ! -e SCAN_DONE ]; do
        sleep 1m
    done
fi
echo 'Hole scanning done'

if [ ! -e MINIMA_DONE ]; then
    echo 'Awaiting minima search - '$(date)
    while [ ! -e MINIMA_DONE ]; do
	sleep 1m
    done
fi
echo 'Minima search done'

echo

nholes=$(wc -l holes.out | cut -d ' ' -f 1)

# Check for errors before creating info file
if [ "$nholes" -eq "$nholes" ]; then
    if [ $nholes -gt 0 ]; then
	minheight=$(grep 'Minimum hole energy' holescan.log | cut -d '=' -f 2)
	echo $nholes' holes found. Minimum wall height is '$minheight
    else
	minheight='99999'
	echo 'No holes found'
    fi
    
    echo $nholes > holes.info
    echo $minheight >> holes.info
    
    MIN_WALL_HEIGHT=$(grep 'Minimum wall height' holescan.log | cut -d '=' -f 2)
    MEAN_WALL_HEIGHT=$(grep 'Mean wall height' holescan.log | cut -d '=' -f 2)
    MIN_WALL_DIST=$(grep 'Minimum wall distance' holescan.log | cut -d '=' -f 2)
    MAX_WALL_DIST=$(grep 'Maximum wall distance' holescan.log | cut -d '=' -f 2)
    
    $BINDIR/gen_report $NAME fit_inter.dat 2 fit_intra.dat minima.dat $COM_RULE_EXP $COM_RULE_DAMP $COM_RULE_LIN $COM_RULE_VDW $MIN_WALL_HEIGHT $MEAN_WALL_HEIGHT $MIN_WALL_DIST $MAX_WALL_DIST > fit_report.out
fi

touch SLAVE_FIT_DONE

