#!/bin/bash

WAIT=$1
. $VARSDIR/vars

if [ ! -e SLAVE_ASYM_STARTED ]; then
    touch SLAVE_ASYM_STARTED
fi

if [ $DISTR = 'T' ]; then
    MODE='distr'
else
    MODE='asymp'
fi

USEDALT=F

if [ ! -e ASYM_RUNNING ] && [ ! -e ASYM_DONE ]; then
    touch ASYM_RUNNING
    
    echo
    echo '.......................................'
    echo '    CALCULATING ASYMPTOTIC ENERGIES    '
    echo '.......................................'
    echo
    
    $BINDIR/make_asym_grid $NAME $DISTR
    
    # Create inputs
    if [ $DISTR = 'F' ] || [ $USEDALT = 'T' ]; then
	$BINDIR/make_dalt_inp $NAME $BASISDIR $BASIS $AUXBASIS $HYBRID $MODE $MB_CUTOFF $SAPT_MEM
    else
	$BINDIR/make_orca_inp $NAME $BASISDIR $BASIS $AUXBASIS $HYBRID $MODE $MB_CUTOFF $NCORES_DFT $SAPT_MEM
    fi
    
#
# Asymptotic SAPT
#
    INFILES=$INPUT2' '$NAME'.geo *.dal *.mol *.data dimer.cnf geoparm.d'
    if [ $DISTR = 'F' ]; then
	INFILES=$INFILES' *.dispinp'
    else
	INFILES=$INFILES' '$NAME'A.aux constr.para'
	if [ $SAME = 'F' ]; then
	  INFILES=$INFILES' '$NAME'B.aux'  
	fi
    fi
    OUTFILES='*.out *.log '$NAME'.ener'
    if [ $DISTR = 'F' ]; then
	WORK='time '$ASYMSCRIPT' '$NAME' '$SAME' > asym.log 2>&1;'
	ASYM_OUT='inddisp/log'
	OUTFILES=$OUTFILES' inddisp/coefd.dat inddisp/coefi.dat inddisp/moments.a inddisp/moments.b *.dalcalmult*'
    else
	WORK='time '$ASYMSCRIPT_DISTR' '$NAME' > asym.log 2>&1;'
	ASYM_OUT=$NAME'_ener.out'
    fi
    WORK=$WORK' '$BINDIR'/extract_ener '$NAME' '$ASYM_OUT' asym '$HYBRID
    $BINDIR/submit.sh $RUN_MODE $NAME 'ASYM' "$INFILES" "$OUTFILES" "$WORK" 1 1 0
fi

if [ ! -e ASYM_DONE ]; then
    if [ $WAIT = 'T' ]; then
	echo 'Awaiting asymptotic calculations - '$(date)
	while [ ! -e ASYM_DONE ]; do
	    sleep 1m
	done
    else
	echo 'Asymptotic calculations in progress'
	exit
    fi
fi
echo 'Asymptotic calculations done'


if [ ! -f ASYMFIT_RUNNING ] && [ ! -f ASYMFIT_DONE ]; then
    touch ASYMFIT_RUNNING
    
    echo
    echo '.......................................'
    echo '    FITTING ASYMPTOTIC POTENTIAL       '
    echo '.......................................'
    echo
    
    INFILES=$INPUT2' '$NAME'.ener loewdin*.dat'
    OUTFILES='fit_asymp.log fit_asymp.dat'
    WORK='time '$BINDIR'/fit_asymp '$NAME' '$COM_RULE_VDW' '$MAX_CN' T > fit_asymp.log 2>&1'
    $BINDIR/submit.sh $RUN_MODE $NAME 'ASYMFIT' "$INFILES" "$OUTFILES" "$WORK" 1 1 1000
fi

if [ ! -e ASYMFIT_DONE ]; then
    if [ $WAIT = 'T' ]; then
	echo 'Awaiting asymptotic fit - '$(date)
	while [ ! -e ASYMFIT_DONE ]; do
	    sleep 1m
	done
    else
	echo 'Asymptotic fit in progress'
        exit
    fi
fi
echo 'Asymptotic fit done'


touch SLAVE_ASYM_DONE
