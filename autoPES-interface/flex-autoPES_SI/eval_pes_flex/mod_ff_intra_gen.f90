
! Module for inputting and evaluating generated intramonomer potentials

module force_field_intra_gen
 use double
 use const
 use dimer
 implicit none
 
 integer, parameter :: NBTERM_MAX = 6
 
 real(8) :: bconst(2)
 real(8), allocatable :: bn(:,:), bmn(:,:,:,:) ! Fit parameters
 real(8), allocatable :: intraDist0(:)
 integer :: nbnTerm, nbmnTerm
 
 real(8), private :: rAB(MAX_NSITE,MAX_NSITE)
 real(8), private :: val
 
contains

! Compute intramonomer potential at current geometry
real(8) function energy_ff_intra_gen(imon)
  integer, intent(in) :: imon
  
  integer :: i, j, k, l, m, n
  integer :: sptype1, sptype2
  real(8) :: ener
  real(8) :: dr1, dr2, drPow1, drPow2
  
  do i = 1,natom(imon)
     rAB(i,i) = 0.0d0
     do j = 1+1,natom(imon)
        rAB(i,j) = sqrt( &
             (sit(imon,i,1) - sit(imon,j,1))**2 + &
             (sit(imon,i,2) - sit(imon,j,2))**2 + &
             (sit(imon,i,3) - sit(imon,j,3))**2)
        rAB(j,i) = rAB(i,j)
     enddo
  enddo
  
  ener = bconst(imon)
  do i = 1,natom(imon)
     do j = i+1,natom(imon)
        sptype1 = typeMap(stype(imon,i),stype(imon,j))
        if (sptype1 > 0) then
           dr1 = rAB(i,j) - intraDist0(sptype1)
           
           drPow1 = dr1
           do k = 1,nbnTerm
              ener = ener + bn(sptype1,k)*drPow1
              drPow1 = drPow1*dr1
           enddo
           
           do k = i,natom(imon)
              do l = k+1,natom(imon)
                 sptype2 = typeMap(stype(imon,k),stype(imon,l))
                 if (sptype2 > 0 .and. (k /= i .or. l > j)) then
                    dr2 = rAB(k,l) - intraDist0(sptype2)
                    
                    drPow1 = dr1
                    do m = 1,nbmnTerm
                       
                       drPow2 = dr2
                      do n = 1,nbmnTerm
                          ener = ener + bmn(sptype1,m,sptype2,n)*drPow1*drPow2
                          drPow2 = drPow2*dr2
                       enddo
                       
                       drPow1 = drPow1*dr1
                    enddo
                    
                 endif
              enddo
           enddo
           
        endif
     enddo
  enddo
  
  energy_ff_intra_gen = ener
  return
end function energy_ff_intra_gen


! Read in PES information from given file
! Ignores info already read from dimer file
subroutine read_fit_intra_gen(filename)
  character(*), intent(in) :: filename
  
  integer :: i, j, k, l
  integer :: stype1, stype2, stype3, stype4, mindex, nindex
  integer :: spt1, spt2
  integer :: nline, nbnt, nbmnt
  real(8) :: rBuf(100)
  
  open(100,file=filename,status='old')
  
  read(100,*) nbnt, nbmnt
  call initIntraFF(nbnt, nbmnt)
  
  do i = 1,2
     read(100,*) bconst(i)
     if (homog) bconst(2) = bconst(1)
     
     read(100,*) nline
     do j = 1,nline
        read(100,*) stype1, stype2, rbuf(1)
        spt1 = typeMap(stype1,stype2)
        intraDist0(spt1) = rbuf(1)*A2bohr
     enddo
     
     read(100,*) nline
     do j = 1,nline
        read(100,*) stype1, stype2, rbuf(1:nbnt)
        spt1 = typeMap(stype1,stype2)
        bn(spt1,1:nbnt) = rbuf(1:nbnt)
     enddo
     
     read(100,*) nline
     do j = 1,nline
        read(100,*) stype1, stype2, stype3, stype4, mindex, rbuf(1:nbmnt)
        spt1 = typeMap(stype1,stype2)
        spt2 = typeMap(stype3,stype4)
        bmn(spt1,mindex,spt2,1:nbmnt) = rbuf(1:nbmnt)
     enddo
     
     if (homog) exit
  enddo
  
  close(100)
  
end subroutine read_fit_intra_gen


! Initialize FF data, used whether or not reading FF from file
subroutine initIntraFF(nbnt, nbmnt)
  integer, intent(in) :: nbnt, nbmnt
  
  if (allocated(intraDist0)) deallocate(intraDist0)
  allocate( intraDist0(nsptype) )
  
  bconst(:) = 0.0d0
  
  if (allocated(bn)) deallocate(bn)
  nbnTerm = nbnt
  allocate( bn(nsptype,NBTERM_MAX) )
  bn(:,:) = 0.0d0
  
  if (allocated(bmn)) deallocate(bmn)
  nbmnTerm = nbmnt
  allocate( bmn(nsptype,NBTERM_MAX,nsptype,NBTERM_MAX) )
  bmn(:,:,:,:) = 0.0d0
  
end subroutine initIntraFF


end module force_field_intra_gen

