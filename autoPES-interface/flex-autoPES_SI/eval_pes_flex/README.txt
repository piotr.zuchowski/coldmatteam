
To compile the programs, just run the command
./compile.sh

This script uses gfortran by default, but the programs have also been tested with ifort.

Each of the three PESs has three input files: NAME.input_long, fit_inter.dat.NAME,
and fit_intra.dat.NAME. The NAME.input_long file is needed to
convert to Cartetian coordinates. The other two files contain fit parameters.
The formats of all three files are described in the user manual under
"Internal File Formats".

Two programs are included: toxyz and eval_pes

The program toxyz converts the coordinates described in Sec.II of the main paper to
standard xyz format in Angstroms. Coordinates are given in a space-separated list,
in units of angstrom (for R and linear IDOFS) and degrees (for Euler angles and
rotational IDOFs). The order of coordinates is the same as in the main paper,
and is also described in the user manual under "Internal File Formats".
The program takes a single argument giving the name of the system, and the
coordinates are given via standard input.

An example run for the global minimum of the ethylene glycol dimer PES:

cd fits
echo "4.2977582057      59.7497018702      89.9999829982     269.9999993430      59.7497155217     269.9999925386    -119.4994369152     -21.0396475397    -163.0633146184     119.4994343503     163.0633736553      21.0396443142     -16.2687082795" | ../toxyz EG

OUTPUT:

   24
EG
  6       -0.000000113586146      -0.759824841632043       0.000000194767052
  6        0.000000113586146       0.759824841632043      -0.000000194767052
  8       -0.658844265130356      -1.291986817877810       1.129725411765490
  8        0.658843611008748       1.291987397046288       1.129725130888221
  1        1.029371901798934      -1.127519710441508      -0.017885822784797
  1       -0.491310533573024      -1.127519710441508      -0.904732211469247
  1       -1.029371891442534       1.127519701271805      -0.017886996838618
  1        0.491311057421610       1.127519246618598      -0.904732505033302
  1        0.008392851511882       1.435945776522661       1.818610953326458
  1       -0.008393739043840      -1.435945808486010       1.818611326881167
  0       -0.219614759486584      -0.430662254986692       0.376575144873837
  0        0.219614541446044       0.430662448042855       0.376575051248092
  6        0.759824841632070      -0.000000058561099       4.297758120224632
  6       -0.759824841632070       0.000000058561099       4.297758291175366
  8        1.291987028087469      -0.658843902394751       3.168032822792782
  8       -1.291987282261003       0.658843837191208       3.168033075447120
  1        1.127519615886351      -0.491310667485058       5.202490462288007
  1        1.127519633323735       1.029371979190091       4.315644435926888
  1       -1.127519412333299       0.491310719702847       5.202490687608650
  1       -1.127519629299530      -1.029371978157755       4.315644749015710
  1       -1.435946432448186       0.008393104915457       2.479147388364535
  1        1.435945973860215      -0.008393218359438       2.479147047443985
  0        0.430662325056580      -0.219614638574715       3.921183070445583
  0       -0.430662409781093       0.219614616840201       3.921183154663691

The eval_pes program takes as input Cartesian coordinates of the dimer and evaluates
the PES. It takes two input arguments, giving the names of the intermolecular
and intramolecular fit parameter files. Coordinates are given via standard input
in standard xyz format (including off-atomic sites).

An example run for the global minimum of the ethylene glycol dimer PES:

cd fits
echo "4.2977582057      59.7497018702      89.9999829982     269.9999993430      59.7497155217     269.9999925386    -119.4994369152     -21.0396475397    -163.0633146184     119.4994343503     163.0633736553      21.0396443142     -16.2687082795" | ../toxyz EG | ../eval_pes fit_inter.dat.EG fit_intra.dat.EG

OUTPUT:

    V_exp=   0.1239930151E+02
    V_els=  -0.8754502826E+01
    V_i+d=  -0.1662095981E+02
    V_pol=   0.0000000000E+00
  V_inter=  -0.1297616113E+02
 V_intraA=  -0.1646274312E+01
 V_intraB=  -0.1646272842E+01
    V_tot=  -0.1626870828E+02

