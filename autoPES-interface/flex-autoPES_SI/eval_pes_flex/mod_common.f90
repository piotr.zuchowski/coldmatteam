
module double
 implicit none
 integer, parameter :: dbl = kind(1.0d0)
end module double

!--------------------------------
!    MODULE : CONST
!--------------------------------
module const
  use double
  implicit none
  
  integer, parameter :: quadp = selected_real_kind(33, 4931)
  integer, parameter :: dblep = selected_real_kind(15, 307)
  integer, parameter :: STDOUT = 6
  integer, parameter :: MAX_ANUM = 36, MAX_NSITE = 100, MAX_NIDOF = 20, MAX_L = 6
  
  ! Energy component indices
  integer, parameter :: n_ener_inter = 8, n_ener = n_ener_inter+2
  integer, parameter :: E_tot = 1, E_els = 2, E_exh = 3, E_ind = 4, E_exhind = 5, E_dsp = 6, E_exhdsp = 7
  integer, parameter :: E_dhf = 8, E_mon(2) = [9,10]
  
  ! PES component indices
  integer, parameter :: n_FF_comp = 4
  integer, parameter :: F_els = 1, F_pol = 2, F_vdw = 3, F_exp = 4
  
  ! Combination rule indices (geometric, arithmetic)
  integer, parameter :: CR_geo = 1, CR_arith = 2
  
  ! Parameter class indices (global, site specific, site-pair specific, or site-pair-pair specific)
  integer, parameter :: PC_sp = 0, PC_s = 1, PC_glb = 2, PC_spp = 3, PC_sspintra = 4
  
  ! IDOF type indices (angular, linear, chiral, bond angle)
  integer, parameter :: MC_ANG = 1, MC_LIN = 2, MC_CHI = 3, MC_BND = 4
  
  ! Atom following type indices (planar, bond)
  integer, parameter :: AF_PLANE = 1, AF_BOND = 2
  
  real(kind=dbl), parameter :: PI = 2.0d0*dacos(0.0d0)
  real(kind=dbl), parameter :: twopi = 2.0d0*PI
  real(kind=dbl), parameter :: rad2d = 180.0d0/PI
  real(kind=dbl), parameter :: d2rad = PI/180.0d0
  real(kind=dbl), parameter :: a0 = 0.529177249d0
  real(kind=dbl), parameter :: bohr2A = a0
  real(kind=dbl), parameter :: A2bohr = 1/bohr2A
  real(kind=dbl), parameter :: J2cal = 0.239005736d0
  real(kind=dbl), parameter :: h2kcal = 627.510d0
  real(kind=dbl), parameter :: kcal2h = 1/h2kcal
  character(2), parameter :: asymbol(0:MAX_ANUM) = [ &
       'Mb', &
       'H ',                                                                                'He', &
       'Li','Be',                                                  'B ','C ','N ','O ','F ','Ne', &
       'Na','Mg',                                                  'Al','Si','P ','S ','Cl','Ar', &
       'K ','Ca','Sc','Ti','V ','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr']
end module const


!--------------------------------
!    MODULE : UTILITY
!--------------------------------
module utility
  use const
  implicit none
  
  contains
    
  ! Counts the number of lines in a file, returns 0 if file does not exist
  function nLines(filename)
    implicit none
    character(*) :: filename
    integer :: nLines
    
    integer, parameter :: nLinesUnit = 100
    
    nLines = 0
    open(nLinesUnit, file = filename, status = 'old', ERR=10)
    
    do
       read(nLinesUnit, *, END=10)
       nLines = nLines + 1
    end do
    
10  close(nLinesUnit)
  end function nLines
  
  
  ! Convert string to atomic number. Return 0 if failed
  function getAnum(str)
    integer :: getAnum
    character(*) :: str
    integer :: i
    
    do i = 1,MAX_ANUM
       if (trim(str) == trim(asymbol(i))) then
          getAnum = i
          return
       endif
    enddo
    
    read(str,*,err=10) getAnum
    return
    
10  continue
    getAnum = 0
  end function getAnum
  
    
  
  ! Construct the rotation matrix coresponding to
  ! the ZYZ Euler angles ad,bd,gd (in radians).
  subroutine rotmat(ad,bd,gd,u)
    real(8), intent(in) :: ad, bd, gd
    real(8), intent(out) :: u(3,3)
    real(8) :: sad, sbd, sgd, cad, cbd, cgd
    
    sad = dsin(ad)
    sbd = dsin(bd)
    sgd = dsin(gd)
    cad = dcos(ad)
    cbd = dcos(bd)
    cgd = dcos(gd)
    u(1,1) = cad*cbd*cgd - sad*sgd
    u(1,2) = -cad*cbd*sgd - sad*cgd
    u(1,3) = cad*sbd
    u(2,1) = sad*cbd*cgd + cad*sgd
    u(2,2) = -sad*cbd*sgd + cad*cgd
    u(2,3) = sad*sbd
    u(3,1) = -sbd*cgd
    u(3,2) = sbd*sgd
    u(3,3) = cbd
    return
  end subroutine rotmat
  
  
  ! Construct the rotation matrix coresponding to
  ! the XYZ Euler angles ad,bd,gd (in radians).
  subroutine rotmatXYZ(ad,bd,gd,u)
    real(8), intent(in) :: ad, bd, gd
    real(8), intent(out) :: u(3,3)
    real(8) :: sad, sbd, sgd, cad, cbd, cgd
    
    sad = dsin(ad)
    sbd = dsin(bd)
    sgd = dsin(gd)
    cad = dcos(ad)
    cbd = dcos(bd)
    cgd = dcos(gd)
    u(1,1) = cbd*cgd
    u(1,2) = -cbd*sgd
    u(1,3) = sbd
    u(2,1) = cad*sgd + cgd*sad*sbd
    u(2,2) = cad*cgd - sad*sbd*sgd
    u(2,3) = -cbd*sad
    u(3,1) = sad*sgd - cad*cgd*sbd
    u(3,2) = cgd*sad + cad*sbd*sgd
    u(3,3) = cad*cbd
    return
  end subroutine rotmatXYZ
  
  
  ! Construct matrix for rotation of a point about the line passing through p
  ! in the direction of unit vector q by an angle theta. Result is in homogeneous coordinates.
  subroutine rotmat2(p, q, theta, u)
    real(8), intent(out) :: u(4,4)
    real(8), intent(in) :: p(3), q(3), theta
    real(8) :: ct, st
    
    ct = cos(theta)
    st = sin(theta)
    u(1,1) = q(1)**2 + (q(2)**2 + q(3)**2)*ct
    u(1,2) = q(1)*q(2)*(1.0d0 - ct) - q(3)*st
    u(1,3) = q(1)*q(3)*(1.0d0 - ct) + q(2)*st
    u(1,4) = (p(1)*(q(2)**2 + q(3)**2) - q(1)*(p(2)*q(2) + p(3)*q(3)))*(1.0d0 - ct) + (p(2)*q(3) - p(3)*q(2))*st
    u(2,1) = q(1)*q(2)*(1.0d0 - ct) + q(3)*st
    u(2,2) = q(2)**2 + (q(1)**2 + q(3)**2)*ct
    u(2,3) = q(2)*q(3)*(1.0d0 - ct) - q(1)*st
    u(2,4) = (p(2)*(q(1)**2 + q(3)**2) - q(2)*(p(1)*q(1) + p(3)*q(3)))*(1.0d0 - ct) + (p(3)*q(1) - p(1)*q(3))*st
    u(3,1) = q(1)*q(3)*(1.0d0 - ct) - q(2)*st
    u(3,2) = q(2)*q(3)*(1.0d0 - ct) + q(1)*st
    u(3,3) = q(3)**2 + (q(1)**2 + q(2)**2)*ct
    u(3,4) = (p(3)*(q(1)**2 + q(2)**2) - q(3)*(p(1)*q(1) + p(2)*q(2)))*(1.0d0 - ct) + (p(1)*q(2) - p(2)*q(1))*st
    u(4,1) = 0.0d0
    u(4,2) = 0.0d0
    u(4,3) = 0.0d0
    u(4,4) = 1.0d0
  end subroutine rotmat2
  
  
  ! Performs a direct calculation of the inverse of a 2×2 matrix.
  function matinv2(A) result(B)
    real(8), intent(in) :: A(2,2)
    real(8) :: B(2,2)
    real(8) :: detinv
    
    ! Calculate the inverse determinant of the matrix
    detinv = A(1,1)*A(2,2) - A(1,2)*A(2,1)
    detinv = 1/detinv
    
    ! Calculate the inverse of the matrix
    B(1,1) = +detinv * A(2,2)
    B(2,1) = -detinv * A(2,1)
    B(1,2) = -detinv * A(1,2)
    B(2,2) = +detinv * A(1,1)
  end function matinv2
  
  
  ! Performs a direct calculation of the inverse of a 3×3 matrix.
  function matinv3(A) result(B)
    real(8), intent(in) :: A(3,3)
    real(8) :: B(3,3)
    real(8) :: detinv
    
    ! Calculate the inverse determinant of the matrix
    detinv = A(1,1)*A(2,2)*A(3,3) - A(1,1)*A(2,3)*A(3,2) &
              - A(1,2)*A(2,1)*A(3,3) + A(1,2)*A(2,3)*A(3,1) &
              + A(1,3)*A(2,1)*A(3,2) - A(1,3)*A(2,2)*A(3,1)
    detinv = 1/detinv
    
    ! Calculate the inverse of the matrix
    B(1,1) = +detinv * (A(2,2)*A(3,3) - A(2,3)*A(3,2))
    B(2,1) = -detinv * (A(2,1)*A(3,3) - A(2,3)*A(3,1))
    B(3,1) = +detinv * (A(2,1)*A(3,2) - A(2,2)*A(3,1))
    B(1,2) = -detinv * (A(1,2)*A(3,3) - A(1,3)*A(3,2))
    B(2,2) = +detinv * (A(1,1)*A(3,3) - A(1,3)*A(3,1))
    B(3,2) = -detinv * (A(1,1)*A(3,2) - A(1,2)*A(3,1))
    B(1,3) = +detinv * (A(1,2)*A(2,3) - A(1,3)*A(2,2))
    B(2,3) = -detinv * (A(1,1)*A(2,3) - A(1,3)*A(2,1))
    B(3,3) = +detinv * (A(1,1)*A(2,2) - A(1,2)*A(2,1))
  end function matinv3
  
  
  ! Vector cross product
  function cross(a, b)
    real(8) :: cross(3)
    real(8), intent(in) :: a(3), b(3)
    
    cross(1) = a(2)*b(3) - a(3)*b(2)
    cross(2) = a(3)*b(1) - a(1)*b(3)
    cross(3) = a(1)*b(2) - a(2)*b(1)
  end function cross
  
  
  ! Tang-Toennies damping funciton
  real(8) function TT(n, delta, r)
    integer, intent(in) :: n
    real(8), intent(in) :: delta, r
    
    real(8) :: rScaled, sum, fact, rtoi
    integer :: i
    
    rScaled = r*delta
    rtoi = 1.0
    fact = 1.0
    sum = 1.0
    do i = 1,n
       fact = fact*i
       rtoi = rtoi*rScaled
       sum = sum + rtoi/fact
    end do
    
    TT = 1.0 - exp(-rScaled)*sum
  end function TT
  
  
  ! Parameter combination rules
  real(8) function combine(val1, val2, rule)
    real(8), intent(in) :: val1, val2
    integer, intent(in) :: rule

    combine = 0.0d0
    select case(rule)
    case(CR_geo)
       if (val1 > 0.0d0 .and. val2 > 0.0d0) then
          combine = sqrt(val1*val2)
       else
          combine = 0.0d0
       endif
    case(CR_arith)
       combine = 0.5d0*(val1 + val2)
    end select
  end function combine
  
  
  ! Parses a string to an array of reals. All array elements after end of valid reals are set to zero.
  !    Ends after the first string which is not a valid real.
  subroutine splitLineReal(line, arr)
    implicit none
    character(*) :: line
    real(8) :: arr(:)

    integer :: i
    integer :: start, arrIndex
    logical :: blank, blankChar

    arr(:) = 0.0d0

    blank = .true.
    arrIndex = 1
    do i = 1,len(line)
       blankChar = index('0123456789.+-EeDd', line(i:i)) == 0

       if (blank) then
          if (.not. blankChar) then
             start = i
             blank = .false.
          endif
       else
          if (blankChar .or. i == len(line)) then
             read(line(start:i-1),*,err=10) arr(arrIndex)
             arrIndex = arrIndex + 1
             blank = .true.
          endif
       endif
    enddo
10  continue
  end subroutine splitLineReal
  
  
  function to_upper(strIn) result(strOut)
    ! Adapted from http://www.star.le.ac.uk/~cgp/fortran.html (25 May 2012) Original author: Clive Page
    implicit none
    character(len=*), intent(in) :: strIn
    character(len=len(strIn)) :: strOut
    integer :: i,j

    do i = 1, len(strIn)
       j = iachar(strIn(i:i))
       if (j>= iachar("a") .and. j<=iachar("z") ) then
          strOut(i:i) = achar(iachar(strIn(i:i))-32)
       else
          strOut(i:i) = strIn(i:i)
       end if
    end do
  end function to_upper
  
end module utility

