
! Program to convert coordinates to .xyz format

program toxyz
  use dimer
  use dgrid
  implicit none
  
  character(2000) :: str
  character(200) :: name, dimer_file
  real(8) :: gpoint(MAX_NGIND)
  integer :: nprint(2)
  integer :: i, j
  
  call getarg(1, name)
  
  dimer_file = trim(name)//'.input_long'
  call read_dimer(dimer_file)
  call initGridCoors(0)
    
  read(*,'(A)') str
  call str2gpoint(str, gpoint)
  call moveDimerGP(gpoint)
  
  nprint(1:2) = nsite(1:2)
  
  write(*,'(I5)') nprint(1) + nprint(2)
  write(*,'(A)') trim(name)
  do i = 1,2
     do j = 1,nprint(i)
        write(*,'(I3,A,3F24.15)') sanum(i,j),' ',sit(i,j,1:3)*bohr2A
     enddo
  enddo
  
end program toxyz

