
! Module for handling arrays of grid points. Each grid point contains at least
!     6 coordinates, more if monomers have IDOFs. Grid points may also contain
!     energy values depending on how they are initialized. In the case that
!     energy values are ab inito data, E_tot is the intermolecular energy only

module dgrid
  use dimer
  use utility
  use const
  implicit none
  
  ! Indices into grid array. Must be initialized after reading in dimer file.
  integer, parameter :: C_rcom = 1, &
       C_betaA = 2, C_gammaA = 3, C_alphaB = 4, C_betaB = 5, C_gammaB = 6
  integer, parameter :: n_dimer_coor = 6
  integer, parameter :: MAX_NCOOR = n_dimer_coor + MAX_NIDOF
  integer, parameter :: MAX_NGIND = MAX_NCOOR + n_ener
  
  integer :: ncoor ! Number of coordinates in each grid point
  integer :: ngind ! Number of coordinates + energy values in each grid point
  integer :: C_ener ! Index of start of energy values
  integer :: C_Etot ! Same as C_ener+E_tot
  integer :: C_mon(2) ! Indices of start of coordinates for each monomer
  logical :: inc_dhf
  
contains  
  
  
  ! Reads in a given grid file and initializes variables 
  subroutine initGrid(fname, grid, ngrid, nener)
    character(*), intent(in) :: fname
    integer, intent(out) :: ngrid
    real(8), allocatable, intent(out) :: grid(:,:)
    integer, intent(in) :: nener
    character(2000) :: line
    real(8) :: factor
    integer :: i, j, k
    
    inc_dhf = .false.
    call initGridCoors(nener)
    
    ngrid = nlines(fname)
    if (allocated(grid)) deallocate(grid)
    allocate( grid(ngrid,ngind) )
    
    if (ngrid > 0) then
       open(199,status='old',file=fname)
       do i = 1,ngrid
          read(199,'(A2000)') line
          
          call str2gpoint(line, grid(i,1:ngind))
          
          if (nener >= E_dhf) then
             if (abs(grid(i,C_ener+E_dhf)) > 1.0d-5) then
                inc_dhf = .true.
             endif
          endif
          
          if (nener >= E_mon(1)) grid(i,C_ener+E_mon(1)) = grid(i,C_ener+E_mon(1))*h2kcal
          if (nener >= E_mon(2)) grid(i,C_ener+E_mon(2)) = grid(i,C_ener+E_mon(2))*h2kcal
       enddo
       close(199)
    endif
  end subroutine initGrid
  
  
  ! Convert a string to a grid point
  subroutine str2gpoint(str, gpoint)
    character(*), intent(in) :: str
    real(8), intent(out) :: gpoint(ngind)
    real(8) :: rbuf(40)
    real(8) :: factor
    integer :: i, j
    
    call splitLineReal(str, rbuf)
    gpoint(1:ngind) = rbuf(1:ngind)
    gpoint(C_rcom) = gpoint(C_rcom)*A2bohr
    gpoint(C_betaA:C_gammaB) = gpoint(C_betaA:C_gammaB)*d2rad
    do i = 1,2
       do j = 1,nmcPhys(i)
          factor = 1.0d0
          if (mctype(i,j) == MC_ANG .or. mctype(i,j) == MC_BND) then
             factor = d2rad
          elseif (mctype(i,j) == MC_LIN) then
             factor = A2bohr
          endif
          gpoint(C_mon(i)+j) = gpoint(C_mon(i)+j)*factor
       enddo
    enddo
  end subroutine str2gpoint
  
  
  ! Initialize indices into grid array
  subroutine initGridCoors(nener)
    integer, intent(in) :: nener
    
    ncoor = n_dimer_coor + nmcPhys(1) + nmcPhys(2)
    ngind = ncoor + nener
    C_mon(1) = n_dimer_coor
    C_mon(2) = C_mon(1) + nmcPhys(1)
    C_ener = C_mon(2) + nmcPhys(2)
    C_Etot = C_ener + E_tot
  end subroutine initGridCoors
  
  
  ! Wrapper routine for moveDimer
  subroutine moveDimerGP(gp, shift)
    real(8), intent(in) :: gp(:)
    real(8), intent(in), optional :: shift(3)
    real(8) :: moncoor(2,MAX_NIDOF)
    integer :: i, j
    
    moncoor(:,:) = 0.0d0
    do i = 1,2
       do j = 1,nmcPhys(i)
          moncoor(i,j) = gp(C_mon(i)+j)
       enddo
    enddo
    if (present(shift)) then
       call moveDimer(gp(C_rcom), gp(C_betaA), gp(C_gammaA), &
            gp(C_alphaB), gp(C_betaB), gp(C_gammaB), moncoor, shift)
    else
       call moveDimer(gp(C_rcom), gp(C_betaA), gp(C_gammaA), &
            gp(C_alphaB), gp(C_betaB), gp(C_gammaB), moncoor)
    endif
  end subroutine moveDimerGP
  
  
end module dgrid

