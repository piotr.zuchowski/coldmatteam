
! This module implements the many-body polarizable model 

module polarization
  implicit none
  
  real(8), parameter :: CONV_THRESH = 1.0d-14
  integer, parameter :: MAX_POL_ITER = 15
  
contains
  
  
  ! Calculate polarization energy for given configuration
  ! site: 3-index array of site coordinates. 1st index is monomer, 2nd is site, 3rd is xyz
  ! niter : maximum number of iterations
  ! charge and pz: arrays of partial charges and polarizabilities of site types
  ! delta1 and deltap (optional): arrays of damping parameters of site pair types
  real(8) function polEnergy(nIter, charge, pz, delta1, deltap)
    use dimer
    use utility
    
    integer, intent(in) :: niter
    real(8), intent(in) :: charge(:), pz(:)
    real(8), intent(in), optional :: delta1(:), deltap(:)
    real(8) :: T(2,nsitemax,2,nsitemax,3,3)
    real(8) :: Efield(2,nsitemax,3), fieldij(3)
    real(8) :: mu(2,nsitemax,3), muNext(2,nsitemax,3)
    real(8) :: rVect(3), r
    real(8) :: val, iterChange, diff(3), pz1, pz2, norm, dampFact
    integer :: st1, st2, sptype
    integer :: imon1, imon2
    integer :: i, j, k, l, m, n
    
    ! Compute E field and dipole tensor
    Efield(:,:,:) = 0.0d0
    
    do i = 1,2
       do j = 1,nsite(i)
          do k = 1,2
             
             ! Compute E field and dipole tensor at monomer i from monomer k and k < i
             if (k < i) then
                
                do l = 1,nsite(k)
                   st1 = stype(i,j)
                   st2 = stype(k,l)
                   sptype = typeMap(st1,st2)
                   
                   rVect = sit(i,j,:) - sit(k,l,:)
                   r = norm2(rVect)
                   
                   ! Calculate E field due to point charges
                   if (present(delta1)) then
                      dampFact = TT(1, delta1(sptype), r)
                   else
                      dampFact = 1.0d0
                   endif
                   fieldij = rVect(:)*dampFact/r**3
                   Efield(i,j,:) = Efield(i,j,:) + fieldij*charge(st2)
                   Efield(k,l,:) = Efield(k,l,:) - fieldij*charge(st1)
                   
                   ! Calculate dipole tensor
                   if (pz(st1) > 1.0d-20 .and. pz(st2) > 1.0d-20) then
                      if (present(deltap)) then
                         dampFact = TT(3, deltap(sptype), r)
                      else
                         dampFact = 1.0d0
                      endif
                      do m = 1,3
                         do n = 1,m
                            val = 3.0d0*dampFact*rVect(m)*rVect(n)/r**5
                            if (m == n) then
                               val = val - dampFact/r**3
                            endif
                            T(i,j,k,l,m,n) = val
                            T(i,j,k,l,n,m) = val
                         enddo
                      enddo
                      T(k,l,i,j,:,:) = T(i,j,k,l,:,:)
                   endif
                   
                enddo
                
             endif
          enddo
       enddo
    enddo
    
    ! Solve for induced dipoles iteratively
    mu(:,:,:) = 0.0d0
    do i = 1,niter
       iterChange = 0.0d0
       
       do imon1 = 1,2
          do imon2 = 1,2
             if (imon1 /= imon2) then
                
                do j = 1,nsite(imon1)
                   st1 = stype(imon1,j)
                   if (pz(st1) > 1.0d-20) then
                      muNext(imon1,j,:) = Efield(imon1,j,:)
                      do k = 1,nsite(imon2)
                         st2 = stype(imon2,k)
                         if (pz(st2) > 1.0d-20) then
                            muNext(imon1,j,:) = muNext(imon1,j,:) + &
                                 matmul(T(imon1,j,imon2,k,:,:), mu(imon2,k,:))
                         endif
                      enddo
                      
                      muNext(imon1,j,:) = muNext(imon1,j,:)*pz(st1)
                      diff = muNext(imon1,j,:) - mu(imon1,j,:)
                      iterChange = iterChange + dot_product(diff, diff)
                      mu(imon1,j,:) = muNext(imon1,j,:)
                   endif
                enddo
                
             endif
          enddo
       enddo
       
       if (iterChange < nsitemax*CONV_THRESH) exit
    end do
    
    ! Compute interaction energy
    polEnergy = 0.0
    do i = 1,2
       do j = 1,nsite(i)
          polEnergy = polEnergy - 0.5d0*dot_product(Efield(i,j,:), mu(i,j,:))
       end do
    end do
    polEnergy = polEnergy*h2kcal
    
  end function polEnergy
  
  
end module polarization

