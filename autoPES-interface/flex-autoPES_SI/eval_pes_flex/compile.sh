#!/bin/bash

MODS='mod_common.f90 mod_dimer.f90 mod_dgrid.f90 mod_pol.f90 mod_ff_gen.f90 mod_ff_intra_gen.f90'
gfortran -o toxyz $MODS toxyz.f90
gfortran -o eval_pes $MODS eval_pes.f90
