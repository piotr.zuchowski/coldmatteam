
! This module reads in the input_long file, and performs geometric
!   manipulations of dimer configurations

module dimer
 use utility
 use const
 implicit none
 
 integer, parameter :: maxbond = 8
 character(1), parameter :: monlabelU(2) = ['A','B']
 character(1), parameter :: monlabelL(2) = ['a','b']
 
 integer :: nsite(2), nsitemax, nstype, natom(2), nsptype
 logical :: homog ! set to true iff monomers are identical
 integer :: stype(2,MAX_NSITE) ! site type by site
 integer :: sanum(2,MAX_NSITE) ! anum by site
 integer, allocatable :: typeMap(:,:) ! Map from pairs of site types to site pair types
 integer, allocatable :: typeMapInv(:,:) ! Map from site pair types to pairs of site types
 real(8), allocatable :: sit0(:,:,:) ! Reference geometry
 real(8), allocatable :: sitOA(:,:,:) ! Reference geometry with updated OA sites (for optimized OA site locations)
 integer, allocatable :: bonds(:,:,:) ! unused
 integer, allocatable :: stcount(:,:) ! Number of each site type in each monomer
 character(5), allocatable :: alabel(:,:)
 integer, allocatable :: guideType(:,:) ! site types for initial guiding potential
 integer, allocatable :: stanum(:) ! anum by site type    0 = OA
 logical, allocatable :: incInter(:,:) ! Which site types include which PES components 
 
 real(8), allocatable :: rAB0(:,:,:) ! Monomer site-site distances in base geometry
 
 real(8) :: sit(2,MAX_NSITE,3) ! General buffer to hold working atom coordinates
 
 ! INTERNAL DEGREE OF FREEDOM VARIABLES
 integer :: nmc(2), nmcmax ! total numbers of monomer coordinates (aka IDOFs)
 integer :: nmcPhys(2), nmcOptim(2) ! numbers of physical IDOFs and optimized OA site IDOFs
 integer :: nmcgroupPhys(2), nmcgroupOptim(2) ! Numbers of IDOF groups (used to constain OA site optimization)
 logical :: rigid
 integer, allocatable :: mcvect(:,:,:) ! Pairs or triples of site indices defining axis of IDOFs
 integer, allocatable :: mcset(:,:,:) ! Sets of site indices defining what is included in IDOFs
 integer, allocatable :: nmcset(:,:) ! Sizes of sets in mcset
 integer, allocatable :: mctype(:,:) ! type of IDOFs
 integer, allocatable :: mcgroup(:,:) ! group of IDOF. IDOFs in the same group move together with a fixed ratio
 real(8), allocatable :: mcfact(:,:) ! IDOF unit multiplier, for groups with multiple IDOFs
 real(8), allocatable :: mcmin(:,:), mcmax(:,:) ! Manually specified IDOF limits
 
 ! ATOM FOLLOWING VARIABLES
 integer :: natfol(2) ! Number of atom following groups
 integer :: aftype(2,MAX_NSITE) ! Atom following group types
 integer :: nafdep(2,MAX_NSITE) ! Number of site dependencies for each atom following group (for AF_PLANE this = 3)
 integer :: afsite(2,MAX_NSITE) ! OA site index for each atom following group
 integer :: afdepIndex(2,MAX_NSITE,5) ! Atom dependency indices for each atom following group
 real(8) :: afdepAmt(2,MAX_NSITE,5)
 real(8) :: afOffset(2,MAX_NSITE) 
 
contains
  
  ! Reads in the main input file and initializes dimer data
  ! If this subrouine is called without providing an input file, then
  !    sit, nsite, stype, and sanum must first be initialized
  subroutine read_dimer(input_file)
    use utility
    implicit none
    character(*), intent(in), optional :: input_file
    character(1000) :: cbuf
    real(8) :: rbuf(3), mass, val
    integer :: i, j, k
    
    ! Read input file
    if (present(input_file)) then
       open(unit=101,file=trim(input_file),status='old')
       do i = 1,2
          read(101,*) nsite(i), nmcPhys(i), nmcOptim(i), natfol(i)
          nmc(i) = nmcPhys(i) + nmcOptim(i)
       enddo
    endif
    
    nsitemax = max(nsite(1), nsite(2))
    nmcmax = max(nmc(1), nmc(2))
    allocate(alabel(2,nsitemax))
    allocate(sit0(2,nsitemax,3))
    allocate(sitOA(2,nsitemax,3))
    allocate(guideType(2,nsitemax))
    allocate(bonds(2,nsitemax,0:maxbond))
    allocate(mcvect(2,nmcmax,3))
    allocate(mcset(2,nmcmax,nsitemax))
    allocate(nmcset(2,nmcmax))
    allocate(mctype(2,nmcmax))
    allocate(mcgroup(2,nmcmax))
    allocate(mcfact(2,nmcmax))
    allocate(mcmin(2,nmcmax))
    allocate(mcmax(2,nmcmax))
    
    sit0(1:2,1:nsitemax,1:3) = sit(1:2,1:nsitemax,1:3)
    sitOA(1:2,1:nsitemax,1:3) = sit(1:2,1:nsitemax,1:3)
    nstype = 0
    natom(:) = 0
    mcgroup(:,:) = 0
    mcfact(:,:) = 1.0d0
    do i = 1,2
       
       ! Read sites
       bonds(i,:,:) = 0
       do j = 1, nsite(i)
          if (present(input_file)) then
             read(101,*) alabel(i,j),(sit0(i,j,k),k=1,3), &
                  sanum(i,j),stype(i,j),guideType(i,j)
             
             sit(i,j,1:3) = sit0(i,j,1:3)
             sitOA(i,j,1:3) = sit0(i,j,1:3)
             
             alabel(i,j) = to_upper(alabel(i,j))
             do k = 1,j-1
                if (alabel(i,j) == alabel(i,k)) then
                   write(*,*) 'Error: duplicate site label:',alabel(i,j)
                   stop
                endif
             enddo             
          endif
          
          nstype = max(nstype, stype(i,j))
          
          if (sanum(i,j) > 0) natom(i) = natom(i) + 1
          
          do k = 1, maxbond
             if(bonds(i,j,k).ne.0) bonds(i,j,0) = bonds(i,j,0) + 1
          end do
       enddo
       
       ! Read IDOFs
       if (present(input_file)) then
          do j = 1,nmc(i)
             read(101,*) mctype(i,j), mcvect(i,j,1:3), nmcset(i,j), mcgroup(i,j), &
                  mcfact(i,j), mcmin(i,j), mcmax(i,j)
             if (nmcset(i,j) > 0) read(101,*) mcset(i,j,1:nmcset(i,j))
          enddo
       endif
       
       ! Read atom following stuff
       if (present(input_file)) then
          do j = 1,natfol(i)
             read(101,*) aftype(i,j), nafdep(i,j), afsite(i,j)
             read(101,*) afdepIndex(i,j,1:nafdep(i,j))
             if (sanum(i,afsite(i,j)) /= 0) then
                write(*,*) 'ERROR: atom-following site is not an off-atomic site:', alabel(i,j)
                stop
             endif
             do k = 1,nafdep(i,j)
                if (sanum(i,afdepIndex(i,j,k)) == 0) then
                   write(*,*) 'ERROR: atom-following dependency is not an atom'
                   stop
                endif
             enddo
          enddo
       endif
       
    enddo
    
    rigid = nmcPhys(1) == 0 .and. nmcPhys(2) == 0
    
    allocate(incInter(nstype,n_FF_comp))
    incInter(:,:) = .true.
    
    if (present(input_file)) then
       do i = 1,nstype
          read(101,*) incInter(i,1:n_FF_comp)
       enddo
       
       close(101)
    endif
    
    ! Done reading input file, now initialize some more stuff
    do i = 1,2
       nmcgroupPhys(i) = 0
       do j = 1,nmcPhys(i)
          nmcgroupPhys(i) = max(nmcgroupPhys(i), mcgroup(i,j))
       enddo
       
       nmcgroupOptim(i) = 0
       do j = nmcPhys(i)+1,nmc(i)
          nmcgroupOptim(i) = max(nmcgroupOptim(i), mcgroup(i,j))
          do k = 1,nmcset(i,j)
             if (sanum(i,mcset(i,j,k)) /= 0) then
                write(*,*) 'ERROR: A physical atom was selected for off-atomic site optimization'
                stop
             endif
          enddo
       enddo
    enddo
    
    homog = nsite(1) == nsite(2)
    if (homog) then
       do i = 1,nsite(1)
          homog = homog .and. stype(1,i) == stype(2,i)
       enddo
    endif
    
    allocate(stanum(nstype))
    stanum(:) = -1
    do i = 1,2
       do j = 1,nsite(i)
          if (stanum(stype(i,j)) == -1) then
             stanum(stype(i,j)) = sanum(i,j)
          elseif (stanum(stype(i,j)) /= sanum(i,j)) then
             write(*,*) 'ERROR: Conflicting atom types'
             stop
          endif
       enddo
    enddo
    
    allocate(stcount(2,nstype))
    stcount(:,:) = 0
    do i = 1,2
       do j = 1,nsite(i)
          stcount(i,stype(i,j)) = stcount(i,stype(i,j)) + 1
       enddo
    enddo
    
    nsptype = nstype*(nstype + 1)/2
    allocate( typeMap(nstype,nstype) )
    allocate( typeMapInv(nsptype,2) )
    
    typeMap(:,:) = 0
    typeMapInv(:,:) = 0
    nsptype = 0
    do i = 1,nstype
       do j = i,nstype
          nsptype = nsptype + 1
          typeMap(i,j) = nsptype
          typeMap(j,i) = nsptype
          typeMapInv(nsptype,1) = i
          typeMapInv(nsptype,2) = j
       enddo
    enddo
    
    call computeRAB0()
    
    call init_atomfollow()
    
  end subroutine read_dimer
  
  
  ! Computes distance matrix for sitOA configuration
  subroutine computeRAB0()
    integer :: i, j, k
    
    if (.not. allocated(rAB0)) allocate( rAB0(2,nsitemax,nsitemax) )
    do i = 1,2
       do j = 1,nsite(i)
          do k = 1,nsite(i)
             rAB0(i,j,k) = sqrt( &
                  (sitOA(i,j,1) - sitOA(i,k,1))**2 + &
                  (sitOA(i,j,2) - sitOA(i,k,2))**2 + &
                  (sitOA(i,j,3) - sitOA(i,k,3))**2)
          enddo
       enddo
    enddo
  end subroutine computeRAB0
  
  
  ! This is called by read_dimer after the necessary variables are read in
  subroutine init_atomfollow
    ! Variables for AF_PLANE
    real(8) :: normal(3), projection(3)
    real(8) :: planeCoors(2,3)
    real(8) :: sitPlane(3,2), projPlane(2), mat(2,2)
    ! Variables for AF_BOND
    real(8) :: dist1, dist2
    
    integer :: i, j, k, l
    
    do i = 1,2
       do j = 1,natfol(i)
          afdepAmt(i,j,:) = 0.0d0
          afOffset(i,j) = 0.0d0
          
          if (aftype(i,j) == AF_PLANE) then
             normal = planeNormal(i, afdepIndex(i,j,1), afdepIndex(i,j,2), afdepIndex(i,j,3))
             
             ! Distance from OA site to the plane
             afOffset(i,j) = dot_product(sit(i,afsite(i,j),:) - sit(i,afdepIndex(i,j,1),:), normal)
             
             ! Projection of OA site onto the plane
             projection = sit(i,afsite(i,j),:) - afOffset(i,j)*normal
             
             ! Coordinate system in the plane
             do k = 1,2
                planeCoors(k,:) = sit(i,afdepIndex(i,j,k+1),:) - sit(i,afdepIndex(i,j,1),:)
                planeCoors(k,:) = planeCoors(k,:)/norm2(planeCoors(k,:))
             enddo
             
             ! Express all 4 vectors in the planar coordinate system
             do k = 1,2
                do l = 1,3
                   sitPlane(l,k) = dot_product(sit(i,afdepIndex(i,j,l),:), planeCoors(k,:))
                enddo
                projPlane(k) = dot_product(projection(:), planeCoors(k,:))
             enddo
             
             ! Find the location of the projection as a linear sum of the three atom coordinates
             do k = 1,2
                do l = 1,2
                   mat(k,l) = sitPlane(l,k) - sitPlane(3,k)
                enddo
             enddo
             afdepAmt(i,j,1:2) = matmul(matinv2(mat), [projPlane(1)-sitPlane(3,1), projPlane(2)-sitPlane(3,2)])
             afdepAmt(i,j,3) = 1 - afdepAmt(i,j,1) - afdepAmt(i,j,2)
             
          elseif (aftype(i,j) == AF_BOND) then
             
             ! Set distance along bond using only the ratio of distances in the reference geometry
             dist1 = cdist(i,afsite(i,j),i,afdepIndex(i,j,1))
             dist2 = cdist(i,afsite(i,j),i,afdepIndex(i,j,2))
             afDepAmt(i,j,1) = dist1/(dist1 + dist2)
             afDepAmt(i,j,2) = dist2/(dist1 + dist2)
             
          endif
          
       enddo
    enddo

  end subroutine init_atomfollow
    
  
  ! Wrapper routine for moveDimer that updates sitOA with given coordinates
  subroutine setOACoor(OACoor)
    real(8), intent(in) :: OACoor(:,:)
    real(8) :: mc(2,MAX_NIDOF)
    integer :: i, j, k
    
    mc(:,:) = 0.0d0
    do i = 1,2
       do j = 1,nmcgroupOptim(i)
          do k = nmcPhys(i)+1,nmc(i)
             if (mcgroup(i,k) == j) then
                mc(i,k) = mcfact(i,k)*OACoor(i,j)
             endif
          enddo
       enddo
    enddo
    
    sitOA(1:2,1:nsitemax,1:3) = sit0(1:2,1:nsitemax,1:3)
    call moveDimer(0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, 0.0d0, mc)
    sitOA(1:2,1:nsitemax,1:3) = sit(1:2,1:nsitemax,1:3)
    call computeRAB0()
  end subroutine setOACoor
  
  
  ! Update sit by transforming sitOA according to the given coordinates
  ! If monomer coordinates are not provided, they are assumed to be zero
  ! If shift direction is not provided, it is assumed to be in +z direction
  subroutine moveDimer(RCOM, betaA, gammaA, alphaB, betaB, gammaB, mc, shiftIn)
    real(8), intent(in) :: RCOM, betaA, gammaA, alphaB, betaB, gammaB
    real(8), intent(in), optional :: mc(:,:)
    real(8), intent(in), optional :: shiftIn(3)
    real(8) :: p(3), q(3), v(4), u3(3,3), u4(4,4)
    real(8) :: shift(3), normal(3)
    integer :: i, j, k
    
    if (present(shiftIn)) then
       shift = shiftIn
    else
       shift = [0.0d0, 0.0d0, 1.0d0]
    endif
    
    do i = 1,2
       do j = 1,nsite(i)
          sit(i,j,:) = sitOA(i,j,:)
       enddo
    enddo
    
    if (present(mc)) then
       
       ! Move atoms according to IDOFs
       do i = 1,2
          do j = 1,nmc(i)
             
             if (abs(mc(i,j)) > 1.0d-20) then
                
                if (mctype(i,j) == MC_CHI) then
                   ! Chiral IDOF
                   if (mc(i,j) > 0.0d0) then
                      do k = 1,nsite(i)
                         sit(i,k,:) = -sit(i,k,:)
                      enddo
                   endif
                else
                   if (mctype(i,j) == MC_BND) then
                      ! Bond angle IDOF type uses plane of 3 sites
                      p(:) = sit(i,mcvect(i,j,2),:)
                      q(:) = planeNormal(i, mcvect(i,j,1), mcvect(i,j,2), mcvect(i,j,3))
                   else
                      ! LIN, ANG IDOF types specify axis directly
                      p(:) = sit(i,mcvect(i,j,1),:)
                      q(:) = sit(i,mcvect(i,j,2),1:3) - sit(i,mcvect(i,j,1),1:3)
                      q(:) = q(:)/sqrt(q(1)**2 + q(2)**2 + q(3)**2)
                   endif
                   
                   if (mctype(i,j) == MC_ANG .or. mctype(i,j) == MC_BND) then
                      ! Rotational IDOF
                      call rotmat2(p, q, mc(i,j), u4)
                      do k = 1,nmcset(i,j)
                         v(1:3) = sit(i,mcset(i,j,k),:)
                         v(4) = 1.0d0
                         v(:) = matmul(u4, v)
                         sit(i,mcset(i,j,k),:) = v(1:3)
                      enddo
                   elseif (mctype(i,j) == MC_LIN) then
                      ! Linear IDOF
                      do k = 1,nmcset(i,j)
                         sit(i,mcset(i,j,k),:) = sit(i,mcset(i,j,k),:) + mc(i,j)*q(:)
                      enddo
                   endif
                endif
                
             endif
             
          enddo
       enddo ! end IDOFs
       
       ! Move OA sites according to atom following rules
       do i = 1,2
          do j = 1,natfol(i)
             
             if (aftype(i,j) == AF_PLANE) then
                p(:) = 0.0d0
                do k = 1,nafdep(i,j)
                   p(:) = p(:) + afdepAmt(i,j,k)*sit(i,afdepIndex(i,j,k),:)
                enddo
                normal = planeNormal(i, afdepIndex(i,j,1), afdepIndex(i,j,2), afdepIndex(i,j,3))
                sit(i,afsite(i,j),:) = p(:) + normal(:)*afOffset(i,j)
             endif
             
          enddo
       enddo ! end atom following
       
    endif
    
    call rotmat(0.0d0, betaA, gammaA, u3)
    do i = 1,nsite(1)
       sit(1,i,:) = matmul(u3, sit(1,i,:))
    enddo
    
    call rotmat(alphaB, betaB, gammaB, u3)
    do i = 1,nsite(2)
       sit(2,i,:) = matmul(u3, sit(2,i,:))
       sit(2,i,:) = sit(2,i,:) + RCOM*shift(:)
    enddo
    
  end subroutine moveDimer
  
 ! Returns cartesian distance between sites
 function cDist(mon1, at1, mon2, at2)
   real(8) :: cDist
   integer :: mon1, at1, mon2, at2
   
   cDist = sqrt( (sit(mon1,at1,1) - sit(mon2,at2,1))**2 + &
        (sit(mon1,at1,2) - sit(mon2,at2,2))**2 + &
        (sit(mon1,at1,3) - (sit(mon2,at2,3)))**2 )
 end function cDist
  
 ! Gives the normal vector to the plane defined by the three sites
 function planeNormal(imon, isit1, isit2, isit3)
   real(8) :: planeNormal(3)
   integer, intent(in) :: imon, isit1, isit2, isit3
   real(8) :: vect12(3), vect23(3)
   
   vect12 = sit(imon,isit2,1:3) - sit(imon,isit1,1:3)
   vect23 = sit(imon,isit3,1:3) - sit(imon,isit2,1:3)
   planeNormal(1) = vect12(2)*vect23(3) - vect12(3)*vect23(2)
   planeNormal(2) = vect12(3)*vect23(1) - vect12(1)*vect23(3)
   planeNormal(3) = vect12(1)*vect23(2) - vect12(2)*vect23(1)
   planeNormal(1:3) = planeNormal(1:3)/norm2(planeNormal(1:3))
 end function planeNormal
 
end module dimer
