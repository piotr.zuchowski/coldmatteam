
! Evaluate the PES at Cartesian coordinates read in from standard input

program eval_pes
  use const
  use dimer
  use force_field_gen
  use force_field_intra_gen
  implicit none
  
  character(200) :: str
  character(200) :: inter_file, intra_file
  real(8) :: einter(n_FF_comp), eintra(2), coors(3)
  integer :: geo_type
  integer :: nstot
  integer :: i,j
  
  call getarg(1, inter_file)
  call getarg(2, intra_file)
  
  open(101,file=trim(inter_file))
  read(101,'(A)') str
  read(101,*) nsite(1), nsite(2)
  do i = 1,2
     read(101,*) stype(i,1:nsite(i))
  enddo
  close(101)
  
  read(*,*) nstot
  if (nstot /= nsite(1) + nsite(2)) then
     write(*,*) 'Error: number of sites does not match'
     stop
  endif
  read(*,'(A)') str
  do i = 1,2
     do j = 1,nsite(i)
        read(*,*) str,coors(1:3)
        sit(i,j,1:3) = coors(1:3)*A2bohr
        sanum(i,j) = getanum(str)
     enddo
  enddo
  
  call read_dimer()
  
  call read_fit_gen(inter_file)
  if (intra_file /= '') then
     call read_fit_intra_gen(intra_file)
  endif
  
  einter(1:n_FF_comp) = energy_ff_gen()
  write(*,'(A,E18.10)') '    V_exp= ',einter(F_exp)
  write(*,'(A,E18.10)') '    V_els= ',einter(F_els)
  write(*,'(A,E18.10)') '    V_i+d= ',einter(F_vdw)
  write(*,'(A,E18.10)') '    V_pol= ',einter(F_pol)
  write(*,'(A,E18.10)') '  V_inter= ',sum(einter(1:n_FF_comp))
  eintra(:) = 0.0d0
  if (intra_file /= '') then
     eintra(1) = energy_ff_intra_gen(1)
     eintra(2) = energy_ff_intra_gen(2)
     write(*,'(A,E18.10)') ' V_intraA= ',eintra(1)
     write(*,'(A,E18.10)') ' V_intraB= ',eintra(2)
  endif
  write(*,'(A,E18.10)') '    V_tot= ',sum(einter(1:n_FF_comp)) + eintra(1) + eintra(2)
  
end program eval_pes

